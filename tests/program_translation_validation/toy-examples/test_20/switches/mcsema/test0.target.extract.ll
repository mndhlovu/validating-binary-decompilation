; ModuleID = 'mcsema/test0.target.opt.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"

%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.anon = type { i64 }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque

; Function Attrs: nounwind readnone
declare i32 @llvm.ctpop.i32(i32) #0

; Function Attrs: alwaysinline
define %struct.Memory* @switches(%struct.State* noalias, i64, %struct.Memory* noalias) local_unnamed_addr #1 {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP.i, align 8
  %5 = add i64 %1, 1
  store i64 %5, i64* %3, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %4, i64* %9, align 8
  store i64 %8, i64* %6, align 8
  %10 = load i64, i64* %3, align 8
  store i64 %8, i64* %RBP.i, align 8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0
  %EDI.i375 = bitcast %union.anon* %11 to i32*
  %12 = add i64 %7, -12
  %13 = load i32, i32* %EDI.i375, align 4
  %14 = add i64 %10, 6
  store i64 %14, i64* %3, align 8
  %15 = inttoptr i64 %12 to i32*
  store i32 %13, i32* %15, align 4
  %16 = load i64, i64* %RBP.i, align 8
  %17 = add i64 %16, -8
  %18 = load i64, i64* %3, align 8
  %19 = add i64 %18, 7
  store i64 %19, i64* %3, align 8
  %20 = inttoptr i64 %17 to i32*
  store i32 0, i32* %20, align 4
  %RDI.i = getelementptr inbounds %union.anon, %union.anon* %11, i64 0, i32 0
  %21 = load i64, i64* %RBP.i, align 8
  %22 = add i64 %21, -4
  %23 = load i64, i64* %3, align 8
  %24 = add i64 %23, 3
  store i64 %24, i64* %3, align 8
  %25 = inttoptr i64 %22 to i32*
  %26 = load i32, i32* %25, align 4
  %27 = zext i32 %26 to i64
  store i64 %27, i64* %RDI.i, align 8
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 0, i8* %28, align 1
  %29 = and i32 %26, 255
  %30 = call i32 @llvm.ctpop.i32(i32 %29)
  %31 = trunc i32 %30 to i8
  %32 = and i8 %31, 1
  %33 = xor i8 %32, 1
  %34 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %33, i8* %34, align 1
  %35 = icmp eq i32 %26, 0
  %36 = zext i1 %35 to i8
  %37 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %36, i8* %37, align 1
  %38 = lshr i32 %26, 31
  %39 = trunc i32 %38 to i8
  %40 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %39, i8* %40, align 1
  %41 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 0, i8* %41, align 1
  %42 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %42, align 1
  %43 = add i64 %21, -12
  %44 = add i64 %23, 8
  store i64 %44, i64* %3, align 8
  %45 = inttoptr i64 %43 to i32*
  store i32 %26, i32* %45, align 4
  %46 = load i64, i64* %3, align 8
  %47 = load i8, i8* %37, align 1
  %48 = icmp ne i8 %47, 0
  %.v = select i1 %48, i64 393, i64 6
  %49 = add i64 %46, %.v
  store i64 %49, i64* %3, align 8
  %cmpBr_400506 = icmp eq i8 %47, 1
  %RAX.i182 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %50 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400506, label %block_.L_40068f, label %block_40050c

block_40050c:                                     ; preds = %entry
  %51 = add i64 %50, -12
  %52 = add i64 %49, 8
  store i64 %52, i64* %3, align 8
  %53 = inttoptr i64 %51 to i32*
  %54 = load i32, i32* %53, align 4
  %55 = add i32 %54, -1
  %56 = zext i32 %55 to i64
  store i64 %56, i64* %RAX.i182, align 8
  %57 = icmp eq i32 %54, 0
  %58 = zext i1 %57 to i8
  store i8 %58, i8* %28, align 1
  %59 = and i32 %55, 255
  %60 = call i32 @llvm.ctpop.i32(i32 %59)
  %61 = trunc i32 %60 to i8
  %62 = and i8 %61, 1
  %63 = xor i8 %62, 1
  store i8 %63, i8* %34, align 1
  %64 = xor i32 %54, %55
  %65 = lshr i32 %64, 4
  %66 = trunc i32 %65 to i8
  %67 = and i8 %66, 1
  store i8 %67, i8* %42, align 1
  %68 = icmp eq i32 %55, 0
  %69 = zext i1 %68 to i8
  store i8 %69, i8* %37, align 1
  %70 = lshr i32 %55, 31
  %71 = trunc i32 %70 to i8
  store i8 %71, i8* %40, align 1
  %72 = lshr i32 %54, 31
  %73 = xor i32 %70, %72
  %74 = add nuw nsw i32 %73, %72
  %75 = icmp eq i32 %74, 2
  %76 = zext i1 %75 to i8
  store i8 %76, i8* %41, align 1
  %77 = add i64 %50, -16
  %78 = add i64 %49, 14
  store i64 %78, i64* %3, align 8
  %79 = inttoptr i64 %77 to i32*
  store i32 %55, i32* %79, align 4
  %80 = load i64, i64* %3, align 8
  %81 = load i8, i8* %37, align 1
  %82 = icmp ne i8 %81, 0
  %.v3 = select i1 %82, i64 387, i64 6
  %83 = add i64 %80, %.v3
  store i64 %83, i64* %3, align 8
  %cmpBr_40051a = icmp eq i8 %81, 1
  %84 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_40051a, label %block_.L_40069d, label %block_400520

block_400520:                                     ; preds = %block_40050c
  %85 = add i64 %84, -12
  %86 = add i64 %83, 8
  store i64 %86, i64* %3, align 8
  %87 = inttoptr i64 %85 to i32*
  %88 = load i32, i32* %87, align 4
  %89 = add i32 %88, -2
  %90 = zext i32 %89 to i64
  store i64 %90, i64* %RAX.i182, align 8
  %91 = icmp ult i32 %88, 2
  %92 = zext i1 %91 to i8
  store i8 %92, i8* %28, align 1
  %93 = and i32 %89, 255
  %94 = call i32 @llvm.ctpop.i32(i32 %93)
  %95 = trunc i32 %94 to i8
  %96 = and i8 %95, 1
  %97 = xor i8 %96, 1
  store i8 %97, i8* %34, align 1
  %98 = xor i32 %88, %89
  %99 = lshr i32 %98, 4
  %100 = trunc i32 %99 to i8
  %101 = and i8 %100, 1
  store i8 %101, i8* %42, align 1
  %102 = icmp eq i32 %89, 0
  %103 = zext i1 %102 to i8
  store i8 %103, i8* %37, align 1
  %104 = lshr i32 %89, 31
  %105 = trunc i32 %104 to i8
  store i8 %105, i8* %40, align 1
  %106 = lshr i32 %88, 31
  %107 = xor i32 %104, %106
  %108 = add nuw nsw i32 %107, %106
  %109 = icmp eq i32 %108, 2
  %110 = zext i1 %109 to i8
  store i8 %110, i8* %41, align 1
  %111 = add i64 %84, -20
  %112 = add i64 %83, 14
  store i64 %112, i64* %3, align 8
  %113 = inttoptr i64 %111 to i32*
  store i32 %89, i32* %113, align 4
  %114 = load i64, i64* %3, align 8
  %115 = load i8, i8* %37, align 1
  %116 = icmp ne i8 %115, 0
  %.v4 = select i1 %116, i64 381, i64 6
  %117 = add i64 %114, %.v4
  store i64 %117, i64* %3, align 8
  %cmpBr_40052e = icmp eq i8 %115, 1
  %118 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_40052e, label %block_.L_4006ab, label %block_400534

block_400534:                                     ; preds = %block_400520
  %119 = add i64 %118, -12
  %120 = add i64 %117, 8
  store i64 %120, i64* %3, align 8
  %121 = inttoptr i64 %119 to i32*
  %122 = load i32, i32* %121, align 4
  %123 = add i32 %122, -4
  %124 = zext i32 %123 to i64
  store i64 %124, i64* %RAX.i182, align 8
  %125 = icmp ult i32 %122, 4
  %126 = zext i1 %125 to i8
  store i8 %126, i8* %28, align 1
  %127 = and i32 %123, 255
  %128 = call i32 @llvm.ctpop.i32(i32 %127)
  %129 = trunc i32 %128 to i8
  %130 = and i8 %129, 1
  %131 = xor i8 %130, 1
  store i8 %131, i8* %34, align 1
  %132 = xor i32 %122, %123
  %133 = lshr i32 %132, 4
  %134 = trunc i32 %133 to i8
  %135 = and i8 %134, 1
  store i8 %135, i8* %42, align 1
  %136 = icmp eq i32 %123, 0
  %137 = zext i1 %136 to i8
  store i8 %137, i8* %37, align 1
  %138 = lshr i32 %123, 31
  %139 = trunc i32 %138 to i8
  store i8 %139, i8* %40, align 1
  %140 = lshr i32 %122, 31
  %141 = xor i32 %138, %140
  %142 = add nuw nsw i32 %141, %140
  %143 = icmp eq i32 %142, 2
  %144 = zext i1 %143 to i8
  store i8 %144, i8* %41, align 1
  %145 = add i64 %118, -24
  %146 = add i64 %117, 14
  store i64 %146, i64* %3, align 8
  %147 = inttoptr i64 %145 to i32*
  store i32 %123, i32* %147, align 4
  %148 = load i64, i64* %3, align 8
  %149 = load i8, i8* %37, align 1
  %150 = icmp ne i8 %149, 0
  %.v5 = select i1 %150, i64 375, i64 6
  %151 = add i64 %148, %.v5
  store i64 %151, i64* %3, align 8
  %cmpBr_400542 = icmp eq i8 %149, 1
  %152 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400542, label %block_.L_4006b9, label %block_400548

block_400548:                                     ; preds = %block_400534
  %153 = add i64 %152, -12
  %154 = add i64 %151, 8
  store i64 %154, i64* %3, align 8
  %155 = inttoptr i64 %153 to i32*
  %156 = load i32, i32* %155, align 4
  %157 = add i32 %156, -6
  %158 = zext i32 %157 to i64
  store i64 %158, i64* %RAX.i182, align 8
  %159 = icmp ult i32 %156, 6
  %160 = zext i1 %159 to i8
  store i8 %160, i8* %28, align 1
  %161 = and i32 %157, 255
  %162 = call i32 @llvm.ctpop.i32(i32 %161)
  %163 = trunc i32 %162 to i8
  %164 = and i8 %163, 1
  %165 = xor i8 %164, 1
  store i8 %165, i8* %34, align 1
  %166 = xor i32 %156, %157
  %167 = lshr i32 %166, 4
  %168 = trunc i32 %167 to i8
  %169 = and i8 %168, 1
  store i8 %169, i8* %42, align 1
  %170 = icmp eq i32 %157, 0
  %171 = zext i1 %170 to i8
  store i8 %171, i8* %37, align 1
  %172 = lshr i32 %157, 31
  %173 = trunc i32 %172 to i8
  store i8 %173, i8* %40, align 1
  %174 = lshr i32 %156, 31
  %175 = xor i32 %172, %174
  %176 = add nuw nsw i32 %175, %174
  %177 = icmp eq i32 %176, 2
  %178 = zext i1 %177 to i8
  store i8 %178, i8* %41, align 1
  %179 = add i64 %152, -28
  %180 = add i64 %151, 14
  store i64 %180, i64* %3, align 8
  %181 = inttoptr i64 %179 to i32*
  store i32 %157, i32* %181, align 4
  %182 = load i64, i64* %3, align 8
  %183 = load i8, i8* %37, align 1
  %184 = icmp ne i8 %183, 0
  %.v6 = select i1 %184, i64 369, i64 6
  %185 = add i64 %182, %.v6
  store i64 %185, i64* %3, align 8
  %cmpBr_400556 = icmp eq i8 %183, 1
  %186 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400556, label %block_.L_4006c7, label %block_40055c

block_40055c:                                     ; preds = %block_400548
  %187 = add i64 %186, -12
  %188 = add i64 %185, 8
  store i64 %188, i64* %3, align 8
  %189 = inttoptr i64 %187 to i32*
  %190 = load i32, i32* %189, align 4
  %191 = add i32 %190, -12
  %192 = zext i32 %191 to i64
  store i64 %192, i64* %RAX.i182, align 8
  %193 = icmp ult i32 %190, 12
  %194 = zext i1 %193 to i8
  store i8 %194, i8* %28, align 1
  %195 = and i32 %191, 255
  %196 = call i32 @llvm.ctpop.i32(i32 %195)
  %197 = trunc i32 %196 to i8
  %198 = and i8 %197, 1
  %199 = xor i8 %198, 1
  store i8 %199, i8* %34, align 1
  %200 = xor i32 %190, %191
  %201 = lshr i32 %200, 4
  %202 = trunc i32 %201 to i8
  %203 = and i8 %202, 1
  store i8 %203, i8* %42, align 1
  %204 = icmp eq i32 %191, 0
  %205 = zext i1 %204 to i8
  store i8 %205, i8* %37, align 1
  %206 = lshr i32 %191, 31
  %207 = trunc i32 %206 to i8
  store i8 %207, i8* %40, align 1
  %208 = lshr i32 %190, 31
  %209 = xor i32 %206, %208
  %210 = add nuw nsw i32 %209, %208
  %211 = icmp eq i32 %210, 2
  %212 = zext i1 %211 to i8
  store i8 %212, i8* %41, align 1
  %213 = add i64 %186, -32
  %214 = add i64 %185, 14
  store i64 %214, i64* %3, align 8
  %215 = inttoptr i64 %213 to i32*
  store i32 %191, i32* %215, align 4
  %216 = load i64, i64* %3, align 8
  %217 = load i8, i8* %37, align 1
  %218 = icmp ne i8 %217, 0
  %.v7 = select i1 %218, i64 363, i64 6
  %219 = add i64 %216, %.v7
  store i64 %219, i64* %3, align 8
  %cmpBr_40056a = icmp eq i8 %217, 1
  %220 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_40056a, label %block_.L_4006d5, label %block_400570

block_400570:                                     ; preds = %block_40055c
  %221 = add i64 %220, -12
  %222 = add i64 %219, 8
  store i64 %222, i64* %3, align 8
  %223 = inttoptr i64 %221 to i32*
  %224 = load i32, i32* %223, align 4
  %225 = add i32 %224, -13
  %226 = zext i32 %225 to i64
  store i64 %226, i64* %RAX.i182, align 8
  %227 = icmp ult i32 %224, 13
  %228 = zext i1 %227 to i8
  store i8 %228, i8* %28, align 1
  %229 = and i32 %225, 255
  %230 = call i32 @llvm.ctpop.i32(i32 %229)
  %231 = trunc i32 %230 to i8
  %232 = and i8 %231, 1
  %233 = xor i8 %232, 1
  store i8 %233, i8* %34, align 1
  %234 = xor i32 %224, %225
  %235 = lshr i32 %234, 4
  %236 = trunc i32 %235 to i8
  %237 = and i8 %236, 1
  store i8 %237, i8* %42, align 1
  %238 = icmp eq i32 %225, 0
  %239 = zext i1 %238 to i8
  store i8 %239, i8* %37, align 1
  %240 = lshr i32 %225, 31
  %241 = trunc i32 %240 to i8
  store i8 %241, i8* %40, align 1
  %242 = lshr i32 %224, 31
  %243 = xor i32 %240, %242
  %244 = add nuw nsw i32 %243, %242
  %245 = icmp eq i32 %244, 2
  %246 = zext i1 %245 to i8
  store i8 %246, i8* %41, align 1
  %247 = add i64 %220, -36
  %248 = add i64 %219, 14
  store i64 %248, i64* %3, align 8
  %249 = inttoptr i64 %247 to i32*
  store i32 %225, i32* %249, align 4
  %250 = load i64, i64* %3, align 8
  %251 = load i8, i8* %37, align 1
  %252 = icmp ne i8 %251, 0
  %.v8 = select i1 %252, i64 357, i64 6
  %253 = add i64 %250, %.v8
  store i64 %253, i64* %3, align 8
  %cmpBr_40057e = icmp eq i8 %251, 1
  %254 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_40057e, label %block_.L_4006e3, label %block_400584

block_400584:                                     ; preds = %block_400570
  %255 = add i64 %254, -12
  %256 = add i64 %253, 8
  store i64 %256, i64* %3, align 8
  %257 = inttoptr i64 %255 to i32*
  %258 = load i32, i32* %257, align 4
  %259 = add i32 %258, -19
  %260 = zext i32 %259 to i64
  store i64 %260, i64* %RAX.i182, align 8
  %261 = icmp ult i32 %258, 19
  %262 = zext i1 %261 to i8
  store i8 %262, i8* %28, align 1
  %263 = and i32 %259, 255
  %264 = call i32 @llvm.ctpop.i32(i32 %263)
  %265 = trunc i32 %264 to i8
  %266 = and i8 %265, 1
  %267 = xor i8 %266, 1
  store i8 %267, i8* %34, align 1
  %268 = xor i32 %258, 16
  %269 = xor i32 %268, %259
  %270 = lshr i32 %269, 4
  %271 = trunc i32 %270 to i8
  %272 = and i8 %271, 1
  store i8 %272, i8* %42, align 1
  %273 = icmp eq i32 %259, 0
  %274 = zext i1 %273 to i8
  store i8 %274, i8* %37, align 1
  %275 = lshr i32 %259, 31
  %276 = trunc i32 %275 to i8
  store i8 %276, i8* %40, align 1
  %277 = lshr i32 %258, 31
  %278 = xor i32 %275, %277
  %279 = add nuw nsw i32 %278, %277
  %280 = icmp eq i32 %279, 2
  %281 = zext i1 %280 to i8
  store i8 %281, i8* %41, align 1
  %282 = add i64 %254, -40
  %283 = add i64 %253, 14
  store i64 %283, i64* %3, align 8
  %284 = inttoptr i64 %282 to i32*
  store i32 %259, i32* %284, align 4
  %285 = load i64, i64* %3, align 8
  %286 = load i8, i8* %37, align 1
  %287 = icmp ne i8 %286, 0
  %.v9 = select i1 %287, i64 351, i64 6
  %288 = add i64 %285, %.v9
  store i64 %288, i64* %3, align 8
  %cmpBr_400592 = icmp eq i8 %286, 1
  %289 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400592, label %block_.L_4006f1, label %block_400598

block_400598:                                     ; preds = %block_400584
  %290 = add i64 %289, -12
  %291 = add i64 %288, 8
  store i64 %291, i64* %3, align 8
  %292 = inttoptr i64 %290 to i32*
  %293 = load i32, i32* %292, align 4
  %294 = add i32 %293, -255
  %295 = zext i32 %294 to i64
  store i64 %295, i64* %RAX.i182, align 8
  %296 = icmp ult i32 %293, 255
  %297 = zext i1 %296 to i8
  store i8 %297, i8* %28, align 1
  %298 = and i32 %294, 255
  %299 = call i32 @llvm.ctpop.i32(i32 %298)
  %300 = trunc i32 %299 to i8
  %301 = and i8 %300, 1
  %302 = xor i8 %301, 1
  store i8 %302, i8* %34, align 1
  %303 = xor i32 %293, 16
  %304 = xor i32 %303, %294
  %305 = lshr i32 %304, 4
  %306 = trunc i32 %305 to i8
  %307 = and i8 %306, 1
  store i8 %307, i8* %42, align 1
  %308 = icmp eq i32 %294, 0
  %309 = zext i1 %308 to i8
  store i8 %309, i8* %37, align 1
  %310 = lshr i32 %294, 31
  %311 = trunc i32 %310 to i8
  store i8 %311, i8* %40, align 1
  %312 = lshr i32 %293, 31
  %313 = xor i32 %310, %312
  %314 = add nuw nsw i32 %313, %312
  %315 = icmp eq i32 %314, 2
  %316 = zext i1 %315 to i8
  store i8 %316, i8* %41, align 1
  %317 = add i64 %289, -44
  %318 = add i64 %288, 16
  store i64 %318, i64* %3, align 8
  %319 = inttoptr i64 %317 to i32*
  store i32 %294, i32* %319, align 4
  %320 = load i64, i64* %3, align 8
  %321 = load i8, i8* %37, align 1
  %322 = icmp ne i8 %321, 0
  %.v10 = select i1 %322, i64 343, i64 6
  %323 = add i64 %320, %.v10
  store i64 %323, i64* %3, align 8
  %cmpBr_4005a8 = icmp eq i8 %321, 1
  %324 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_4005a8, label %block_.L_4006ff, label %block_4005ae

block_4005ae:                                     ; preds = %block_400598
  %325 = add i64 %324, -12
  %326 = add i64 %323, 8
  store i64 %326, i64* %3, align 8
  %327 = inttoptr i64 %325 to i32*
  %328 = load i32, i32* %327, align 4
  %329 = add i32 %328, -74633
  %330 = zext i32 %329 to i64
  store i64 %330, i64* %RAX.i182, align 8
  %331 = icmp ult i32 %328, 74633
  %332 = zext i1 %331 to i8
  store i8 %332, i8* %28, align 1
  %333 = and i32 %329, 255
  %334 = call i32 @llvm.ctpop.i32(i32 %333)
  %335 = trunc i32 %334 to i8
  %336 = and i8 %335, 1
  %337 = xor i8 %336, 1
  store i8 %337, i8* %34, align 1
  %338 = xor i32 %328, %329
  %339 = lshr i32 %338, 4
  %340 = trunc i32 %339 to i8
  %341 = and i8 %340, 1
  store i8 %341, i8* %42, align 1
  %342 = icmp eq i32 %329, 0
  %343 = zext i1 %342 to i8
  store i8 %343, i8* %37, align 1
  %344 = lshr i32 %329, 31
  %345 = trunc i32 %344 to i8
  store i8 %345, i8* %40, align 1
  %346 = lshr i32 %328, 31
  %347 = xor i32 %344, %346
  %348 = add nuw nsw i32 %347, %346
  %349 = icmp eq i32 %348, 2
  %350 = zext i1 %349 to i8
  store i8 %350, i8* %41, align 1
  %351 = add i64 %324, -48
  %352 = add i64 %323, 16
  store i64 %352, i64* %3, align 8
  %353 = inttoptr i64 %351 to i32*
  store i32 %329, i32* %353, align 4
  %354 = load i64, i64* %3, align 8
  %355 = load i8, i8* %37, align 1
  %356 = icmp ne i8 %355, 0
  %.v11 = select i1 %356, i64 337, i64 6
  %357 = add i64 %354, %.v11
  store i64 %357, i64* %3, align 8
  %cmpBr_4005be = icmp eq i8 %355, 1
  %358 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_4005be, label %block_.L_40070f, label %block_4005c4

block_4005c4:                                     ; preds = %block_4005ae
  %359 = add i64 %358, -12
  %360 = add i64 %357, 8
  store i64 %360, i64* %3, align 8
  %361 = inttoptr i64 %359 to i32*
  %362 = load i32, i32* %361, align 4
  %363 = add i32 %362, -74634
  %364 = zext i32 %363 to i64
  store i64 %364, i64* %RAX.i182, align 8
  %365 = icmp ult i32 %362, 74634
  %366 = zext i1 %365 to i8
  store i8 %366, i8* %28, align 1
  %367 = and i32 %363, 255
  %368 = call i32 @llvm.ctpop.i32(i32 %367)
  %369 = trunc i32 %368 to i8
  %370 = and i8 %369, 1
  %371 = xor i8 %370, 1
  store i8 %371, i8* %34, align 1
  %372 = xor i32 %362, %363
  %373 = lshr i32 %372, 4
  %374 = trunc i32 %373 to i8
  %375 = and i8 %374, 1
  store i8 %375, i8* %42, align 1
  %376 = icmp eq i32 %363, 0
  %377 = zext i1 %376 to i8
  store i8 %377, i8* %37, align 1
  %378 = lshr i32 %363, 31
  %379 = trunc i32 %378 to i8
  store i8 %379, i8* %40, align 1
  %380 = lshr i32 %362, 31
  %381 = xor i32 %378, %380
  %382 = add nuw nsw i32 %381, %380
  %383 = icmp eq i32 %382, 2
  %384 = zext i1 %383 to i8
  store i8 %384, i8* %41, align 1
  %385 = add i64 %358, -52
  %386 = add i64 %357, 16
  store i64 %386, i64* %3, align 8
  %387 = inttoptr i64 %385 to i32*
  store i32 %363, i32* %387, align 4
  %388 = load i64, i64* %3, align 8
  %389 = load i8, i8* %37, align 1
  %390 = icmp ne i8 %389, 0
  %.v12 = select i1 %390, i64 331, i64 6
  %391 = add i64 %388, %.v12
  store i64 %391, i64* %3, align 8
  %cmpBr_4005d4 = icmp eq i8 %389, 1
  %392 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_4005d4, label %block_.L_40071f, label %block_4005da

block_4005da:                                     ; preds = %block_4005c4
  %393 = add i64 %392, -12
  %394 = add i64 %391, 8
  store i64 %394, i64* %3, align 8
  %395 = inttoptr i64 %393 to i32*
  %396 = load i32, i32* %395, align 4
  %397 = add i32 %396, -74635
  %398 = zext i32 %397 to i64
  store i64 %398, i64* %RAX.i182, align 8
  %399 = icmp ult i32 %396, 74635
  %400 = zext i1 %399 to i8
  store i8 %400, i8* %28, align 1
  %401 = and i32 %397, 255
  %402 = call i32 @llvm.ctpop.i32(i32 %401)
  %403 = trunc i32 %402 to i8
  %404 = and i8 %403, 1
  %405 = xor i8 %404, 1
  store i8 %405, i8* %34, align 1
  %406 = xor i32 %396, %397
  %407 = lshr i32 %406, 4
  %408 = trunc i32 %407 to i8
  %409 = and i8 %408, 1
  store i8 %409, i8* %42, align 1
  %410 = icmp eq i32 %397, 0
  %411 = zext i1 %410 to i8
  store i8 %411, i8* %37, align 1
  %412 = lshr i32 %397, 31
  %413 = trunc i32 %412 to i8
  store i8 %413, i8* %40, align 1
  %414 = lshr i32 %396, 31
  %415 = xor i32 %412, %414
  %416 = add nuw nsw i32 %415, %414
  %417 = icmp eq i32 %416, 2
  %418 = zext i1 %417 to i8
  store i8 %418, i8* %41, align 1
  %419 = add i64 %392, -56
  %420 = add i64 %391, 16
  store i64 %420, i64* %3, align 8
  %421 = inttoptr i64 %419 to i32*
  store i32 %397, i32* %421, align 4
  %422 = load i64, i64* %3, align 8
  %423 = load i8, i8* %37, align 1
  %424 = icmp ne i8 %423, 0
  %.v13 = select i1 %424, i64 325, i64 6
  %425 = add i64 %422, %.v13
  store i64 %425, i64* %3, align 8
  %cmpBr_4005ea = icmp eq i8 %423, 1
  %426 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_4005ea, label %block_.L_40072f, label %block_4005f0

block_4005f0:                                     ; preds = %block_4005da
  %427 = add i64 %426, -12
  %428 = add i64 %425, 8
  store i64 %428, i64* %3, align 8
  %429 = inttoptr i64 %427 to i32*
  %430 = load i32, i32* %429, align 4
  %431 = add i32 %430, -74636
  %432 = zext i32 %431 to i64
  store i64 %432, i64* %RAX.i182, align 8
  %433 = icmp ult i32 %430, 74636
  %434 = zext i1 %433 to i8
  store i8 %434, i8* %28, align 1
  %435 = and i32 %431, 255
  %436 = call i32 @llvm.ctpop.i32(i32 %435)
  %437 = trunc i32 %436 to i8
  %438 = and i8 %437, 1
  %439 = xor i8 %438, 1
  store i8 %439, i8* %34, align 1
  %440 = xor i32 %430, %431
  %441 = lshr i32 %440, 4
  %442 = trunc i32 %441 to i8
  %443 = and i8 %442, 1
  store i8 %443, i8* %42, align 1
  %444 = icmp eq i32 %431, 0
  %445 = zext i1 %444 to i8
  store i8 %445, i8* %37, align 1
  %446 = lshr i32 %431, 31
  %447 = trunc i32 %446 to i8
  store i8 %447, i8* %40, align 1
  %448 = lshr i32 %430, 31
  %449 = xor i32 %446, %448
  %450 = add nuw nsw i32 %449, %448
  %451 = icmp eq i32 %450, 2
  %452 = zext i1 %451 to i8
  store i8 %452, i8* %41, align 1
  %453 = add i64 %426, -60
  %454 = add i64 %425, 16
  store i64 %454, i64* %3, align 8
  %455 = inttoptr i64 %453 to i32*
  store i32 %431, i32* %455, align 4
  %456 = load i64, i64* %3, align 8
  %457 = load i8, i8* %37, align 1
  %458 = icmp ne i8 %457, 0
  %.v14 = select i1 %458, i64 319, i64 6
  %459 = add i64 %456, %.v14
  store i64 %459, i64* %3, align 8
  %cmpBr_400600 = icmp eq i8 %457, 1
  %460 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400600, label %block_.L_40073f, label %block_400606

block_400606:                                     ; preds = %block_4005f0
  %461 = add i64 %460, -12
  %462 = add i64 %459, 8
  store i64 %462, i64* %3, align 8
  %463 = inttoptr i64 %461 to i32*
  %464 = load i32, i32* %463, align 4
  %465 = add i32 %464, -74637
  %466 = zext i32 %465 to i64
  store i64 %466, i64* %RAX.i182, align 8
  %467 = icmp ult i32 %464, 74637
  %468 = zext i1 %467 to i8
  store i8 %468, i8* %28, align 1
  %469 = and i32 %465, 255
  %470 = call i32 @llvm.ctpop.i32(i32 %469)
  %471 = trunc i32 %470 to i8
  %472 = and i8 %471, 1
  %473 = xor i8 %472, 1
  store i8 %473, i8* %34, align 1
  %474 = xor i32 %464, %465
  %475 = lshr i32 %474, 4
  %476 = trunc i32 %475 to i8
  %477 = and i8 %476, 1
  store i8 %477, i8* %42, align 1
  %478 = icmp eq i32 %465, 0
  %479 = zext i1 %478 to i8
  store i8 %479, i8* %37, align 1
  %480 = lshr i32 %465, 31
  %481 = trunc i32 %480 to i8
  store i8 %481, i8* %40, align 1
  %482 = lshr i32 %464, 31
  %483 = xor i32 %480, %482
  %484 = add nuw nsw i32 %483, %482
  %485 = icmp eq i32 %484, 2
  %486 = zext i1 %485 to i8
  store i8 %486, i8* %41, align 1
  %487 = add i64 %460, -64
  %488 = add i64 %459, 16
  store i64 %488, i64* %3, align 8
  %489 = inttoptr i64 %487 to i32*
  store i32 %465, i32* %489, align 4
  %490 = load i64, i64* %3, align 8
  %491 = load i8, i8* %37, align 1
  %492 = icmp ne i8 %491, 0
  %.v15 = select i1 %492, i64 313, i64 6
  %493 = add i64 %490, %.v15
  store i64 %493, i64* %3, align 8
  %cmpBr_400616 = icmp eq i8 %491, 1
  %494 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400616, label %block_.L_40074f, label %block_40061c

block_40061c:                                     ; preds = %block_400606
  %495 = add i64 %494, -12
  %496 = add i64 %493, 8
  store i64 %496, i64* %3, align 8
  %497 = inttoptr i64 %495 to i32*
  %498 = load i32, i32* %497, align 4
  %499 = add i32 %498, -74639
  %500 = zext i32 %499 to i64
  store i64 %500, i64* %RAX.i182, align 8
  %501 = icmp ult i32 %498, 74639
  %502 = zext i1 %501 to i8
  store i8 %502, i8* %28, align 1
  %503 = and i32 %499, 255
  %504 = call i32 @llvm.ctpop.i32(i32 %503)
  %505 = trunc i32 %504 to i8
  %506 = and i8 %505, 1
  %507 = xor i8 %506, 1
  store i8 %507, i8* %34, align 1
  %508 = xor i32 %498, %499
  %509 = lshr i32 %508, 4
  %510 = trunc i32 %509 to i8
  %511 = and i8 %510, 1
  store i8 %511, i8* %42, align 1
  %512 = icmp eq i32 %499, 0
  %513 = zext i1 %512 to i8
  store i8 %513, i8* %37, align 1
  %514 = lshr i32 %499, 31
  %515 = trunc i32 %514 to i8
  store i8 %515, i8* %40, align 1
  %516 = lshr i32 %498, 31
  %517 = xor i32 %514, %516
  %518 = add nuw nsw i32 %517, %516
  %519 = icmp eq i32 %518, 2
  %520 = zext i1 %519 to i8
  store i8 %520, i8* %41, align 1
  %521 = add i64 %494, -68
  %522 = add i64 %493, 16
  store i64 %522, i64* %3, align 8
  %523 = inttoptr i64 %521 to i32*
  store i32 %499, i32* %523, align 4
  %524 = load i64, i64* %3, align 8
  %525 = load i8, i8* %37, align 1
  %526 = icmp ne i8 %525, 0
  %.v16 = select i1 %526, i64 307, i64 6
  %527 = add i64 %524, %.v16
  store i64 %527, i64* %3, align 8
  %cmpBr_40062c = icmp eq i8 %525, 1
  %528 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_40062c, label %block_.L_40075f, label %block_400632

block_400632:                                     ; preds = %block_40061c
  %529 = add i64 %528, -12
  %530 = add i64 %527, 8
  store i64 %530, i64* %3, align 8
  %531 = inttoptr i64 %529 to i32*
  %532 = load i32, i32* %531, align 4
  %533 = add i32 %532, -74640
  %534 = zext i32 %533 to i64
  store i64 %534, i64* %RAX.i182, align 8
  %535 = icmp ult i32 %532, 74640
  %536 = zext i1 %535 to i8
  store i8 %536, i8* %28, align 1
  %537 = and i32 %533, 255
  %538 = call i32 @llvm.ctpop.i32(i32 %537)
  %539 = trunc i32 %538 to i8
  %540 = and i8 %539, 1
  %541 = xor i8 %540, 1
  store i8 %541, i8* %34, align 1
  %542 = xor i32 %532, 16
  %543 = xor i32 %542, %533
  %544 = lshr i32 %543, 4
  %545 = trunc i32 %544 to i8
  %546 = and i8 %545, 1
  store i8 %546, i8* %42, align 1
  %547 = icmp eq i32 %533, 0
  %548 = zext i1 %547 to i8
  store i8 %548, i8* %37, align 1
  %549 = lshr i32 %533, 31
  %550 = trunc i32 %549 to i8
  store i8 %550, i8* %40, align 1
  %551 = lshr i32 %532, 31
  %552 = xor i32 %549, %551
  %553 = add nuw nsw i32 %552, %551
  %554 = icmp eq i32 %553, 2
  %555 = zext i1 %554 to i8
  store i8 %555, i8* %41, align 1
  %556 = add i64 %528, -72
  %557 = add i64 %527, 16
  store i64 %557, i64* %3, align 8
  %558 = inttoptr i64 %556 to i32*
  store i32 %533, i32* %558, align 4
  %559 = load i64, i64* %3, align 8
  %560 = load i8, i8* %37, align 1
  %561 = icmp ne i8 %560, 0
  %.v17 = select i1 %561, i64 301, i64 6
  %562 = add i64 %559, %.v17
  store i64 %562, i64* %3, align 8
  %cmpBr_400642 = icmp eq i8 %560, 1
  %563 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400642, label %block_.L_40076f, label %block_400648

block_400648:                                     ; preds = %block_400632
  %564 = add i64 %563, -12
  %565 = add i64 %562, 8
  store i64 %565, i64* %3, align 8
  %566 = inttoptr i64 %564 to i32*
  %567 = load i32, i32* %566, align 4
  %568 = add i32 %567, -74641
  %569 = zext i32 %568 to i64
  store i64 %569, i64* %RAX.i182, align 8
  %570 = icmp ult i32 %567, 74641
  %571 = zext i1 %570 to i8
  store i8 %571, i8* %28, align 1
  %572 = and i32 %568, 255
  %573 = call i32 @llvm.ctpop.i32(i32 %572)
  %574 = trunc i32 %573 to i8
  %575 = and i8 %574, 1
  %576 = xor i8 %575, 1
  store i8 %576, i8* %34, align 1
  %577 = xor i32 %567, 16
  %578 = xor i32 %577, %568
  %579 = lshr i32 %578, 4
  %580 = trunc i32 %579 to i8
  %581 = and i8 %580, 1
  store i8 %581, i8* %42, align 1
  %582 = icmp eq i32 %568, 0
  %583 = zext i1 %582 to i8
  store i8 %583, i8* %37, align 1
  %584 = lshr i32 %568, 31
  %585 = trunc i32 %584 to i8
  store i8 %585, i8* %40, align 1
  %586 = lshr i32 %567, 31
  %587 = xor i32 %584, %586
  %588 = add nuw nsw i32 %587, %586
  %589 = icmp eq i32 %588, 2
  %590 = zext i1 %589 to i8
  store i8 %590, i8* %41, align 1
  %591 = add i64 %563, -76
  %592 = add i64 %562, 16
  store i64 %592, i64* %3, align 8
  %593 = inttoptr i64 %591 to i32*
  store i32 %568, i32* %593, align 4
  %594 = load i64, i64* %3, align 8
  %595 = load i8, i8* %37, align 1
  %596 = icmp ne i8 %595, 0
  %.v18 = select i1 %596, i64 295, i64 6
  %597 = add i64 %594, %.v18
  store i64 %597, i64* %3, align 8
  %cmpBr_400658 = icmp eq i8 %595, 1
  %598 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_400658, label %block_.L_40077f, label %block_40065e

block_40065e:                                     ; preds = %block_400648
  %599 = add i64 %598, -12
  %600 = add i64 %597, 8
  store i64 %600, i64* %3, align 8
  %601 = inttoptr i64 %599 to i32*
  %602 = load i32, i32* %601, align 4
  %603 = add i32 %602, -74642
  %604 = zext i32 %603 to i64
  store i64 %604, i64* %RAX.i182, align 8
  %605 = icmp ult i32 %602, 74642
  %606 = zext i1 %605 to i8
  store i8 %606, i8* %28, align 1
  %607 = and i32 %603, 255
  %608 = call i32 @llvm.ctpop.i32(i32 %607)
  %609 = trunc i32 %608 to i8
  %610 = and i8 %609, 1
  %611 = xor i8 %610, 1
  store i8 %611, i8* %34, align 1
  %612 = xor i32 %602, 16
  %613 = xor i32 %612, %603
  %614 = lshr i32 %613, 4
  %615 = trunc i32 %614 to i8
  %616 = and i8 %615, 1
  store i8 %616, i8* %42, align 1
  %617 = icmp eq i32 %603, 0
  %618 = zext i1 %617 to i8
  store i8 %618, i8* %37, align 1
  %619 = lshr i32 %603, 31
  %620 = trunc i32 %619 to i8
  store i8 %620, i8* %40, align 1
  %621 = lshr i32 %602, 31
  %622 = xor i32 %619, %621
  %623 = add nuw nsw i32 %622, %621
  %624 = icmp eq i32 %623, 2
  %625 = zext i1 %624 to i8
  store i8 %625, i8* %41, align 1
  %626 = add i64 %598, -80
  %627 = add i64 %597, 16
  store i64 %627, i64* %3, align 8
  %628 = inttoptr i64 %626 to i32*
  store i32 %603, i32* %628, align 4
  %629 = load i64, i64* %3, align 8
  %630 = load i8, i8* %37, align 1
  %631 = icmp ne i8 %630, 0
  %.v19 = select i1 %631, i64 289, i64 6
  %632 = add i64 %629, %.v19
  store i64 %632, i64* %3, align 8
  %cmpBr_40066e = icmp eq i8 %630, 1
  %633 = load i64, i64* %RBP.i, align 8
  br i1 %cmpBr_40066e, label %block_.L_40078f, label %block_400674

block_400674:                                     ; preds = %block_40065e
  %634 = add i64 %633, -12
  %635 = add i64 %632, 8
  store i64 %635, i64* %3, align 8
  %636 = inttoptr i64 %634 to i32*
  %637 = load i32, i32* %636, align 4
  %638 = add i32 %637, -74643
  %639 = zext i32 %638 to i64
  store i64 %639, i64* %RAX.i182, align 8
  %640 = icmp ult i32 %637, 74643
  %641 = zext i1 %640 to i8
  store i8 %641, i8* %28, align 1
  %642 = and i32 %638, 255
  %643 = call i32 @llvm.ctpop.i32(i32 %642)
  %644 = trunc i32 %643 to i8
  %645 = and i8 %644, 1
  %646 = xor i8 %645, 1
  store i8 %646, i8* %34, align 1
  %647 = xor i32 %637, 16
  %648 = xor i32 %647, %638
  %649 = lshr i32 %648, 4
  %650 = trunc i32 %649 to i8
  %651 = and i8 %650, 1
  store i8 %651, i8* %42, align 1
  %652 = icmp eq i32 %638, 0
  %653 = zext i1 %652 to i8
  store i8 %653, i8* %37, align 1
  %654 = lshr i32 %638, 31
  %655 = trunc i32 %654 to i8
  store i8 %655, i8* %40, align 1
  %656 = lshr i32 %637, 31
  %657 = xor i32 %654, %656
  %658 = add nuw nsw i32 %657, %656
  %659 = icmp eq i32 %658, 2
  %660 = zext i1 %659 to i8
  store i8 %660, i8* %41, align 1
  %661 = add i64 %633, -84
  %662 = add i64 %632, 16
  store i64 %662, i64* %3, align 8
  %663 = inttoptr i64 %661 to i32*
  store i32 %638, i32* %663, align 4
  %664 = load i64, i64* %3, align 8
  %665 = load i8, i8* %37, align 1
  %666 = icmp ne i8 %665, 0
  %.v20 = select i1 %666, i64 283, i64 6
  %667 = add i64 %664, %.v20
  store i64 %667, i64* %3, align 8
  %cmpBr_400684 = icmp eq i8 %665, 1
  %668 = load i64, i64* %RBP.i, align 8
  %669 = add i64 %668, -8
  br i1 %cmpBr_400684, label %block_.L_40079f, label %block_40068a

block_40068a:                                     ; preds = %block_400674
  %670 = add i64 %667, 296
  store i64 %670, i64* %3, align 8
  %671 = inttoptr i64 %669 to i32*
  %672 = load i32, i32* %671, align 4
  %673 = zext i32 %672 to i64
  store i64 %673, i64* %RAX.i182, align 8
  store i8 0, i8* %28, align 1
  %674 = and i32 %672, 255
  %675 = call i32 @llvm.ctpop.i32(i32 %674)
  %676 = trunc i32 %675 to i8
  %677 = and i8 %676, 1
  %678 = xor i8 %677, 1
  store i8 %678, i8* %34, align 1
  store i8 0, i8* %42, align 1
  %679 = icmp eq i32 %672, 0
  %680 = zext i1 %679 to i8
  store i8 %680, i8* %37, align 1
  %681 = lshr i32 %672, 31
  %682 = trunc i32 %681 to i8
  store i8 %682, i8* %40, align 1
  store i8 0, i8* %41, align 1
  %683 = add i64 %667, 302
  store i64 %683, i64* %3, align 8
  store i32 %672, i32* %671, align 4
  %.pre = load i64, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40068f:                                  ; preds = %entry
  %684 = add i64 %50, -8
  %685 = add i64 %49, 3
  store i64 %685, i64* %3, align 8
  %686 = inttoptr i64 %684 to i32*
  %687 = load i32, i32* %686, align 4
  %688 = zext i32 %687 to i64
  store i64 %688, i64* %RAX.i182, align 8
  store i8 0, i8* %28, align 1
  %689 = and i32 %687, 255
  %690 = call i32 @llvm.ctpop.i32(i32 %689)
  %691 = trunc i32 %690 to i8
  %692 = and i8 %691, 1
  %693 = xor i8 %692, 1
  store i8 %693, i8* %34, align 1
  store i8 0, i8* %42, align 1
  %694 = icmp eq i32 %687, 0
  %695 = zext i1 %694 to i8
  store i8 %695, i8* %37, align 1
  %696 = lshr i32 %687, 31
  %697 = trunc i32 %696 to i8
  store i8 %697, i8* %40, align 1
  store i8 0, i8* %41, align 1
  %698 = add i64 %49, 9
  store i64 %698, i64* %3, align 8
  store i32 %687, i32* %686, align 4
  %699 = load i64, i64* %3, align 8
  %700 = add i64 %699, 288
  store i64 %700, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40069d:                                  ; preds = %block_40050c
  %701 = add i64 %84, -8
  %702 = add i64 %83, 3
  store i64 %702, i64* %3, align 8
  %703 = inttoptr i64 %701 to i32*
  %704 = load i32, i32* %703, align 4
  %705 = add i32 %704, 1
  %706 = zext i32 %705 to i64
  store i64 %706, i64* %RAX.i182, align 8
  %707 = icmp eq i32 %704, -1
  %708 = icmp eq i32 %705, 0
  %709 = or i1 %707, %708
  %710 = zext i1 %709 to i8
  store i8 %710, i8* %28, align 1
  %711 = and i32 %705, 255
  %712 = call i32 @llvm.ctpop.i32(i32 %711)
  %713 = trunc i32 %712 to i8
  %714 = and i8 %713, 1
  %715 = xor i8 %714, 1
  store i8 %715, i8* %34, align 1
  %716 = xor i32 %704, %705
  %717 = lshr i32 %716, 4
  %718 = trunc i32 %717 to i8
  %719 = and i8 %718, 1
  store i8 %719, i8* %42, align 1
  %720 = zext i1 %708 to i8
  store i8 %720, i8* %37, align 1
  %721 = lshr i32 %705, 31
  %722 = trunc i32 %721 to i8
  store i8 %722, i8* %40, align 1
  %723 = lshr i32 %704, 31
  %724 = xor i32 %721, %723
  %725 = add nuw nsw i32 %724, %721
  %726 = icmp eq i32 %725, 2
  %727 = zext i1 %726 to i8
  store i8 %727, i8* %41, align 1
  %728 = add i64 %83, 9
  store i64 %728, i64* %3, align 8
  store i32 %705, i32* %703, align 4
  %729 = load i64, i64* %3, align 8
  %730 = add i64 %729, 274
  store i64 %730, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006ab:                                  ; preds = %block_400520
  %731 = add i64 %118, -8
  %732 = add i64 %117, 3
  store i64 %732, i64* %3, align 8
  %733 = inttoptr i64 %731 to i32*
  %734 = load i32, i32* %733, align 4
  %735 = add i32 %734, 2
  %736 = zext i32 %735 to i64
  store i64 %736, i64* %RAX.i182, align 8
  %737 = icmp ugt i32 %734, -3
  %738 = zext i1 %737 to i8
  store i8 %738, i8* %28, align 1
  %739 = and i32 %735, 255
  %740 = call i32 @llvm.ctpop.i32(i32 %739)
  %741 = trunc i32 %740 to i8
  %742 = and i8 %741, 1
  %743 = xor i8 %742, 1
  store i8 %743, i8* %34, align 1
  %744 = xor i32 %734, %735
  %745 = lshr i32 %744, 4
  %746 = trunc i32 %745 to i8
  %747 = and i8 %746, 1
  store i8 %747, i8* %42, align 1
  %748 = icmp eq i32 %735, 0
  %749 = zext i1 %748 to i8
  store i8 %749, i8* %37, align 1
  %750 = lshr i32 %735, 31
  %751 = trunc i32 %750 to i8
  store i8 %751, i8* %40, align 1
  %752 = lshr i32 %734, 31
  %753 = xor i32 %750, %752
  %754 = add nuw nsw i32 %753, %750
  %755 = icmp eq i32 %754, 2
  %756 = zext i1 %755 to i8
  store i8 %756, i8* %41, align 1
  %757 = add i64 %117, 9
  store i64 %757, i64* %3, align 8
  store i32 %735, i32* %733, align 4
  %758 = load i64, i64* %3, align 8
  %759 = add i64 %758, 260
  store i64 %759, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006b9:                                  ; preds = %block_400534
  %760 = add i64 %152, -8
  %761 = add i64 %151, 3
  store i64 %761, i64* %3, align 8
  %762 = inttoptr i64 %760 to i32*
  %763 = load i32, i32* %762, align 4
  %764 = add i32 %763, 4
  %765 = zext i32 %764 to i64
  store i64 %765, i64* %RAX.i182, align 8
  %766 = icmp ugt i32 %763, -5
  %767 = zext i1 %766 to i8
  store i8 %767, i8* %28, align 1
  %768 = and i32 %764, 255
  %769 = call i32 @llvm.ctpop.i32(i32 %768)
  %770 = trunc i32 %769 to i8
  %771 = and i8 %770, 1
  %772 = xor i8 %771, 1
  store i8 %772, i8* %34, align 1
  %773 = xor i32 %763, %764
  %774 = lshr i32 %773, 4
  %775 = trunc i32 %774 to i8
  %776 = and i8 %775, 1
  store i8 %776, i8* %42, align 1
  %777 = icmp eq i32 %764, 0
  %778 = zext i1 %777 to i8
  store i8 %778, i8* %37, align 1
  %779 = lshr i32 %764, 31
  %780 = trunc i32 %779 to i8
  store i8 %780, i8* %40, align 1
  %781 = lshr i32 %763, 31
  %782 = xor i32 %779, %781
  %783 = add nuw nsw i32 %782, %779
  %784 = icmp eq i32 %783, 2
  %785 = zext i1 %784 to i8
  store i8 %785, i8* %41, align 1
  %786 = add i64 %151, 9
  store i64 %786, i64* %3, align 8
  store i32 %764, i32* %762, align 4
  %787 = load i64, i64* %3, align 8
  %788 = add i64 %787, 246
  store i64 %788, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006c7:                                  ; preds = %block_400548
  %789 = add i64 %186, -8
  %790 = add i64 %185, 3
  store i64 %790, i64* %3, align 8
  %791 = inttoptr i64 %789 to i32*
  %792 = load i32, i32* %791, align 4
  %793 = add i32 %792, 6
  %794 = zext i32 %793 to i64
  store i64 %794, i64* %RAX.i182, align 8
  %795 = icmp ugt i32 %792, -7
  %796 = zext i1 %795 to i8
  store i8 %796, i8* %28, align 1
  %797 = and i32 %793, 255
  %798 = call i32 @llvm.ctpop.i32(i32 %797)
  %799 = trunc i32 %798 to i8
  %800 = and i8 %799, 1
  %801 = xor i8 %800, 1
  store i8 %801, i8* %34, align 1
  %802 = xor i32 %792, %793
  %803 = lshr i32 %802, 4
  %804 = trunc i32 %803 to i8
  %805 = and i8 %804, 1
  store i8 %805, i8* %42, align 1
  %806 = icmp eq i32 %793, 0
  %807 = zext i1 %806 to i8
  store i8 %807, i8* %37, align 1
  %808 = lshr i32 %793, 31
  %809 = trunc i32 %808 to i8
  store i8 %809, i8* %40, align 1
  %810 = lshr i32 %792, 31
  %811 = xor i32 %808, %810
  %812 = add nuw nsw i32 %811, %808
  %813 = icmp eq i32 %812, 2
  %814 = zext i1 %813 to i8
  store i8 %814, i8* %41, align 1
  %815 = add i64 %185, 9
  store i64 %815, i64* %3, align 8
  store i32 %793, i32* %791, align 4
  %816 = load i64, i64* %3, align 8
  %817 = add i64 %816, 232
  store i64 %817, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006d5:                                  ; preds = %block_40055c
  %818 = add i64 %220, -8
  %819 = add i64 %219, 3
  store i64 %819, i64* %3, align 8
  %820 = inttoptr i64 %818 to i32*
  %821 = load i32, i32* %820, align 4
  %822 = add i32 %821, 12
  %823 = zext i32 %822 to i64
  store i64 %823, i64* %RAX.i182, align 8
  %824 = icmp ugt i32 %821, -13
  %825 = zext i1 %824 to i8
  store i8 %825, i8* %28, align 1
  %826 = and i32 %822, 255
  %827 = call i32 @llvm.ctpop.i32(i32 %826)
  %828 = trunc i32 %827 to i8
  %829 = and i8 %828, 1
  %830 = xor i8 %829, 1
  store i8 %830, i8* %34, align 1
  %831 = xor i32 %821, %822
  %832 = lshr i32 %831, 4
  %833 = trunc i32 %832 to i8
  %834 = and i8 %833, 1
  store i8 %834, i8* %42, align 1
  %835 = icmp eq i32 %822, 0
  %836 = zext i1 %835 to i8
  store i8 %836, i8* %37, align 1
  %837 = lshr i32 %822, 31
  %838 = trunc i32 %837 to i8
  store i8 %838, i8* %40, align 1
  %839 = lshr i32 %821, 31
  %840 = xor i32 %837, %839
  %841 = add nuw nsw i32 %840, %837
  %842 = icmp eq i32 %841, 2
  %843 = zext i1 %842 to i8
  store i8 %843, i8* %41, align 1
  %844 = add i64 %219, 9
  store i64 %844, i64* %3, align 8
  store i32 %822, i32* %820, align 4
  %845 = load i64, i64* %3, align 8
  %846 = add i64 %845, 218
  store i64 %846, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006e3:                                  ; preds = %block_400570
  %847 = add i64 %254, -8
  %848 = add i64 %253, 3
  store i64 %848, i64* %3, align 8
  %849 = inttoptr i64 %847 to i32*
  %850 = load i32, i32* %849, align 4
  %851 = add i32 %850, 13
  %852 = zext i32 %851 to i64
  store i64 %852, i64* %RAX.i182, align 8
  %853 = icmp ugt i32 %850, -14
  %854 = zext i1 %853 to i8
  store i8 %854, i8* %28, align 1
  %855 = and i32 %851, 255
  %856 = call i32 @llvm.ctpop.i32(i32 %855)
  %857 = trunc i32 %856 to i8
  %858 = and i8 %857, 1
  %859 = xor i8 %858, 1
  store i8 %859, i8* %34, align 1
  %860 = xor i32 %850, %851
  %861 = lshr i32 %860, 4
  %862 = trunc i32 %861 to i8
  %863 = and i8 %862, 1
  store i8 %863, i8* %42, align 1
  %864 = icmp eq i32 %851, 0
  %865 = zext i1 %864 to i8
  store i8 %865, i8* %37, align 1
  %866 = lshr i32 %851, 31
  %867 = trunc i32 %866 to i8
  store i8 %867, i8* %40, align 1
  %868 = lshr i32 %850, 31
  %869 = xor i32 %866, %868
  %870 = add nuw nsw i32 %869, %866
  %871 = icmp eq i32 %870, 2
  %872 = zext i1 %871 to i8
  store i8 %872, i8* %41, align 1
  %873 = add i64 %253, 9
  store i64 %873, i64* %3, align 8
  store i32 %851, i32* %849, align 4
  %874 = load i64, i64* %3, align 8
  %875 = add i64 %874, 204
  store i64 %875, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006f1:                                  ; preds = %block_400584
  %876 = add i64 %289, -8
  %877 = add i64 %288, 3
  store i64 %877, i64* %3, align 8
  %878 = inttoptr i64 %876 to i32*
  %879 = load i32, i32* %878, align 4
  %880 = add i32 %879, 19
  %881 = zext i32 %880 to i64
  store i64 %881, i64* %RAX.i182, align 8
  %882 = icmp ugt i32 %879, -20
  %883 = zext i1 %882 to i8
  store i8 %883, i8* %28, align 1
  %884 = and i32 %880, 255
  %885 = call i32 @llvm.ctpop.i32(i32 %884)
  %886 = trunc i32 %885 to i8
  %887 = and i8 %886, 1
  %888 = xor i8 %887, 1
  store i8 %888, i8* %34, align 1
  %889 = xor i32 %879, 16
  %890 = xor i32 %889, %880
  %891 = lshr i32 %890, 4
  %892 = trunc i32 %891 to i8
  %893 = and i8 %892, 1
  store i8 %893, i8* %42, align 1
  %894 = icmp eq i32 %880, 0
  %895 = zext i1 %894 to i8
  store i8 %895, i8* %37, align 1
  %896 = lshr i32 %880, 31
  %897 = trunc i32 %896 to i8
  store i8 %897, i8* %40, align 1
  %898 = lshr i32 %879, 31
  %899 = xor i32 %896, %898
  %900 = add nuw nsw i32 %899, %896
  %901 = icmp eq i32 %900, 2
  %902 = zext i1 %901 to i8
  store i8 %902, i8* %41, align 1
  %903 = add i64 %288, 9
  store i64 %903, i64* %3, align 8
  store i32 %880, i32* %878, align 4
  %904 = load i64, i64* %3, align 8
  %905 = add i64 %904, 190
  store i64 %905, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4006ff:                                  ; preds = %block_400598
  %906 = add i64 %324, -8
  %907 = add i64 %323, 3
  store i64 %907, i64* %3, align 8
  %908 = inttoptr i64 %906 to i32*
  %909 = load i32, i32* %908, align 4
  %910 = add i32 %909, 255
  %911 = zext i32 %910 to i64
  store i64 %911, i64* %RAX.i182, align 8
  %912 = icmp ugt i32 %909, -256
  %913 = zext i1 %912 to i8
  store i8 %913, i8* %28, align 1
  %914 = and i32 %910, 255
  %915 = call i32 @llvm.ctpop.i32(i32 %914)
  %916 = trunc i32 %915 to i8
  %917 = and i8 %916, 1
  %918 = xor i8 %917, 1
  store i8 %918, i8* %34, align 1
  %919 = xor i32 %909, 16
  %920 = xor i32 %919, %910
  %921 = lshr i32 %920, 4
  %922 = trunc i32 %921 to i8
  %923 = and i8 %922, 1
  store i8 %923, i8* %42, align 1
  %924 = icmp eq i32 %910, 0
  %925 = zext i1 %924 to i8
  store i8 %925, i8* %37, align 1
  %926 = lshr i32 %910, 31
  %927 = trunc i32 %926 to i8
  store i8 %927, i8* %40, align 1
  %928 = lshr i32 %909, 31
  %929 = xor i32 %926, %928
  %930 = add nuw nsw i32 %929, %926
  %931 = icmp eq i32 %930, 2
  %932 = zext i1 %931 to i8
  store i8 %932, i8* %41, align 1
  %933 = add i64 %323, 11
  store i64 %933, i64* %3, align 8
  store i32 %910, i32* %908, align 4
  %934 = load i64, i64* %3, align 8
  %935 = add i64 %934, 174
  store i64 %935, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40070f:                                  ; preds = %block_4005ae
  %936 = add i64 %358, -8
  %937 = add i64 %357, 3
  store i64 %937, i64* %3, align 8
  %938 = inttoptr i64 %936 to i32*
  %939 = load i32, i32* %938, align 4
  %940 = add i32 %939, 74633
  %941 = zext i32 %940 to i64
  store i64 %941, i64* %RAX.i182, align 8
  %942 = icmp ugt i32 %939, -74634
  %943 = zext i1 %942 to i8
  store i8 %943, i8* %28, align 1
  %944 = and i32 %940, 255
  %945 = call i32 @llvm.ctpop.i32(i32 %944)
  %946 = trunc i32 %945 to i8
  %947 = and i8 %946, 1
  %948 = xor i8 %947, 1
  store i8 %948, i8* %34, align 1
  %949 = xor i32 %939, %940
  %950 = lshr i32 %949, 4
  %951 = trunc i32 %950 to i8
  %952 = and i8 %951, 1
  store i8 %952, i8* %42, align 1
  %953 = icmp eq i32 %940, 0
  %954 = zext i1 %953 to i8
  store i8 %954, i8* %37, align 1
  %955 = lshr i32 %940, 31
  %956 = trunc i32 %955 to i8
  store i8 %956, i8* %40, align 1
  %957 = lshr i32 %939, 31
  %958 = xor i32 %955, %957
  %959 = add nuw nsw i32 %958, %955
  %960 = icmp eq i32 %959, 2
  %961 = zext i1 %960 to i8
  store i8 %961, i8* %41, align 1
  %962 = add i64 %357, 11
  store i64 %962, i64* %3, align 8
  store i32 %940, i32* %938, align 4
  %963 = load i64, i64* %3, align 8
  %964 = add i64 %963, 158
  store i64 %964, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40071f:                                  ; preds = %block_4005c4
  %965 = add i64 %392, -8
  %966 = add i64 %391, 3
  store i64 %966, i64* %3, align 8
  %967 = inttoptr i64 %965 to i32*
  %968 = load i32, i32* %967, align 4
  %969 = add i32 %968, 74634
  %970 = zext i32 %969 to i64
  store i64 %970, i64* %RAX.i182, align 8
  %971 = icmp ugt i32 %968, -74635
  %972 = zext i1 %971 to i8
  store i8 %972, i8* %28, align 1
  %973 = and i32 %969, 255
  %974 = call i32 @llvm.ctpop.i32(i32 %973)
  %975 = trunc i32 %974 to i8
  %976 = and i8 %975, 1
  %977 = xor i8 %976, 1
  store i8 %977, i8* %34, align 1
  %978 = xor i32 %968, %969
  %979 = lshr i32 %978, 4
  %980 = trunc i32 %979 to i8
  %981 = and i8 %980, 1
  store i8 %981, i8* %42, align 1
  %982 = icmp eq i32 %969, 0
  %983 = zext i1 %982 to i8
  store i8 %983, i8* %37, align 1
  %984 = lshr i32 %969, 31
  %985 = trunc i32 %984 to i8
  store i8 %985, i8* %40, align 1
  %986 = lshr i32 %968, 31
  %987 = xor i32 %984, %986
  %988 = add nuw nsw i32 %987, %984
  %989 = icmp eq i32 %988, 2
  %990 = zext i1 %989 to i8
  store i8 %990, i8* %41, align 1
  %991 = add i64 %391, 11
  store i64 %991, i64* %3, align 8
  store i32 %969, i32* %967, align 4
  %992 = load i64, i64* %3, align 8
  %993 = add i64 %992, 142
  store i64 %993, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40072f:                                  ; preds = %block_4005da
  %994 = add i64 %426, -8
  %995 = add i64 %425, 3
  store i64 %995, i64* %3, align 8
  %996 = inttoptr i64 %994 to i32*
  %997 = load i32, i32* %996, align 4
  %998 = add i32 %997, 74635
  %999 = zext i32 %998 to i64
  store i64 %999, i64* %RAX.i182, align 8
  %1000 = icmp ugt i32 %997, -74636
  %1001 = zext i1 %1000 to i8
  store i8 %1001, i8* %28, align 1
  %1002 = and i32 %998, 255
  %1003 = call i32 @llvm.ctpop.i32(i32 %1002)
  %1004 = trunc i32 %1003 to i8
  %1005 = and i8 %1004, 1
  %1006 = xor i8 %1005, 1
  store i8 %1006, i8* %34, align 1
  %1007 = xor i32 %997, %998
  %1008 = lshr i32 %1007, 4
  %1009 = trunc i32 %1008 to i8
  %1010 = and i8 %1009, 1
  store i8 %1010, i8* %42, align 1
  %1011 = icmp eq i32 %998, 0
  %1012 = zext i1 %1011 to i8
  store i8 %1012, i8* %37, align 1
  %1013 = lshr i32 %998, 31
  %1014 = trunc i32 %1013 to i8
  store i8 %1014, i8* %40, align 1
  %1015 = lshr i32 %997, 31
  %1016 = xor i32 %1013, %1015
  %1017 = add nuw nsw i32 %1016, %1013
  %1018 = icmp eq i32 %1017, 2
  %1019 = zext i1 %1018 to i8
  store i8 %1019, i8* %41, align 1
  %1020 = add i64 %425, 11
  store i64 %1020, i64* %3, align 8
  store i32 %998, i32* %996, align 4
  %1021 = load i64, i64* %3, align 8
  %1022 = add i64 %1021, 126
  store i64 %1022, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40073f:                                  ; preds = %block_4005f0
  %1023 = add i64 %460, -8
  %1024 = add i64 %459, 3
  store i64 %1024, i64* %3, align 8
  %1025 = inttoptr i64 %1023 to i32*
  %1026 = load i32, i32* %1025, align 4
  %1027 = add i32 %1026, 74636
  %1028 = zext i32 %1027 to i64
  store i64 %1028, i64* %RAX.i182, align 8
  %1029 = icmp ugt i32 %1026, -74637
  %1030 = zext i1 %1029 to i8
  store i8 %1030, i8* %28, align 1
  %1031 = and i32 %1027, 255
  %1032 = call i32 @llvm.ctpop.i32(i32 %1031)
  %1033 = trunc i32 %1032 to i8
  %1034 = and i8 %1033, 1
  %1035 = xor i8 %1034, 1
  store i8 %1035, i8* %34, align 1
  %1036 = xor i32 %1026, %1027
  %1037 = lshr i32 %1036, 4
  %1038 = trunc i32 %1037 to i8
  %1039 = and i8 %1038, 1
  store i8 %1039, i8* %42, align 1
  %1040 = icmp eq i32 %1027, 0
  %1041 = zext i1 %1040 to i8
  store i8 %1041, i8* %37, align 1
  %1042 = lshr i32 %1027, 31
  %1043 = trunc i32 %1042 to i8
  store i8 %1043, i8* %40, align 1
  %1044 = lshr i32 %1026, 31
  %1045 = xor i32 %1042, %1044
  %1046 = add nuw nsw i32 %1045, %1042
  %1047 = icmp eq i32 %1046, 2
  %1048 = zext i1 %1047 to i8
  store i8 %1048, i8* %41, align 1
  %1049 = add i64 %459, 11
  store i64 %1049, i64* %3, align 8
  store i32 %1027, i32* %1025, align 4
  %1050 = load i64, i64* %3, align 8
  %1051 = add i64 %1050, 110
  store i64 %1051, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40074f:                                  ; preds = %block_400606
  %1052 = add i64 %494, -8
  %1053 = add i64 %493, 3
  store i64 %1053, i64* %3, align 8
  %1054 = inttoptr i64 %1052 to i32*
  %1055 = load i32, i32* %1054, align 4
  %1056 = add i32 %1055, 74637
  %1057 = zext i32 %1056 to i64
  store i64 %1057, i64* %RAX.i182, align 8
  %1058 = icmp ugt i32 %1055, -74638
  %1059 = zext i1 %1058 to i8
  store i8 %1059, i8* %28, align 1
  %1060 = and i32 %1056, 255
  %1061 = call i32 @llvm.ctpop.i32(i32 %1060)
  %1062 = trunc i32 %1061 to i8
  %1063 = and i8 %1062, 1
  %1064 = xor i8 %1063, 1
  store i8 %1064, i8* %34, align 1
  %1065 = xor i32 %1055, %1056
  %1066 = lshr i32 %1065, 4
  %1067 = trunc i32 %1066 to i8
  %1068 = and i8 %1067, 1
  store i8 %1068, i8* %42, align 1
  %1069 = icmp eq i32 %1056, 0
  %1070 = zext i1 %1069 to i8
  store i8 %1070, i8* %37, align 1
  %1071 = lshr i32 %1056, 31
  %1072 = trunc i32 %1071 to i8
  store i8 %1072, i8* %40, align 1
  %1073 = lshr i32 %1055, 31
  %1074 = xor i32 %1071, %1073
  %1075 = add nuw nsw i32 %1074, %1071
  %1076 = icmp eq i32 %1075, 2
  %1077 = zext i1 %1076 to i8
  store i8 %1077, i8* %41, align 1
  %1078 = add i64 %493, 11
  store i64 %1078, i64* %3, align 8
  store i32 %1056, i32* %1054, align 4
  %1079 = load i64, i64* %3, align 8
  %1080 = add i64 %1079, 94
  store i64 %1080, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40075f:                                  ; preds = %block_40061c
  %1081 = add i64 %528, -8
  %1082 = add i64 %527, 3
  store i64 %1082, i64* %3, align 8
  %1083 = inttoptr i64 %1081 to i32*
  %1084 = load i32, i32* %1083, align 4
  %1085 = add i32 %1084, 74639
  %1086 = zext i32 %1085 to i64
  store i64 %1086, i64* %RAX.i182, align 8
  %1087 = icmp ugt i32 %1084, -74640
  %1088 = zext i1 %1087 to i8
  store i8 %1088, i8* %28, align 1
  %1089 = and i32 %1085, 255
  %1090 = call i32 @llvm.ctpop.i32(i32 %1089)
  %1091 = trunc i32 %1090 to i8
  %1092 = and i8 %1091, 1
  %1093 = xor i8 %1092, 1
  store i8 %1093, i8* %34, align 1
  %1094 = xor i32 %1084, %1085
  %1095 = lshr i32 %1094, 4
  %1096 = trunc i32 %1095 to i8
  %1097 = and i8 %1096, 1
  store i8 %1097, i8* %42, align 1
  %1098 = icmp eq i32 %1085, 0
  %1099 = zext i1 %1098 to i8
  store i8 %1099, i8* %37, align 1
  %1100 = lshr i32 %1085, 31
  %1101 = trunc i32 %1100 to i8
  store i8 %1101, i8* %40, align 1
  %1102 = lshr i32 %1084, 31
  %1103 = xor i32 %1100, %1102
  %1104 = add nuw nsw i32 %1103, %1100
  %1105 = icmp eq i32 %1104, 2
  %1106 = zext i1 %1105 to i8
  store i8 %1106, i8* %41, align 1
  %1107 = add i64 %527, 11
  store i64 %1107, i64* %3, align 8
  store i32 %1085, i32* %1083, align 4
  %1108 = load i64, i64* %3, align 8
  %1109 = add i64 %1108, 78
  store i64 %1109, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40076f:                                  ; preds = %block_400632
  %1110 = add i64 %563, -8
  %1111 = add i64 %562, 3
  store i64 %1111, i64* %3, align 8
  %1112 = inttoptr i64 %1110 to i32*
  %1113 = load i32, i32* %1112, align 4
  %1114 = add i32 %1113, 74640
  %1115 = zext i32 %1114 to i64
  store i64 %1115, i64* %RAX.i182, align 8
  %1116 = icmp ugt i32 %1113, -74641
  %1117 = zext i1 %1116 to i8
  store i8 %1117, i8* %28, align 1
  %1118 = and i32 %1114, 255
  %1119 = call i32 @llvm.ctpop.i32(i32 %1118)
  %1120 = trunc i32 %1119 to i8
  %1121 = and i8 %1120, 1
  %1122 = xor i8 %1121, 1
  store i8 %1122, i8* %34, align 1
  %1123 = xor i32 %1113, 16
  %1124 = xor i32 %1123, %1114
  %1125 = lshr i32 %1124, 4
  %1126 = trunc i32 %1125 to i8
  %1127 = and i8 %1126, 1
  store i8 %1127, i8* %42, align 1
  %1128 = icmp eq i32 %1114, 0
  %1129 = zext i1 %1128 to i8
  store i8 %1129, i8* %37, align 1
  %1130 = lshr i32 %1114, 31
  %1131 = trunc i32 %1130 to i8
  store i8 %1131, i8* %40, align 1
  %1132 = lshr i32 %1113, 31
  %1133 = xor i32 %1130, %1132
  %1134 = add nuw nsw i32 %1133, %1130
  %1135 = icmp eq i32 %1134, 2
  %1136 = zext i1 %1135 to i8
  store i8 %1136, i8* %41, align 1
  %1137 = add i64 %562, 11
  store i64 %1137, i64* %3, align 8
  store i32 %1114, i32* %1112, align 4
  %1138 = load i64, i64* %3, align 8
  %1139 = add i64 %1138, 62
  store i64 %1139, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40077f:                                  ; preds = %block_400648
  %1140 = add i64 %598, -8
  %1141 = add i64 %597, 3
  store i64 %1141, i64* %3, align 8
  %1142 = inttoptr i64 %1140 to i32*
  %1143 = load i32, i32* %1142, align 4
  %1144 = add i32 %1143, 74641
  %1145 = zext i32 %1144 to i64
  store i64 %1145, i64* %RAX.i182, align 8
  %1146 = icmp ugt i32 %1143, -74642
  %1147 = zext i1 %1146 to i8
  store i8 %1147, i8* %28, align 1
  %1148 = and i32 %1144, 255
  %1149 = call i32 @llvm.ctpop.i32(i32 %1148)
  %1150 = trunc i32 %1149 to i8
  %1151 = and i8 %1150, 1
  %1152 = xor i8 %1151, 1
  store i8 %1152, i8* %34, align 1
  %1153 = xor i32 %1143, 16
  %1154 = xor i32 %1153, %1144
  %1155 = lshr i32 %1154, 4
  %1156 = trunc i32 %1155 to i8
  %1157 = and i8 %1156, 1
  store i8 %1157, i8* %42, align 1
  %1158 = icmp eq i32 %1144, 0
  %1159 = zext i1 %1158 to i8
  store i8 %1159, i8* %37, align 1
  %1160 = lshr i32 %1144, 31
  %1161 = trunc i32 %1160 to i8
  store i8 %1161, i8* %40, align 1
  %1162 = lshr i32 %1143, 31
  %1163 = xor i32 %1160, %1162
  %1164 = add nuw nsw i32 %1163, %1160
  %1165 = icmp eq i32 %1164, 2
  %1166 = zext i1 %1165 to i8
  store i8 %1166, i8* %41, align 1
  %1167 = add i64 %597, 11
  store i64 %1167, i64* %3, align 8
  store i32 %1144, i32* %1142, align 4
  %1168 = load i64, i64* %3, align 8
  %1169 = add i64 %1168, 46
  store i64 %1169, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40078f:                                  ; preds = %block_40065e
  %1170 = add i64 %633, -8
  %1171 = add i64 %632, 3
  store i64 %1171, i64* %3, align 8
  %1172 = inttoptr i64 %1170 to i32*
  %1173 = load i32, i32* %1172, align 4
  %1174 = add i32 %1173, 74642
  %1175 = zext i32 %1174 to i64
  store i64 %1175, i64* %RAX.i182, align 8
  %1176 = icmp ugt i32 %1173, -74643
  %1177 = zext i1 %1176 to i8
  store i8 %1177, i8* %28, align 1
  %1178 = and i32 %1174, 255
  %1179 = call i32 @llvm.ctpop.i32(i32 %1178)
  %1180 = trunc i32 %1179 to i8
  %1181 = and i8 %1180, 1
  %1182 = xor i8 %1181, 1
  store i8 %1182, i8* %34, align 1
  %1183 = xor i32 %1173, 16
  %1184 = xor i32 %1183, %1174
  %1185 = lshr i32 %1184, 4
  %1186 = trunc i32 %1185 to i8
  %1187 = and i8 %1186, 1
  store i8 %1187, i8* %42, align 1
  %1188 = icmp eq i32 %1174, 0
  %1189 = zext i1 %1188 to i8
  store i8 %1189, i8* %37, align 1
  %1190 = lshr i32 %1174, 31
  %1191 = trunc i32 %1190 to i8
  store i8 %1191, i8* %40, align 1
  %1192 = lshr i32 %1173, 31
  %1193 = xor i32 %1190, %1192
  %1194 = add nuw nsw i32 %1193, %1190
  %1195 = icmp eq i32 %1194, 2
  %1196 = zext i1 %1195 to i8
  store i8 %1196, i8* %41, align 1
  %1197 = add i64 %632, 11
  store i64 %1197, i64* %3, align 8
  store i32 %1174, i32* %1172, align 4
  %1198 = load i64, i64* %3, align 8
  %1199 = add i64 %1198, 30
  store i64 %1199, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_40079f:                                  ; preds = %block_400674
  %1200 = add i64 %667, 3
  store i64 %1200, i64* %3, align 8
  %1201 = inttoptr i64 %669 to i32*
  %1202 = load i32, i32* %1201, align 4
  %1203 = add i32 %1202, 74643
  %1204 = zext i32 %1203 to i64
  store i64 %1204, i64* %RAX.i182, align 8
  %1205 = icmp ugt i32 %1202, -74644
  %1206 = zext i1 %1205 to i8
  store i8 %1206, i8* %28, align 1
  %1207 = and i32 %1203, 255
  %1208 = call i32 @llvm.ctpop.i32(i32 %1207)
  %1209 = trunc i32 %1208 to i8
  %1210 = and i8 %1209, 1
  %1211 = xor i8 %1210, 1
  store i8 %1211, i8* %34, align 1
  %1212 = xor i32 %1202, 16
  %1213 = xor i32 %1212, %1203
  %1214 = lshr i32 %1213, 4
  %1215 = trunc i32 %1214 to i8
  %1216 = and i8 %1215, 1
  store i8 %1216, i8* %42, align 1
  %1217 = icmp eq i32 %1203, 0
  %1218 = zext i1 %1217 to i8
  store i8 %1218, i8* %37, align 1
  %1219 = lshr i32 %1203, 31
  %1220 = trunc i32 %1219 to i8
  store i8 %1220, i8* %40, align 1
  %1221 = lshr i32 %1202, 31
  %1222 = xor i32 %1219, %1221
  %1223 = add nuw nsw i32 %1222, %1219
  %1224 = icmp eq i32 %1223, 2
  %1225 = zext i1 %1224 to i8
  store i8 %1225, i8* %41, align 1
  %1226 = add i64 %667, 11
  store i64 %1226, i64* %3, align 8
  store i32 %1203, i32* %1201, align 4
  %1227 = load i64, i64* %3, align 8
  %1228 = add i64 %1227, 14
  store i64 %1228, i64* %3, align 8
  br label %block_.L_4007b8

block_.L_4007b8:                                  ; preds = %block_.L_40079f, %block_.L_40078f, %block_.L_40077f, %block_.L_40076f, %block_.L_40075f, %block_.L_40074f, %block_.L_40073f, %block_.L_40072f, %block_.L_40071f, %block_.L_40070f, %block_.L_4006ff, %block_.L_4006f1, %block_.L_4006e3, %block_.L_4006d5, %block_.L_4006c7, %block_.L_4006b9, %block_.L_4006ab, %block_.L_40069d, %block_.L_40068f, %block_40068a
  %1229 = phi i64 [ %.pre, %block_40068a ], [ %1228, %block_.L_40079f ], [ %1199, %block_.L_40078f ], [ %1169, %block_.L_40077f ], [ %1139, %block_.L_40076f ], [ %1109, %block_.L_40075f ], [ %1080, %block_.L_40074f ], [ %1051, %block_.L_40073f ], [ %1022, %block_.L_40072f ], [ %993, %block_.L_40071f ], [ %964, %block_.L_40070f ], [ %935, %block_.L_4006ff ], [ %905, %block_.L_4006f1 ], [ %875, %block_.L_4006e3 ], [ %846, %block_.L_4006d5 ], [ %817, %block_.L_4006c7 ], [ %788, %block_.L_4006b9 ], [ %759, %block_.L_4006ab ], [ %730, %block_.L_40069d ], [ %700, %block_.L_40068f ]
  %1230 = load i64, i64* %RBP.i, align 8
  %1231 = add i64 %1230, -8
  %1232 = add i64 %1229, 3
  store i64 %1232, i64* %3, align 8
  %1233 = inttoptr i64 %1231 to i32*
  %1234 = load i32, i32* %1233, align 4
  %1235 = zext i32 %1234 to i64
  store i64 %1235, i64* %RAX.i182, align 8
  %1236 = add i64 %1229, 4
  store i64 %1236, i64* %3, align 8
  %1237 = load i64, i64* %6, align 8
  %1238 = add i64 %1237, 8
  %1239 = inttoptr i64 %1237 to i64*
  %1240 = load i64, i64* %1239, align 8
  store i64 %1240, i64* %RBP.i, align 8
  store i64 %1238, i64* %6, align 8
  %1241 = add i64 %1229, 5
  store i64 %1241, i64* %3, align 8
  %1242 = inttoptr i64 %1238 to i64*
  %1243 = load i64, i64* %1242, align 8
  store i64 %1243, i64* %3, align 8
  %1244 = add i64 %1237, 16
  store i64 %1244, i64* %6, align 8
  ret %struct.Memory* %2
}

attributes #0 = { nounwind readnone }
attributes #1 = { alwaysinline }
