; ModuleID = 'mcsema/test0.target.opt.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"

%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.anon = type { i64 }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque

; Function Attrs: nounwind readnone
declare i32 @llvm.ctpop.i32(i32) #0

; Function Attrs: alwaysinline
define %struct.Memory* @sum_2_n(%struct.State* noalias, i64, %struct.Memory* noalias) #1 {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %PC.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP.i, align 8
  %5 = add i64 %1, 1
  store i64 %5, i64* %PC.i, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %4, i64* %9, align 8
  store i64 %8, i64* %6, align 8
  %10 = load i64, i64* %PC.i, align 8
  store i64 %8, i64* %RBP.i, align 8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0
  %EDI.i = bitcast %union.anon* %11 to i32*
  %12 = add i64 %7, -12
  %13 = load i32, i32* %EDI.i, align 4
  %14 = add i64 %10, 6
  store i64 %14, i64* %PC.i, align 8
  %15 = inttoptr i64 %12 to i32*
  store i32 %13, i32* %15, align 4
  %16 = load i64, i64* %RBP.i, align 8
  %17 = add i64 %16, -8
  %18 = load i64, i64* %PC.i, align 8
  %19 = add i64 %18, 7
  store i64 %19, i64* %PC.i, align 8
  %20 = inttoptr i64 %17 to i32*
  store i32 0, i32* %20, align 4
  %21 = load i64, i64* %RBP.i, align 8
  %22 = add i64 %21, -12
  %23 = load i64, i64* %PC.i, align 8
  %24 = add i64 %23, 7
  store i64 %24, i64* %PC.i, align 8
  %25 = inttoptr i64 %22 to i32*
  store i32 0, i32* %25, align 4
  %RAX.i28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %27 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %30 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %31 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %.pre = load i64, i64* %PC.i, align 8
  br label %block_.L_4004a5

block_.L_4004a5:                                  ; preds = %block_4004b1, %entry
  %32 = phi i64 [ %137, %block_4004b1 ], [ %.pre, %entry ]
  %33 = load i64, i64* %RBP.i, align 8
  %34 = add i64 %33, -12
  %35 = add i64 %32, 3
  store i64 %35, i64* %PC.i, align 8
  %36 = inttoptr i64 %34 to i32*
  %37 = load i32, i32* %36, align 4
  %38 = zext i32 %37 to i64
  store i64 %38, i64* %RAX.i28, align 8
  %39 = add i64 %33, -4
  %40 = add i64 %32, 6
  store i64 %40, i64* %PC.i, align 8
  %41 = inttoptr i64 %39 to i32*
  %42 = load i32, i32* %41, align 4
  %43 = sub i32 %37, %42
  %44 = icmp ult i32 %37, %42
  %45 = zext i1 %44 to i8
  store i8 %45, i8* %26, align 1
  %46 = and i32 %43, 255
  %47 = call i32 @llvm.ctpop.i32(i32 %46)
  %48 = trunc i32 %47 to i8
  %49 = and i8 %48, 1
  %50 = xor i8 %49, 1
  store i8 %50, i8* %27, align 1
  %51 = xor i32 %42, %37
  %52 = xor i32 %51, %43
  %53 = lshr i32 %52, 4
  %54 = trunc i32 %53 to i8
  %55 = and i8 %54, 1
  store i8 %55, i8* %28, align 1
  %56 = icmp eq i32 %43, 0
  %57 = zext i1 %56 to i8
  store i8 %57, i8* %29, align 1
  %58 = lshr i32 %43, 31
  %59 = trunc i32 %58 to i8
  store i8 %59, i8* %30, align 1
  %60 = lshr i32 %37, 31
  %61 = lshr i32 %42, 31
  %62 = xor i32 %61, %60
  %63 = xor i32 %58, %60
  %64 = add nuw nsw i32 %63, %62
  %65 = icmp eq i32 %64, 2
  %66 = zext i1 %65 to i8
  store i8 %66, i8* %31, align 1
  %67 = icmp ne i8 %59, 0
  %68 = xor i1 %67, %65
  %.v = select i1 %68, i64 12, i64 35
  %69 = add i64 %32, %.v
  store i64 %69, i64* %3, align 8
  br i1 %68, label %block_4004b1, label %block_.L_4004c8

block_4004b1:                                     ; preds = %block_.L_4004a5
  %70 = add i64 %69, 3
  store i64 %70, i64* %PC.i, align 8
  %71 = load i32, i32* %36, align 4
  %72 = zext i32 %71 to i64
  store i64 %72, i64* %RAX.i28, align 8
  %73 = add i64 %33, -8
  %74 = add i64 %69, 6
  store i64 %74, i64* %PC.i, align 8
  %75 = inttoptr i64 %73 to i32*
  %76 = load i32, i32* %75, align 4
  %77 = add i32 %76, %71
  %78 = zext i32 %77 to i64
  store i64 %78, i64* %RAX.i28, align 8
  %79 = icmp ult i32 %77, %71
  %80 = icmp ult i32 %77, %76
  %81 = or i1 %79, %80
  %82 = zext i1 %81 to i8
  store i8 %82, i8* %26, align 1
  %83 = and i32 %77, 255
  %84 = call i32 @llvm.ctpop.i32(i32 %83)
  %85 = trunc i32 %84 to i8
  %86 = and i8 %85, 1
  %87 = xor i8 %86, 1
  store i8 %87, i8* %27, align 1
  %88 = xor i32 %76, %71
  %89 = xor i32 %88, %77
  %90 = lshr i32 %89, 4
  %91 = trunc i32 %90 to i8
  %92 = and i8 %91, 1
  store i8 %92, i8* %28, align 1
  %93 = icmp eq i32 %77, 0
  %94 = zext i1 %93 to i8
  store i8 %94, i8* %29, align 1
  %95 = lshr i32 %77, 31
  %96 = trunc i32 %95 to i8
  store i8 %96, i8* %30, align 1
  %97 = lshr i32 %71, 31
  %98 = lshr i32 %76, 31
  %99 = xor i32 %95, %97
  %100 = xor i32 %95, %98
  %101 = add nuw nsw i32 %99, %100
  %102 = icmp eq i32 %101, 2
  %103 = zext i1 %102 to i8
  store i8 %103, i8* %31, align 1
  %104 = add i64 %69, 9
  store i64 %104, i64* %PC.i, align 8
  store i32 %77, i32* %75, align 4
  %105 = load i64, i64* %RBP.i, align 8
  %106 = add i64 %105, -12
  %107 = load i64, i64* %PC.i, align 8
  %108 = add i64 %107, 3
  store i64 %108, i64* %PC.i, align 8
  %109 = inttoptr i64 %106 to i32*
  %110 = load i32, i32* %109, align 4
  %111 = add i32 %110, 1
  %112 = zext i32 %111 to i64
  store i64 %112, i64* %RAX.i28, align 8
  %113 = icmp eq i32 %110, -1
  %114 = icmp eq i32 %111, 0
  %115 = or i1 %113, %114
  %116 = zext i1 %115 to i8
  store i8 %116, i8* %26, align 1
  %117 = and i32 %111, 255
  %118 = call i32 @llvm.ctpop.i32(i32 %117)
  %119 = trunc i32 %118 to i8
  %120 = and i8 %119, 1
  %121 = xor i8 %120, 1
  store i8 %121, i8* %27, align 1
  %122 = xor i32 %110, %111
  %123 = lshr i32 %122, 4
  %124 = trunc i32 %123 to i8
  %125 = and i8 %124, 1
  store i8 %125, i8* %28, align 1
  %126 = icmp eq i32 %111, 0
  %127 = zext i1 %126 to i8
  store i8 %127, i8* %29, align 1
  %128 = lshr i32 %111, 31
  %129 = trunc i32 %128 to i8
  store i8 %129, i8* %30, align 1
  %130 = lshr i32 %110, 31
  %131 = xor i32 %128, %130
  %132 = add nuw nsw i32 %131, %128
  %133 = icmp eq i32 %132, 2
  %134 = zext i1 %133 to i8
  store i8 %134, i8* %31, align 1
  %135 = add i64 %107, 9
  store i64 %135, i64* %PC.i, align 8
  store i32 %111, i32* %109, align 4
  %136 = load i64, i64* %PC.i, align 8
  %137 = add i64 %136, -30
  store i64 %137, i64* %3, align 8
  br label %block_.L_4004a5

block_.L_4004c8:                                  ; preds = %block_.L_4004a5
  %138 = add i64 %33, -8
  %139 = add i64 %69, 3
  store i64 %139, i64* %PC.i, align 8
  %140 = inttoptr i64 %138 to i32*
  %141 = load i32, i32* %140, align 4
  %142 = zext i32 %141 to i64
  store i64 %142, i64* %RAX.i28, align 8
  %143 = add i64 %69, 4
  store i64 %143, i64* %PC.i, align 8
  %144 = load i64, i64* %6, align 8
  %145 = add i64 %144, 8
  %146 = inttoptr i64 %144 to i64*
  %147 = load i64, i64* %146, align 8
  store i64 %147, i64* %RBP.i, align 8
  store i64 %145, i64* %6, align 8
  %148 = add i64 %69, 5
  store i64 %148, i64* %PC.i, align 8
  %149 = inttoptr i64 %145 to i64*
  %150 = load i64, i64* %149, align 8
  store i64 %150, i64* %3, align 8
  %151 = add i64 %144, 16
  store i64 %151, i64* %6, align 8
  ret %struct.Memory* %2
}

attributes #0 = { nounwind readnone }
attributes #1 = { alwaysinline }
