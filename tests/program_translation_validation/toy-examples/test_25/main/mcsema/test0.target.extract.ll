; ModuleID = 'mcsema/test0.target.opt.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"

%G__0x400674_type = type <{ [8 x i8] }>
%G__0x40067e_type = type <{ [8 x i8] }>
%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.anon = type { i64 }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque

@G__0x400674 = external global %G__0x400674_type
@G__0x40067e = external global %G__0x40067e_type

; Function Attrs: nounwind readnone
declare i32 @llvm.ctpop.i32(i32) #0

declare extern_weak x86_64_sysvcc i64 @printf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)

declare extern_weak x86_64_sysvcc i64 @strlen(i64)

declare %struct.Memory* @__remill_function_call(%struct.State* dereferenceable(3376), i64, %struct.Memory*) local_unnamed_addr

; Function Attrs: alwaysinline
define %struct.Memory* @main(%struct.State* noalias, i64, %struct.Memory* noalias) local_unnamed_addr #1 {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %PC.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP.i, align 8
  %5 = add i64 %1, 1
  store i64 %5, i64* %PC.i, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %4, i64* %9, align 8
  %RSP.i11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %10 = load i64, i64* %PC.i, align 8
  store i64 %8, i64* %RBP.i, align 8
  %11 = add i64 %7, -56
  store i64 %11, i64* %RSP.i11, align 8
  %12 = icmp ult i64 %8, 48
  %13 = zext i1 %12 to i8
  %14 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %13, i8* %14, align 1
  %15 = trunc i64 %11 to i32
  %16 = and i32 %15, 255
  %17 = call i32 @llvm.ctpop.i32(i32 %16)
  %18 = trunc i32 %17 to i8
  %19 = and i8 %18, 1
  %20 = xor i8 %19, 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %20, i8* %21, align 1
  %22 = xor i64 %8, 16
  %23 = xor i64 %22, %11
  %24 = lshr i64 %23, 4
  %25 = trunc i64 %24 to i8
  %26 = and i8 %25, 1
  %27 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %26, i8* %27, align 1
  %28 = icmp eq i64 %11, 0
  %29 = zext i1 %28 to i8
  %30 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %29, i8* %30, align 1
  %31 = lshr i64 %11, 63
  %32 = trunc i64 %31 to i8
  %33 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %32, i8* %33, align 1
  %34 = lshr i64 %8, 63
  %35 = xor i64 %31, %34
  %36 = add nuw nsw i64 %35, %34
  %37 = icmp eq i64 %36, 2
  %38 = zext i1 %37 to i8
  %39 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %38, i8* %39, align 1
  %40 = add i64 %7, -12
  %41 = add i64 %10, 14
  store i64 %41, i64* %PC.i, align 8
  %42 = inttoptr i64 %40 to i32*
  store i32 0, i32* %42, align 4
  %43 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0
  %EDI.i = bitcast %union.anon* %43 to i32*
  %44 = load i64, i64* %RBP.i, align 8
  %45 = add i64 %44, -8
  %46 = load i32, i32* %EDI.i, align 4
  %47 = load i64, i64* %PC.i, align 8
  %48 = add i64 %47, 3
  store i64 %48, i64* %PC.i, align 8
  %49 = inttoptr i64 %45 to i32*
  store i32 %46, i32* %49, align 4
  %50 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0
  %RSI.i87 = getelementptr inbounds %union.anon, %union.anon* %50, i64 0, i32 0
  %51 = load i64, i64* %RBP.i, align 8
  %52 = add i64 %51, -16
  %53 = load i64, i64* %RSI.i87, align 8
  %54 = load i64, i64* %PC.i, align 8
  %55 = add i64 %54, 4
  store i64 %55, i64* %PC.i, align 8
  %56 = inttoptr i64 %52 to i64*
  store i64 %53, i64* %56, align 8
  %57 = load i64, i64* %RBP.i, align 8
  %58 = add i64 %57, -20
  %59 = load i64, i64* %PC.i, align 8
  %60 = add i64 %59, 7
  store i64 %60, i64* %PC.i, align 8
  %61 = inttoptr i64 %58 to i32*
  store i32 0, i32* %61, align 4
  %62 = load i64, i64* %RBP.i, align 8
  %63 = add i64 %62, -24
  %64 = load i64, i64* %PC.i, align 8
  %65 = add i64 %64, 7
  store i64 %65, i64* %PC.i, align 8
  %66 = inttoptr i64 %63 to i32*
  store i32 0, i32* %66, align 4
  %67 = load i64, i64* %RBP.i, align 8
  %68 = add i64 %67, -16
  %69 = load i64, i64* %PC.i, align 8
  %70 = add i64 %69, 4
  store i64 %70, i64* %PC.i, align 8
  %71 = inttoptr i64 %68 to i64*
  %72 = load i64, i64* %71, align 8
  store i64 %72, i64* %RSI.i87, align 8
  %RDI.i78 = getelementptr inbounds %union.anon, %union.anon* %43, i64 0, i32 0
  %73 = add i64 %72, 8
  %74 = add i64 %69, 8
  store i64 %74, i64* %PC.i, align 8
  %75 = inttoptr i64 %73 to i64*
  %76 = load i64, i64* %75, align 8
  store i64 %76, i64* %RDI.i78, align 8
  %77 = add i64 %69, -308
  %78 = add i64 %69, 13
  %79 = load i64, i64* %6, align 8
  %80 = add i64 %79, -8
  %81 = inttoptr i64 %80 to i64*
  store i64 %78, i64* %81, align 8
  store i64 %80, i64* %6, align 8
  store i64 %77, i64* %3, align 8
  %82 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @strlen to i64), %struct.Memory* %2)
  %83 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %EAX.i71 = bitcast %union.anon* %83 to i32*
  %RCX.i72 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %84 = load i32, i32* %EAX.i71, align 4
  %85 = zext i32 %84 to i64
  %86 = load i64, i64* %PC.i, align 8
  store i64 %85, i64* %RCX.i72, align 8
  %87 = load i64, i64* %RBP.i, align 8
  %88 = add i64 %87, -28
  %89 = add i64 %86, 5
  store i64 %89, i64* %PC.i, align 8
  %90 = inttoptr i64 %88 to i32*
  store i32 %84, i32* %90, align 4
  %RAX.i65 = getelementptr inbounds %union.anon, %union.anon* %83, i64 0, i32 0
  %AL.i51 = bitcast %union.anon* %83 to i8*
  %.pre = load i64, i64* %PC.i, align 8
  br label %block_.L_400576

block_.L_400576:                                  ; preds = %block_400582, %entry
  %91 = phi i64 [ %236, %block_400582 ], [ %.pre, %entry ]
  %loadMem_4005c2 = phi %struct.Memory* [ %143, %block_400582 ], [ %82, %entry ]
  %92 = load i64, i64* %RBP.i, align 8
  %93 = add i64 %92, -24
  %94 = add i64 %91, 3
  store i64 %94, i64* %PC.i, align 8
  %95 = inttoptr i64 %93 to i32*
  %96 = load i32, i32* %95, align 4
  %97 = zext i32 %96 to i64
  store i64 %97, i64* %RAX.i65, align 8
  %98 = add i64 %92, -28
  %99 = add i64 %91, 6
  store i64 %99, i64* %PC.i, align 8
  %100 = inttoptr i64 %98 to i32*
  %101 = load i32, i32* %100, align 4
  %102 = sub i32 %96, %101
  %103 = icmp ult i32 %96, %101
  %104 = zext i1 %103 to i8
  store i8 %104, i8* %14, align 1
  %105 = and i32 %102, 255
  %106 = call i32 @llvm.ctpop.i32(i32 %105)
  %107 = trunc i32 %106 to i8
  %108 = and i8 %107, 1
  %109 = xor i8 %108, 1
  store i8 %109, i8* %21, align 1
  %110 = xor i32 %101, %96
  %111 = xor i32 %110, %102
  %112 = lshr i32 %111, 4
  %113 = trunc i32 %112 to i8
  %114 = and i8 %113, 1
  store i8 %114, i8* %27, align 1
  %115 = icmp eq i32 %102, 0
  %116 = zext i1 %115 to i8
  store i8 %116, i8* %30, align 1
  %117 = lshr i32 %102, 31
  %118 = trunc i32 %117 to i8
  store i8 %118, i8* %33, align 1
  %119 = lshr i32 %96, 31
  %120 = lshr i32 %101, 31
  %121 = xor i32 %120, %119
  %122 = xor i32 %117, %119
  %123 = add nuw nsw i32 %122, %121
  %124 = icmp eq i32 %123, 2
  %125 = zext i1 %124 to i8
  store i8 %125, i8* %39, align 1
  %126 = icmp ne i8 %118, 0
  %127 = xor i1 %126, %124
  %.v = select i1 %127, i64 12, i64 76
  %128 = add i64 %91, %.v
  %129 = add i64 %128, 10
  store i64 %129, i64* %PC.i, align 8
  br i1 %127, label %block_400582, label %block_.L_4005c2

block_400582:                                     ; preds = %block_.L_400576
  store i64 ptrtoint (%G__0x400674_type* @G__0x400674 to i64), i64* %RDI.i78, align 8
  %130 = add i64 %92, -16
  %131 = add i64 %128, 14
  store i64 %131, i64* %PC.i, align 8
  %132 = inttoptr i64 %130 to i64*
  %133 = load i64, i64* %132, align 8
  store i64 %133, i64* %RAX.i65, align 8
  %134 = add i64 %133, 8
  %135 = add i64 %128, 18
  store i64 %135, i64* %PC.i, align 8
  %136 = inttoptr i64 %134 to i64*
  %137 = load i64, i64* %136, align 8
  store i64 %137, i64* %RSI.i87, align 8
  store i8 0, i8* %AL.i51, align 1
  %138 = add i64 %128, -322
  %139 = add i64 %128, 25
  %140 = load i64, i64* %6, align 8
  %141 = add i64 %140, -8
  %142 = inttoptr i64 %141 to i64*
  store i64 %139, i64* %142, align 8
  store i64 %141, i64* %6, align 8
  store i64 %138, i64* %3, align 8
  %143 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @printf to i64), %struct.Memory* %loadMem_4005c2)
  %144 = load i64, i64* %RBP.i, align 8
  %145 = add i64 %144, -16
  %146 = load i64, i64* %PC.i, align 8
  %147 = add i64 %146, 4
  store i64 %147, i64* %PC.i, align 8
  %148 = inttoptr i64 %145 to i64*
  %149 = load i64, i64* %148, align 8
  store i64 %149, i64* %RSI.i87, align 8
  %150 = add i64 %149, 8
  %151 = add i64 %146, 8
  store i64 %151, i64* %PC.i, align 8
  %152 = inttoptr i64 %150 to i64*
  %153 = load i64, i64* %152, align 8
  store i64 %153, i64* %RSI.i87, align 8
  %154 = add i64 %144, -24
  %155 = add i64 %146, 12
  store i64 %155, i64* %PC.i, align 8
  %156 = inttoptr i64 %154 to i32*
  %157 = load i32, i32* %156, align 4
  %158 = sext i32 %157 to i64
  store i64 %158, i64* %RDI.i78, align 8
  %159 = add i64 %158, %153
  %160 = add i64 %146, 16
  store i64 %160, i64* %PC.i, align 8
  %161 = inttoptr i64 %159 to i8*
  %162 = load i8, i8* %161, align 1
  %163 = sext i8 %162 to i64
  %164 = and i64 %163, 4294967295
  store i64 %164, i64* %RCX.i72, align 8
  %165 = add i64 %144, -20
  %166 = add i64 %146, 19
  store i64 %166, i64* %PC.i, align 8
  %167 = sext i8 %162 to i32
  %168 = inttoptr i64 %165 to i32*
  %169 = load i32, i32* %168, align 4
  %170 = add i32 %169, %167
  %171 = zext i32 %170 to i64
  store i64 %171, i64* %RCX.i72, align 8
  %172 = icmp ult i32 %170, %167
  %173 = icmp ult i32 %170, %169
  %174 = or i1 %172, %173
  %175 = zext i1 %174 to i8
  store i8 %175, i8* %14, align 1
  %176 = and i32 %170, 255
  %177 = call i32 @llvm.ctpop.i32(i32 %176)
  %178 = trunc i32 %177 to i8
  %179 = and i8 %178, 1
  %180 = xor i8 %179, 1
  store i8 %180, i8* %21, align 1
  %181 = xor i32 %169, %167
  %182 = xor i32 %181, %170
  %183 = lshr i32 %182, 4
  %184 = trunc i32 %183 to i8
  %185 = and i8 %184, 1
  store i8 %185, i8* %27, align 1
  %186 = icmp eq i32 %170, 0
  %187 = zext i1 %186 to i8
  store i8 %187, i8* %30, align 1
  %188 = lshr i32 %170, 31
  %189 = trunc i32 %188 to i8
  store i8 %189, i8* %33, align 1
  %190 = lshr i32 %167, 31
  %191 = lshr i32 %169, 31
  %192 = xor i32 %188, %190
  %193 = xor i32 %188, %191
  %194 = add nuw nsw i32 %192, %193
  %195 = icmp eq i32 %194, 2
  %196 = zext i1 %195 to i8
  store i8 %196, i8* %39, align 1
  %197 = add i64 %146, 22
  store i64 %197, i64* %PC.i, align 8
  store i32 %170, i32* %168, align 4
  %198 = load i64, i64* %RBP.i, align 8
  %199 = add i64 %198, -32
  %200 = load i32, i32* %EAX.i71, align 4
  %201 = load i64, i64* %PC.i, align 8
  %202 = add i64 %201, 3
  store i64 %202, i64* %PC.i, align 8
  %203 = inttoptr i64 %199 to i32*
  store i32 %200, i32* %203, align 4
  %204 = load i64, i64* %RBP.i, align 8
  %205 = add i64 %204, -24
  %206 = load i64, i64* %PC.i, align 8
  %207 = add i64 %206, 3
  store i64 %207, i64* %PC.i, align 8
  %208 = inttoptr i64 %205 to i32*
  %209 = load i32, i32* %208, align 4
  %210 = add i32 %209, 1
  %211 = zext i32 %210 to i64
  store i64 %211, i64* %RAX.i65, align 8
  %212 = icmp eq i32 %209, -1
  %213 = icmp eq i32 %210, 0
  %214 = or i1 %212, %213
  %215 = zext i1 %214 to i8
  store i8 %215, i8* %14, align 1
  %216 = and i32 %210, 255
  %217 = call i32 @llvm.ctpop.i32(i32 %216)
  %218 = trunc i32 %217 to i8
  %219 = and i8 %218, 1
  %220 = xor i8 %219, 1
  store i8 %220, i8* %21, align 1
  %221 = xor i32 %209, %210
  %222 = lshr i32 %221, 4
  %223 = trunc i32 %222 to i8
  %224 = and i8 %223, 1
  store i8 %224, i8* %27, align 1
  %225 = icmp eq i32 %210, 0
  %226 = zext i1 %225 to i8
  store i8 %226, i8* %30, align 1
  %227 = lshr i32 %210, 31
  %228 = trunc i32 %227 to i8
  store i8 %228, i8* %33, align 1
  %229 = lshr i32 %209, 31
  %230 = xor i32 %227, %229
  %231 = add nuw nsw i32 %230, %227
  %232 = icmp eq i32 %231, 2
  %233 = zext i1 %232 to i8
  store i8 %233, i8* %39, align 1
  %234 = add i64 %206, 9
  store i64 %234, i64* %PC.i, align 8
  store i32 %210, i32* %208, align 4
  %235 = load i64, i64* %PC.i, align 8
  %236 = add i64 %235, -71
  store i64 %236, i64* %3, align 8
  br label %block_.L_400576

block_.L_4005c2:                                  ; preds = %block_.L_400576
  store i64 ptrtoint (%G__0x40067e_type* @G__0x40067e to i64), i64* %RDI.i78, align 8
  %237 = add i64 %128, 13
  store i64 %237, i64* %PC.i, align 8
  %238 = load i32, i32* %100, align 4
  %239 = zext i32 %238 to i64
  store i64 %239, i64* %RSI.i87, align 8
  store i8 0, i8* %AL.i51, align 1
  %240 = add i64 %128, -386
  %241 = add i64 %128, 20
  %242 = load i64, i64* %6, align 8
  %243 = add i64 %242, -8
  %244 = inttoptr i64 %243 to i64*
  store i64 %241, i64* %244, align 8
  store i64 %243, i64* %6, align 8
  store i64 %240, i64* %3, align 8
  %245 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @printf to i64), %struct.Memory* %loadMem_4005c2)
  %246 = load i64, i64* %RBP.i, align 8
  %247 = add i64 %246, -28
  %248 = load i64, i64* %PC.i, align 8
  %249 = add i64 %248, 3
  store i64 %249, i64* %PC.i, align 8
  %250 = inttoptr i64 %247 to i32*
  %251 = load i32, i32* %250, align 4
  %252 = zext i32 %251 to i64
  store i64 %252, i64* %RSI.i87, align 8
  %253 = add i64 %246, -36
  %254 = load i32, i32* %EAX.i71, align 4
  %255 = add i64 %248, 6
  store i64 %255, i64* %PC.i, align 8
  %256 = inttoptr i64 %253 to i32*
  store i32 %254, i32* %256, align 4
  %ESI.i = bitcast %union.anon* %50 to i32*
  %257 = load i32, i32* %ESI.i, align 4
  %258 = zext i32 %257 to i64
  %259 = load i64, i64* %PC.i, align 8
  store i64 %258, i64* %RAX.i65, align 8
  %260 = load i64, i64* %RSP.i11, align 8
  %261 = add i64 %260, 48
  store i64 %261, i64* %RSP.i11, align 8
  %262 = icmp ugt i64 %260, -49
  %263 = zext i1 %262 to i8
  store i8 %263, i8* %14, align 1
  %264 = trunc i64 %261 to i32
  %265 = and i32 %264, 255
  %266 = call i32 @llvm.ctpop.i32(i32 %265)
  %267 = trunc i32 %266 to i8
  %268 = and i8 %267, 1
  %269 = xor i8 %268, 1
  store i8 %269, i8* %21, align 1
  %270 = xor i64 %260, 16
  %271 = xor i64 %270, %261
  %272 = lshr i64 %271, 4
  %273 = trunc i64 %272 to i8
  %274 = and i8 %273, 1
  store i8 %274, i8* %27, align 1
  %275 = icmp eq i64 %261, 0
  %276 = zext i1 %275 to i8
  store i8 %276, i8* %30, align 1
  %277 = lshr i64 %261, 63
  %278 = trunc i64 %277 to i8
  store i8 %278, i8* %33, align 1
  %279 = lshr i64 %260, 63
  %280 = xor i64 %277, %279
  %281 = add nuw nsw i64 %280, %277
  %282 = icmp eq i64 %281, 2
  %283 = zext i1 %282 to i8
  store i8 %283, i8* %39, align 1
  %284 = add i64 %259, 7
  store i64 %284, i64* %PC.i, align 8
  %285 = add i64 %260, 56
  %286 = inttoptr i64 %261 to i64*
  %287 = load i64, i64* %286, align 8
  store i64 %287, i64* %RBP.i, align 8
  store i64 %285, i64* %6, align 8
  %288 = add i64 %259, 8
  store i64 %288, i64* %PC.i, align 8
  %289 = inttoptr i64 %285 to i64*
  %290 = load i64, i64* %289, align 8
  store i64 %290, i64* %3, align 8
  %291 = add i64 %260, 64
  store i64 %291, i64* %6, align 8
  ret %struct.Memory* %245
}

attributes #0 = { nounwind readnone }
attributes #1 = { alwaysinline }
