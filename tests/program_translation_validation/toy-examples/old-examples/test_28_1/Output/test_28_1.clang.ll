; ModuleID = 'Output/test_28_1.clang.bc'
source_filename = "Output/test_28_1.clang.bc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

module asm "  .globl sub_0;"
module asm "  .globl callback_sub_0;"
module asm "  .type callback_sub_0,@function"
module asm "callback_sub_0:"
module asm "  .cfi_startproc;"
module asm "  pushq %rax;"
module asm "  leaq sub_0@plt(%rip), %rax;"
module asm "  xchgq (%rsp), %rax;"
module asm "  jmp __mcsema_attach_call;"
module asm "0:"
module asm "  .size callback_sub_0,0b-callback_sub_0;"
module asm "  .cfi_endproc;"
module asm "  .globl printf;"
module asm "  .globl _printf;"
module asm "  .type _printf,@function"
module asm "_printf:"
module asm "  .cfi_startproc;"
module asm "  pushq %rax;"
module asm "  leaq printf@plt(%rip), %rax;"
module asm "  xchgq (%rsp), %rax;"
module asm "  jmp __mcsema_detach_call;"
module asm "0:"
module asm "  .size _printf,0b-_printf;"
module asm "  .cfi_endproc;"
module asm "  .globl sub_30;"
module asm "  .globl main;"
module asm "  .type main,@function"
module asm "main:"
module asm "  .cfi_startproc;"
module asm "  pushq %rax;"
module asm "  leaq sub_30(%rip), %rax;"
module asm "  xchgq (%rsp), %rax;"
module asm "  jmp __mcsema_attach_call;"
module asm "0:"
module asm "  .size main,0b-main;"
module asm "  .cfi_endproc;"

%0 = type <{ [13 x i8] }>
%1 = type <{ [32 x i8], i32, [24 x i8], i32, [16 x i8] }>
%RegState = type <{ i64, i64, i64, i64, i64, i64, i64, i64, i64, i8, i8, i8, i8, i8, i8, i8, [8 x x86_fp80], i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [8 x i8], i16, i64, i16, i64, i16, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i128, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64 }>

@data_0x51 = internal constant %0 <{ [13 x i8] c"Hello World!\00" }>, align 64
@data_0x60 = internal constant %1 <{ [32 x i8] c"\14\00\00\00\00\00\00\00\01zR\00\01x\10\01\1B\0C\07\08\90\01\00\00\18\00\00\00\1C\00\00\00", i32 ptrtoint (void ()* @callback_sub_0 to i32), [24 x i8] c"(\00\00\00\00A\0E\10\86\02C\0D\06\00\00\00\18\00\00\008\00\00\00", i32 ptrtoint (void ()* @callback_sub_0 to i32), [16 x i8] c"!\00\00\00\00A\0E\10\86\02C\0D\06\00\00\00" }>, align 64

; Function Attrs: naked
declare void @__mcsema_attach_call() #0

; Function Attrs: naked
declare void @__mcsema_attach_ret() #0

; Function Attrs: naked
declare void @__mcsema_detach_call() #0

; Function Attrs: naked
declare void @__mcsema_detach_call_value() #0

; Function Attrs: naked
declare void @__mcsema_detach_ret() #0

; Function Attrs: noinline
define x86_64_sysvcc void @sub_0(%RegState*) #1 {
entry:
  %XIP = getelementptr %RegState, %RegState* %0, i32 0, i32 0, !mcsema_real_eip !2
  %XAX = getelementptr %RegState, %RegState* %0, i32 0, i32 1, !mcsema_real_eip !2
  %XBX = getelementptr %RegState, %RegState* %0, i32 0, i32 2, !mcsema_real_eip !2
  %XCX = getelementptr %RegState, %RegState* %0, i32 0, i32 3, !mcsema_real_eip !2
  %XDX = getelementptr %RegState, %RegState* %0, i32 0, i32 4, !mcsema_real_eip !2
  %XSI = getelementptr %RegState, %RegState* %0, i32 0, i32 5, !mcsema_real_eip !2
  %XDI = getelementptr %RegState, %RegState* %0, i32 0, i32 6, !mcsema_real_eip !2
  %XBP = getelementptr %RegState, %RegState* %0, i32 0, i32 8, !mcsema_real_eip !2
  %XSP = getelementptr %RegState, %RegState* %0, i32 0, i32 7, !mcsema_real_eip !2
  %ZF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 12, !mcsema_real_eip !2
  %ZF = bitcast i8* %ZF_full to i1*, !mcsema_real_eip !2
  %PF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 10, !mcsema_real_eip !2
  %PF = bitcast i8* %PF_full to i1*, !mcsema_real_eip !2
  %AF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 11, !mcsema_real_eip !2
  %AF = bitcast i8* %AF_full to i1*, !mcsema_real_eip !2
  %CF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 9, !mcsema_real_eip !2
  %CF = bitcast i8* %CF_full to i1*, !mcsema_real_eip !2
  %SF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 13, !mcsema_real_eip !2
  %SF = bitcast i8* %SF_full to i1*, !mcsema_real_eip !2
  %OF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 14, !mcsema_real_eip !2
  %OF = bitcast i8* %OF_full to i1*, !mcsema_real_eip !2
  %DF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 15, !mcsema_real_eip !2
  %DF = bitcast i8* %DF_full to i1*, !mcsema_real_eip !2
  %STi = getelementptr %RegState, %RegState* %0, i32 0, i32 16, !mcsema_real_eip !2
  %FPU_FLAG_BUSY_full = getelementptr %RegState, %RegState* %0, i32 0, i32 17, !mcsema_real_eip !2
  %FPU_FLAG_BUSY = bitcast i8* %FPU_FLAG_BUSY_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_C3_full = getelementptr %RegState, %RegState* %0, i32 0, i32 18, !mcsema_real_eip !2
  %FPU_FLAG_C3 = bitcast i8* %FPU_FLAG_C3_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_TOP_full = getelementptr %RegState, %RegState* %0, i32 0, i32 19, !mcsema_real_eip !2
  %FPU_FLAG_TOP = bitcast i8* %FPU_FLAG_TOP_full to i3*, !mcsema_real_eip !2
  %FPU_FLAG_C2_full = getelementptr %RegState, %RegState* %0, i32 0, i32 20, !mcsema_real_eip !2
  %FPU_FLAG_C2 = bitcast i8* %FPU_FLAG_C2_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_C1_full = getelementptr %RegState, %RegState* %0, i32 0, i32 21, !mcsema_real_eip !2
  %FPU_FLAG_C1 = bitcast i8* %FPU_FLAG_C1_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_C0_full = getelementptr %RegState, %RegState* %0, i32 0, i32 22, !mcsema_real_eip !2
  %FPU_FLAG_C0 = bitcast i8* %FPU_FLAG_C0_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_ES_full = getelementptr %RegState, %RegState* %0, i32 0, i32 23, !mcsema_real_eip !2
  %FPU_FLAG_ES = bitcast i8* %FPU_FLAG_ES_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_SF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 24, !mcsema_real_eip !2
  %FPU_FLAG_SF = bitcast i8* %FPU_FLAG_SF_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_PE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 25, !mcsema_real_eip !2
  %FPU_FLAG_PE = bitcast i8* %FPU_FLAG_PE_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_UE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 26, !mcsema_real_eip !2
  %FPU_FLAG_UE = bitcast i8* %FPU_FLAG_UE_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_OE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 27, !mcsema_real_eip !2
  %FPU_FLAG_OE = bitcast i8* %FPU_FLAG_OE_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_ZE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 28, !mcsema_real_eip !2
  %FPU_FLAG_ZE = bitcast i8* %FPU_FLAG_ZE_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_DE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 29, !mcsema_real_eip !2
  %FPU_FLAG_DE = bitcast i8* %FPU_FLAG_DE_full to i1*, !mcsema_real_eip !2
  %FPU_FLAG_IE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 30, !mcsema_real_eip !2
  %FPU_FLAG_IE = bitcast i8* %FPU_FLAG_IE_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_X_full = getelementptr %RegState, %RegState* %0, i32 0, i32 31, !mcsema_real_eip !2
  %FPU_CONTROL_X = bitcast i8* %FPU_CONTROL_X_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_RC_full = getelementptr %RegState, %RegState* %0, i32 0, i32 32, !mcsema_real_eip !2
  %FPU_CONTROL_RC = bitcast i8* %FPU_CONTROL_RC_full to i2*, !mcsema_real_eip !2
  %FPU_CONTROL_PC_full = getelementptr %RegState, %RegState* %0, i32 0, i32 33, !mcsema_real_eip !2
  %FPU_CONTROL_PC = bitcast i8* %FPU_CONTROL_PC_full to i2*, !mcsema_real_eip !2
  %FPU_CONTROL_PM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 34, !mcsema_real_eip !2
  %FPU_CONTROL_PM = bitcast i8* %FPU_CONTROL_PM_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_UM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 35, !mcsema_real_eip !2
  %FPU_CONTROL_UM = bitcast i8* %FPU_CONTROL_UM_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_OM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 36, !mcsema_real_eip !2
  %FPU_CONTROL_OM = bitcast i8* %FPU_CONTROL_OM_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_ZM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 37, !mcsema_real_eip !2
  %FPU_CONTROL_ZM = bitcast i8* %FPU_CONTROL_ZM_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_DM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 38, !mcsema_real_eip !2
  %FPU_CONTROL_DM = bitcast i8* %FPU_CONTROL_DM_full to i1*, !mcsema_real_eip !2
  %FPU_CONTROL_IM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 39, !mcsema_real_eip !2
  %FPU_CONTROL_IM = bitcast i8* %FPU_CONTROL_IM_full to i1*, !mcsema_real_eip !2
  %FPU_TAG = getelementptr %RegState, %RegState* %0, i32 0, i32 40, !mcsema_real_eip !2
  %FPU_LASTIP_SEG = getelementptr %RegState, %RegState* %0, i32 0, i32 41, !mcsema_real_eip !2
  %FPU_LASTIP_OFF = getelementptr %RegState, %RegState* %0, i32 0, i32 42, !mcsema_real_eip !2
  %FPU_LASTDATA_SEG = getelementptr %RegState, %RegState* %0, i32 0, i32 43, !mcsema_real_eip !2
  %FPU_LASTDATA_OFF = getelementptr %RegState, %RegState* %0, i32 0, i32 44, !mcsema_real_eip !2
  %FPU_FOPCODE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 45, !mcsema_real_eip !2
  %FPU_FOPCODE = bitcast i16* %FPU_FOPCODE_full to i11*, !mcsema_real_eip !2
  %XMM0 = getelementptr %RegState, %RegState* %0, i32 0, i32 46, !mcsema_real_eip !2
  %XMM1 = getelementptr %RegState, %RegState* %0, i32 0, i32 47, !mcsema_real_eip !2
  %XMM2 = getelementptr %RegState, %RegState* %0, i32 0, i32 48, !mcsema_real_eip !2
  %XMM3 = getelementptr %RegState, %RegState* %0, i32 0, i32 49, !mcsema_real_eip !2
  %XMM4 = getelementptr %RegState, %RegState* %0, i32 0, i32 50, !mcsema_real_eip !2
  %XMM5 = getelementptr %RegState, %RegState* %0, i32 0, i32 51, !mcsema_real_eip !2
  %XMM6 = getelementptr %RegState, %RegState* %0, i32 0, i32 52, !mcsema_real_eip !2
  %XMM7 = getelementptr %RegState, %RegState* %0, i32 0, i32 53, !mcsema_real_eip !2
  %XMM8 = getelementptr %RegState, %RegState* %0, i32 0, i32 54, !mcsema_real_eip !2
  %XMM9 = getelementptr %RegState, %RegState* %0, i32 0, i32 55, !mcsema_real_eip !2
  %XMM10 = getelementptr %RegState, %RegState* %0, i32 0, i32 56, !mcsema_real_eip !2
  %XMM11 = getelementptr %RegState, %RegState* %0, i32 0, i32 57, !mcsema_real_eip !2
  %XMM12 = getelementptr %RegState, %RegState* %0, i32 0, i32 58, !mcsema_real_eip !2
  %XMM13 = getelementptr %RegState, %RegState* %0, i32 0, i32 59, !mcsema_real_eip !2
  %XMM14 = getelementptr %RegState, %RegState* %0, i32 0, i32 60, !mcsema_real_eip !2
  %XMM15 = getelementptr %RegState, %RegState* %0, i32 0, i32 61, !mcsema_real_eip !2
  %STACK_BASE = getelementptr %RegState, %RegState* %0, i32 0, i32 62, !mcsema_real_eip !2
  %STACK_LIMIT = getelementptr %RegState, %RegState* %0, i32 0, i32 63, !mcsema_real_eip !2
  %R8 = getelementptr %RegState, %RegState* %0, i32 0, i32 64, !mcsema_real_eip !2
  %R9 = getelementptr %RegState, %RegState* %0, i32 0, i32 65, !mcsema_real_eip !2
  %R10 = getelementptr %RegState, %RegState* %0, i32 0, i32 66, !mcsema_real_eip !2
  %R11 = getelementptr %RegState, %RegState* %0, i32 0, i32 67, !mcsema_real_eip !2
  %R12 = getelementptr %RegState, %RegState* %0, i32 0, i32 68, !mcsema_real_eip !2
  %R13 = getelementptr %RegState, %RegState* %0, i32 0, i32 69, !mcsema_real_eip !2
  %R14 = getelementptr %RegState, %RegState* %0, i32 0, i32 70, !mcsema_real_eip !2
  %R15 = getelementptr %RegState, %RegState* %0, i32 0, i32 71, !mcsema_real_eip !2
  br label %block_0x0, !mcsema_real_eip !2

block_0x0:                                        ; preds = %entry
  %RBP_val.0 = load i64, i64* %XBP, !mcsema_real_eip !2
  %RSP_val.1 = load i64, i64* %XSP, !mcsema_real_eip !2
  %1 = sub i64 %RSP_val.1, 8, !mcsema_real_eip !2
  %2 = inttoptr i64 %1 to i64*, !mcsema_real_eip !2
  store i64 %RBP_val.0, i64* %2, !mcsema_real_eip !2
  store i64 %1, i64* %XSP, !mcsema_real_eip !2
  %RSP_val.2 = load i64, i64* %XSP, !mcsema_real_eip !3
  store i64 %RSP_val.2, i64* %XBP, !mcsema_real_eip !3
  %RSP_val.3 = load i64, i64* %XSP, !mcsema_real_eip !4
  %3 = sext i8 16 to i64, !mcsema_real_eip !4
  %4 = sub i64 %RSP_val.3, %3, !mcsema_real_eip !4
  %5 = xor i64 %4, %RSP_val.3, !mcsema_real_eip !4
  %6 = xor i64 %5, %3, !mcsema_real_eip !4
  %7 = and i64 %6, 16, !mcsema_real_eip !4
  %8 = icmp ne i64 %7, 0, !mcsema_real_eip !4
  store i1 %8, i1* %AF, !mcsema_real_eip !4
  %9 = trunc i64 %4 to i8, !mcsema_real_eip !4
  %10 = call i8 @llvm.ctpop.i8(i8 %9), !mcsema_real_eip !4
  %11 = trunc i8 %10 to i1, !mcsema_real_eip !4
  %12 = xor i1 %11, true, !mcsema_real_eip !4
  store i1 %12, i1* %PF, !mcsema_real_eip !4
  %13 = icmp eq i64 %4, 0, !mcsema_real_eip !4
  store i1 %13, i1* %ZF, !mcsema_real_eip !4
  %14 = lshr i64 %4, 63, !mcsema_real_eip !4
  %15 = trunc i64 %14 to i1, !mcsema_real_eip !4
  store i1 %15, i1* %SF, !mcsema_real_eip !4
  %16 = icmp ult i64 %RSP_val.3, %3, !mcsema_real_eip !4
  store i1 %16, i1* %CF, !mcsema_real_eip !4
  %17 = xor i64 %RSP_val.3, %3, !mcsema_real_eip !4
  %18 = xor i64 %RSP_val.3, %4, !mcsema_real_eip !4
  %19 = and i64 %17, %18, !mcsema_real_eip !4
  %20 = lshr i64 %19, 63, !mcsema_real_eip !4
  %21 = trunc i64 %20 to i1, !mcsema_real_eip !4
  store i1 %21, i1* %OF, !mcsema_real_eip !4
  store i64 %4, i64* %XSP, !mcsema_real_eip !4
  %22 = getelementptr %0, %0* @data_0x51, i64 0, i32 0, !mcsema_real_eip !5
  %23 = ptrtoint [13 x i8]* %22 to i64, !mcsema_real_eip !5
  %24 = add i64 %23, 0, !mcsema_real_eip !5
  store i64 %24, i64* %XAX, !mcsema_real_eip !5
  %RBP_val.4 = load i64, i64* %XBP, !mcsema_real_eip !6
  %25 = add i64 %RBP_val.4, -4, !mcsema_real_eip !6
  %26 = inttoptr i64 %25 to i64*, !mcsema_real_eip !6
  %EDI.5 = bitcast i64* %XDI to i32*, !mcsema_real_eip !6
  %EDI_val.6 = load i32, i32* %EDI.5, !mcsema_real_eip !6
  %27 = ptrtoint i64* %26 to i64, !mcsema_real_eip !6
  %28 = inttoptr i64 %27 to i32*, !mcsema_real_eip !6
  store i32 %EDI_val.6, i32* %28, !mcsema_real_eip !6
  %RAX_val.7 = load i64, i64* %XAX, !mcsema_real_eip !7
  store i64 %RAX_val.7, i64* %XDI, !mcsema_real_eip !7
  %AL.8 = bitcast i64* %XAX to i8*, !mcsema_real_eip !8
  store i8 0, i8* %AL.8, !mcsema_real_eip !8
  %RDI_val.9 = load i64, i64* %XDI, !mcsema_real_eip !9
  %RSI_val.10 = load i64, i64* %XSI, !mcsema_real_eip !9
  %RDX_val.11 = load i64, i64* %XDX, !mcsema_real_eip !9
  %RCX_val.12 = load i64, i64* %XCX, !mcsema_real_eip !9
  %R8_val.13 = load i64, i64* %R8, !mcsema_real_eip !9
  %R9_val.14 = load i64, i64* %R9, !mcsema_real_eip !9
  %RSP_val.15 = load i64, i64* %XSP, !mcsema_real_eip !9
  %29 = inttoptr i64 %RSP_val.15 to i64*, !mcsema_real_eip !9
  %30 = load i64, i64* %29, !mcsema_real_eip !9
  %31 = add i64 %RSP_val.15, 8, !mcsema_real_eip !9
  %32 = inttoptr i64 %31 to i64*, !mcsema_real_eip !9
  %33 = load i64, i64* %32, !mcsema_real_eip !9
  %34 = add i64 %31, 8, !mcsema_real_eip !9
  %35 = inttoptr i64 %34 to i64*, !mcsema_real_eip !9
  %36 = load i64, i64* %35, !mcsema_real_eip !9
  %37 = add i64 %34, 8, !mcsema_real_eip !9
  %38 = inttoptr i64 %37 to i64*, !mcsema_real_eip !9
  %39 = load i64, i64* %38, !mcsema_real_eip !9
  %40 = add i64 %37, 8, !mcsema_real_eip !9
  %41 = inttoptr i64 %40 to i64*, !mcsema_real_eip !9
  %42 = load i64, i64* %41, !mcsema_real_eip !9
  %43 = add i64 %40, 8, !mcsema_real_eip !9
  %44 = inttoptr i64 %43 to i64*, !mcsema_real_eip !9
  %45 = load i64, i64* %44, !mcsema_real_eip !9
  %46 = add i64 %43, 8, !mcsema_real_eip !9
  %47 = inttoptr i64 %46 to i64*, !mcsema_real_eip !9
  %48 = load i64, i64* %47, !mcsema_real_eip !9
  %49 = add i64 %46, 8, !mcsema_real_eip !9
  %50 = inttoptr i64 %49 to i64*, !mcsema_real_eip !9
  %51 = load i64, i64* %50, !mcsema_real_eip !9
  %52 = add i64 %49, 8, !mcsema_real_eip !9
  %53 = inttoptr i64 %52 to i64*, !mcsema_real_eip !9
  %54 = load i64, i64* %53, !mcsema_real_eip !9
  %55 = add i64 %52, 8, !mcsema_real_eip !9
  %56 = inttoptr i64 %55 to i64*, !mcsema_real_eip !9
  %57 = load i64, i64* %56, !mcsema_real_eip !9
  %RSP_val.16 = load i64, i64* %XSP, !mcsema_real_eip !9
  %58 = sub i64 %RSP_val.16, 8, !mcsema_real_eip !9
  %59 = inttoptr i64 %58 to i64*, !mcsema_real_eip !9
  store i64 -2415393069852865332, i64* %59, !mcsema_real_eip !9
  store i64 %58, i64* %XSP, !mcsema_real_eip !9
  %60 = call x86_64_sysvcc i64 @_printf(i64 %RDI_val.9, i64 %RSI_val.10, i64 %RDX_val.11, i64 %RCX_val.12, i64 %R8_val.13, i64 %R9_val.14, i64 %30, i64 %33, i64 %36, i64 %39, i64 %42, i64 %45, i64 %48, i64 %51, i64 %54, i64 %57), !mcsema_real_eip !9
  store i64 %60, i64* %XAX, !mcsema_real_eip !9
  %RBP_val.17 = load i64, i64* %XBP, !mcsema_real_eip !10
  %61 = add i64 %RBP_val.17, -8, !mcsema_real_eip !10
  %62 = inttoptr i64 %61 to i64*, !mcsema_real_eip !10
  %EAX.18 = bitcast i64* %XAX to i32*, !mcsema_real_eip !10
  %EAX_val.19 = load i32, i32* %EAX.18, !mcsema_real_eip !10
  %63 = ptrtoint i64* %62 to i64, !mcsema_real_eip !10
  %64 = inttoptr i64 %63 to i32*, !mcsema_real_eip !10
  store i32 %EAX_val.19, i32* %64, !mcsema_real_eip !10
  %RSP_val.20 = load i64, i64* %XSP, !mcsema_real_eip !11
  %65 = add i64 16, %RSP_val.20, !mcsema_real_eip !11
  %66 = xor i64 %65, %RSP_val.20, !mcsema_real_eip !11
  %67 = xor i64 %66, 16, !mcsema_real_eip !11
  %68 = and i64 %67, 16, !mcsema_real_eip !11
  %69 = icmp ne i64 %68, 0, !mcsema_real_eip !11
  store i1 %69, i1* %AF, !mcsema_real_eip !11
  %70 = lshr i64 %65, 63, !mcsema_real_eip !11
  %71 = trunc i64 %70 to i1, !mcsema_real_eip !11
  store i1 %71, i1* %SF, !mcsema_real_eip !11
  %72 = icmp eq i64 %65, 0, !mcsema_real_eip !11
  store i1 %72, i1* %ZF, !mcsema_real_eip !11
  %73 = xor i64 %RSP_val.20, 16, !mcsema_real_eip !11
  %74 = xor i64 %73, -1, !mcsema_real_eip !11
  %75 = xor i64 %RSP_val.20, %65, !mcsema_real_eip !11
  %76 = and i64 %74, %75, !mcsema_real_eip !11
  %77 = lshr i64 %76, 63, !mcsema_real_eip !11
  %78 = and i64 %77, 1, !mcsema_real_eip !11
  %79 = trunc i64 %78 to i1, !mcsema_real_eip !11
  store i1 %79, i1* %OF, !mcsema_real_eip !11
  %80 = trunc i64 %65 to i8, !mcsema_real_eip !11
  %81 = call i8 @llvm.ctpop.i8(i8 %80), !mcsema_real_eip !11
  %82 = trunc i8 %81 to i1, !mcsema_real_eip !11
  %83 = xor i1 %82, true, !mcsema_real_eip !11
  store i1 %83, i1* %PF, !mcsema_real_eip !11
  %84 = icmp ult i64 %65, %RSP_val.20, !mcsema_real_eip !11
  store i1 %84, i1* %CF, !mcsema_real_eip !11
  store i64 %65, i64* %XSP, !mcsema_real_eip !11
  %RSP_val.21 = load i64, i64* %XSP, !mcsema_real_eip !12
  %85 = inttoptr i64 %RSP_val.21 to i64*, !mcsema_real_eip !12
  %86 = load i64, i64* %85, !mcsema_real_eip !12
  store i64 %86, i64* %XBP, !mcsema_real_eip !12
  %87 = add i64 %RSP_val.21, 8, !mcsema_real_eip !12
  store i64 %87, i64* %XSP, !mcsema_real_eip !12
  %RSP_val.22 = load i64, i64* %XSP, !mcsema_real_eip !13
  %88 = add i64 %RSP_val.22, 8, !mcsema_real_eip !13
  %89 = inttoptr i64 %RSP_val.22 to i64*, !mcsema_real_eip !13
  %90 = load i64, i64* %89, !mcsema_real_eip !13
  store i64 %90, i64* %XIP, !mcsema_real_eip !13
  store i64 %88, i64* %XSP, !mcsema_real_eip !13
  ret void, !mcsema_real_eip !13
}

; Function Attrs: noinline
define x86_64_sysvcc void @sub_30(%RegState*) #1 {
entry:
  %XIP = getelementptr %RegState, %RegState* %0, i32 0, i32 0, !mcsema_real_eip !14
  %XAX = getelementptr %RegState, %RegState* %0, i32 0, i32 1, !mcsema_real_eip !14
  %XBX = getelementptr %RegState, %RegState* %0, i32 0, i32 2, !mcsema_real_eip !14
  %XCX = getelementptr %RegState, %RegState* %0, i32 0, i32 3, !mcsema_real_eip !14
  %XDX = getelementptr %RegState, %RegState* %0, i32 0, i32 4, !mcsema_real_eip !14
  %XSI = getelementptr %RegState, %RegState* %0, i32 0, i32 5, !mcsema_real_eip !14
  %XDI = getelementptr %RegState, %RegState* %0, i32 0, i32 6, !mcsema_real_eip !14
  %XBP = getelementptr %RegState, %RegState* %0, i32 0, i32 8, !mcsema_real_eip !14
  %XSP = getelementptr %RegState, %RegState* %0, i32 0, i32 7, !mcsema_real_eip !14
  %ZF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 12, !mcsema_real_eip !14
  %ZF = bitcast i8* %ZF_full to i1*, !mcsema_real_eip !14
  %PF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 10, !mcsema_real_eip !14
  %PF = bitcast i8* %PF_full to i1*, !mcsema_real_eip !14
  %AF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 11, !mcsema_real_eip !14
  %AF = bitcast i8* %AF_full to i1*, !mcsema_real_eip !14
  %CF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 9, !mcsema_real_eip !14
  %CF = bitcast i8* %CF_full to i1*, !mcsema_real_eip !14
  %SF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 13, !mcsema_real_eip !14
  %SF = bitcast i8* %SF_full to i1*, !mcsema_real_eip !14
  %OF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 14, !mcsema_real_eip !14
  %OF = bitcast i8* %OF_full to i1*, !mcsema_real_eip !14
  %DF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 15, !mcsema_real_eip !14
  %DF = bitcast i8* %DF_full to i1*, !mcsema_real_eip !14
  %STi = getelementptr %RegState, %RegState* %0, i32 0, i32 16, !mcsema_real_eip !14
  %FPU_FLAG_BUSY_full = getelementptr %RegState, %RegState* %0, i32 0, i32 17, !mcsema_real_eip !14
  %FPU_FLAG_BUSY = bitcast i8* %FPU_FLAG_BUSY_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_C3_full = getelementptr %RegState, %RegState* %0, i32 0, i32 18, !mcsema_real_eip !14
  %FPU_FLAG_C3 = bitcast i8* %FPU_FLAG_C3_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_TOP_full = getelementptr %RegState, %RegState* %0, i32 0, i32 19, !mcsema_real_eip !14
  %FPU_FLAG_TOP = bitcast i8* %FPU_FLAG_TOP_full to i3*, !mcsema_real_eip !14
  %FPU_FLAG_C2_full = getelementptr %RegState, %RegState* %0, i32 0, i32 20, !mcsema_real_eip !14
  %FPU_FLAG_C2 = bitcast i8* %FPU_FLAG_C2_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_C1_full = getelementptr %RegState, %RegState* %0, i32 0, i32 21, !mcsema_real_eip !14
  %FPU_FLAG_C1 = bitcast i8* %FPU_FLAG_C1_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_C0_full = getelementptr %RegState, %RegState* %0, i32 0, i32 22, !mcsema_real_eip !14
  %FPU_FLAG_C0 = bitcast i8* %FPU_FLAG_C0_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_ES_full = getelementptr %RegState, %RegState* %0, i32 0, i32 23, !mcsema_real_eip !14
  %FPU_FLAG_ES = bitcast i8* %FPU_FLAG_ES_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_SF_full = getelementptr %RegState, %RegState* %0, i32 0, i32 24, !mcsema_real_eip !14
  %FPU_FLAG_SF = bitcast i8* %FPU_FLAG_SF_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_PE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 25, !mcsema_real_eip !14
  %FPU_FLAG_PE = bitcast i8* %FPU_FLAG_PE_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_UE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 26, !mcsema_real_eip !14
  %FPU_FLAG_UE = bitcast i8* %FPU_FLAG_UE_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_OE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 27, !mcsema_real_eip !14
  %FPU_FLAG_OE = bitcast i8* %FPU_FLAG_OE_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_ZE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 28, !mcsema_real_eip !14
  %FPU_FLAG_ZE = bitcast i8* %FPU_FLAG_ZE_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_DE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 29, !mcsema_real_eip !14
  %FPU_FLAG_DE = bitcast i8* %FPU_FLAG_DE_full to i1*, !mcsema_real_eip !14
  %FPU_FLAG_IE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 30, !mcsema_real_eip !14
  %FPU_FLAG_IE = bitcast i8* %FPU_FLAG_IE_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_X_full = getelementptr %RegState, %RegState* %0, i32 0, i32 31, !mcsema_real_eip !14
  %FPU_CONTROL_X = bitcast i8* %FPU_CONTROL_X_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_RC_full = getelementptr %RegState, %RegState* %0, i32 0, i32 32, !mcsema_real_eip !14
  %FPU_CONTROL_RC = bitcast i8* %FPU_CONTROL_RC_full to i2*, !mcsema_real_eip !14
  %FPU_CONTROL_PC_full = getelementptr %RegState, %RegState* %0, i32 0, i32 33, !mcsema_real_eip !14
  %FPU_CONTROL_PC = bitcast i8* %FPU_CONTROL_PC_full to i2*, !mcsema_real_eip !14
  %FPU_CONTROL_PM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 34, !mcsema_real_eip !14
  %FPU_CONTROL_PM = bitcast i8* %FPU_CONTROL_PM_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_UM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 35, !mcsema_real_eip !14
  %FPU_CONTROL_UM = bitcast i8* %FPU_CONTROL_UM_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_OM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 36, !mcsema_real_eip !14
  %FPU_CONTROL_OM = bitcast i8* %FPU_CONTROL_OM_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_ZM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 37, !mcsema_real_eip !14
  %FPU_CONTROL_ZM = bitcast i8* %FPU_CONTROL_ZM_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_DM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 38, !mcsema_real_eip !14
  %FPU_CONTROL_DM = bitcast i8* %FPU_CONTROL_DM_full to i1*, !mcsema_real_eip !14
  %FPU_CONTROL_IM_full = getelementptr %RegState, %RegState* %0, i32 0, i32 39, !mcsema_real_eip !14
  %FPU_CONTROL_IM = bitcast i8* %FPU_CONTROL_IM_full to i1*, !mcsema_real_eip !14
  %FPU_TAG = getelementptr %RegState, %RegState* %0, i32 0, i32 40, !mcsema_real_eip !14
  %FPU_LASTIP_SEG = getelementptr %RegState, %RegState* %0, i32 0, i32 41, !mcsema_real_eip !14
  %FPU_LASTIP_OFF = getelementptr %RegState, %RegState* %0, i32 0, i32 42, !mcsema_real_eip !14
  %FPU_LASTDATA_SEG = getelementptr %RegState, %RegState* %0, i32 0, i32 43, !mcsema_real_eip !14
  %FPU_LASTDATA_OFF = getelementptr %RegState, %RegState* %0, i32 0, i32 44, !mcsema_real_eip !14
  %FPU_FOPCODE_full = getelementptr %RegState, %RegState* %0, i32 0, i32 45, !mcsema_real_eip !14
  %FPU_FOPCODE = bitcast i16* %FPU_FOPCODE_full to i11*, !mcsema_real_eip !14
  %XMM0 = getelementptr %RegState, %RegState* %0, i32 0, i32 46, !mcsema_real_eip !14
  %XMM1 = getelementptr %RegState, %RegState* %0, i32 0, i32 47, !mcsema_real_eip !14
  %XMM2 = getelementptr %RegState, %RegState* %0, i32 0, i32 48, !mcsema_real_eip !14
  %XMM3 = getelementptr %RegState, %RegState* %0, i32 0, i32 49, !mcsema_real_eip !14
  %XMM4 = getelementptr %RegState, %RegState* %0, i32 0, i32 50, !mcsema_real_eip !14
  %XMM5 = getelementptr %RegState, %RegState* %0, i32 0, i32 51, !mcsema_real_eip !14
  %XMM6 = getelementptr %RegState, %RegState* %0, i32 0, i32 52, !mcsema_real_eip !14
  %XMM7 = getelementptr %RegState, %RegState* %0, i32 0, i32 53, !mcsema_real_eip !14
  %XMM8 = getelementptr %RegState, %RegState* %0, i32 0, i32 54, !mcsema_real_eip !14
  %XMM9 = getelementptr %RegState, %RegState* %0, i32 0, i32 55, !mcsema_real_eip !14
  %XMM10 = getelementptr %RegState, %RegState* %0, i32 0, i32 56, !mcsema_real_eip !14
  %XMM11 = getelementptr %RegState, %RegState* %0, i32 0, i32 57, !mcsema_real_eip !14
  %XMM12 = getelementptr %RegState, %RegState* %0, i32 0, i32 58, !mcsema_real_eip !14
  %XMM13 = getelementptr %RegState, %RegState* %0, i32 0, i32 59, !mcsema_real_eip !14
  %XMM14 = getelementptr %RegState, %RegState* %0, i32 0, i32 60, !mcsema_real_eip !14
  %XMM15 = getelementptr %RegState, %RegState* %0, i32 0, i32 61, !mcsema_real_eip !14
  %STACK_BASE = getelementptr %RegState, %RegState* %0, i32 0, i32 62, !mcsema_real_eip !14
  %STACK_LIMIT = getelementptr %RegState, %RegState* %0, i32 0, i32 63, !mcsema_real_eip !14
  %R8 = getelementptr %RegState, %RegState* %0, i32 0, i32 64, !mcsema_real_eip !14
  %R9 = getelementptr %RegState, %RegState* %0, i32 0, i32 65, !mcsema_real_eip !14
  %R10 = getelementptr %RegState, %RegState* %0, i32 0, i32 66, !mcsema_real_eip !14
  %R11 = getelementptr %RegState, %RegState* %0, i32 0, i32 67, !mcsema_real_eip !14
  %R12 = getelementptr %RegState, %RegState* %0, i32 0, i32 68, !mcsema_real_eip !14
  %R13 = getelementptr %RegState, %RegState* %0, i32 0, i32 69, !mcsema_real_eip !14
  %R14 = getelementptr %RegState, %RegState* %0, i32 0, i32 70, !mcsema_real_eip !14
  %R15 = getelementptr %RegState, %RegState* %0, i32 0, i32 71, !mcsema_real_eip !14
  br label %block_0x30, !mcsema_real_eip !14

block_0x30:                                       ; preds = %entry
  %RBP_val.23 = load i64, i64* %XBP, !mcsema_real_eip !14
  %RSP_val.24 = load i64, i64* %XSP, !mcsema_real_eip !14
  %1 = sub i64 %RSP_val.24, 8, !mcsema_real_eip !14
  %2 = inttoptr i64 %1 to i64*, !mcsema_real_eip !14
  store i64 %RBP_val.23, i64* %2, !mcsema_real_eip !14
  store i64 %1, i64* %XSP, !mcsema_real_eip !14
  %RSP_val.25 = load i64, i64* %XSP, !mcsema_real_eip !15
  store i64 %RSP_val.25, i64* %XBP, !mcsema_real_eip !15
  %RSP_val.26 = load i64, i64* %XSP, !mcsema_real_eip !16
  %3 = sext i8 16 to i64, !mcsema_real_eip !16
  %4 = sub i64 %RSP_val.26, %3, !mcsema_real_eip !16
  %5 = xor i64 %4, %RSP_val.26, !mcsema_real_eip !16
  %6 = xor i64 %5, %3, !mcsema_real_eip !16
  %7 = and i64 %6, 16, !mcsema_real_eip !16
  %8 = icmp ne i64 %7, 0, !mcsema_real_eip !16
  store i1 %8, i1* %AF, !mcsema_real_eip !16
  %9 = trunc i64 %4 to i8, !mcsema_real_eip !16
  %10 = call i8 @llvm.ctpop.i8(i8 %9), !mcsema_real_eip !16
  %11 = trunc i8 %10 to i1, !mcsema_real_eip !16
  %12 = xor i1 %11, true, !mcsema_real_eip !16
  store i1 %12, i1* %PF, !mcsema_real_eip !16
  %13 = icmp eq i64 %4, 0, !mcsema_real_eip !16
  store i1 %13, i1* %ZF, !mcsema_real_eip !16
  %14 = lshr i64 %4, 63, !mcsema_real_eip !16
  %15 = trunc i64 %14 to i1, !mcsema_real_eip !16
  store i1 %15, i1* %SF, !mcsema_real_eip !16
  %16 = icmp ult i64 %RSP_val.26, %3, !mcsema_real_eip !16
  store i1 %16, i1* %CF, !mcsema_real_eip !16
  %17 = xor i64 %RSP_val.26, %3, !mcsema_real_eip !16
  %18 = xor i64 %RSP_val.26, %4, !mcsema_real_eip !16
  %19 = and i64 %17, %18, !mcsema_real_eip !16
  %20 = lshr i64 %19, 63, !mcsema_real_eip !16
  %21 = trunc i64 %20 to i1, !mcsema_real_eip !16
  store i1 %21, i1* %OF, !mcsema_real_eip !16
  store i64 %4, i64* %XSP, !mcsema_real_eip !16
  %22 = zext i32 2 to i64, !mcsema_real_eip !17
  store i64 %22, i64* %XDI, !mcsema_real_eip !17
  %RBP_val.27 = load i64, i64* %XBP, !mcsema_real_eip !18
  %23 = add i64 %RBP_val.27, -4, !mcsema_real_eip !18
  %24 = inttoptr i64 %23 to i64*, !mcsema_real_eip !18
  %25 = ptrtoint i64* %24 to i64, !mcsema_real_eip !18
  %26 = inttoptr i64 %25 to i32*, !mcsema_real_eip !18
  store i32 0, i32* %26, !mcsema_real_eip !18
  %RSP_val.28 = load i64, i64* %XSP, !mcsema_real_eip !19
  %27 = sub i64 %RSP_val.28, 8, !mcsema_real_eip !19
  %28 = inttoptr i64 %27 to i64*, !mcsema_real_eip !19
  store i64 -4981261766360305936, i64* %28, !mcsema_real_eip !19
  store i64 %27, i64* %XSP, !mcsema_real_eip !19
  call x86_64_sysvcc void @sub_0(%RegState* %0), !mcsema_real_eip !19
  %EAX.29 = bitcast i64* %XAX to i32*, !mcsema_real_eip !20
  %EAX_val.30 = load i32, i32* %EAX.29, !mcsema_real_eip !20
  %EAX.31 = bitcast i64* %XAX to i32*, !mcsema_real_eip !20
  %EAX_val.32 = load i32, i32* %EAX.31, !mcsema_real_eip !20
  %29 = xor i32 %EAX_val.30, %EAX_val.32, !mcsema_real_eip !20
  store i1 false, i1* %CF, !mcsema_real_eip !20
  store i1 false, i1* %OF, !mcsema_real_eip !20
  %30 = lshr i32 %29, 31, !mcsema_real_eip !20
  %31 = trunc i32 %30 to i1, !mcsema_real_eip !20
  store i1 %31, i1* %SF, !mcsema_real_eip !20
  %32 = icmp eq i32 %29, 0, !mcsema_real_eip !20
  store i1 %32, i1* %ZF, !mcsema_real_eip !20
  %33 = trunc i32 %29 to i8, !mcsema_real_eip !20
  %34 = call i8 @llvm.ctpop.i8(i8 %33), !mcsema_real_eip !20
  %35 = trunc i8 %34 to i1, !mcsema_real_eip !20
  %36 = xor i1 %35, true, !mcsema_real_eip !20
  store i1 %36, i1* %PF, !mcsema_real_eip !20
  store i1 undef, i1* %AF, !mcsema_real_eip !20
  %37 = zext i32 %29 to i64, !mcsema_real_eip !20
  store i64 %37, i64* %XAX, !mcsema_real_eip !20
  %RSP_val.33 = load i64, i64* %XSP, !mcsema_real_eip !21
  %38 = add i64 16, %RSP_val.33, !mcsema_real_eip !21
  %39 = xor i64 %38, %RSP_val.33, !mcsema_real_eip !21
  %40 = xor i64 %39, 16, !mcsema_real_eip !21
  %41 = and i64 %40, 16, !mcsema_real_eip !21
  %42 = icmp ne i64 %41, 0, !mcsema_real_eip !21
  store i1 %42, i1* %AF, !mcsema_real_eip !21
  %43 = lshr i64 %38, 63, !mcsema_real_eip !21
  %44 = trunc i64 %43 to i1, !mcsema_real_eip !21
  store i1 %44, i1* %SF, !mcsema_real_eip !21
  %45 = icmp eq i64 %38, 0, !mcsema_real_eip !21
  store i1 %45, i1* %ZF, !mcsema_real_eip !21
  %46 = xor i64 %RSP_val.33, 16, !mcsema_real_eip !21
  %47 = xor i64 %46, -1, !mcsema_real_eip !21
  %48 = xor i64 %RSP_val.33, %38, !mcsema_real_eip !21
  %49 = and i64 %47, %48, !mcsema_real_eip !21
  %50 = lshr i64 %49, 63, !mcsema_real_eip !21
  %51 = and i64 %50, 1, !mcsema_real_eip !21
  %52 = trunc i64 %51 to i1, !mcsema_real_eip !21
  store i1 %52, i1* %OF, !mcsema_real_eip !21
  %53 = trunc i64 %38 to i8, !mcsema_real_eip !21
  %54 = call i8 @llvm.ctpop.i8(i8 %53), !mcsema_real_eip !21
  %55 = trunc i8 %54 to i1, !mcsema_real_eip !21
  %56 = xor i1 %55, true, !mcsema_real_eip !21
  store i1 %56, i1* %PF, !mcsema_real_eip !21
  %57 = icmp ult i64 %38, %RSP_val.33, !mcsema_real_eip !21
  store i1 %57, i1* %CF, !mcsema_real_eip !21
  store i64 %38, i64* %XSP, !mcsema_real_eip !21
  %RSP_val.34 = load i64, i64* %XSP, !mcsema_real_eip !22
  %58 = inttoptr i64 %RSP_val.34 to i64*, !mcsema_real_eip !22
  %59 = load i64, i64* %58, !mcsema_real_eip !22
  store i64 %59, i64* %XBP, !mcsema_real_eip !22
  %60 = add i64 %RSP_val.34, 8, !mcsema_real_eip !22
  store i64 %60, i64* %XSP, !mcsema_real_eip !22
  %RSP_val.35 = load i64, i64* %XSP, !mcsema_real_eip !23
  %61 = add i64 %RSP_val.35, 8, !mcsema_real_eip !23
  %62 = inttoptr i64 %RSP_val.35 to i64*, !mcsema_real_eip !23
  %63 = load i64, i64* %62, !mcsema_real_eip !23
  store i64 %63, i64* %XIP, !mcsema_real_eip !23
  store i64 %61, i64* %XSP, !mcsema_real_eip !23
  ret void, !mcsema_real_eip !23
}

declare x86_64_sysvcc i64 @printf(i64 inreg, i64 inreg, i64 inreg, i64 inreg, i64 inreg, i64 inreg, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)

; Function Attrs: naked noinline
declare void @callback_sub_0() #2

; Function Attrs: nounwind readnone
declare i8 @llvm.ctpop.i8(i8) #3

; Function Attrs: naked noinline
declare x86_64_sysvcc i64 @_printf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #2

; Function Attrs: naked noinline
declare void @main() #2

attributes #0 = { naked }
attributes #1 = { noinline }
attributes #2 = { naked noinline }
attributes #3 = { nounwind readnone }

!llvm.module.flags = !{!0, !1}

!0 = !{i32 1, !"Debug Info Version", i32 1}
!1 = !{i32 1, !"Dwarf Version", i32 3}
!2 = !{i64 0}
!3 = !{i64 1}
!4 = !{i64 4}
!5 = !{i64 8}
!6 = !{i64 18}
!7 = !{i64 21}
!8 = !{i64 24}
!9 = !{i64 26}
!10 = !{i64 31}
!11 = !{i64 34}
!12 = !{i64 38}
!13 = !{i64 39}
!14 = !{i64 48}
!15 = !{i64 49}
!16 = !{i64 52}
!17 = !{i64 56}
!18 = !{i64 61}
!19 = !{i64 68}
!20 = !{i64 73}
!21 = !{i64 75}
!22 = !{i64 79}
!23 = !{i64 80}
