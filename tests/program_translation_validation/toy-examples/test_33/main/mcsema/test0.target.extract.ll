; ModuleID = 'mcsema/test0.target.opt.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"

%G__0x4006b4_type = type <{ [8 x i8] }>
%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.anon = type { i64 }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque

@G__0x4006b4 = external global %G__0x4006b4_type

; Function Attrs: nounwind readnone
declare i32 @llvm.ctpop.i32(i32) #0

declare extern_weak x86_64_sysvcc i64 @atoi(i64)

declare extern_weak x86_64_sysvcc i64 @printf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)

declare %struct.Memory* @__remill_function_call(%struct.State* dereferenceable(3376), i64, %struct.Memory*) local_unnamed_addr

declare %struct.Memory* @sub_400530.Ack(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias readnone returned) local_unnamed_addr

; Function Attrs: alwaysinline
define %struct.Memory* @main(%struct.State* noalias, i64, %struct.Memory* noalias) local_unnamed_addr #1 {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP.i, align 8
  %5 = add i64 %1, 1
  store i64 %5, i64* %3, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %4, i64* %9, align 8
  %10 = load i64, i64* %3, align 8
  store i64 %8, i64* %RBP.i, align 8
  %11 = add i64 %7, -40
  store i64 %11, i64* %6, align 8
  %12 = icmp ult i64 %8, 32
  %13 = zext i1 %12 to i8
  %14 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %13, i8* %14, align 1
  %15 = trunc i64 %11 to i32
  %16 = and i32 %15, 255
  %17 = tail call i32 @llvm.ctpop.i32(i32 %16)
  %18 = trunc i32 %17 to i8
  %19 = and i8 %18, 1
  %20 = xor i8 %19, 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %20, i8* %21, align 1
  %22 = xor i64 %8, %11
  %23 = lshr i64 %22, 4
  %24 = trunc i64 %23 to i8
  %25 = and i8 %24, 1
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %25, i8* %26, align 1
  %27 = icmp eq i64 %11, 0
  %28 = zext i1 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %28, i8* %29, align 1
  %30 = lshr i64 %11, 63
  %31 = trunc i64 %30 to i8
  %32 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %31, i8* %32, align 1
  %33 = lshr i64 %8, 63
  %34 = xor i64 %30, %33
  %35 = add nuw nsw i64 %34, %33
  %36 = icmp eq i64 %35, 2
  %37 = zext i1 %36 to i8
  %38 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %37, i8* %38, align 1
  %39 = add i64 %7, -12
  %40 = add i64 %10, 14
  store i64 %40, i64* %3, align 8
  %41 = inttoptr i64 %39 to i32*
  store i32 0, i32* %41, align 4
  %42 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0
  %EDI.i = bitcast %union.anon* %42 to i32*
  %43 = load i64, i64* %RBP.i, align 8
  %44 = add i64 %43, -8
  %45 = load i32, i32* %EDI.i, align 4
  %46 = load i64, i64* %3, align 8
  %47 = add i64 %46, 3
  store i64 %47, i64* %3, align 8
  %48 = inttoptr i64 %44 to i32*
  store i32 %45, i32* %48, align 4
  %RSI.i64 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %49 = load i64, i64* %RBP.i, align 8
  %50 = add i64 %49, -16
  %51 = load i64, i64* %RSI.i64, align 8
  %52 = load i64, i64* %3, align 8
  %53 = add i64 %52, 4
  store i64 %53, i64* %3, align 8
  %54 = inttoptr i64 %50 to i64*
  store i64 %51, i64* %54, align 8
  %55 = load i64, i64* %RBP.i, align 8
  %56 = add i64 %55, -8
  %57 = load i64, i64* %3, align 8
  %58 = add i64 %57, 4
  store i64 %58, i64* %3, align 8
  %59 = inttoptr i64 %56 to i32*
  %60 = load i32, i32* %59, align 4
  %61 = add i32 %60, -2
  %62 = icmp ult i32 %60, 2
  %63 = zext i1 %62 to i8
  store i8 %63, i8* %14, align 1
  %64 = and i32 %61, 255
  %65 = tail call i32 @llvm.ctpop.i32(i32 %64)
  %66 = trunc i32 %65 to i8
  %67 = and i8 %66, 1
  %68 = xor i8 %67, 1
  store i8 %68, i8* %21, align 1
  %69 = xor i32 %60, %61
  %70 = lshr i32 %69, 4
  %71 = trunc i32 %70 to i8
  %72 = and i8 %71, 1
  store i8 %72, i8* %26, align 1
  %73 = icmp eq i32 %61, 0
  %74 = zext i1 %73 to i8
  store i8 %74, i8* %29, align 1
  %75 = lshr i32 %61, 31
  %76 = trunc i32 %75 to i8
  store i8 %76, i8* %32, align 1
  %77 = lshr i32 %60, 31
  %78 = xor i32 %75, %77
  %79 = add nuw nsw i32 %78, %77
  %80 = icmp eq i32 %79, 2
  %81 = zext i1 %80 to i8
  store i8 %81, i8* %38, align 1
  %.v = select i1 %73, i64 10, i64 31
  %82 = add i64 %57, %.v
  store i64 %82, i64* %3, align 8
  %83 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %RAX.i46 = getelementptr inbounds %union.anon, %union.anon* %83, i64 0, i32 0
  br i1 %73, label %block_4005d0, label %block_.L_4005e5

block_4005d0:                                     ; preds = %entry
  %84 = add i64 %55, -16
  %85 = add i64 %82, 4
  store i64 %85, i64* %3, align 8
  %86 = inttoptr i64 %84 to i64*
  %87 = load i64, i64* %86, align 8
  store i64 %87, i64* %RAX.i46, align 8
  %RDI.i56 = getelementptr inbounds %union.anon, %union.anon* %42, i64 0, i32 0
  %88 = add i64 %87, 8
  %89 = add i64 %82, 8
  store i64 %89, i64* %3, align 8
  %90 = inttoptr i64 %88 to i64*
  %91 = load i64, i64* %90, align 8
  store i64 %91, i64* %RDI.i56, align 8
  %92 = add i64 %82, -416
  %93 = add i64 %82, 13
  %94 = load i64, i64* %6, align 8
  %95 = add i64 %94, -8
  %96 = inttoptr i64 %95 to i64*
  store i64 %93, i64* %96, align 8
  store i64 %95, i64* %6, align 8
  store i64 %92, i64* %3, align 8
  %97 = tail call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @atoi to i64), %struct.Memory* %2)
  %EAX.i49 = bitcast %union.anon* %83 to i32*
  %98 = load i64, i64* %RBP.i, align 8
  %99 = add i64 %98, -24
  %100 = load i32, i32* %EAX.i49, align 4
  %101 = load i64, i64* %3, align 8
  %102 = add i64 %101, 3
  store i64 %102, i64* %3, align 8
  %103 = inttoptr i64 %99 to i32*
  store i32 %100, i32* %103, align 4
  %104 = load i64, i64* %3, align 8
  %105 = add i64 %104, 18
  br label %block_.L_4005f2

block_.L_4005e5:                                  ; preds = %entry
  store i64 8, i64* %RAX.i46, align 8
  %EAX.i43 = bitcast %union.anon* %83 to i32*
  %106 = add i64 %55, -24
  %107 = add i64 %82, 8
  store i64 %107, i64* %3, align 8
  %108 = inttoptr i64 %106 to i32*
  store i32 8, i32* %108, align 4
  %109 = load i64, i64* %3, align 8
  %110 = add i64 %109, 5
  %.pre = getelementptr inbounds %union.anon, %union.anon* %42, i64 0, i32 0
  br label %block_.L_4005f2

block_.L_4005f2:                                  ; preds = %block_.L_4005e5, %block_4005d0
  %storemerge = phi i64 [ %105, %block_4005d0 ], [ %110, %block_.L_4005e5 ]
  %EAX.i34.pre-phi = phi i32* [ %EAX.i49, %block_4005d0 ], [ %EAX.i43, %block_.L_4005e5 ]
  %RDI.i37.pre-phi = phi i64* [ %RDI.i56, %block_4005d0 ], [ %.pre, %block_.L_4005e5 ]
  %MEMORY.0 = phi %struct.Memory* [ %97, %block_4005d0 ], [ %2, %block_.L_4005e5 ]
  %111 = load i64, i64* %RBP.i, align 8
  %112 = add i64 %111, -24
  %113 = add i64 %storemerge, 3
  store i64 %113, i64* %3, align 8
  %114 = inttoptr i64 %112 to i32*
  %115 = load i32, i32* %114, align 4
  %116 = zext i32 %115 to i64
  store i64 %116, i64* %RAX.i46, align 8
  store i64 3, i64* %RDI.i37.pre-phi, align 8
  %117 = add i64 %111, -20
  %118 = add i64 %storemerge, 11
  store i64 %118, i64* %3, align 8
  %119 = inttoptr i64 %117 to i32*
  store i32 %115, i32* %119, align 4
  %120 = load i64, i64* %RBP.i, align 8
  %121 = add i64 %120, -20
  %122 = load i64, i64* %3, align 8
  %123 = add i64 %122, 3
  store i64 %123, i64* %3, align 8
  %124 = inttoptr i64 %121 to i32*
  %125 = load i32, i32* %124, align 4
  %126 = zext i32 %125 to i64
  store i64 %126, i64* %RSI.i64, align 8
  %127 = add i64 %122, 6
  store i64 %127, i64* %3, align 8
  %128 = load i32, i32* %124, align 4
  %129 = zext i32 %128 to i64
  store i64 %129, i64* %RAX.i46, align 8
  %130 = add i64 %120, -28
  %131 = add i64 %122, 9
  store i64 %131, i64* %3, align 8
  %132 = inttoptr i64 %130 to i32*
  store i32 %125, i32* %132, align 4
  %133 = load i32, i32* %EAX.i34.pre-phi, align 4
  %134 = zext i32 %133 to i64
  %135 = load i64, i64* %3, align 8
  store i64 %134, i64* %RSI.i64, align 8
  %136 = add i64 %135, -214
  %137 = add i64 %135, 7
  %138 = load i64, i64* %6, align 8
  %139 = add i64 %138, -8
  %140 = inttoptr i64 %139 to i64*
  store i64 %137, i64* %140, align 8
  store i64 %139, i64* %6, align 8
  store i64 %136, i64* %3, align 8
  %call2_400608 = tail call %struct.Memory* @sub_400530.Ack(%struct.State* %0, i64 %136, %struct.Memory* %MEMORY.0)
  %141 = load i64, i64* %3, align 8
  store i64 ptrtoint (%G__0x4006b4_type* @G__0x4006b4 to i64), i64* %RDI.i37.pre-phi, align 8
  %142 = load i64, i64* %RBP.i, align 8
  %143 = add i64 %142, -28
  %144 = add i64 %141, 13
  store i64 %144, i64* %3, align 8
  %145 = inttoptr i64 %143 to i32*
  %146 = load i32, i32* %145, align 4
  %147 = zext i32 %146 to i64
  store i64 %147, i64* %RSI.i64, align 8
  %148 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %RDX.i17 = getelementptr inbounds %union.anon, %union.anon* %148, i64 0, i32 0
  %149 = load i32, i32* %EAX.i34.pre-phi, align 4
  %150 = zext i32 %149 to i64
  store i64 %150, i64* %RDX.i17, align 8
  %AL.i = bitcast %union.anon* %83 to i8*
  store i8 0, i8* %AL.i, align 1
  %151 = add i64 %141, -493
  %152 = add i64 %141, 22
  %153 = load i64, i64* %6, align 8
  %154 = add i64 %153, -8
  %155 = inttoptr i64 %154 to i64*
  store i64 %152, i64* %155, align 8
  store i64 %154, i64* %6, align 8
  store i64 %151, i64* %3, align 8
  %156 = tail call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @printf to i64), %struct.Memory* %call2_400608)
  %EDX.i9 = bitcast %union.anon* %148 to i32*
  %157 = load i64, i64* %3, align 8
  store i64 0, i64* %RDX.i17, align 8
  %158 = load i64, i64* %RBP.i, align 8
  %159 = add i64 %158, -32
  %160 = load i32, i32* %EAX.i34.pre-phi, align 4
  %161 = add i64 %157, 5
  store i64 %161, i64* %3, align 8
  %162 = inttoptr i64 %159 to i32*
  store i32 %160, i32* %162, align 4
  %163 = load i32, i32* %EDX.i9, align 4
  %164 = zext i32 %163 to i64
  %165 = load i64, i64* %3, align 8
  store i64 %164, i64* %RAX.i46, align 8
  %166 = load i64, i64* %6, align 8
  %167 = add i64 %166, 32
  store i64 %167, i64* %6, align 8
  %168 = icmp ugt i64 %166, -33
  %169 = zext i1 %168 to i8
  store i8 %169, i8* %14, align 1
  %170 = trunc i64 %167 to i32
  %171 = and i32 %170, 255
  %172 = tail call i32 @llvm.ctpop.i32(i32 %171)
  %173 = trunc i32 %172 to i8
  %174 = and i8 %173, 1
  %175 = xor i8 %174, 1
  store i8 %175, i8* %21, align 1
  %176 = xor i64 %166, %167
  %177 = lshr i64 %176, 4
  %178 = trunc i64 %177 to i8
  %179 = and i8 %178, 1
  store i8 %179, i8* %26, align 1
  %180 = icmp eq i64 %167, 0
  %181 = zext i1 %180 to i8
  store i8 %181, i8* %29, align 1
  %182 = lshr i64 %167, 63
  %183 = trunc i64 %182 to i8
  store i8 %183, i8* %32, align 1
  %184 = lshr i64 %166, 63
  %185 = xor i64 %182, %184
  %186 = add nuw nsw i64 %185, %182
  %187 = icmp eq i64 %186, 2
  %188 = zext i1 %187 to i8
  store i8 %188, i8* %38, align 1
  %189 = add i64 %165, 7
  store i64 %189, i64* %3, align 8
  %190 = add i64 %166, 40
  %191 = inttoptr i64 %167 to i64*
  %192 = load i64, i64* %191, align 8
  store i64 %192, i64* %RBP.i, align 8
  store i64 %190, i64* %6, align 8
  %193 = add i64 %165, 8
  store i64 %193, i64* %3, align 8
  %194 = inttoptr i64 %190 to i64*
  %195 = load i64, i64* %194, align 8
  store i64 %195, i64* %3, align 8
  %196 = add i64 %166, 48
  store i64 %196, i64* %6, align 8
  ret %struct.Memory* %156
}

attributes #0 = { nounwind readnone }
attributes #1 = { alwaysinline }
