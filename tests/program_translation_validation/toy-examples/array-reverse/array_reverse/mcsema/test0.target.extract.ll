; ModuleID = 'mcsema/test0.target.opt.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"

%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.anon = type { i64 }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque

; Function Attrs: nounwind readnone
declare i32 @llvm.ctpop.i32(i32) #0

; Function Attrs: alwaysinline
define %struct.Memory* @array_reverse(%struct.State* noalias, i64, %struct.Memory* noalias) #1 {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %PC.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP.i, align 8
  %5 = add i64 %1, 1
  store i64 %5, i64* %PC.i, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %4, i64* %9, align 8
  store i64 %8, i64* %6, align 8
  %10 = load i64, i64* %PC.i, align 8
  store i64 %8, i64* %RBP.i, align 8
  %RDI.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %11 = add i64 %7, -16
  %12 = load i64, i64* %RDI.i, align 8
  %13 = add i64 %10, 7
  store i64 %13, i64* %PC.i, align 8
  %14 = inttoptr i64 %11 to i64*
  store i64 %12, i64* %14, align 8
  %15 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0
  %ESI.i78 = bitcast %union.anon* %15 to i32*
  %16 = load i64, i64* %RBP.i, align 8
  %17 = add i64 %16, -12
  %18 = load i32, i32* %ESI.i78, align 4
  %19 = load i64, i64* %PC.i, align 8
  %20 = add i64 %19, 3
  store i64 %20, i64* %PC.i, align 8
  %21 = inttoptr i64 %17 to i32*
  store i32 %18, i32* %21, align 4
  %22 = load i64, i64* %RBP.i, align 8
  %23 = add i64 %22, -16
  %24 = load i64, i64* %PC.i, align 8
  %25 = add i64 %24, 7
  store i64 %25, i64* %PC.i, align 8
  %26 = inttoptr i64 %23 to i32*
  store i32 0, i32* %26, align 4
  %RSI.i73 = getelementptr inbounds %union.anon, %union.anon* %15, i64 0, i32 0
  %27 = load i64, i64* %RBP.i, align 8
  %28 = add i64 %27, -12
  %29 = load i64, i64* %PC.i, align 8
  %30 = add i64 %29, 3
  store i64 %30, i64* %PC.i, align 8
  %31 = inttoptr i64 %28 to i32*
  %32 = load i32, i32* %31, align 4
  %33 = add i32 %32, -1
  %34 = zext i32 %33 to i64
  store i64 %34, i64* %RSI.i73, align 8
  %35 = icmp eq i32 %32, 0
  %36 = zext i1 %35 to i8
  %37 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %36, i8* %37, align 1
  %38 = and i32 %33, 255
  %39 = call i32 @llvm.ctpop.i32(i32 %38)
  %40 = trunc i32 %39 to i8
  %41 = and i8 %40, 1
  %42 = xor i8 %41, 1
  %43 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %42, i8* %43, align 1
  %44 = xor i32 %32, %33
  %45 = lshr i32 %44, 4
  %46 = trunc i32 %45 to i8
  %47 = and i8 %46, 1
  %48 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %47, i8* %48, align 1
  %49 = icmp eq i32 %33, 0
  %50 = zext i1 %49 to i8
  %51 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %50, i8* %51, align 1
  %52 = lshr i32 %33, 31
  %53 = trunc i32 %52 to i8
  %54 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %53, i8* %54, align 1
  %55 = lshr i32 %32, 31
  %56 = xor i32 %52, %55
  %57 = add nuw nsw i32 %56, %55
  %58 = icmp eq i32 %57, 2
  %59 = zext i1 %58 to i8
  %60 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %59, i8* %60, align 1
  %61 = add i64 %27, -20
  %62 = add i64 %29, 9
  store i64 %62, i64* %PC.i, align 8
  %63 = inttoptr i64 %61 to i32*
  store i32 %33, i32* %63, align 4
  %RAX.i67 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX.i58 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %RDX.i56 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0, i32 0
  %.pre = load i64, i64* %PC.i, align 8
  br label %block_.L_4004ab

block_.L_4004ab:                                  ; preds = %block_4004b7, %entry
  %64 = phi i64 [ %229, %block_4004b7 ], [ %.pre, %entry ]
  %65 = load i64, i64* %RBP.i, align 8
  %66 = add i64 %65, -16
  %67 = add i64 %64, 3
  store i64 %67, i64* %PC.i, align 8
  %68 = inttoptr i64 %66 to i32*
  %69 = load i32, i32* %68, align 4
  %70 = zext i32 %69 to i64
  store i64 %70, i64* %RAX.i67, align 8
  %71 = add i64 %65, -20
  %72 = add i64 %64, 6
  store i64 %72, i64* %PC.i, align 8
  %73 = inttoptr i64 %71 to i32*
  %74 = load i32, i32* %73, align 4
  %75 = sub i32 %69, %74
  %76 = icmp ult i32 %69, %74
  %77 = zext i1 %76 to i8
  store i8 %77, i8* %37, align 1
  %78 = and i32 %75, 255
  %79 = call i32 @llvm.ctpop.i32(i32 %78)
  %80 = trunc i32 %79 to i8
  %81 = and i8 %80, 1
  %82 = xor i8 %81, 1
  store i8 %82, i8* %43, align 1
  %83 = xor i32 %74, %69
  %84 = xor i32 %83, %75
  %85 = lshr i32 %84, 4
  %86 = trunc i32 %85 to i8
  %87 = and i8 %86, 1
  store i8 %87, i8* %48, align 1
  %88 = icmp eq i32 %75, 0
  %89 = zext i1 %88 to i8
  store i8 %89, i8* %51, align 1
  %90 = lshr i32 %75, 31
  %91 = trunc i32 %90 to i8
  store i8 %91, i8* %54, align 1
  %92 = lshr i32 %69, 31
  %93 = lshr i32 %74, 31
  %94 = xor i32 %93, %92
  %95 = xor i32 %90, %92
  %96 = add nuw nsw i32 %95, %94
  %97 = icmp eq i32 %96, 2
  %98 = zext i1 %97 to i8
  store i8 %98, i8* %60, align 1
  %99 = icmp ne i8 %91, 0
  %100 = xor i1 %99, %97
  %.v = select i1 %100, i64 12, i64 85
  %101 = add i64 %64, %.v
  store i64 %101, i64* %3, align 8
  br i1 %100, label %block_4004b7, label %block_.L_400500

block_4004b7:                                     ; preds = %block_.L_4004ab
  %102 = add i64 %65, -8
  %103 = add i64 %101, 4
  store i64 %103, i64* %PC.i, align 8
  %104 = inttoptr i64 %102 to i64*
  %105 = load i64, i64* %104, align 8
  store i64 %105, i64* %RAX.i67, align 8
  %106 = add i64 %101, 8
  store i64 %106, i64* %PC.i, align 8
  %107 = load i32, i32* %68, align 4
  %108 = sext i32 %107 to i64
  store i64 %108, i64* %RCX.i58, align 8
  %109 = shl nsw i64 %108, 2
  %110 = add i64 %109, %105
  %111 = add i64 %101, 11
  store i64 %111, i64* %PC.i, align 8
  %112 = inttoptr i64 %110 to i32*
  %113 = load i32, i32* %112, align 4
  %114 = zext i32 %113 to i64
  store i64 %114, i64* %RDX.i56, align 8
  %115 = add i64 %65, -24
  %116 = add i64 %101, 14
  store i64 %116, i64* %PC.i, align 8
  %117 = inttoptr i64 %115 to i32*
  store i32 %113, i32* %117, align 4
  %118 = load i64, i64* %RBP.i, align 8
  %119 = add i64 %118, -8
  %120 = load i64, i64* %PC.i, align 8
  %121 = add i64 %120, 4
  store i64 %121, i64* %PC.i, align 8
  %122 = inttoptr i64 %119 to i64*
  %123 = load i64, i64* %122, align 8
  store i64 %123, i64* %RAX.i67, align 8
  %124 = add i64 %118, -20
  %125 = add i64 %120, 8
  store i64 %125, i64* %PC.i, align 8
  %126 = inttoptr i64 %124 to i32*
  %127 = load i32, i32* %126, align 4
  %128 = sext i32 %127 to i64
  store i64 %128, i64* %RCX.i58, align 8
  %129 = shl nsw i64 %128, 2
  %130 = add i64 %129, %123
  %131 = add i64 %120, 11
  store i64 %131, i64* %PC.i, align 8
  %132 = inttoptr i64 %130 to i32*
  %133 = load i32, i32* %132, align 4
  %134 = zext i32 %133 to i64
  store i64 %134, i64* %RDX.i56, align 8
  %135 = add i64 %120, 15
  store i64 %135, i64* %PC.i, align 8
  %136 = load i64, i64* %122, align 8
  store i64 %136, i64* %RAX.i67, align 8
  %137 = add i64 %118, -16
  %138 = add i64 %120, 19
  store i64 %138, i64* %PC.i, align 8
  %139 = inttoptr i64 %137 to i32*
  %140 = load i32, i32* %139, align 4
  %141 = sext i32 %140 to i64
  store i64 %141, i64* %RCX.i58, align 8
  %142 = shl nsw i64 %141, 2
  %143 = add i64 %142, %136
  %144 = add i64 %120, 22
  store i64 %144, i64* %PC.i, align 8
  %145 = inttoptr i64 %143 to i32*
  store i32 %133, i32* %145, align 4
  %146 = load i64, i64* %RBP.i, align 8
  %147 = add i64 %146, -24
  %148 = load i64, i64* %PC.i, align 8
  %149 = add i64 %148, 3
  store i64 %149, i64* %PC.i, align 8
  %150 = inttoptr i64 %147 to i32*
  %151 = load i32, i32* %150, align 4
  %152 = zext i32 %151 to i64
  store i64 %152, i64* %RDX.i56, align 8
  %153 = add i64 %146, -8
  %154 = add i64 %148, 7
  store i64 %154, i64* %PC.i, align 8
  %155 = inttoptr i64 %153 to i64*
  %156 = load i64, i64* %155, align 8
  store i64 %156, i64* %RAX.i67, align 8
  %157 = add i64 %146, -20
  %158 = add i64 %148, 11
  store i64 %158, i64* %PC.i, align 8
  %159 = inttoptr i64 %157 to i32*
  %160 = load i32, i32* %159, align 4
  %161 = sext i32 %160 to i64
  store i64 %161, i64* %RCX.i58, align 8
  %162 = shl nsw i64 %161, 2
  %163 = add i64 %162, %156
  %164 = add i64 %148, 14
  store i64 %164, i64* %PC.i, align 8
  %165 = inttoptr i64 %163 to i32*
  store i32 %151, i32* %165, align 4
  %166 = load i64, i64* %RBP.i, align 8
  %167 = add i64 %166, -16
  %168 = load i64, i64* %PC.i, align 8
  %169 = add i64 %168, 3
  store i64 %169, i64* %PC.i, align 8
  %170 = inttoptr i64 %167 to i32*
  %171 = load i32, i32* %170, align 4
  %172 = add i32 %171, 1
  %173 = zext i32 %172 to i64
  store i64 %173, i64* %RDX.i56, align 8
  %174 = icmp eq i32 %171, -1
  %175 = icmp eq i32 %172, 0
  %176 = or i1 %174, %175
  %177 = zext i1 %176 to i8
  store i8 %177, i8* %37, align 1
  %178 = and i32 %172, 255
  %179 = call i32 @llvm.ctpop.i32(i32 %178)
  %180 = trunc i32 %179 to i8
  %181 = and i8 %180, 1
  %182 = xor i8 %181, 1
  store i8 %182, i8* %43, align 1
  %183 = xor i32 %171, %172
  %184 = lshr i32 %183, 4
  %185 = trunc i32 %184 to i8
  %186 = and i8 %185, 1
  store i8 %186, i8* %48, align 1
  %187 = icmp eq i32 %172, 0
  %188 = zext i1 %187 to i8
  store i8 %188, i8* %51, align 1
  %189 = lshr i32 %172, 31
  %190 = trunc i32 %189 to i8
  store i8 %190, i8* %54, align 1
  %191 = lshr i32 %171, 31
  %192 = xor i32 %189, %191
  %193 = add nuw nsw i32 %192, %189
  %194 = icmp eq i32 %193, 2
  %195 = zext i1 %194 to i8
  store i8 %195, i8* %60, align 1
  %196 = add i64 %168, 9
  store i64 %196, i64* %PC.i, align 8
  store i32 %172, i32* %170, align 4
  %197 = load i64, i64* %RBP.i, align 8
  %198 = add i64 %197, -20
  %199 = load i64, i64* %PC.i, align 8
  %200 = add i64 %199, 3
  store i64 %200, i64* %PC.i, align 8
  %201 = inttoptr i64 %198 to i32*
  %202 = load i32, i32* %201, align 4
  %203 = add i32 %202, -1
  %204 = zext i32 %203 to i64
  store i64 %204, i64* %RDX.i56, align 8
  %205 = icmp ne i32 %202, 0
  %206 = zext i1 %205 to i8
  store i8 %206, i8* %37, align 1
  %207 = and i32 %203, 255
  %208 = call i32 @llvm.ctpop.i32(i32 %207)
  %209 = trunc i32 %208 to i8
  %210 = and i8 %209, 1
  %211 = xor i8 %210, 1
  store i8 %211, i8* %43, align 1
  %212 = xor i32 %202, 16
  %213 = xor i32 %203, %212
  %214 = lshr i32 %213, 4
  %215 = trunc i32 %214 to i8
  %216 = and i8 %215, 1
  store i8 %216, i8* %48, align 1
  %217 = icmp eq i32 %203, 0
  %218 = zext i1 %217 to i8
  store i8 %218, i8* %51, align 1
  %219 = lshr i32 %203, 31
  %220 = trunc i32 %219 to i8
  store i8 %220, i8* %54, align 1
  %221 = lshr i32 %202, 31
  %222 = xor i32 %219, %221
  %223 = xor i32 %219, 1
  %224 = add nuw nsw i32 %222, %223
  %225 = icmp eq i32 %224, 2
  %226 = zext i1 %225 to i8
  store i8 %226, i8* %60, align 1
  %227 = add i64 %199, 9
  store i64 %227, i64* %PC.i, align 8
  store i32 %203, i32* %201, align 4
  %228 = load i64, i64* %PC.i, align 8
  %229 = add i64 %228, -80
  store i64 %229, i64* %3, align 8
  br label %block_.L_4004ab

block_.L_400500:                                  ; preds = %block_.L_4004ab
  %230 = add i64 %101, 1
  store i64 %230, i64* %PC.i, align 8
  %231 = load i64, i64* %6, align 8
  %232 = add i64 %231, 8
  %233 = inttoptr i64 %231 to i64*
  %234 = load i64, i64* %233, align 8
  store i64 %234, i64* %RBP.i, align 8
  store i64 %232, i64* %6, align 8
  %235 = add i64 %101, 2
  store i64 %235, i64* %PC.i, align 8
  %236 = inttoptr i64 %232 to i64*
  %237 = load i64, i64* %236, align 8
  store i64 %237, i64* %3, align 8
  %238 = add i64 %231, 16
  store i64 %238, i64* %6, align 8
  ret %struct.Memory* %2
}

attributes #0 = { nounwind readnone }
attributes #1 = { alwaysinline }
