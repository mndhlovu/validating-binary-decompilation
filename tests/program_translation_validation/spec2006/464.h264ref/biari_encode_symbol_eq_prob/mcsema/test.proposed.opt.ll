; ModuleID = 'mcsema/test.proposed.inline.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"

%__bss_start_type = type <{ [8 x i8] }>
%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.anon = type { i64 }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque

@__bss_start = local_unnamed_addr global %__bss_start_type zeroinitializer

; Function Attrs: nounwind readnone
declare i32 @llvm.ctpop.i32(i32) #0

; Function Attrs: alwaysinline nounwind
define %struct.Memory* @biari_encode_symbol_eq_prob(%struct.State* noalias, i64, %struct.Memory* noalias readnone returned) local_unnamed_addr #1 {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP.i = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP.i, align 8
  %5 = add i64 %1, 1
  store i64 %5, i64* %3, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %4, i64* %9, align 8
  store i64 %8, i64* %6, align 8
  %10 = load i64, i64* %3, align 8
  store i64 %8, i64* %RBP.i, align 8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %AX.i551 = bitcast %union.anon* %11 to i16*
  %12 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0
  %SI.i = bitcast %union.anon* %12 to i16*
  %13 = load i16, i16* %SI.i, align 2
  store i16 %13, i16* %AX.i551, align 2
  %RDI.i548 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %14 = add i64 %7, -16
  %15 = load i64, i64* %RDI.i548, align 8
  %16 = add i64 %10, 10
  store i64 %16, i64* %3, align 8
  %17 = inttoptr i64 %14 to i64*
  store i64 %15, i64* %17, align 8
  %18 = load i64, i64* %RBP.i, align 8
  %19 = add i64 %18, -10
  %20 = load i16, i16* %AX.i551, align 2
  %21 = load i64, i64* %3, align 8
  %22 = add i64 %21, 4
  store i64 %22, i64* %3, align 8
  %23 = inttoptr i64 %19 to i16*
  store i16 %20, i16* %23, align 2
  %24 = load i64, i64* %RBP.i, align 8
  %25 = add i64 %24, -8
  %26 = load i64, i64* %3, align 8
  %27 = add i64 %26, 4
  store i64 %27, i64* %3, align 8
  %28 = inttoptr i64 %25 to i64*
  %29 = load i64, i64* %28, align 8
  store i64 %29, i64* %RDI.i548, align 8
  %RSI.i540 = getelementptr inbounds %union.anon, %union.anon* %12, i64 0, i32 0
  %30 = add i64 %26, 6
  store i64 %30, i64* %3, align 8
  %31 = inttoptr i64 %29 to i32*
  %32 = load i32, i32* %31, align 4
  %33 = shl i32 %32, 1
  %34 = icmp slt i32 %32, 0
  %35 = icmp slt i32 %33, 0
  %36 = xor i1 %34, %35
  %37 = zext i32 %33 to i64
  store i64 %37, i64* %RSI.i540, align 8
  %38 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %.lobit = lshr i32 %32, 31
  %39 = trunc i32 %.lobit to i8
  store i8 %39, i8* %38, align 1
  %40 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  %41 = and i32 %33, 254
  %42 = tail call i32 @llvm.ctpop.i32(i32 %41)
  %43 = trunc i32 %42 to i8
  %44 = and i8 %43, 1
  %45 = xor i8 %44, 1
  store i8 %45, i8* %40, align 1
  %46 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %46, align 1
  %47 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %48 = icmp eq i32 %33, 0
  %49 = zext i1 %48 to i8
  store i8 %49, i8* %47, align 1
  %50 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %51 = lshr i32 %32, 30
  %52 = trunc i32 %51 to i8
  %53 = and i8 %52, 1
  store i8 %53, i8* %50, align 1
  %54 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %55 = zext i1 %36 to i8
  store i8 %55, i8* %54, align 1
  %56 = add i64 %24, -16
  %57 = add i64 %26, 11
  store i64 %57, i64* %3, align 8
  %58 = inttoptr i64 %56 to i32*
  store i32 %33, i32* %58, align 4
  %59 = load i64, i64* %RBP.i, align 8
  %60 = add i64 %59, -10
  %61 = load i64, i64* %3, align 8
  %62 = add i64 %61, 4
  store i64 %62, i64* %3, align 8
  %63 = inttoptr i64 %60 to i16*
  %64 = load i16, i16* %63, align 2
  %65 = sext i16 %64 to i64
  %66 = and i64 %65, 4294967295
  store i64 %66, i64* %RSI.i540, align 8
  %67 = sext i16 %64 to i32
  store i8 0, i8* %38, align 1
  %68 = and i32 %67, 255
  %69 = tail call i32 @llvm.ctpop.i32(i32 %68)
  %70 = trunc i32 %69 to i8
  %71 = and i8 %70, 1
  %72 = xor i8 %71, 1
  store i8 %72, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %73 = icmp eq i16 %64, 0
  %74 = zext i1 %73 to i8
  store i8 %74, i8* %47, align 1
  %75 = lshr i32 %67, 31
  %76 = trunc i32 %75 to i8
  store i8 %76, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v52 = select i1 %73, i64 26, i64 13
  %77 = add i64 %61, %.v52
  store i64 %77, i64* %3, align 8
  br i1 %73, label %block_.L_401e15, label %block_401e08

block_401e08:                                     ; preds = %entry
  %RAX.i527 = getelementptr inbounds %union.anon, %union.anon* %11, i64 0, i32 0
  %78 = add i64 %59, -8
  %79 = add i64 %77, 4
  store i64 %79, i64* %3, align 8
  %80 = inttoptr i64 %78 to i64*
  %81 = load i64, i64* %80, align 8
  store i64 %81, i64* %RAX.i527, align 8
  %RCX.i525 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %82 = add i64 %81, 4
  %83 = add i64 %77, 7
  store i64 %83, i64* %3, align 8
  %84 = inttoptr i64 %82 to i32*
  %85 = load i32, i32* %84, align 4
  %86 = zext i32 %85 to i64
  store i64 %86, i64* %RCX.i525, align 8
  %87 = add i64 %59, -16
  %88 = add i64 %77, 10
  store i64 %88, i64* %3, align 8
  %89 = inttoptr i64 %87 to i32*
  %90 = load i32, i32* %89, align 4
  %91 = add i32 %90, %85
  %92 = zext i32 %91 to i64
  store i64 %92, i64* %RCX.i525, align 8
  %93 = icmp ult i32 %91, %85
  %94 = icmp ult i32 %91, %90
  %95 = or i1 %93, %94
  %96 = zext i1 %95 to i8
  store i8 %96, i8* %38, align 1
  %97 = and i32 %91, 255
  %98 = tail call i32 @llvm.ctpop.i32(i32 %97)
  %99 = trunc i32 %98 to i8
  %100 = and i8 %99, 1
  %101 = xor i8 %100, 1
  store i8 %101, i8* %40, align 1
  %102 = xor i32 %90, %85
  %103 = xor i32 %102, %91
  %104 = lshr i32 %103, 4
  %105 = trunc i32 %104 to i8
  %106 = and i8 %105, 1
  store i8 %106, i8* %46, align 1
  %107 = icmp eq i32 %91, 0
  %108 = zext i1 %107 to i8
  store i8 %108, i8* %47, align 1
  %109 = lshr i32 %91, 31
  %110 = trunc i32 %109 to i8
  store i8 %110, i8* %50, align 1
  %111 = lshr i32 %85, 31
  %112 = lshr i32 %90, 31
  %113 = xor i32 %109, %111
  %114 = xor i32 %109, %112
  %115 = add nuw nsw i32 %113, %114
  %116 = icmp eq i32 %115, 2
  %117 = zext i1 %116 to i8
  store i8 %117, i8* %54, align 1
  %118 = add i64 %77, 13
  store i64 %118, i64* %3, align 8
  store i32 %91, i32* %89, align 4
  %.pre = load i64, i64* %RBP.i, align 8
  %.pre31 = load i64, i64* %3, align 8
  br label %block_.L_401e15

block_.L_401e15:                                  ; preds = %block_401e08, %entry
  %119 = phi i64 [ %.pre31, %block_401e08 ], [ %77, %entry ]
  %120 = phi i64 [ %.pre, %block_401e08 ], [ %59, %entry ]
  %121 = add i64 %120, -16
  %122 = add i64 %119, 7
  store i64 %122, i64* %3, align 8
  %123 = inttoptr i64 %121 to i32*
  %124 = load i32, i32* %123, align 4
  %125 = add i32 %124, -1024
  %126 = icmp ult i32 %124, 1024
  %127 = zext i1 %126 to i8
  store i8 %127, i8* %38, align 1
  %128 = and i32 %125, 255
  %129 = tail call i32 @llvm.ctpop.i32(i32 %128)
  %130 = trunc i32 %129 to i8
  %131 = and i8 %130, 1
  %132 = xor i8 %131, 1
  store i8 %132, i8* %40, align 1
  %133 = xor i32 %125, %124
  %134 = lshr i32 %133, 4
  %135 = trunc i32 %134 to i8
  %136 = and i8 %135, 1
  store i8 %136, i8* %46, align 1
  %137 = icmp eq i32 %125, 0
  %138 = zext i1 %137 to i8
  store i8 %138, i8* %47, align 1
  %139 = lshr i32 %125, 31
  %140 = trunc i32 %139 to i8
  store i8 %140, i8* %50, align 1
  %141 = lshr i32 %124, 31
  %142 = xor i32 %139, %141
  %143 = add nuw nsw i32 %142, %141
  %144 = icmp eq i32 %143, 2
  %145 = zext i1 %144 to i8
  store i8 %145, i8* %54, align 1
  %.v51 = select i1 %126, i64 361, i64 13
  %146 = add i64 %119, %.v51
  store i64 %146, i64* %3, align 8
  br i1 %126, label %block_.L_401f7e, label %block_401e22

block_401e22:                                     ; preds = %block_.L_401e15
  %RAX.i512 = getelementptr inbounds %union.anon, %union.anon* %11, i64 0, i32 0
  %147 = add i64 %120, -8
  %148 = add i64 %146, 4
  store i64 %148, i64* %3, align 8
  %149 = inttoptr i64 %147 to i64*
  %150 = load i64, i64* %149, align 8
  store i64 %150, i64* %RAX.i512, align 8
  %151 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %RCX.i510 = getelementptr inbounds %union.anon, %union.anon* %151, i64 0, i32 0
  %152 = add i64 %150, 8
  %153 = add i64 %146, 7
  store i64 %153, i64* %3, align 8
  %154 = inttoptr i64 %152 to i32*
  %155 = load i32, i32* %154, align 4
  %156 = shl i32 %155, 1
  %157 = icmp slt i32 %155, 0
  %158 = icmp slt i32 %156, 0
  %159 = xor i1 %157, %158
  %160 = zext i32 %156 to i64
  store i64 %160, i64* %RCX.i510, align 8
  %.lobit13 = lshr i32 %155, 31
  %161 = trunc i32 %.lobit13 to i8
  store i8 %161, i8* %38, align 1
  %162 = and i32 %156, 254
  %163 = tail call i32 @llvm.ctpop.i32(i32 %162)
  %164 = trunc i32 %163 to i8
  %165 = and i8 %164, 1
  %166 = xor i8 %165, 1
  store i8 %166, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %167 = icmp eq i32 %156, 0
  %168 = zext i1 %167 to i8
  store i8 %168, i8* %47, align 1
  %169 = lshr i32 %155, 30
  %170 = trunc i32 %169 to i8
  %171 = and i8 %170, 1
  store i8 %171, i8* %50, align 1
  %172 = zext i1 %159 to i8
  store i8 %172, i8* %54, align 1
  %ECX.i504 = bitcast %union.anon* %151 to i32*
  %173 = add i64 %146, 12
  store i64 %173, i64* %3, align 8
  store i32 %156, i32* %154, align 4
  %174 = load i64, i64* %RBP.i, align 8
  %175 = add i64 %174, -8
  %176 = load i64, i64* %3, align 8
  %177 = add i64 %176, 4
  store i64 %177, i64* %3, align 8
  %178 = inttoptr i64 %175 to i64*
  %179 = load i64, i64* %178, align 8
  store i64 %179, i64* %RAX.i512, align 8
  %180 = add i64 %179, 8
  %181 = add i64 %176, 7
  store i64 %181, i64* %3, align 8
  %182 = inttoptr i64 %180 to i32*
  %183 = load i32, i32* %182, align 4
  %184 = or i32 %183, 1
  %185 = zext i32 %184 to i64
  store i64 %185, i64* %RCX.i510, align 8
  store i8 0, i8* %38, align 1
  %186 = and i32 %184, 255
  %187 = tail call i32 @llvm.ctpop.i32(i32 %186)
  %188 = trunc i32 %187 to i8
  %189 = and i8 %188, 1
  %190 = xor i8 %189, 1
  store i8 %190, i8* %40, align 1
  store i8 0, i8* %47, align 1
  %191 = lshr i32 %183, 31
  %192 = trunc i32 %191 to i8
  store i8 %192, i8* %50, align 1
  store i8 0, i8* %54, align 1
  store i8 0, i8* %46, align 1
  %193 = add i64 %176, 13
  store i64 %193, i64* %3, align 8
  store i32 %184, i32* %182, align 4
  %194 = load i64, i64* %RBP.i, align 8
  %195 = add i64 %194, -8
  %196 = load i64, i64* %3, align 8
  %197 = add i64 %196, 4
  store i64 %197, i64* %3, align 8
  %198 = inttoptr i64 %195 to i64*
  %199 = load i64, i64* %198, align 8
  store i64 %199, i64* %RAX.i512, align 8
  %200 = add i64 %199, 12
  %201 = add i64 %196, 7
  store i64 %201, i64* %3, align 8
  %202 = inttoptr i64 %200 to i32*
  %203 = load i32, i32* %202, align 4
  %204 = add i32 %203, -1
  %205 = zext i32 %204 to i64
  store i64 %205, i64* %RCX.i510, align 8
  %206 = add i64 %196, 13
  store i64 %206, i64* %3, align 8
  store i32 %204, i32* %202, align 4
  %207 = load i32, i32* %ECX.i504, align 4
  %208 = load i64, i64* %3, align 8
  store i8 0, i8* %38, align 1
  %209 = and i32 %207, 255
  %210 = tail call i32 @llvm.ctpop.i32(i32 %209)
  %211 = trunc i32 %210 to i8
  %212 = and i8 %211, 1
  %213 = xor i8 %212, 1
  store i8 %213, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %214 = icmp eq i32 %207, 0
  %215 = zext i1 %214 to i8
  store i8 %215, i8* %47, align 1
  %216 = lshr i32 %207, 31
  %217 = trunc i32 %216 to i8
  store i8 %217, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v53 = select i1 %214, i64 9, i64 110
  %218 = add i64 %208, %.v53
  store i64 %218, i64* %3, align 8
  br i1 %214, label %block_401e52, label %block_401e22.block_.L_401eb7_crit_edge

block_401e22.block_.L_401eb7_crit_edge:           ; preds = %block_401e22
  %.pre45 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %.pre47 = bitcast %union.anon* %.pre45 to i8*
  br label %block_.L_401eb7

block_401e52:                                     ; preds = %block_401e22
  %219 = load i64, i64* %RBP.i, align 8
  %220 = add i64 %219, -8
  %221 = add i64 %218, 4
  store i64 %221, i64* %3, align 8
  %222 = inttoptr i64 %220 to i64*
  %223 = load i64, i64* %222, align 8
  store i64 %223, i64* %RAX.i512, align 8
  %224 = add i64 %223, 8
  %225 = add i64 %218, 7
  store i64 %225, i64* %3, align 8
  %226 = inttoptr i64 %224 to i32*
  %227 = load i32, i32* %226, align 4
  %228 = zext i32 %227 to i64
  store i64 %228, i64* %RCX.i510, align 8
  %229 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %DL.i471 = bitcast %union.anon* %229 to i8*
  %230 = trunc i32 %227 to i8
  store i8 %230, i8* %DL.i471, align 1
  %231 = add i64 %218, 13
  store i64 %231, i64* %3, align 8
  %232 = load i64, i64* %222, align 8
  store i64 %232, i64* %RAX.i512, align 8
  %233 = add i64 %232, 24
  %234 = add i64 %218, 17
  store i64 %234, i64* %3, align 8
  %235 = inttoptr i64 %233 to i64*
  %236 = load i64, i64* %235, align 8
  store i64 %236, i64* %RAX.i512, align 8
  %237 = add i64 %218, 21
  store i64 %237, i64* %3, align 8
  %238 = load i64, i64* %222, align 8
  store i64 %238, i64* %RSI.i540, align 8
  %239 = add i64 %238, 32
  %240 = add i64 %218, 25
  store i64 %240, i64* %3, align 8
  %241 = inttoptr i64 %239 to i64*
  %242 = load i64, i64* %241, align 8
  store i64 %242, i64* %RSI.i540, align 8
  %243 = add i64 %218, 27
  store i64 %243, i64* %3, align 8
  %244 = inttoptr i64 %242 to i32*
  %245 = load i32, i32* %244, align 4
  %246 = zext i32 %245 to i64
  store i64 %246, i64* %RCX.i510, align 8
  %247 = add i32 %245, 1
  %248 = zext i32 %247 to i64
  store i64 %248, i64* %RDI.i548, align 8
  %249 = icmp eq i32 %245, -1
  %250 = icmp eq i32 %247, 0
  %251 = or i1 %249, %250
  %252 = zext i1 %251 to i8
  store i8 %252, i8* %38, align 1
  %253 = and i32 %247, 255
  %254 = tail call i32 @llvm.ctpop.i32(i32 %253)
  %255 = trunc i32 %254 to i8
  %256 = and i8 %255, 1
  %257 = xor i8 %256, 1
  store i8 %257, i8* %40, align 1
  %258 = xor i32 %247, %245
  %259 = lshr i32 %258, 4
  %260 = trunc i32 %259 to i8
  %261 = and i8 %260, 1
  store i8 %261, i8* %46, align 1
  %262 = zext i1 %250 to i8
  store i8 %262, i8* %47, align 1
  %263 = lshr i32 %247, 31
  %264 = trunc i32 %263 to i8
  store i8 %264, i8* %50, align 1
  %265 = lshr i32 %245, 31
  %266 = xor i32 %263, %265
  %267 = add nuw nsw i32 %266, %263
  %268 = icmp eq i32 %267, 2
  %269 = zext i1 %268 to i8
  store i8 %269, i8* %54, align 1
  %270 = add i64 %218, 34
  store i64 %270, i64* %3, align 8
  store i32 %247, i32* %244, align 4
  %271 = load i32, i32* %ECX.i504, align 4
  %272 = load i64, i64* %3, align 8
  %273 = sext i32 %271 to i64
  store i64 %273, i64* %RSI.i540, align 8
  %274 = load i64, i64* %RAX.i512, align 8
  %275 = add i64 %274, %273
  %276 = load i8, i8* %DL.i471, align 1
  %277 = add i64 %272, 6
  store i64 %277, i64* %3, align 8
  %278 = inttoptr i64 %275 to i8*
  store i8 %276, i8* %278, align 1
  %279 = load i64, i64* %RBP.i, align 8
  %280 = add i64 %279, -8
  %281 = load i64, i64* %3, align 8
  %282 = add i64 %281, 4
  store i64 %282, i64* %3, align 8
  %283 = inttoptr i64 %280 to i64*
  %284 = load i64, i64* %283, align 8
  store i64 %284, i64* %RAX.i512, align 8
  %285 = add i64 %284, 12
  %286 = add i64 %281, 11
  store i64 %286, i64* %3, align 8
  %287 = inttoptr i64 %285 to i32*
  store i32 8, i32* %287, align 4
  %.pre32 = load i64, i64* %3, align 8
  br label %block_.L_401e85

block_.L_401e85:                                  ; preds = %block_401e93, %block_401e52
  %288 = phi i64 [ %387, %block_401e93 ], [ %.pre32, %block_401e52 ]
  %289 = load i64, i64* %RBP.i, align 8
  %290 = add i64 %289, -8
  %291 = add i64 %288, 4
  store i64 %291, i64* %3, align 8
  %292 = inttoptr i64 %290 to i64*
  %293 = load i64, i64* %292, align 8
  store i64 %293, i64* %RAX.i512, align 8
  %294 = add i64 %293, 80
  %295 = add i64 %288, 8
  store i64 %295, i64* %3, align 8
  %296 = inttoptr i64 %294 to i32*
  %297 = load i32, i32* %296, align 4
  %298 = add i32 %297, -7
  %299 = icmp ult i32 %297, 7
  %300 = zext i1 %299 to i8
  store i8 %300, i8* %38, align 1
  %301 = and i32 %298, 255
  %302 = tail call i32 @llvm.ctpop.i32(i32 %301)
  %303 = trunc i32 %302 to i8
  %304 = and i8 %303, 1
  %305 = xor i8 %304, 1
  store i8 %305, i8* %40, align 1
  %306 = xor i32 %298, %297
  %307 = lshr i32 %306, 4
  %308 = trunc i32 %307 to i8
  %309 = and i8 %308, 1
  store i8 %309, i8* %46, align 1
  %310 = icmp eq i32 %298, 0
  %311 = zext i1 %310 to i8
  store i8 %311, i8* %47, align 1
  %312 = lshr i32 %298, 31
  %313 = trunc i32 %312 to i8
  store i8 %313, i8* %50, align 1
  %314 = lshr i32 %297, 31
  %315 = xor i32 %312, %314
  %316 = add nuw nsw i32 %315, %314
  %317 = icmp eq i32 %316, 2
  %318 = zext i1 %317 to i8
  store i8 %318, i8* %54, align 1
  %319 = icmp ne i8 %313, 0
  %320 = xor i1 %319, %317
  %321 = or i1 %310, %320
  %.v57 = select i1 %321, i64 45, i64 14
  %322 = add i64 %288, %.v57
  store i64 %322, i64* %3, align 8
  br i1 %321, label %block_.L_401eb2, label %block_401e93

block_401e93:                                     ; preds = %block_.L_401e85
  %323 = add i64 %322, 4
  store i64 %323, i64* %3, align 8
  %324 = load i64, i64* %292, align 8
  store i64 %324, i64* %RAX.i512, align 8
  %325 = add i64 %324, 80
  %326 = add i64 %322, 7
  store i64 %326, i64* %3, align 8
  %327 = inttoptr i64 %325 to i32*
  %328 = load i32, i32* %327, align 4
  %329 = add i32 %328, -8
  %330 = zext i32 %329 to i64
  store i64 %330, i64* %RCX.i510, align 8
  %331 = icmp ult i32 %328, 8
  %332 = zext i1 %331 to i8
  store i8 %332, i8* %38, align 1
  %333 = and i32 %329, 255
  %334 = tail call i32 @llvm.ctpop.i32(i32 %333)
  %335 = trunc i32 %334 to i8
  %336 = and i8 %335, 1
  %337 = xor i8 %336, 1
  store i8 %337, i8* %40, align 1
  %338 = xor i32 %329, %328
  %339 = lshr i32 %338, 4
  %340 = trunc i32 %339 to i8
  %341 = and i8 %340, 1
  store i8 %341, i8* %46, align 1
  %342 = icmp eq i32 %329, 0
  %343 = zext i1 %342 to i8
  store i8 %343, i8* %47, align 1
  %344 = lshr i32 %329, 31
  %345 = trunc i32 %344 to i8
  store i8 %345, i8* %50, align 1
  %346 = lshr i32 %328, 31
  %347 = xor i32 %344, %346
  %348 = add nuw nsw i32 %347, %346
  %349 = icmp eq i32 %348, 2
  %350 = zext i1 %349 to i8
  store i8 %350, i8* %54, align 1
  %351 = add i64 %322, 13
  store i64 %351, i64* %3, align 8
  store i32 %329, i32* %327, align 4
  %352 = load i64, i64* %RBP.i, align 8
  %353 = add i64 %352, -8
  %354 = load i64, i64* %3, align 8
  %355 = add i64 %354, 4
  store i64 %355, i64* %3, align 8
  %356 = inttoptr i64 %353 to i64*
  %357 = load i64, i64* %356, align 8
  store i64 %357, i64* %RAX.i512, align 8
  %358 = add i64 %357, 88
  %359 = add i64 %354, 7
  store i64 %359, i64* %3, align 8
  %360 = inttoptr i64 %358 to i32*
  %361 = load i32, i32* %360, align 4
  %362 = add i32 %361, 1
  %363 = zext i32 %362 to i64
  store i64 %363, i64* %RCX.i510, align 8
  %364 = icmp eq i32 %361, -1
  %365 = icmp eq i32 %362, 0
  %366 = or i1 %364, %365
  %367 = zext i1 %366 to i8
  store i8 %367, i8* %38, align 1
  %368 = and i32 %362, 255
  %369 = tail call i32 @llvm.ctpop.i32(i32 %368)
  %370 = trunc i32 %369 to i8
  %371 = and i8 %370, 1
  %372 = xor i8 %371, 1
  store i8 %372, i8* %40, align 1
  %373 = xor i32 %362, %361
  %374 = lshr i32 %373, 4
  %375 = trunc i32 %374 to i8
  %376 = and i8 %375, 1
  store i8 %376, i8* %46, align 1
  %377 = zext i1 %365 to i8
  store i8 %377, i8* %47, align 1
  %378 = lshr i32 %362, 31
  %379 = trunc i32 %378 to i8
  store i8 %379, i8* %50, align 1
  %380 = lshr i32 %361, 31
  %381 = xor i32 %378, %380
  %382 = add nuw nsw i32 %381, %378
  %383 = icmp eq i32 %382, 2
  %384 = zext i1 %383 to i8
  store i8 %384, i8* %54, align 1
  %385 = add i64 %354, 13
  store i64 %385, i64* %3, align 8
  store i32 %362, i32* %360, align 4
  %386 = load i64, i64* %3, align 8
  %387 = add i64 %386, -40
  store i64 %387, i64* %3, align 8
  br label %block_.L_401e85

block_.L_401eb2:                                  ; preds = %block_.L_401e85
  %388 = add i64 %322, 5
  store i64 %388, i64* %3, align 8
  br label %block_.L_401eb7

block_.L_401eb7:                                  ; preds = %block_401e22.block_.L_401eb7_crit_edge, %block_.L_401eb2
  %DL.i347.pre-phi = phi i8* [ %.pre47, %block_401e22.block_.L_401eb7_crit_edge ], [ %DL.i471, %block_.L_401eb2 ]
  %389 = phi i64 [ %218, %block_401e22.block_.L_401eb7_crit_edge ], [ %388, %block_.L_401eb2 ]
  %390 = add i64 %389, 5
  br label %block_.L_401ebc

block_.L_401ebc:                                  ; preds = %block_.L_401f69, %block_.L_401eb7
  %storemerge = phi i64 [ %390, %block_.L_401eb7 ], [ %676, %block_.L_401f69 ]
  %391 = load i64, i64* %RBP.i, align 8
  %392 = add i64 %391, -8
  %393 = add i64 %storemerge, 4
  store i64 %393, i64* %3, align 8
  %394 = inttoptr i64 %392 to i64*
  %395 = load i64, i64* %394, align 8
  store i64 %395, i64* %RAX.i512, align 8
  %396 = add i64 %395, 16
  %397 = add i64 %storemerge, 8
  store i64 %397, i64* %3, align 8
  %398 = inttoptr i64 %396 to i32*
  %399 = load i32, i32* %398, align 4
  store i8 0, i8* %38, align 1
  %400 = and i32 %399, 255
  %401 = tail call i32 @llvm.ctpop.i32(i32 %400)
  %402 = trunc i32 %401 to i8
  %403 = and i8 %402, 1
  %404 = xor i8 %403, 1
  store i8 %404, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %405 = icmp eq i32 %399, 0
  %406 = zext i1 %405 to i8
  store i8 %406, i8* %47, align 1
  %407 = lshr i32 %399, 31
  %408 = trunc i32 %407 to i8
  store i8 %408, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v54 = select i1 %405, i64 178, i64 14
  %409 = add i64 %storemerge, %.v54
  store i64 %409, i64* %3, align 8
  br i1 %405, label %block_.L_401f6e, label %block_401eca

block_401eca:                                     ; preds = %block_.L_401ebc
  %410 = add i64 %409, 4
  store i64 %410, i64* %3, align 8
  %411 = load i64, i64* %394, align 8
  store i64 %411, i64* %RAX.i512, align 8
  %412 = add i64 %411, 16
  %413 = add i64 %409, 7
  store i64 %413, i64* %3, align 8
  %414 = inttoptr i64 %412 to i32*
  %415 = load i32, i32* %414, align 4
  %416 = add i32 %415, -1
  %417 = zext i32 %416 to i64
  store i64 %417, i64* %RCX.i510, align 8
  %418 = icmp ne i32 %415, 0
  %419 = zext i1 %418 to i8
  store i8 %419, i8* %38, align 1
  %420 = and i32 %416, 255
  %421 = tail call i32 @llvm.ctpop.i32(i32 %420)
  %422 = trunc i32 %421 to i8
  %423 = and i8 %422, 1
  %424 = xor i8 %423, 1
  store i8 %424, i8* %40, align 1
  %425 = xor i32 %415, 16
  %426 = xor i32 %425, %416
  %427 = lshr i32 %426, 4
  %428 = trunc i32 %427 to i8
  %429 = and i8 %428, 1
  store i8 %429, i8* %46, align 1
  %430 = icmp eq i32 %416, 0
  %431 = zext i1 %430 to i8
  store i8 %431, i8* %47, align 1
  %432 = lshr i32 %416, 31
  %433 = trunc i32 %432 to i8
  store i8 %433, i8* %50, align 1
  %434 = lshr i32 %415, 31
  %435 = xor i32 %432, %434
  %436 = xor i32 %432, 1
  %437 = add nuw nsw i32 %435, %436
  %438 = icmp eq i32 %437, 2
  %439 = zext i1 %438 to i8
  store i8 %439, i8* %54, align 1
  %440 = add i64 %409, 13
  store i64 %440, i64* %3, align 8
  store i32 %416, i32* %414, align 4
  %441 = load i64, i64* %RBP.i, align 8
  %442 = add i64 %441, -8
  %443 = load i64, i64* %3, align 8
  %444 = add i64 %443, 4
  store i64 %444, i64* %3, align 8
  %445 = inttoptr i64 %442 to i64*
  %446 = load i64, i64* %445, align 8
  store i64 %446, i64* %RAX.i512, align 8
  %447 = add i64 %446, 8
  %448 = add i64 %443, 7
  store i64 %448, i64* %3, align 8
  %449 = inttoptr i64 %447 to i32*
  %450 = load i32, i32* %449, align 4
  %451 = shl i32 %450, 1
  %452 = icmp slt i32 %450, 0
  %453 = icmp slt i32 %451, 0
  %454 = xor i1 %452, %453
  %455 = zext i32 %451 to i64
  store i64 %455, i64* %RCX.i510, align 8
  %.lobit14 = lshr i32 %450, 31
  %456 = trunc i32 %.lobit14 to i8
  store i8 %456, i8* %38, align 1
  %457 = and i32 %451, 254
  %458 = tail call i32 @llvm.ctpop.i32(i32 %457)
  %459 = trunc i32 %458 to i8
  %460 = and i8 %459, 1
  %461 = xor i8 %460, 1
  store i8 %461, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %462 = icmp eq i32 %451, 0
  %463 = zext i1 %462 to i8
  store i8 %463, i8* %47, align 1
  %464 = lshr i32 %450, 30
  %465 = trunc i32 %464 to i8
  %466 = and i8 %465, 1
  store i8 %466, i8* %50, align 1
  %467 = zext i1 %454 to i8
  store i8 %467, i8* %54, align 1
  %468 = add i64 %443, 12
  store i64 %468, i64* %3, align 8
  store i32 %451, i32* %449, align 4
  %469 = load i64, i64* %RBP.i, align 8
  %470 = add i64 %469, -8
  %471 = load i64, i64* %3, align 8
  %472 = add i64 %471, 4
  store i64 %472, i64* %3, align 8
  %473 = inttoptr i64 %470 to i64*
  %474 = load i64, i64* %473, align 8
  store i64 %474, i64* %RAX.i512, align 8
  %475 = add i64 %474, 8
  %476 = add i64 %471, 7
  store i64 %476, i64* %3, align 8
  %477 = inttoptr i64 %475 to i32*
  %478 = load i32, i32* %477, align 4
  %479 = zext i32 %478 to i64
  store i64 %479, i64* %RCX.i510, align 8
  %480 = add i64 %471, 10
  store i64 %480, i64* %3, align 8
  store i32 %478, i32* %477, align 4
  %481 = load i64, i64* %RBP.i, align 8
  %482 = add i64 %481, -8
  %483 = load i64, i64* %3, align 8
  %484 = add i64 %483, 4
  store i64 %484, i64* %3, align 8
  %485 = inttoptr i64 %482 to i64*
  %486 = load i64, i64* %485, align 8
  store i64 %486, i64* %RAX.i512, align 8
  %487 = add i64 %486, 12
  %488 = add i64 %483, 7
  store i64 %488, i64* %3, align 8
  %489 = inttoptr i64 %487 to i32*
  %490 = load i32, i32* %489, align 4
  %491 = add i32 %490, -1
  %492 = zext i32 %491 to i64
  store i64 %492, i64* %RCX.i510, align 8
  %493 = add i64 %483, 13
  store i64 %493, i64* %3, align 8
  store i32 %491, i32* %489, align 4
  %494 = load i32, i32* %ECX.i504, align 4
  %495 = load i64, i64* %3, align 8
  store i8 0, i8* %38, align 1
  %496 = and i32 %494, 255
  %497 = tail call i32 @llvm.ctpop.i32(i32 %496)
  %498 = trunc i32 %497 to i8
  %499 = and i8 %498, 1
  %500 = xor i8 %499, 1
  store i8 %500, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %501 = icmp eq i32 %494, 0
  %502 = zext i1 %501 to i8
  store i8 %502, i8* %47, align 1
  %503 = lshr i32 %494, 31
  %504 = trunc i32 %503 to i8
  store i8 %504, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v55 = select i1 %501, i64 9, i64 110
  %505 = add i64 %495, %.v55
  store i64 %505, i64* %3, align 8
  br i1 %501, label %block_401f04, label %block_.L_401f69

block_401f04:                                     ; preds = %block_401eca
  %506 = load i64, i64* %RBP.i, align 8
  %507 = add i64 %506, -8
  %508 = add i64 %505, 4
  store i64 %508, i64* %3, align 8
  %509 = inttoptr i64 %507 to i64*
  %510 = load i64, i64* %509, align 8
  store i64 %510, i64* %RAX.i512, align 8
  %511 = add i64 %510, 8
  %512 = add i64 %505, 7
  store i64 %512, i64* %3, align 8
  %513 = inttoptr i64 %511 to i32*
  %514 = load i32, i32* %513, align 4
  %515 = zext i32 %514 to i64
  store i64 %515, i64* %RCX.i510, align 8
  %516 = trunc i32 %514 to i8
  store i8 %516, i8* %DL.i347.pre-phi, align 1
  %517 = add i64 %505, 13
  store i64 %517, i64* %3, align 8
  %518 = load i64, i64* %509, align 8
  store i64 %518, i64* %RAX.i512, align 8
  %519 = add i64 %518, 24
  %520 = add i64 %505, 17
  store i64 %520, i64* %3, align 8
  %521 = inttoptr i64 %519 to i64*
  %522 = load i64, i64* %521, align 8
  store i64 %522, i64* %RAX.i512, align 8
  %523 = add i64 %505, 21
  store i64 %523, i64* %3, align 8
  %524 = load i64, i64* %509, align 8
  store i64 %524, i64* %RSI.i540, align 8
  %525 = add i64 %524, 32
  %526 = add i64 %505, 25
  store i64 %526, i64* %3, align 8
  %527 = inttoptr i64 %525 to i64*
  %528 = load i64, i64* %527, align 8
  store i64 %528, i64* %RSI.i540, align 8
  %529 = add i64 %505, 27
  store i64 %529, i64* %3, align 8
  %530 = inttoptr i64 %528 to i32*
  %531 = load i32, i32* %530, align 4
  %532 = zext i32 %531 to i64
  store i64 %532, i64* %RCX.i510, align 8
  %533 = add i32 %531, 1
  %534 = zext i32 %533 to i64
  store i64 %534, i64* %RDI.i548, align 8
  %535 = icmp eq i32 %531, -1
  %536 = icmp eq i32 %533, 0
  %537 = or i1 %535, %536
  %538 = zext i1 %537 to i8
  store i8 %538, i8* %38, align 1
  %539 = and i32 %533, 255
  %540 = tail call i32 @llvm.ctpop.i32(i32 %539)
  %541 = trunc i32 %540 to i8
  %542 = and i8 %541, 1
  %543 = xor i8 %542, 1
  store i8 %543, i8* %40, align 1
  %544 = xor i32 %533, %531
  %545 = lshr i32 %544, 4
  %546 = trunc i32 %545 to i8
  %547 = and i8 %546, 1
  store i8 %547, i8* %46, align 1
  %548 = zext i1 %536 to i8
  store i8 %548, i8* %47, align 1
  %549 = lshr i32 %533, 31
  %550 = trunc i32 %549 to i8
  store i8 %550, i8* %50, align 1
  %551 = lshr i32 %531, 31
  %552 = xor i32 %549, %551
  %553 = add nuw nsw i32 %552, %549
  %554 = icmp eq i32 %553, 2
  %555 = zext i1 %554 to i8
  store i8 %555, i8* %54, align 1
  %556 = add i64 %505, 34
  store i64 %556, i64* %3, align 8
  store i32 %533, i32* %530, align 4
  %557 = load i32, i32* %ECX.i504, align 4
  %558 = load i64, i64* %3, align 8
  %559 = sext i32 %557 to i64
  store i64 %559, i64* %RSI.i540, align 8
  %560 = load i64, i64* %RAX.i512, align 8
  %561 = add i64 %560, %559
  %562 = load i8, i8* %DL.i347.pre-phi, align 1
  %563 = add i64 %558, 6
  store i64 %563, i64* %3, align 8
  %564 = inttoptr i64 %561 to i8*
  store i8 %562, i8* %564, align 1
  %565 = load i64, i64* %RBP.i, align 8
  %566 = add i64 %565, -8
  %567 = load i64, i64* %3, align 8
  %568 = add i64 %567, 4
  store i64 %568, i64* %3, align 8
  %569 = inttoptr i64 %566 to i64*
  %570 = load i64, i64* %569, align 8
  store i64 %570, i64* %RAX.i512, align 8
  %571 = add i64 %570, 12
  %572 = add i64 %567, 11
  store i64 %572, i64* %3, align 8
  %573 = inttoptr i64 %571 to i32*
  store i32 8, i32* %573, align 4
  %.pre33 = load i64, i64* %3, align 8
  br label %block_.L_401f37

block_.L_401f37:                                  ; preds = %block_401f45, %block_401f04
  %574 = phi i64 [ %673, %block_401f45 ], [ %.pre33, %block_401f04 ]
  %575 = load i64, i64* %RBP.i, align 8
  %576 = add i64 %575, -8
  %577 = add i64 %574, 4
  store i64 %577, i64* %3, align 8
  %578 = inttoptr i64 %576 to i64*
  %579 = load i64, i64* %578, align 8
  store i64 %579, i64* %RAX.i512, align 8
  %580 = add i64 %579, 80
  %581 = add i64 %574, 8
  store i64 %581, i64* %3, align 8
  %582 = inttoptr i64 %580 to i32*
  %583 = load i32, i32* %582, align 4
  %584 = add i32 %583, -7
  %585 = icmp ult i32 %583, 7
  %586 = zext i1 %585 to i8
  store i8 %586, i8* %38, align 1
  %587 = and i32 %584, 255
  %588 = tail call i32 @llvm.ctpop.i32(i32 %587)
  %589 = trunc i32 %588 to i8
  %590 = and i8 %589, 1
  %591 = xor i8 %590, 1
  store i8 %591, i8* %40, align 1
  %592 = xor i32 %584, %583
  %593 = lshr i32 %592, 4
  %594 = trunc i32 %593 to i8
  %595 = and i8 %594, 1
  store i8 %595, i8* %46, align 1
  %596 = icmp eq i32 %584, 0
  %597 = zext i1 %596 to i8
  store i8 %597, i8* %47, align 1
  %598 = lshr i32 %584, 31
  %599 = trunc i32 %598 to i8
  store i8 %599, i8* %50, align 1
  %600 = lshr i32 %583, 31
  %601 = xor i32 %598, %600
  %602 = add nuw nsw i32 %601, %600
  %603 = icmp eq i32 %602, 2
  %604 = zext i1 %603 to i8
  store i8 %604, i8* %54, align 1
  %605 = icmp ne i8 %599, 0
  %606 = xor i1 %605, %603
  %607 = or i1 %596, %606
  %.v56 = select i1 %607, i64 45, i64 14
  %608 = add i64 %574, %.v56
  store i64 %608, i64* %3, align 8
  br i1 %607, label %block_.L_401f64, label %block_401f45

block_401f45:                                     ; preds = %block_.L_401f37
  %609 = add i64 %608, 4
  store i64 %609, i64* %3, align 8
  %610 = load i64, i64* %578, align 8
  store i64 %610, i64* %RAX.i512, align 8
  %611 = add i64 %610, 80
  %612 = add i64 %608, 7
  store i64 %612, i64* %3, align 8
  %613 = inttoptr i64 %611 to i32*
  %614 = load i32, i32* %613, align 4
  %615 = add i32 %614, -8
  %616 = zext i32 %615 to i64
  store i64 %616, i64* %RCX.i510, align 8
  %617 = icmp ult i32 %614, 8
  %618 = zext i1 %617 to i8
  store i8 %618, i8* %38, align 1
  %619 = and i32 %615, 255
  %620 = tail call i32 @llvm.ctpop.i32(i32 %619)
  %621 = trunc i32 %620 to i8
  %622 = and i8 %621, 1
  %623 = xor i8 %622, 1
  store i8 %623, i8* %40, align 1
  %624 = xor i32 %615, %614
  %625 = lshr i32 %624, 4
  %626 = trunc i32 %625 to i8
  %627 = and i8 %626, 1
  store i8 %627, i8* %46, align 1
  %628 = icmp eq i32 %615, 0
  %629 = zext i1 %628 to i8
  store i8 %629, i8* %47, align 1
  %630 = lshr i32 %615, 31
  %631 = trunc i32 %630 to i8
  store i8 %631, i8* %50, align 1
  %632 = lshr i32 %614, 31
  %633 = xor i32 %630, %632
  %634 = add nuw nsw i32 %633, %632
  %635 = icmp eq i32 %634, 2
  %636 = zext i1 %635 to i8
  store i8 %636, i8* %54, align 1
  %637 = add i64 %608, 13
  store i64 %637, i64* %3, align 8
  store i32 %615, i32* %613, align 4
  %638 = load i64, i64* %RBP.i, align 8
  %639 = add i64 %638, -8
  %640 = load i64, i64* %3, align 8
  %641 = add i64 %640, 4
  store i64 %641, i64* %3, align 8
  %642 = inttoptr i64 %639 to i64*
  %643 = load i64, i64* %642, align 8
  store i64 %643, i64* %RAX.i512, align 8
  %644 = add i64 %643, 88
  %645 = add i64 %640, 7
  store i64 %645, i64* %3, align 8
  %646 = inttoptr i64 %644 to i32*
  %647 = load i32, i32* %646, align 4
  %648 = add i32 %647, 1
  %649 = zext i32 %648 to i64
  store i64 %649, i64* %RCX.i510, align 8
  %650 = icmp eq i32 %647, -1
  %651 = icmp eq i32 %648, 0
  %652 = or i1 %650, %651
  %653 = zext i1 %652 to i8
  store i8 %653, i8* %38, align 1
  %654 = and i32 %648, 255
  %655 = tail call i32 @llvm.ctpop.i32(i32 %654)
  %656 = trunc i32 %655 to i8
  %657 = and i8 %656, 1
  %658 = xor i8 %657, 1
  store i8 %658, i8* %40, align 1
  %659 = xor i32 %648, %647
  %660 = lshr i32 %659, 4
  %661 = trunc i32 %660 to i8
  %662 = and i8 %661, 1
  store i8 %662, i8* %46, align 1
  %663 = zext i1 %651 to i8
  store i8 %663, i8* %47, align 1
  %664 = lshr i32 %648, 31
  %665 = trunc i32 %664 to i8
  store i8 %665, i8* %50, align 1
  %666 = lshr i32 %647, 31
  %667 = xor i32 %664, %666
  %668 = add nuw nsw i32 %667, %664
  %669 = icmp eq i32 %668, 2
  %670 = zext i1 %669 to i8
  store i8 %670, i8* %54, align 1
  %671 = add i64 %640, 13
  store i64 %671, i64* %3, align 8
  store i32 %648, i32* %646, align 4
  %672 = load i64, i64* %3, align 8
  %673 = add i64 %672, -40
  store i64 %673, i64* %3, align 8
  br label %block_.L_401f37

block_.L_401f64:                                  ; preds = %block_.L_401f37
  %674 = add i64 %608, 5
  store i64 %674, i64* %3, align 8
  br label %block_.L_401f69

block_.L_401f69:                                  ; preds = %block_401eca, %block_.L_401f64
  %675 = phi i64 [ %674, %block_.L_401f64 ], [ %505, %block_401eca ]
  %676 = add i64 %675, -173
  %677 = add i64 %675, 5
  store i64 %677, i64* %3, align 8
  br label %block_.L_401ebc

block_.L_401f6e:                                  ; preds = %block_.L_401ebc
  %678 = add i64 %391, -16
  %679 = add i64 %409, 3
  store i64 %679, i64* %3, align 8
  %680 = inttoptr i64 %678 to i32*
  %681 = load i32, i32* %680, align 4
  %682 = add i32 %681, -1024
  %683 = zext i32 %682 to i64
  store i64 %683, i64* %RAX.i512, align 8
  %684 = icmp ult i32 %681, 1024
  %685 = zext i1 %684 to i8
  store i8 %685, i8* %38, align 1
  %686 = and i32 %682, 255
  %687 = tail call i32 @llvm.ctpop.i32(i32 %686)
  %688 = trunc i32 %687 to i8
  %689 = and i8 %688, 1
  %690 = xor i8 %689, 1
  store i8 %690, i8* %40, align 1
  %691 = xor i32 %682, %681
  %692 = lshr i32 %691, 4
  %693 = trunc i32 %692 to i8
  %694 = and i8 %693, 1
  store i8 %694, i8* %46, align 1
  %695 = icmp eq i32 %682, 0
  %696 = zext i1 %695 to i8
  store i8 %696, i8* %47, align 1
  %697 = lshr i32 %682, 31
  %698 = trunc i32 %697 to i8
  store i8 %698, i8* %50, align 1
  %699 = lshr i32 %681, 31
  %700 = xor i32 %697, %699
  %701 = add nuw nsw i32 %700, %699
  %702 = icmp eq i32 %701, 2
  %703 = zext i1 %702 to i8
  store i8 %703, i8* %54, align 1
  %704 = add i64 %409, 11
  store i64 %704, i64* %3, align 8
  store i32 %682, i32* %680, align 4
  %705 = load i64, i64* %3, align 8
  %706 = add i64 %705, 385
  br label %block_.L_4020fa

block_.L_401f7e:                                  ; preds = %block_.L_401e15
  %707 = add i64 %146, 7
  store i64 %707, i64* %3, align 8
  %708 = load i32, i32* %123, align 4
  %709 = add i32 %708, -512
  %710 = icmp ult i32 %708, 512
  %711 = zext i1 %710 to i8
  store i8 %711, i8* %38, align 1
  %712 = and i32 %709, 255
  %713 = tail call i32 @llvm.ctpop.i32(i32 %712)
  %714 = trunc i32 %713 to i8
  %715 = and i8 %714, 1
  %716 = xor i8 %715, 1
  store i8 %716, i8* %40, align 1
  %717 = xor i32 %709, %708
  %718 = lshr i32 %717, 4
  %719 = trunc i32 %718 to i8
  %720 = and i8 %719, 1
  store i8 %720, i8* %46, align 1
  %721 = icmp eq i32 %709, 0
  %722 = zext i1 %721 to i8
  store i8 %722, i8* %47, align 1
  %723 = lshr i32 %709, 31
  %724 = trunc i32 %723 to i8
  store i8 %724, i8* %50, align 1
  %725 = lshr i32 %708, 31
  %726 = xor i32 %723, %725
  %727 = add nuw nsw i32 %726, %725
  %728 = icmp eq i32 %727, 2
  %729 = zext i1 %728 to i8
  store i8 %729, i8* %54, align 1
  %.v = select i1 %710, i64 13, i64 350
  %730 = add i64 %146, %.v
  %RAX.i38 = getelementptr inbounds %union.anon, %union.anon* %11, i64 0, i32 0
  %731 = add i64 %120, -8
  %732 = add i64 %730, 4
  store i64 %732, i64* %3, align 8
  %733 = inttoptr i64 %731 to i64*
  %734 = load i64, i64* %733, align 8
  store i64 %734, i64* %RAX.i38, align 8
  %735 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %RCX.i36 = getelementptr inbounds %union.anon, %union.anon* %735, i64 0, i32 0
  br i1 %710, label %block_401f8b, label %block_.L_4020dc

block_401f8b:                                     ; preds = %block_.L_401f7e
  %736 = add i64 %734, 8
  %737 = add i64 %730, 7
  store i64 %737, i64* %3, align 8
  %738 = inttoptr i64 %736 to i32*
  %739 = load i32, i32* %738, align 4
  %740 = shl i32 %739, 1
  %741 = icmp slt i32 %739, 0
  %742 = icmp slt i32 %740, 0
  %743 = xor i1 %741, %742
  %744 = zext i32 %740 to i64
  store i64 %744, i64* %RCX.i36, align 8
  %.lobit16 = lshr i32 %739, 31
  %745 = trunc i32 %.lobit16 to i8
  store i8 %745, i8* %38, align 1
  %746 = and i32 %740, 254
  %747 = tail call i32 @llvm.ctpop.i32(i32 %746)
  %748 = trunc i32 %747 to i8
  %749 = and i8 %748, 1
  %750 = xor i8 %749, 1
  store i8 %750, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %751 = icmp eq i32 %740, 0
  %752 = zext i1 %751 to i8
  store i8 %752, i8* %47, align 1
  %753 = lshr i32 %739, 30
  %754 = trunc i32 %753 to i8
  %755 = and i8 %754, 1
  store i8 %755, i8* %50, align 1
  %756 = zext i1 %743 to i8
  store i8 %756, i8* %54, align 1
  %ECX.i259 = bitcast %union.anon* %735 to i32*
  %757 = add i64 %730, 12
  store i64 %757, i64* %3, align 8
  store i32 %740, i32* %738, align 4
  %758 = load i64, i64* %RBP.i, align 8
  %759 = add i64 %758, -8
  %760 = load i64, i64* %3, align 8
  %761 = add i64 %760, 4
  store i64 %761, i64* %3, align 8
  %762 = inttoptr i64 %759 to i64*
  %763 = load i64, i64* %762, align 8
  store i64 %763, i64* %RAX.i38, align 8
  %764 = add i64 %763, 8
  %765 = add i64 %760, 7
  store i64 %765, i64* %3, align 8
  %766 = inttoptr i64 %764 to i32*
  %767 = load i32, i32* %766, align 4
  %768 = zext i32 %767 to i64
  store i64 %768, i64* %RCX.i36, align 8
  %769 = add i64 %760, 10
  store i64 %769, i64* %3, align 8
  store i32 %767, i32* %766, align 4
  %770 = load i64, i64* %RBP.i, align 8
  %771 = add i64 %770, -8
  %772 = load i64, i64* %3, align 8
  %773 = add i64 %772, 4
  store i64 %773, i64* %3, align 8
  %774 = inttoptr i64 %771 to i64*
  %775 = load i64, i64* %774, align 8
  store i64 %775, i64* %RAX.i38, align 8
  %776 = add i64 %775, 12
  %777 = add i64 %772, 7
  store i64 %777, i64* %3, align 8
  %778 = inttoptr i64 %776 to i32*
  %779 = load i32, i32* %778, align 4
  %780 = add i32 %779, -1
  %781 = zext i32 %780 to i64
  store i64 %781, i64* %RCX.i36, align 8
  %782 = add i64 %772, 13
  store i64 %782, i64* %3, align 8
  store i32 %780, i32* %778, align 4
  %783 = load i32, i32* %ECX.i259, align 4
  %784 = load i64, i64* %3, align 8
  store i8 0, i8* %38, align 1
  %785 = and i32 %783, 255
  %786 = tail call i32 @llvm.ctpop.i32(i32 %785)
  %787 = trunc i32 %786 to i8
  %788 = and i8 %787, 1
  %789 = xor i8 %788, 1
  store i8 %789, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %790 = icmp eq i32 %783, 0
  %791 = zext i1 %790 to i8
  store i8 %791, i8* %47, align 1
  %792 = lshr i32 %783, 31
  %793 = trunc i32 %792 to i8
  store i8 %793, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v58 = select i1 %790, i64 9, i64 110
  %794 = add i64 %784, %.v58
  store i64 %794, i64* %3, align 8
  br i1 %790, label %block_401fb8, label %block_401f8b.block_.L_40201d_crit_edge

block_401f8b.block_.L_40201d_crit_edge:           ; preds = %block_401f8b
  %.pre39 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %.pre40 = bitcast %union.anon* %.pre39 to i8*
  br label %block_.L_40201d

block_401fb8:                                     ; preds = %block_401f8b
  %795 = load i64, i64* %RBP.i, align 8
  %796 = add i64 %795, -8
  %797 = add i64 %794, 4
  store i64 %797, i64* %3, align 8
  %798 = inttoptr i64 %796 to i64*
  %799 = load i64, i64* %798, align 8
  store i64 %799, i64* %RAX.i38, align 8
  %800 = add i64 %799, 8
  %801 = add i64 %794, 7
  store i64 %801, i64* %3, align 8
  %802 = inttoptr i64 %800 to i32*
  %803 = load i32, i32* %802, align 4
  %804 = zext i32 %803 to i64
  store i64 %804, i64* %RCX.i36, align 8
  %805 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %DL.i228 = bitcast %union.anon* %805 to i8*
  %806 = trunc i32 %803 to i8
  store i8 %806, i8* %DL.i228, align 1
  %807 = add i64 %794, 13
  store i64 %807, i64* %3, align 8
  %808 = load i64, i64* %798, align 8
  store i64 %808, i64* %RAX.i38, align 8
  %809 = add i64 %808, 24
  %810 = add i64 %794, 17
  store i64 %810, i64* %3, align 8
  %811 = inttoptr i64 %809 to i64*
  %812 = load i64, i64* %811, align 8
  store i64 %812, i64* %RAX.i38, align 8
  %813 = add i64 %794, 21
  store i64 %813, i64* %3, align 8
  %814 = load i64, i64* %798, align 8
  store i64 %814, i64* %RSI.i540, align 8
  %815 = add i64 %814, 32
  %816 = add i64 %794, 25
  store i64 %816, i64* %3, align 8
  %817 = inttoptr i64 %815 to i64*
  %818 = load i64, i64* %817, align 8
  store i64 %818, i64* %RSI.i540, align 8
  %819 = add i64 %794, 27
  store i64 %819, i64* %3, align 8
  %820 = inttoptr i64 %818 to i32*
  %821 = load i32, i32* %820, align 4
  %822 = zext i32 %821 to i64
  store i64 %822, i64* %RCX.i36, align 8
  %823 = add i32 %821, 1
  %824 = zext i32 %823 to i64
  store i64 %824, i64* %RDI.i548, align 8
  %825 = icmp eq i32 %821, -1
  %826 = icmp eq i32 %823, 0
  %827 = or i1 %825, %826
  %828 = zext i1 %827 to i8
  store i8 %828, i8* %38, align 1
  %829 = and i32 %823, 255
  %830 = tail call i32 @llvm.ctpop.i32(i32 %829)
  %831 = trunc i32 %830 to i8
  %832 = and i8 %831, 1
  %833 = xor i8 %832, 1
  store i8 %833, i8* %40, align 1
  %834 = xor i32 %823, %821
  %835 = lshr i32 %834, 4
  %836 = trunc i32 %835 to i8
  %837 = and i8 %836, 1
  store i8 %837, i8* %46, align 1
  %838 = zext i1 %826 to i8
  store i8 %838, i8* %47, align 1
  %839 = lshr i32 %823, 31
  %840 = trunc i32 %839 to i8
  store i8 %840, i8* %50, align 1
  %841 = lshr i32 %821, 31
  %842 = xor i32 %839, %841
  %843 = add nuw nsw i32 %842, %839
  %844 = icmp eq i32 %843, 2
  %845 = zext i1 %844 to i8
  store i8 %845, i8* %54, align 1
  %846 = add i64 %794, 34
  store i64 %846, i64* %3, align 8
  store i32 %823, i32* %820, align 4
  %847 = load i32, i32* %ECX.i259, align 4
  %848 = load i64, i64* %3, align 8
  %849 = sext i32 %847 to i64
  store i64 %849, i64* %RSI.i540, align 8
  %850 = load i64, i64* %RAX.i38, align 8
  %851 = add i64 %850, %849
  %852 = load i8, i8* %DL.i228, align 1
  %853 = add i64 %848, 6
  store i64 %853, i64* %3, align 8
  %854 = inttoptr i64 %851 to i8*
  store i8 %852, i8* %854, align 1
  %855 = load i64, i64* %RBP.i, align 8
  %856 = add i64 %855, -8
  %857 = load i64, i64* %3, align 8
  %858 = add i64 %857, 4
  store i64 %858, i64* %3, align 8
  %859 = inttoptr i64 %856 to i64*
  %860 = load i64, i64* %859, align 8
  store i64 %860, i64* %RAX.i38, align 8
  %861 = add i64 %860, 12
  %862 = add i64 %857, 11
  store i64 %862, i64* %3, align 8
  %863 = inttoptr i64 %861 to i32*
  store i32 8, i32* %863, align 4
  %.pre34 = load i64, i64* %3, align 8
  br label %block_.L_401feb

block_.L_401feb:                                  ; preds = %block_401ff9, %block_401fb8
  %864 = phi i64 [ %963, %block_401ff9 ], [ %.pre34, %block_401fb8 ]
  %865 = load i64, i64* %RBP.i, align 8
  %866 = add i64 %865, -8
  %867 = add i64 %864, 4
  store i64 %867, i64* %3, align 8
  %868 = inttoptr i64 %866 to i64*
  %869 = load i64, i64* %868, align 8
  store i64 %869, i64* %RAX.i38, align 8
  %870 = add i64 %869, 80
  %871 = add i64 %864, 8
  store i64 %871, i64* %3, align 8
  %872 = inttoptr i64 %870 to i32*
  %873 = load i32, i32* %872, align 4
  %874 = add i32 %873, -7
  %875 = icmp ult i32 %873, 7
  %876 = zext i1 %875 to i8
  store i8 %876, i8* %38, align 1
  %877 = and i32 %874, 255
  %878 = tail call i32 @llvm.ctpop.i32(i32 %877)
  %879 = trunc i32 %878 to i8
  %880 = and i8 %879, 1
  %881 = xor i8 %880, 1
  store i8 %881, i8* %40, align 1
  %882 = xor i32 %874, %873
  %883 = lshr i32 %882, 4
  %884 = trunc i32 %883 to i8
  %885 = and i8 %884, 1
  store i8 %885, i8* %46, align 1
  %886 = icmp eq i32 %874, 0
  %887 = zext i1 %886 to i8
  store i8 %887, i8* %47, align 1
  %888 = lshr i32 %874, 31
  %889 = trunc i32 %888 to i8
  store i8 %889, i8* %50, align 1
  %890 = lshr i32 %873, 31
  %891 = xor i32 %888, %890
  %892 = add nuw nsw i32 %891, %890
  %893 = icmp eq i32 %892, 2
  %894 = zext i1 %893 to i8
  store i8 %894, i8* %54, align 1
  %895 = icmp ne i8 %889, 0
  %896 = xor i1 %895, %893
  %897 = or i1 %886, %896
  %.v62 = select i1 %897, i64 45, i64 14
  %898 = add i64 %864, %.v62
  store i64 %898, i64* %3, align 8
  br i1 %897, label %block_.L_402018, label %block_401ff9

block_401ff9:                                     ; preds = %block_.L_401feb
  %899 = add i64 %898, 4
  store i64 %899, i64* %3, align 8
  %900 = load i64, i64* %868, align 8
  store i64 %900, i64* %RAX.i38, align 8
  %901 = add i64 %900, 80
  %902 = add i64 %898, 7
  store i64 %902, i64* %3, align 8
  %903 = inttoptr i64 %901 to i32*
  %904 = load i32, i32* %903, align 4
  %905 = add i32 %904, -8
  %906 = zext i32 %905 to i64
  store i64 %906, i64* %RCX.i36, align 8
  %907 = icmp ult i32 %904, 8
  %908 = zext i1 %907 to i8
  store i8 %908, i8* %38, align 1
  %909 = and i32 %905, 255
  %910 = tail call i32 @llvm.ctpop.i32(i32 %909)
  %911 = trunc i32 %910 to i8
  %912 = and i8 %911, 1
  %913 = xor i8 %912, 1
  store i8 %913, i8* %40, align 1
  %914 = xor i32 %905, %904
  %915 = lshr i32 %914, 4
  %916 = trunc i32 %915 to i8
  %917 = and i8 %916, 1
  store i8 %917, i8* %46, align 1
  %918 = icmp eq i32 %905, 0
  %919 = zext i1 %918 to i8
  store i8 %919, i8* %47, align 1
  %920 = lshr i32 %905, 31
  %921 = trunc i32 %920 to i8
  store i8 %921, i8* %50, align 1
  %922 = lshr i32 %904, 31
  %923 = xor i32 %920, %922
  %924 = add nuw nsw i32 %923, %922
  %925 = icmp eq i32 %924, 2
  %926 = zext i1 %925 to i8
  store i8 %926, i8* %54, align 1
  %927 = add i64 %898, 13
  store i64 %927, i64* %3, align 8
  store i32 %905, i32* %903, align 4
  %928 = load i64, i64* %RBP.i, align 8
  %929 = add i64 %928, -8
  %930 = load i64, i64* %3, align 8
  %931 = add i64 %930, 4
  store i64 %931, i64* %3, align 8
  %932 = inttoptr i64 %929 to i64*
  %933 = load i64, i64* %932, align 8
  store i64 %933, i64* %RAX.i38, align 8
  %934 = add i64 %933, 88
  %935 = add i64 %930, 7
  store i64 %935, i64* %3, align 8
  %936 = inttoptr i64 %934 to i32*
  %937 = load i32, i32* %936, align 4
  %938 = add i32 %937, 1
  %939 = zext i32 %938 to i64
  store i64 %939, i64* %RCX.i36, align 8
  %940 = icmp eq i32 %937, -1
  %941 = icmp eq i32 %938, 0
  %942 = or i1 %940, %941
  %943 = zext i1 %942 to i8
  store i8 %943, i8* %38, align 1
  %944 = and i32 %938, 255
  %945 = tail call i32 @llvm.ctpop.i32(i32 %944)
  %946 = trunc i32 %945 to i8
  %947 = and i8 %946, 1
  %948 = xor i8 %947, 1
  store i8 %948, i8* %40, align 1
  %949 = xor i32 %938, %937
  %950 = lshr i32 %949, 4
  %951 = trunc i32 %950 to i8
  %952 = and i8 %951, 1
  store i8 %952, i8* %46, align 1
  %953 = zext i1 %941 to i8
  store i8 %953, i8* %47, align 1
  %954 = lshr i32 %938, 31
  %955 = trunc i32 %954 to i8
  store i8 %955, i8* %50, align 1
  %956 = lshr i32 %937, 31
  %957 = xor i32 %954, %956
  %958 = add nuw nsw i32 %957, %954
  %959 = icmp eq i32 %958, 2
  %960 = zext i1 %959 to i8
  store i8 %960, i8* %54, align 1
  %961 = add i64 %930, 13
  store i64 %961, i64* %3, align 8
  store i32 %938, i32* %936, align 4
  %962 = load i64, i64* %3, align 8
  %963 = add i64 %962, -40
  store i64 %963, i64* %3, align 8
  br label %block_.L_401feb

block_.L_402018:                                  ; preds = %block_.L_401feb
  %964 = add i64 %898, 5
  store i64 %964, i64* %3, align 8
  br label %block_.L_40201d

block_.L_40201d:                                  ; preds = %block_401f8b.block_.L_40201d_crit_edge, %block_.L_402018
  %DL.i102.pre-phi = phi i8* [ %.pre40, %block_401f8b.block_.L_40201d_crit_edge ], [ %DL.i228, %block_.L_402018 ]
  %965 = phi i64 [ %794, %block_401f8b.block_.L_40201d_crit_edge ], [ %964, %block_.L_402018 ]
  %966 = add i64 %965, 5
  br label %block_.L_402022

block_.L_402022:                                  ; preds = %block_.L_4020d2, %block_.L_40201d
  %storemerge17 = phi i64 [ %966, %block_.L_40201d ], [ %1260, %block_.L_4020d2 ]
  %967 = load i64, i64* %RBP.i, align 8
  %968 = add i64 %967, -8
  %969 = add i64 %storemerge17, 4
  store i64 %969, i64* %3, align 8
  %970 = inttoptr i64 %968 to i64*
  %971 = load i64, i64* %970, align 8
  store i64 %971, i64* %RAX.i38, align 8
  %972 = add i64 %971, 16
  %973 = add i64 %storemerge17, 8
  store i64 %973, i64* %3, align 8
  %974 = inttoptr i64 %972 to i32*
  %975 = load i32, i32* %974, align 4
  store i8 0, i8* %38, align 1
  %976 = and i32 %975, 255
  %977 = tail call i32 @llvm.ctpop.i32(i32 %976)
  %978 = trunc i32 %977 to i8
  %979 = and i8 %978, 1
  %980 = xor i8 %979, 1
  store i8 %980, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %981 = icmp eq i32 %975, 0
  %982 = zext i1 %981 to i8
  store i8 %982, i8* %47, align 1
  %983 = lshr i32 %975, 31
  %984 = trunc i32 %983 to i8
  store i8 %984, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v59 = select i1 %981, i64 181, i64 14
  %985 = add i64 %storemerge17, %.v59
  store i64 %985, i64* %3, align 8
  br i1 %981, label %block_.L_4020d7, label %block_402030

block_402030:                                     ; preds = %block_.L_402022
  %986 = add i64 %985, 4
  store i64 %986, i64* %3, align 8
  %987 = load i64, i64* %970, align 8
  store i64 %987, i64* %RAX.i38, align 8
  %988 = add i64 %987, 16
  %989 = add i64 %985, 7
  store i64 %989, i64* %3, align 8
  %990 = inttoptr i64 %988 to i32*
  %991 = load i32, i32* %990, align 4
  %992 = add i32 %991, -1
  %993 = zext i32 %992 to i64
  store i64 %993, i64* %RCX.i36, align 8
  %994 = icmp ne i32 %991, 0
  %995 = zext i1 %994 to i8
  store i8 %995, i8* %38, align 1
  %996 = and i32 %992, 255
  %997 = tail call i32 @llvm.ctpop.i32(i32 %996)
  %998 = trunc i32 %997 to i8
  %999 = and i8 %998, 1
  %1000 = xor i8 %999, 1
  store i8 %1000, i8* %40, align 1
  %1001 = xor i32 %991, 16
  %1002 = xor i32 %1001, %992
  %1003 = lshr i32 %1002, 4
  %1004 = trunc i32 %1003 to i8
  %1005 = and i8 %1004, 1
  store i8 %1005, i8* %46, align 1
  %1006 = icmp eq i32 %992, 0
  %1007 = zext i1 %1006 to i8
  store i8 %1007, i8* %47, align 1
  %1008 = lshr i32 %992, 31
  %1009 = trunc i32 %1008 to i8
  store i8 %1009, i8* %50, align 1
  %1010 = lshr i32 %991, 31
  %1011 = xor i32 %1008, %1010
  %1012 = xor i32 %1008, 1
  %1013 = add nuw nsw i32 %1011, %1012
  %1014 = icmp eq i32 %1013, 2
  %1015 = zext i1 %1014 to i8
  store i8 %1015, i8* %54, align 1
  %1016 = add i64 %985, 13
  store i64 %1016, i64* %3, align 8
  store i32 %992, i32* %990, align 4
  %1017 = load i64, i64* %RBP.i, align 8
  %1018 = add i64 %1017, -8
  %1019 = load i64, i64* %3, align 8
  %1020 = add i64 %1019, 4
  store i64 %1020, i64* %3, align 8
  %1021 = inttoptr i64 %1018 to i64*
  %1022 = load i64, i64* %1021, align 8
  store i64 %1022, i64* %RAX.i38, align 8
  %1023 = add i64 %1022, 8
  %1024 = add i64 %1019, 7
  store i64 %1024, i64* %3, align 8
  %1025 = inttoptr i64 %1023 to i32*
  %1026 = load i32, i32* %1025, align 4
  %1027 = shl i32 %1026, 1
  %1028 = icmp slt i32 %1026, 0
  %1029 = icmp slt i32 %1027, 0
  %1030 = xor i1 %1028, %1029
  %1031 = zext i32 %1027 to i64
  store i64 %1031, i64* %RCX.i36, align 8
  %.lobit18 = lshr i32 %1026, 31
  %1032 = trunc i32 %.lobit18 to i8
  store i8 %1032, i8* %38, align 1
  %1033 = and i32 %1027, 254
  %1034 = tail call i32 @llvm.ctpop.i32(i32 %1033)
  %1035 = trunc i32 %1034 to i8
  %1036 = and i8 %1035, 1
  %1037 = xor i8 %1036, 1
  store i8 %1037, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %1038 = icmp eq i32 %1027, 0
  %1039 = zext i1 %1038 to i8
  store i8 %1039, i8* %47, align 1
  %1040 = lshr i32 %1026, 30
  %1041 = trunc i32 %1040 to i8
  %1042 = and i8 %1041, 1
  store i8 %1042, i8* %50, align 1
  %1043 = zext i1 %1030 to i8
  store i8 %1043, i8* %54, align 1
  %1044 = add i64 %1019, 12
  store i64 %1044, i64* %3, align 8
  store i32 %1027, i32* %1025, align 4
  %1045 = load i64, i64* %RBP.i, align 8
  %1046 = add i64 %1045, -8
  %1047 = load i64, i64* %3, align 8
  %1048 = add i64 %1047, 4
  store i64 %1048, i64* %3, align 8
  %1049 = inttoptr i64 %1046 to i64*
  %1050 = load i64, i64* %1049, align 8
  store i64 %1050, i64* %RAX.i38, align 8
  %1051 = add i64 %1050, 8
  %1052 = add i64 %1047, 7
  store i64 %1052, i64* %3, align 8
  %1053 = inttoptr i64 %1051 to i32*
  %1054 = load i32, i32* %1053, align 4
  %1055 = or i32 %1054, 1
  %1056 = zext i32 %1055 to i64
  store i64 %1056, i64* %RCX.i36, align 8
  store i8 0, i8* %38, align 1
  %1057 = and i32 %1055, 255
  %1058 = tail call i32 @llvm.ctpop.i32(i32 %1057)
  %1059 = trunc i32 %1058 to i8
  %1060 = and i8 %1059, 1
  %1061 = xor i8 %1060, 1
  store i8 %1061, i8* %40, align 1
  store i8 0, i8* %47, align 1
  %1062 = lshr i32 %1054, 31
  %1063 = trunc i32 %1062 to i8
  store i8 %1063, i8* %50, align 1
  store i8 0, i8* %54, align 1
  store i8 0, i8* %46, align 1
  %1064 = add i64 %1047, 13
  store i64 %1064, i64* %3, align 8
  store i32 %1055, i32* %1053, align 4
  %1065 = load i64, i64* %RBP.i, align 8
  %1066 = add i64 %1065, -8
  %1067 = load i64, i64* %3, align 8
  %1068 = add i64 %1067, 4
  store i64 %1068, i64* %3, align 8
  %1069 = inttoptr i64 %1066 to i64*
  %1070 = load i64, i64* %1069, align 8
  store i64 %1070, i64* %RAX.i38, align 8
  %1071 = add i64 %1070, 12
  %1072 = add i64 %1067, 7
  store i64 %1072, i64* %3, align 8
  %1073 = inttoptr i64 %1071 to i32*
  %1074 = load i32, i32* %1073, align 4
  %1075 = add i32 %1074, -1
  %1076 = zext i32 %1075 to i64
  store i64 %1076, i64* %RCX.i36, align 8
  %1077 = add i64 %1067, 13
  store i64 %1077, i64* %3, align 8
  store i32 %1075, i32* %1073, align 4
  %1078 = load i32, i32* %ECX.i259, align 4
  %1079 = load i64, i64* %3, align 8
  store i8 0, i8* %38, align 1
  %1080 = and i32 %1078, 255
  %1081 = tail call i32 @llvm.ctpop.i32(i32 %1080)
  %1082 = trunc i32 %1081 to i8
  %1083 = and i8 %1082, 1
  %1084 = xor i8 %1083, 1
  store i8 %1084, i8* %40, align 1
  store i8 0, i8* %46, align 1
  %1085 = icmp eq i32 %1078, 0
  %1086 = zext i1 %1085 to i8
  store i8 %1086, i8* %47, align 1
  %1087 = lshr i32 %1078, 31
  %1088 = trunc i32 %1087 to i8
  store i8 %1088, i8* %50, align 1
  store i8 0, i8* %54, align 1
  %.v60 = select i1 %1085, i64 9, i64 110
  %1089 = add i64 %1079, %.v60
  store i64 %1089, i64* %3, align 8
  br i1 %1085, label %block_40206d, label %block_.L_4020d2

block_40206d:                                     ; preds = %block_402030
  %1090 = load i64, i64* %RBP.i, align 8
  %1091 = add i64 %1090, -8
  %1092 = add i64 %1089, 4
  store i64 %1092, i64* %3, align 8
  %1093 = inttoptr i64 %1091 to i64*
  %1094 = load i64, i64* %1093, align 8
  store i64 %1094, i64* %RAX.i38, align 8
  %1095 = add i64 %1094, 8
  %1096 = add i64 %1089, 7
  store i64 %1096, i64* %3, align 8
  %1097 = inttoptr i64 %1095 to i32*
  %1098 = load i32, i32* %1097, align 4
  %1099 = zext i32 %1098 to i64
  store i64 %1099, i64* %RCX.i36, align 8
  %1100 = trunc i32 %1098 to i8
  store i8 %1100, i8* %DL.i102.pre-phi, align 1
  %1101 = add i64 %1089, 13
  store i64 %1101, i64* %3, align 8
  %1102 = load i64, i64* %1093, align 8
  store i64 %1102, i64* %RAX.i38, align 8
  %1103 = add i64 %1102, 24
  %1104 = add i64 %1089, 17
  store i64 %1104, i64* %3, align 8
  %1105 = inttoptr i64 %1103 to i64*
  %1106 = load i64, i64* %1105, align 8
  store i64 %1106, i64* %RAX.i38, align 8
  %1107 = add i64 %1089, 21
  store i64 %1107, i64* %3, align 8
  %1108 = load i64, i64* %1093, align 8
  store i64 %1108, i64* %RSI.i540, align 8
  %1109 = add i64 %1108, 32
  %1110 = add i64 %1089, 25
  store i64 %1110, i64* %3, align 8
  %1111 = inttoptr i64 %1109 to i64*
  %1112 = load i64, i64* %1111, align 8
  store i64 %1112, i64* %RSI.i540, align 8
  %1113 = add i64 %1089, 27
  store i64 %1113, i64* %3, align 8
  %1114 = inttoptr i64 %1112 to i32*
  %1115 = load i32, i32* %1114, align 4
  %1116 = zext i32 %1115 to i64
  store i64 %1116, i64* %RCX.i36, align 8
  %1117 = add i32 %1115, 1
  %1118 = zext i32 %1117 to i64
  store i64 %1118, i64* %RDI.i548, align 8
  %1119 = icmp eq i32 %1115, -1
  %1120 = icmp eq i32 %1117, 0
  %1121 = or i1 %1119, %1120
  %1122 = zext i1 %1121 to i8
  store i8 %1122, i8* %38, align 1
  %1123 = and i32 %1117, 255
  %1124 = tail call i32 @llvm.ctpop.i32(i32 %1123)
  %1125 = trunc i32 %1124 to i8
  %1126 = and i8 %1125, 1
  %1127 = xor i8 %1126, 1
  store i8 %1127, i8* %40, align 1
  %1128 = xor i32 %1117, %1115
  %1129 = lshr i32 %1128, 4
  %1130 = trunc i32 %1129 to i8
  %1131 = and i8 %1130, 1
  store i8 %1131, i8* %46, align 1
  %1132 = zext i1 %1120 to i8
  store i8 %1132, i8* %47, align 1
  %1133 = lshr i32 %1117, 31
  %1134 = trunc i32 %1133 to i8
  store i8 %1134, i8* %50, align 1
  %1135 = lshr i32 %1115, 31
  %1136 = xor i32 %1133, %1135
  %1137 = add nuw nsw i32 %1136, %1133
  %1138 = icmp eq i32 %1137, 2
  %1139 = zext i1 %1138 to i8
  store i8 %1139, i8* %54, align 1
  %1140 = add i64 %1089, 34
  store i64 %1140, i64* %3, align 8
  store i32 %1117, i32* %1114, align 4
  %1141 = load i32, i32* %ECX.i259, align 4
  %1142 = load i64, i64* %3, align 8
  %1143 = sext i32 %1141 to i64
  store i64 %1143, i64* %RSI.i540, align 8
  %1144 = load i64, i64* %RAX.i38, align 8
  %1145 = add i64 %1144, %1143
  %1146 = load i8, i8* %DL.i102.pre-phi, align 1
  %1147 = add i64 %1142, 6
  store i64 %1147, i64* %3, align 8
  %1148 = inttoptr i64 %1145 to i8*
  store i8 %1146, i8* %1148, align 1
  %1149 = load i64, i64* %RBP.i, align 8
  %1150 = add i64 %1149, -8
  %1151 = load i64, i64* %3, align 8
  %1152 = add i64 %1151, 4
  store i64 %1152, i64* %3, align 8
  %1153 = inttoptr i64 %1150 to i64*
  %1154 = load i64, i64* %1153, align 8
  store i64 %1154, i64* %RAX.i38, align 8
  %1155 = add i64 %1154, 12
  %1156 = add i64 %1151, 11
  store i64 %1156, i64* %3, align 8
  %1157 = inttoptr i64 %1155 to i32*
  store i32 8, i32* %1157, align 4
  %.pre35 = load i64, i64* %3, align 8
  br label %block_.L_4020a0

block_.L_4020a0:                                  ; preds = %block_4020ae, %block_40206d
  %1158 = phi i64 [ %1257, %block_4020ae ], [ %.pre35, %block_40206d ]
  %1159 = load i64, i64* %RBP.i, align 8
  %1160 = add i64 %1159, -8
  %1161 = add i64 %1158, 4
  store i64 %1161, i64* %3, align 8
  %1162 = inttoptr i64 %1160 to i64*
  %1163 = load i64, i64* %1162, align 8
  store i64 %1163, i64* %RAX.i38, align 8
  %1164 = add i64 %1163, 80
  %1165 = add i64 %1158, 8
  store i64 %1165, i64* %3, align 8
  %1166 = inttoptr i64 %1164 to i32*
  %1167 = load i32, i32* %1166, align 4
  %1168 = add i32 %1167, -7
  %1169 = icmp ult i32 %1167, 7
  %1170 = zext i1 %1169 to i8
  store i8 %1170, i8* %38, align 1
  %1171 = and i32 %1168, 255
  %1172 = tail call i32 @llvm.ctpop.i32(i32 %1171)
  %1173 = trunc i32 %1172 to i8
  %1174 = and i8 %1173, 1
  %1175 = xor i8 %1174, 1
  store i8 %1175, i8* %40, align 1
  %1176 = xor i32 %1168, %1167
  %1177 = lshr i32 %1176, 4
  %1178 = trunc i32 %1177 to i8
  %1179 = and i8 %1178, 1
  store i8 %1179, i8* %46, align 1
  %1180 = icmp eq i32 %1168, 0
  %1181 = zext i1 %1180 to i8
  store i8 %1181, i8* %47, align 1
  %1182 = lshr i32 %1168, 31
  %1183 = trunc i32 %1182 to i8
  store i8 %1183, i8* %50, align 1
  %1184 = lshr i32 %1167, 31
  %1185 = xor i32 %1182, %1184
  %1186 = add nuw nsw i32 %1185, %1184
  %1187 = icmp eq i32 %1186, 2
  %1188 = zext i1 %1187 to i8
  store i8 %1188, i8* %54, align 1
  %1189 = icmp ne i8 %1183, 0
  %1190 = xor i1 %1189, %1187
  %1191 = or i1 %1180, %1190
  %.v61 = select i1 %1191, i64 45, i64 14
  %1192 = add i64 %1158, %.v61
  store i64 %1192, i64* %3, align 8
  br i1 %1191, label %block_.L_4020cd, label %block_4020ae

block_4020ae:                                     ; preds = %block_.L_4020a0
  %1193 = add i64 %1192, 4
  store i64 %1193, i64* %3, align 8
  %1194 = load i64, i64* %1162, align 8
  store i64 %1194, i64* %RAX.i38, align 8
  %1195 = add i64 %1194, 80
  %1196 = add i64 %1192, 7
  store i64 %1196, i64* %3, align 8
  %1197 = inttoptr i64 %1195 to i32*
  %1198 = load i32, i32* %1197, align 4
  %1199 = add i32 %1198, -8
  %1200 = zext i32 %1199 to i64
  store i64 %1200, i64* %RCX.i36, align 8
  %1201 = icmp ult i32 %1198, 8
  %1202 = zext i1 %1201 to i8
  store i8 %1202, i8* %38, align 1
  %1203 = and i32 %1199, 255
  %1204 = tail call i32 @llvm.ctpop.i32(i32 %1203)
  %1205 = trunc i32 %1204 to i8
  %1206 = and i8 %1205, 1
  %1207 = xor i8 %1206, 1
  store i8 %1207, i8* %40, align 1
  %1208 = xor i32 %1199, %1198
  %1209 = lshr i32 %1208, 4
  %1210 = trunc i32 %1209 to i8
  %1211 = and i8 %1210, 1
  store i8 %1211, i8* %46, align 1
  %1212 = icmp eq i32 %1199, 0
  %1213 = zext i1 %1212 to i8
  store i8 %1213, i8* %47, align 1
  %1214 = lshr i32 %1199, 31
  %1215 = trunc i32 %1214 to i8
  store i8 %1215, i8* %50, align 1
  %1216 = lshr i32 %1198, 31
  %1217 = xor i32 %1214, %1216
  %1218 = add nuw nsw i32 %1217, %1216
  %1219 = icmp eq i32 %1218, 2
  %1220 = zext i1 %1219 to i8
  store i8 %1220, i8* %54, align 1
  %1221 = add i64 %1192, 13
  store i64 %1221, i64* %3, align 8
  store i32 %1199, i32* %1197, align 4
  %1222 = load i64, i64* %RBP.i, align 8
  %1223 = add i64 %1222, -8
  %1224 = load i64, i64* %3, align 8
  %1225 = add i64 %1224, 4
  store i64 %1225, i64* %3, align 8
  %1226 = inttoptr i64 %1223 to i64*
  %1227 = load i64, i64* %1226, align 8
  store i64 %1227, i64* %RAX.i38, align 8
  %1228 = add i64 %1227, 88
  %1229 = add i64 %1224, 7
  store i64 %1229, i64* %3, align 8
  %1230 = inttoptr i64 %1228 to i32*
  %1231 = load i32, i32* %1230, align 4
  %1232 = add i32 %1231, 1
  %1233 = zext i32 %1232 to i64
  store i64 %1233, i64* %RCX.i36, align 8
  %1234 = icmp eq i32 %1231, -1
  %1235 = icmp eq i32 %1232, 0
  %1236 = or i1 %1234, %1235
  %1237 = zext i1 %1236 to i8
  store i8 %1237, i8* %38, align 1
  %1238 = and i32 %1232, 255
  %1239 = tail call i32 @llvm.ctpop.i32(i32 %1238)
  %1240 = trunc i32 %1239 to i8
  %1241 = and i8 %1240, 1
  %1242 = xor i8 %1241, 1
  store i8 %1242, i8* %40, align 1
  %1243 = xor i32 %1232, %1231
  %1244 = lshr i32 %1243, 4
  %1245 = trunc i32 %1244 to i8
  %1246 = and i8 %1245, 1
  store i8 %1246, i8* %46, align 1
  %1247 = zext i1 %1235 to i8
  store i8 %1247, i8* %47, align 1
  %1248 = lshr i32 %1232, 31
  %1249 = trunc i32 %1248 to i8
  store i8 %1249, i8* %50, align 1
  %1250 = lshr i32 %1231, 31
  %1251 = xor i32 %1248, %1250
  %1252 = add nuw nsw i32 %1251, %1248
  %1253 = icmp eq i32 %1252, 2
  %1254 = zext i1 %1253 to i8
  store i8 %1254, i8* %54, align 1
  %1255 = add i64 %1224, 13
  store i64 %1255, i64* %3, align 8
  store i32 %1232, i32* %1230, align 4
  %1256 = load i64, i64* %3, align 8
  %1257 = add i64 %1256, -40
  store i64 %1257, i64* %3, align 8
  br label %block_.L_4020a0

block_.L_4020cd:                                  ; preds = %block_.L_4020a0
  %1258 = add i64 %1192, 5
  store i64 %1258, i64* %3, align 8
  br label %block_.L_4020d2

block_.L_4020d2:                                  ; preds = %block_402030, %block_.L_4020cd
  %1259 = phi i64 [ %1258, %block_.L_4020cd ], [ %1089, %block_402030 ]
  %1260 = add i64 %1259, -176
  %1261 = add i64 %1259, 5
  store i64 %1261, i64* %3, align 8
  br label %block_.L_402022

block_.L_4020d7:                                  ; preds = %block_.L_402022
  %1262 = add i64 %985, 30
  store i64 %1262, i64* %3, align 8
  br label %block_.L_4020f5

block_.L_4020dc:                                  ; preds = %block_.L_401f7e
  %1263 = add i64 %734, 16
  %1264 = add i64 %730, 7
  store i64 %1264, i64* %3, align 8
  %1265 = inttoptr i64 %1263 to i32*
  %1266 = load i32, i32* %1265, align 4
  %1267 = add i32 %1266, 1
  %1268 = zext i32 %1267 to i64
  store i64 %1268, i64* %RCX.i36, align 8
  %1269 = icmp eq i32 %1266, -1
  %1270 = icmp eq i32 %1267, 0
  %1271 = or i1 %1269, %1270
  %1272 = zext i1 %1271 to i8
  store i8 %1272, i8* %38, align 1
  %1273 = and i32 %1267, 255
  %1274 = tail call i32 @llvm.ctpop.i32(i32 %1273)
  %1275 = trunc i32 %1274 to i8
  %1276 = and i8 %1275, 1
  %1277 = xor i8 %1276, 1
  store i8 %1277, i8* %40, align 1
  %1278 = xor i32 %1267, %1266
  %1279 = lshr i32 %1278, 4
  %1280 = trunc i32 %1279 to i8
  %1281 = and i8 %1280, 1
  store i8 %1281, i8* %46, align 1
  %1282 = zext i1 %1270 to i8
  store i8 %1282, i8* %47, align 1
  %1283 = lshr i32 %1267, 31
  %1284 = trunc i32 %1283 to i8
  store i8 %1284, i8* %50, align 1
  %1285 = lshr i32 %1266, 31
  %1286 = xor i32 %1283, %1285
  %1287 = add nuw nsw i32 %1286, %1283
  %1288 = icmp eq i32 %1287, 2
  %1289 = zext i1 %1288 to i8
  store i8 %1289, i8* %54, align 1
  %1290 = add i64 %730, 13
  store i64 %1290, i64* %3, align 8
  store i32 %1267, i32* %1265, align 4
  %1291 = load i64, i64* %RBP.i, align 8
  %1292 = add i64 %1291, -16
  %1293 = load i64, i64* %3, align 8
  %1294 = add i64 %1293, 3
  store i64 %1294, i64* %3, align 8
  %1295 = inttoptr i64 %1292 to i32*
  %1296 = load i32, i32* %1295, align 4
  %1297 = add i32 %1296, -512
  %1298 = zext i32 %1297 to i64
  store i64 %1298, i64* %RCX.i36, align 8
  %1299 = icmp ult i32 %1296, 512
  %1300 = zext i1 %1299 to i8
  store i8 %1300, i8* %38, align 1
  %1301 = and i32 %1297, 255
  %1302 = tail call i32 @llvm.ctpop.i32(i32 %1301)
  %1303 = trunc i32 %1302 to i8
  %1304 = and i8 %1303, 1
  %1305 = xor i8 %1304, 1
  store i8 %1305, i8* %40, align 1
  %1306 = xor i32 %1297, %1296
  %1307 = lshr i32 %1306, 4
  %1308 = trunc i32 %1307 to i8
  %1309 = and i8 %1308, 1
  store i8 %1309, i8* %46, align 1
  %1310 = icmp eq i32 %1297, 0
  %1311 = zext i1 %1310 to i8
  store i8 %1311, i8* %47, align 1
  %1312 = lshr i32 %1297, 31
  %1313 = trunc i32 %1312 to i8
  store i8 %1313, i8* %50, align 1
  %1314 = lshr i32 %1296, 31
  %1315 = xor i32 %1312, %1314
  %1316 = add nuw nsw i32 %1315, %1314
  %1317 = icmp eq i32 %1316, 2
  %1318 = zext i1 %1317 to i8
  store i8 %1318, i8* %54, align 1
  %1319 = add i64 %1293, 12
  store i64 %1319, i64* %3, align 8
  store i32 %1297, i32* %1295, align 4
  %.pre36 = load i64, i64* %3, align 8
  br label %block_.L_4020f5

block_.L_4020f5:                                  ; preds = %block_.L_4020dc, %block_.L_4020d7
  %1320 = phi i64 [ %.pre36, %block_.L_4020dc ], [ %1262, %block_.L_4020d7 ]
  %1321 = add i64 %1320, 5
  store i64 %1321, i64* %3, align 8
  br label %block_.L_4020fa

block_.L_4020fa:                                  ; preds = %block_.L_4020f5, %block_.L_401f6e
  %RAX.i19.pre-phi = phi i64* [ %RAX.i38, %block_.L_4020f5 ], [ %RAX.i512, %block_.L_401f6e ]
  %storemerge15 = phi i64 [ %1321, %block_.L_4020f5 ], [ %706, %block_.L_401f6e ]
  %1322 = load i64, i64* %RBP.i, align 8
  %1323 = add i64 %1322, -16
  %1324 = add i64 %storemerge15, 3
  store i64 %1324, i64* %3, align 8
  %1325 = inttoptr i64 %1323 to i32*
  %1326 = load i32, i32* %1325, align 4
  %1327 = zext i32 %1326 to i64
  store i64 %1327, i64* %RAX.i19.pre-phi, align 8
  %RCX.i16 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %1328 = add i64 %1322, -8
  %1329 = add i64 %storemerge15, 7
  store i64 %1329, i64* %3, align 8
  %1330 = inttoptr i64 %1328 to i64*
  %1331 = load i64, i64* %1330, align 8
  store i64 %1331, i64* %RCX.i16, align 8
  %1332 = add i64 %storemerge15, 9
  store i64 %1332, i64* %3, align 8
  %1333 = inttoptr i64 %1331 to i32*
  store i32 %1326, i32* %1333, align 4
  %1334 = load i64, i64* %RBP.i, align 8
  %1335 = add i64 %1334, -8
  %1336 = load i64, i64* %3, align 8
  %1337 = add i64 %1336, 4
  store i64 %1337, i64* %3, align 8
  %1338 = inttoptr i64 %1335 to i64*
  %1339 = load i64, i64* %1338, align 8
  store i64 %1339, i64* %RCX.i16, align 8
  %1340 = add i64 %1339, 80
  %1341 = add i64 %1336, 7
  store i64 %1341, i64* %3, align 8
  %1342 = inttoptr i64 %1340 to i32*
  %1343 = load i32, i32* %1342, align 4
  %1344 = add i32 %1343, 1
  %1345 = zext i32 %1344 to i64
  store i64 %1345, i64* %RAX.i19.pre-phi, align 8
  %1346 = icmp eq i32 %1343, -1
  %1347 = icmp eq i32 %1344, 0
  %1348 = or i1 %1346, %1347
  %1349 = zext i1 %1348 to i8
  store i8 %1349, i8* %38, align 1
  %1350 = and i32 %1344, 255
  %1351 = tail call i32 @llvm.ctpop.i32(i32 %1350)
  %1352 = trunc i32 %1351 to i8
  %1353 = and i8 %1352, 1
  %1354 = xor i8 %1353, 1
  store i8 %1354, i8* %40, align 1
  %1355 = xor i32 %1344, %1343
  %1356 = lshr i32 %1355, 4
  %1357 = trunc i32 %1356 to i8
  %1358 = and i8 %1357, 1
  store i8 %1358, i8* %46, align 1
  %1359 = zext i1 %1347 to i8
  store i8 %1359, i8* %47, align 1
  %1360 = lshr i32 %1344, 31
  %1361 = trunc i32 %1360 to i8
  store i8 %1361, i8* %50, align 1
  %1362 = lshr i32 %1343, 31
  %1363 = xor i32 %1360, %1362
  %1364 = add nuw nsw i32 %1363, %1360
  %1365 = icmp eq i32 %1364, 2
  %1366 = zext i1 %1365 to i8
  store i8 %1366, i8* %54, align 1
  %1367 = add i64 %1336, 13
  store i64 %1367, i64* %3, align 8
  store i32 %1344, i32* %1342, align 4
  %1368 = load i64, i64* %3, align 8
  %1369 = add i64 %1368, 1
  store i64 %1369, i64* %3, align 8
  %1370 = load i64, i64* %6, align 8
  %1371 = add i64 %1370, 8
  %1372 = inttoptr i64 %1370 to i64*
  %1373 = load i64, i64* %1372, align 8
  store i64 %1373, i64* %RBP.i, align 8
  store i64 %1371, i64* %6, align 8
  %1374 = add i64 %1368, 2
  store i64 %1374, i64* %3, align 8
  %1375 = inttoptr i64 %1371 to i64*
  %1376 = load i64, i64* %1375, align 8
  store i64 %1376, i64* %3, align 8
  %1377 = add i64 %1370, 16
  store i64 %1377, i64* %6, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_pushq__rbp(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 1
  store i64 %5, i64* %PC, align 8
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %7 = load i64, i64* %6, align 8
  %8 = add i64 %7, -8
  %9 = inttoptr i64 %8 to i64*
  store i64 %3, i64* %9, align 8
  store i64 %8, i64* %6, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq__rsp___rbp(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RSP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RSP, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  store i64 %3, i64* %RBP, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movw__si___ax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %AX = bitcast %union.anon* %3 to i16*
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0
  %SI = bitcast %union.anon* %4 to i16*
  %5 = load i16, i16* %SI, align 2
  %6 = load i64, i64* %PC, align 8
  %7 = add i64 %6, 3
  store i64 %7, i64* %PC, align 8
  store i16 %5, i16* %AX, align 2
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq__rdi__MINUS0x8__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RDI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -8
  %5 = load i64, i64* %RDI, align 8
  %6 = load i64, i64* %PC, align 8
  %7 = add i64 %6, 4
  store i64 %7, i64* %PC, align 8
  %8 = inttoptr i64 %4 to i64*
  store i64 %5, i64* %8, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movw__ax__MINUS0xa__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %AX = bitcast %union.anon* %3 to i16*
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP, align 8
  %5 = add i64 %4, -10
  %6 = load i16, i16* %AX, align 2
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 4
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i16*
  store i16 %6, i16* %9, align 2
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq_MINUS0x8__rbp____rdi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RDI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %RDI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl___rdi____esi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %RDI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %3 = bitcast i64* %RDI to i32**
  %4 = load i32*, i32** %3, align 8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 2
  store i64 %6, i64* %PC, align 8
  %7 = load i32, i32* %4, align 4
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RSI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_shll__0x1___esi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %3 = load i64, i64* %RSI, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 2
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = shl i32 %6, 1
  %8 = icmp slt i32 %6, 0
  %9 = icmp slt i32 %7, 0
  %10 = xor i1 %8, %9
  %11 = zext i32 %7 to i64
  store i64 %11, i64* %RSI, align 8
  %12 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %.lobit = lshr i32 %6, 31
  %13 = trunc i32 %.lobit to i8
  store i8 %13, i8* %12, align 1
  %14 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  %15 = and i32 %7, 254
  %16 = tail call i32 @llvm.ctpop.i32(i32 %15)
  %17 = trunc i32 %16 to i8
  %18 = and i8 %17, 1
  %19 = xor i8 %18, 1
  store i8 %19, i8* %14, align 1
  %20 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %20, align 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %22 = icmp eq i32 %7, 0
  %23 = zext i1 %22 to i8
  store i8 %23, i8* %21, align 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %25 = lshr i32 %6, 30
  %26 = trunc i32 %25 to i8
  %27 = and i8 %26, 1
  store i8 %27, i8* %24, align 1
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %29 = zext i1 %10 to i8
  store i8 %29, i8* %28, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__esi__MINUS0x10__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0
  %ESI = bitcast %union.anon* %3 to i32*
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP, align 8
  %5 = add i64 %4, -16
  %6 = load i32, i32* %ESI, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movswl_MINUS0xa__rbp____esi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -10
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i16*
  %8 = load i16, i16* %7, align 2
  %9 = sext i16 %8 to i64
  %10 = and i64 %9, 4294967295
  store i64 %10, i64* %RSI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_cmpl__0x0___esi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0
  %ESI = bitcast %union.anon* %3 to i32*
  %4 = load i32, i32* %ESI, align 4
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 0, i8* %7, align 1
  %8 = and i32 %4, 255
  %9 = tail call i32 @llvm.ctpop.i32(i32 %8)
  %10 = trunc i32 %9 to i8
  %11 = and i8 %10, 1
  %12 = xor i8 %11, 1
  %13 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %12, i8* %13, align 1
  %14 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %14, align 1
  %15 = icmp eq i32 %4, 0
  %16 = zext i1 %15 to i8
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %16, i8* %17, align 1
  %18 = lshr i32 %4, 31
  %19 = trunc i32 %18 to i8
  %20 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %19, i8* %20, align 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 0, i8* %21, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_je_.L_401e15(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  store i8 %5, i8* %BRANCH_TAKEN, align 1
  %6 = icmp ne i8 %5, 0
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %7 = add i64 %.v, %3
  store i64 %7, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq_MINUS0x8__rbp____rax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %RAX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0x4__rax____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 4
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_addl_MINUS0x10__rbp____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %RBP, align 8
  %5 = add i64 %4, -16
  %6 = load i64, i64* %PC, align 8
  %7 = add i64 %6, 3
  store i64 %7, i64* %PC, align 8
  %8 = trunc i64 %3 to i32
  %9 = inttoptr i64 %5 to i32*
  %10 = load i32, i32* %9, align 4
  %11 = add i32 %10, %8
  %12 = zext i32 %11 to i64
  store i64 %12, i64* %RCX, align 8
  %13 = icmp ult i32 %11, %8
  %14 = icmp ult i32 %11, %10
  %15 = or i1 %13, %14
  %16 = zext i1 %15 to i8
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %16, i8* %17, align 1
  %18 = and i32 %11, 255
  %19 = tail call i32 @llvm.ctpop.i32(i32 %18)
  %20 = trunc i32 %19 to i8
  %21 = and i8 %20, 1
  %22 = xor i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %22, i8* %23, align 1
  %24 = xor i32 %10, %8
  %25 = xor i32 %24, %11
  %26 = lshr i32 %25, 4
  %27 = trunc i32 %26 to i8
  %28 = and i8 %27, 1
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %28, i8* %29, align 1
  %30 = icmp eq i32 %11, 0
  %31 = zext i1 %30 to i8
  %32 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %31, i8* %32, align 1
  %33 = lshr i32 %11, 31
  %34 = trunc i32 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %34, i8* %35, align 1
  %36 = lshr i32 %8, 31
  %37 = lshr i32 %10, 31
  %38 = xor i32 %33, %36
  %39 = xor i32 %33, %37
  %40 = add nuw nsw i32 %38, %39
  %41 = icmp eq i32 %40, 2
  %42 = zext i1 %41 to i8
  %43 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %42, i8* %43, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx__MINUS0x10__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP, align 8
  %5 = add i64 %4, -16
  %6 = load i32, i32* %ECX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_cmpl__0x400__MINUS0x10__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -16
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 7
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = add i32 %8, -1024
  %10 = icmp ult i32 %8, 1024
  %11 = zext i1 %10 to i8
  %12 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %11, i8* %12, align 1
  %13 = and i32 %9, 255
  %14 = tail call i32 @llvm.ctpop.i32(i32 %13)
  %15 = trunc i32 %14 to i8
  %16 = and i8 %15, 1
  %17 = xor i8 %16, 1
  %18 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %17, i8* %18, align 1
  %19 = xor i32 %9, %8
  %20 = lshr i32 %19, 4
  %21 = trunc i32 %20 to i8
  %22 = and i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %22, i8* %23, align 1
  %24 = icmp eq i32 %9, 0
  %25 = zext i1 %24 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %9, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %8, 31
  %31 = xor i32 %27, %30
  %32 = add nuw nsw i32 %31, %30
  %33 = icmp eq i32 %32, 2
  %34 = zext i1 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %34, i8* %35, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jb_.L_401f7e(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %5 = load i8, i8* %4, align 1
  store i8 %5, i8* %BRANCH_TAKEN, align 1
  %6 = icmp ne i8 %5, 0
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %7 = add i64 %.v, %3
  store i64 %7, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0x8__rax____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_shll__0x1___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 2
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = shl i32 %6, 1
  %8 = icmp slt i32 %6, 0
  %9 = icmp slt i32 %7, 0
  %10 = xor i1 %8, %9
  %11 = zext i32 %7 to i64
  store i64 %11, i64* %RCX, align 8
  %12 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %.lobit = lshr i32 %6, 31
  %13 = trunc i32 %.lobit to i8
  store i8 %13, i8* %12, align 1
  %14 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  %15 = and i32 %7, 254
  %16 = tail call i32 @llvm.ctpop.i32(i32 %15)
  %17 = trunc i32 %16 to i8
  %18 = and i8 %17, 1
  %19 = xor i8 %18, 1
  store i8 %19, i8* %14, align 1
  %20 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %20, align 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %22 = icmp eq i32 %7, 0
  %23 = zext i1 %22 to i8
  store i8 %23, i8* %21, align 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %25 = lshr i32 %6, 30
  %26 = trunc i32 %25 to i8
  %27 = and i8 %26, 1
  store i8 %27, i8* %24, align 1
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %29 = zext i1 %10 to i8
  store i8 %29, i8* %28, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx__0x8__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %4 = load i64, i64* %RAX, align 8
  %5 = add i64 %4, 8
  %6 = load i32, i32* %ECX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_orl__0x1___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  %6 = or i64 %3, 1
  %7 = trunc i64 %6 to i32
  %8 = and i64 %6, 4294967295
  store i64 %8, i64* %RCX, align 8
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 0, i8* %9, align 1
  %10 = and i32 %7, 255
  %11 = tail call i32 @llvm.ctpop.i32(i32 %10)
  %12 = trunc i32 %11 to i8
  %13 = and i8 %12, 1
  %14 = xor i8 %13, 1
  %15 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %14, i8* %15, align 1
  %16 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 0, i8* %16, align 1
  %17 = lshr i32 %7, 31
  %18 = trunc i32 %17 to i8
  %19 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %18, i8* %19, align 1
  %20 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 0, i8* %20, align 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %21, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0xc__rax____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 12
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_addl__0xffffffff___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, -1
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RCX, align 8
  %9 = icmp ne i32 %6, 0
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = and i32 %7, 255
  %13 = tail call i32 @llvm.ctpop.i32(i32 %12)
  %14 = trunc i32 %13 to i8
  %15 = and i8 %14, 1
  %16 = xor i8 %15, 1
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %16, i8* %17, align 1
  %18 = xor i32 %6, 16
  %19 = xor i32 %18, %7
  %20 = lshr i32 %19, 4
  %21 = trunc i32 %20 to i8
  %22 = and i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %22, i8* %23, align 1
  %24 = icmp eq i32 %7, 0
  %25 = zext i1 %24 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %7, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %6, 31
  %31 = xor i32 %27, %30
  %32 = xor i32 %27, 1
  %33 = add nuw nsw i32 %31, %32
  %34 = icmp eq i32 %33, 2
  %35 = zext i1 %34 to i8
  %36 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %35, i8* %36, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx__0xc__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %4 = load i64, i64* %RAX, align 8
  %5 = add i64 %4, 12
  %6 = load i32, i32* %ECX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_cmpl__0x0___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %4 = load i32, i32* %ECX, align 4
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 0, i8* %7, align 1
  %8 = and i32 %4, 255
  %9 = tail call i32 @llvm.ctpop.i32(i32 %8)
  %10 = trunc i32 %9 to i8
  %11 = and i8 %10, 1
  %12 = xor i8 %11, 1
  %13 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %12, i8* %13, align 1
  %14 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %14, align 1
  %15 = icmp eq i32 %4, 0
  %16 = zext i1 %15 to i8
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %16, i8* %17, align 1
  %18 = lshr i32 %4, 31
  %19 = trunc i32 %18 to i8
  %20 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %19, i8* %20, align 1
  %21 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 0, i8* %21, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jne_.L_401eb7(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp eq i8 %5, 0
  %7 = zext i1 %6 to i8
  store i8 %7, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %8 = add i64 %.v, %3
  store i64 %8, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movb__cl___dl(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %CL = bitcast %union.anon* %3 to i8*
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %DL = bitcast %union.anon* %4 to i8*
  %5 = load i8, i8* %CL, align 1
  %6 = load i64, i64* %PC, align 8
  %7 = add i64 %6, 2
  store i64 %7, i64* %PC, align 8
  store i8 %5, i8* %DL, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq_0x18__rax____rax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 24
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %RAX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq_MINUS0x8__rbp____rsi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %RSI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq_0x20__rsi____rsi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %3 = load i64, i64* %RSI, align 8
  %4 = add i64 %3, 32
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %RSI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl___rsi____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %3 = bitcast i64* %RSI to i32**
  %4 = load i32*, i32** %3, align 8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 2
  store i64 %6, i64* %PC, align 8
  %7 = load i32, i32* %4, align 4
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx___edi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RDI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %4 = load i32, i32* %ECX, align 4
  %5 = zext i32 %4 to i64
  %6 = load i64, i64* %PC, align 8
  %7 = add i64 %6, 2
  store i64 %7, i64* %PC, align 8
  store i64 %5, i64* %RDI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_addl__0x1___edi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RDI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0, i32 0
  %3 = load i64, i64* %RDI, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, 1
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RDI, align 8
  %9 = icmp eq i32 %6, -1
  %10 = icmp eq i32 %7, 0
  %11 = or i1 %9, %10
  %12 = zext i1 %11 to i8
  %13 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %12, i8* %13, align 1
  %14 = and i32 %7, 255
  %15 = tail call i32 @llvm.ctpop.i32(i32 %14)
  %16 = trunc i32 %15 to i8
  %17 = and i8 %16, 1
  %18 = xor i8 %17, 1
  %19 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %18, i8* %19, align 1
  %20 = xor i32 %7, %6
  %21 = lshr i32 %20, 4
  %22 = trunc i32 %21 to i8
  %23 = and i8 %22, 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %23, i8* %24, align 1
  %25 = zext i1 %10 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %7, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %6, 31
  %31 = xor i32 %27, %30
  %32 = add nuw nsw i32 %31, %27
  %33 = icmp eq i32 %32, 2
  %34 = zext i1 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %34, i8* %35, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__edi____rsi_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 11, i32 0
  %EDI = bitcast %union.anon* %3 to i32*
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %4 = bitcast i64* %RSI to i32**
  %5 = load i32*, i32** %4, align 8
  %6 = load i32, i32* %EDI, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 2
  store i64 %8, i64* %PC, align 8
  store i32 %6, i32* %5, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movslq__ecx___rsi(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %4 = load i32, i32* %ECX, align 4
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = sext i32 %4 to i64
  store i64 %7, i64* %RSI, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movb__dl____rax__rsi_1_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 7, i32 0
  %DL = bitcast %union.anon* %3 to i8*
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RSI = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 9, i32 0, i32 0
  %4 = load i64, i64* %RAX, align 8
  %5 = load i64, i64* %RSI, align 8
  %6 = add i64 %5, %4
  %7 = load i8, i8* %DL, align 1
  %8 = load i64, i64* %PC, align 8
  %9 = add i64 %8, 3
  store i64 %9, i64* %PC, align 8
  %10 = inttoptr i64 %6 to i8*
  store i8 %7, i8* %10, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__0x8__0xc__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 12
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 7
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  store i32 8, i32* %7, align 4
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_cmpl__0x7__0x50__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 80
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = add i32 %8, -7
  %10 = icmp ult i32 %8, 7
  %11 = zext i1 %10 to i8
  %12 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %11, i8* %12, align 1
  %13 = and i32 %9, 255
  %14 = tail call i32 @llvm.ctpop.i32(i32 %13)
  %15 = trunc i32 %14 to i8
  %16 = and i8 %15, 1
  %17 = xor i8 %16, 1
  %18 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %17, i8* %18, align 1
  %19 = xor i32 %9, %8
  %20 = lshr i32 %19, 4
  %21 = trunc i32 %20 to i8
  %22 = and i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %22, i8* %23, align 1
  %24 = icmp eq i32 %9, 0
  %25 = zext i1 %24 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %9, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %8, 31
  %31 = xor i32 %27, %30
  %32 = add nuw nsw i32 %31, %30
  %33 = icmp eq i32 %32, 2
  %34 = zext i1 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %34, i8* %35, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jle_.L_401eb2(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp ne i8 %5, 0
  %7 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %8 = load i8, i8* %7, align 1
  %9 = icmp ne i8 %8, 0
  %10 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %11 = load i8, i8* %10, align 1
  %12 = icmp ne i8 %11, 0
  %13 = xor i1 %9, %12
  %14 = or i1 %6, %13
  %15 = zext i1 %14 to i8
  store i8 %15, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %14, i64 %rel_off1, i64 %rel_off2
  %16 = add i64 %.v, %3
  store i64 %16, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0x50__rax____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 80
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_subl__0x8___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, -8
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RCX, align 8
  %9 = icmp ult i32 %6, 8
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = and i32 %7, 255
  %13 = tail call i32 @llvm.ctpop.i32(i32 %12)
  %14 = trunc i32 %13 to i8
  %15 = and i8 %14, 1
  %16 = xor i8 %15, 1
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %16, i8* %17, align 1
  %18 = xor i32 %7, %6
  %19 = lshr i32 %18, 4
  %20 = trunc i32 %19 to i8
  %21 = and i8 %20, 1
  %22 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %21, i8* %22, align 1
  %23 = icmp eq i32 %7, 0
  %24 = zext i1 %23 to i8
  %25 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %24, i8* %25, align 1
  %26 = lshr i32 %7, 31
  %27 = trunc i32 %26 to i8
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %27, i8* %28, align 1
  %29 = lshr i32 %6, 31
  %30 = xor i32 %26, %29
  %31 = add nuw nsw i32 %30, %29
  %32 = icmp eq i32 %31, 2
  %33 = zext i1 %32 to i8
  %34 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %33, i8* %34, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx__0x50__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %4 = load i64, i64* %RAX, align 8
  %5 = add i64 %4, 80
  %6 = load i32, i32* %ECX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0x58__rax____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 88
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_addl__0x1___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, 1
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RCX, align 8
  %9 = icmp eq i32 %6, -1
  %10 = icmp eq i32 %7, 0
  %11 = or i1 %9, %10
  %12 = zext i1 %11 to i8
  %13 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %12, i8* %13, align 1
  %14 = and i32 %7, 255
  %15 = tail call i32 @llvm.ctpop.i32(i32 %14)
  %16 = trunc i32 %15 to i8
  %17 = and i8 %16, 1
  %18 = xor i8 %17, 1
  %19 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %18, i8* %19, align 1
  %20 = xor i32 %7, %6
  %21 = lshr i32 %20, 4
  %22 = trunc i32 %21 to i8
  %23 = and i8 %22, 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %23, i8* %24, align 1
  %25 = zext i1 %10 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %7, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %6, 31
  %31 = xor i32 %27, %30
  %32 = add nuw nsw i32 %31, %27
  %33 = icmp eq i32 %32, 2
  %34 = zext i1 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %34, i8* %35, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx__0x58__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %4 = load i64, i64* %RAX, align 8
  %5 = add i64 %4, 88
  %6 = load i32, i32* %ECX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_401e85(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_401eb7(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_401ebc(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_cmpl__0x0__0x10__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 16
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 0, i8* %9, align 1
  %10 = and i32 %8, 255
  %11 = tail call i32 @llvm.ctpop.i32(i32 %10)
  %12 = trunc i32 %11 to i8
  %13 = and i8 %12, 1
  %14 = xor i8 %13, 1
  %15 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %14, i8* %15, align 1
  %16 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 0, i8* %16, align 1
  %17 = icmp eq i32 %8, 0
  %18 = zext i1 %17 to i8
  %19 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %18, i8* %19, align 1
  %20 = lshr i32 %8, 31
  %21 = trunc i32 %20 to i8
  %22 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %21, i8* %22, align 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 0, i8* %23, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jbe_.L_401f6e(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %5 = load i8, i8* %4, align 1
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %7 = load i8, i8* %6, align 1
  %8 = or i8 %7, %5
  %9 = icmp ne i8 %8, 0
  %10 = zext i1 %9 to i8
  store i8 %10, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %9, i64 %rel_off1, i64 %rel_off2
  %11 = add i64 %.v, %3
  store i64 %11, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0x10__rax____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = add i64 %3, 16
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__ecx__0x10__rax_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0
  %ECX = bitcast %union.anon* %3 to i32*
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %4 = load i64, i64* %RAX, align 8
  %5 = add i64 %4, 16
  %6 = load i32, i32* %ECX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jne_.L_401f69(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp eq i8 %5, 0
  %7 = zext i1 %6 to i8
  store i8 %7, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %8 = add i64 %.v, %3
  store i64 %8, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jle_.L_401f64(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp ne i8 %5, 0
  %7 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %8 = load i8, i8* %7, align 1
  %9 = icmp ne i8 %8, 0
  %10 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %11 = load i8, i8* %10, align 1
  %12 = icmp ne i8 %11, 0
  %13 = xor i1 %9, %12
  %14 = or i1 %6, %13
  %15 = zext i1 %14 to i8
  store i8 %15, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %14, i64 %rel_off1, i64 %rel_off2
  %16 = add i64 %.v, %3
  store i64 %16, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_401f37(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_401f69(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_MINUS0x10__rbp____eax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -16
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RAX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_subl__0x400___eax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 5
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, -1024
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RAX, align 8
  %9 = icmp ult i32 %6, 1024
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = and i32 %7, 255
  %13 = tail call i32 @llvm.ctpop.i32(i32 %12)
  %14 = trunc i32 %13 to i8
  %15 = and i8 %14, 1
  %16 = xor i8 %15, 1
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %16, i8* %17, align 1
  %18 = xor i32 %7, %6
  %19 = lshr i32 %18, 4
  %20 = trunc i32 %19 to i8
  %21 = and i8 %20, 1
  %22 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %21, i8* %22, align 1
  %23 = icmp eq i32 %7, 0
  %24 = zext i1 %23 to i8
  %25 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %24, i8* %25, align 1
  %26 = lshr i32 %7, 31
  %27 = trunc i32 %26 to i8
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %27, i8* %28, align 1
  %29 = lshr i32 %6, 31
  %30 = xor i32 %26, %29
  %31 = add nuw nsw i32 %30, %29
  %32 = icmp eq i32 %31, 2
  %33 = zext i1 %32 to i8
  %34 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %33, i8* %34, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__eax__MINUS0x10__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %EAX = bitcast %union.anon* %3 to i32*
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %4 = load i64, i64* %RBP, align 8
  %5 = add i64 %4, -16
  %6 = load i32, i32* %EAX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_4020fa(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_cmpl__0x200__MINUS0x10__rbp_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -16
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 7
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = add i32 %8, -512
  %10 = icmp ult i32 %8, 512
  %11 = zext i1 %10 to i8
  %12 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %11, i8* %12, align 1
  %13 = and i32 %9, 255
  %14 = tail call i32 @llvm.ctpop.i32(i32 %13)
  %15 = trunc i32 %14 to i8
  %16 = and i8 %15, 1
  %17 = xor i8 %16, 1
  %18 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %17, i8* %18, align 1
  %19 = xor i32 %9, %8
  %20 = lshr i32 %19, 4
  %21 = trunc i32 %20 to i8
  %22 = and i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %22, i8* %23, align 1
  %24 = icmp eq i32 %9, 0
  %25 = zext i1 %24 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %9, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %8, 31
  %31 = xor i32 %27, %30
  %32 = add nuw nsw i32 %31, %30
  %33 = icmp eq i32 %32, 2
  %34 = zext i1 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %34, i8* %35, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jae_.L_4020dc(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %5 = load i8, i8* %4, align 1
  %6 = icmp eq i8 %5, 0
  %7 = zext i1 %6 to i8
  store i8 %7, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %8 = add i64 %.v, %3
  store i64 %8, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jne_.L_40201d(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp eq i8 %5, 0
  %7 = zext i1 %6 to i8
  store i8 %7, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %8 = add i64 %.v, %3
  store i64 %8, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jle_.L_402018(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp ne i8 %5, 0
  %7 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %8 = load i8, i8* %7, align 1
  %9 = icmp ne i8 %8, 0
  %10 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %11 = load i8, i8* %10, align 1
  %12 = icmp ne i8 %11, 0
  %13 = xor i1 %9, %12
  %14 = or i1 %6, %13
  %15 = zext i1 %14 to i8
  store i8 %15, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %14, i64 %rel_off1, i64 %rel_off2
  %16 = add i64 %.v, %3
  store i64 %16, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_401feb(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_40201d(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_402022(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jbe_.L_4020d7(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  %5 = load i8, i8* %4, align 1
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %7 = load i8, i8* %6, align 1
  %8 = or i8 %7, %5
  %9 = icmp ne i8 %8, 0
  %10 = zext i1 %9 to i8
  store i8 %10, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %9, i64 %rel_off1, i64 %rel_off2
  %11 = add i64 %.v, %3
  store i64 %11, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jne_.L_4020d2(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp eq i8 %5, 0
  %7 = zext i1 %6 to i8
  store i8 %7, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %6, i64 %rel_off1, i64 %rel_off2
  %8 = add i64 %.v, %3
  store i64 %8, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jle_.L_4020cd(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i8* nocapture %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  %5 = load i8, i8* %4, align 1
  %6 = icmp ne i8 %5, 0
  %7 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  %8 = load i8, i8* %7, align 1
  %9 = icmp ne i8 %8, 0
  %10 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  %11 = load i8, i8* %10, align 1
  %12 = icmp ne i8 %11, 0
  %13 = xor i1 %9, %12
  %14 = or i1 %6, %13
  %15 = zext i1 %14 to i8
  store i8 %15, i8* %BRANCH_TAKEN, align 1
  %.v = select i1 %14, i64 %rel_off1, i64 %rel_off2
  %16 = add i64 %.v, %3
  store i64 %16, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_4020a0(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_4020d2(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_jmpq_.L_4020f5(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned, i64 %rel_off1, i64 %rel_off2) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, %rel_off1
  store i64 %4, i64* %PC, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_MINUS0x10__rbp____ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -16
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_subl__0x200___ecx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 6
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, -512
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RCX, align 8
  %9 = icmp ult i32 %6, 512
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = and i32 %7, 255
  %13 = tail call i32 @llvm.ctpop.i32(i32 %12)
  %14 = trunc i32 %13 to i8
  %15 = and i8 %14, 1
  %16 = xor i8 %15, 1
  %17 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %16, i8* %17, align 1
  %18 = xor i32 %7, %6
  %19 = lshr i32 %18, 4
  %20 = trunc i32 %19 to i8
  %21 = and i8 %20, 1
  %22 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %21, i8* %22, align 1
  %23 = icmp eq i32 %7, 0
  %24 = zext i1 %23 to i8
  %25 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %24, i8* %25, align 1
  %26 = lshr i32 %7, 31
  %27 = trunc i32 %26 to i8
  %28 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %27, i8* %28, align 1
  %29 = lshr i32 %6, 31
  %30 = xor i32 %26, %29
  %31 = add nuw nsw i32 %30, %29
  %32 = icmp eq i32 %31, 2
  %33 = zext i1 %32 to i8
  %34 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %33, i8* %34, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movq_MINUS0x8__rbp____rcx(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %RBP, align 8
  %4 = add i64 %3, -8
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 4
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %RCX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__eax____rcx_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %EAX = bitcast %union.anon* %3 to i32*
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %4 = bitcast i64* %RCX to i32**
  %5 = load i32*, i32** %4, align 8
  %6 = load i32, i32* %EAX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 2
  store i64 %8, i64* %PC, align 8
  store i32 %6, i32* %5, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl_0x50__rcx____eax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %3 = load i64, i64* %RCX, align 8
  %4 = add i64 %3, 80
  %5 = load i64, i64* %PC, align 8
  %6 = add i64 %5, 3
  store i64 %6, i64* %PC, align 8
  %7 = inttoptr i64 %4 to i32*
  %8 = load i32, i32* %7, align 4
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %RAX, align 8
  ret %struct.Memory* %2
}

; Function Attrs: nounwind
define %struct.Memory* @routine_addl__0x1___eax(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #3 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RAX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0, i32 0
  %3 = load i64, i64* %RAX, align 8
  %4 = load i64, i64* %PC, align 8
  %5 = add i64 %4, 3
  store i64 %5, i64* %PC, align 8
  %6 = trunc i64 %3 to i32
  %7 = add i32 %6, 1
  %8 = zext i32 %7 to i64
  store i64 %8, i64* %RAX, align 8
  %9 = icmp eq i32 %6, -1
  %10 = icmp eq i32 %7, 0
  %11 = or i1 %9, %10
  %12 = zext i1 %11 to i8
  %13 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 1
  store i8 %12, i8* %13, align 1
  %14 = and i32 %7, 255
  %15 = tail call i32 @llvm.ctpop.i32(i32 %14)
  %16 = trunc i32 %15 to i8
  %17 = and i8 %16, 1
  %18 = xor i8 %17, 1
  %19 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 3
  store i8 %18, i8* %19, align 1
  %20 = xor i32 %7, %6
  %21 = lshr i32 %20, 4
  %22 = trunc i32 %21 to i8
  %23 = and i8 %22, 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 5
  store i8 %23, i8* %24, align 1
  %25 = zext i1 %10 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %7, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %6, 31
  %31 = xor i32 %27, %30
  %32 = add nuw nsw i32 %31, %27
  %33 = icmp eq i32 %32, 2
  %34 = zext i1 %33 to i8
  %35 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 2, i32 13
  store i8 %34, i8* %35, align 1
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_movl__eax__0x50__rcx_(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 1, i32 0
  %EAX = bitcast %union.anon* %3 to i32*
  %RCX = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 5, i32 0, i32 0
  %4 = load i64, i64* %RCX, align 8
  %5 = add i64 %4, 80
  %6 = load i32, i32* %EAX, align 4
  %7 = load i64, i64* %PC, align 8
  %8 = add i64 %7, 3
  store i64 %8, i64* %PC, align 8
  %9 = inttoptr i64 %5 to i32*
  store i32 %6, i32* %9, align 4
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_popq__rbp(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %RBP = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 15, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, 1
  store i64 %4, i64* %PC, align 8
  %5 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %6 = load i64, i64* %5, align 8
  %7 = add i64 %6, 8
  %8 = inttoptr i64 %6 to i64*
  %9 = load i64, i64* %8, align 8
  store i64 %9, i64* %RBP, align 8
  store i64 %7, i64* %5, align 8
  ret %struct.Memory* %2
}

; Function Attrs: norecurse nounwind
define %struct.Memory* @routine_retq(%struct.State* nocapture dereferenceable(3376), i64, %struct.Memory* readnone returned) local_unnamed_addr #2 {
block_400488:
  %PC = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  %3 = load i64, i64* %PC, align 8
  %4 = add i64 %3, 1
  store i64 %4, i64* %PC, align 8
  %5 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 13, i32 0, i32 0
  %6 = load i64, i64* %5, align 8
  %7 = inttoptr i64 %6 to i64*
  %8 = load i64, i64* %7, align 8
  store i64 %8, i64* %PC, align 8
  %9 = add i64 %6, 8
  store i64 %9, i64* %5, align 8
  ret %struct.Memory* %2
}

attributes #0 = { nounwind readnone }
attributes #1 = { alwaysinline nounwind }
attributes #2 = { norecurse nounwind }
attributes #3 = { nounwind }
