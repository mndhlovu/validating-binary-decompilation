; ModuleID = 'test.bc'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu-elf"
%union.anon = type { i64 }
%seg_400370__init_type = type <{ [23 x i8] }>
%seg_400390__text_type = type <{ [354 x i8] }>
%seg_4004f4__fini_type = type <{ [9 x i8] }>
%seg_400500__rodata_type = type <{ [4 x i8] }>
%seg_400504__eh_frame_hdr_type = type <{ [52 x i8] }>
%seg_400538__eh_frame_type = type <{ [208 x i8] }>
%seg_600e50__init_array_type = type <{ i64, i64 }>
%seg_601018__data_type = type <{ [16 x i8] }>
%__bss_start_type = type <{ [8 x i8] }>
%struct.State = type { %struct.ArchState, [32 x %union.VectorReg], %struct.ArithFlags, %union.anon, %struct.Segments, %struct.AddressSpace, %struct.GPR, %struct.X87Stack, %struct.MMX, %struct.FPUStatusFlags, %union.anon, %union.FPU, %struct.SegmentCaches }
%struct.ArchState = type { i32, i32, %union.anon }
%union.VectorReg = type { %union.vec512_t }
%union.vec512_t = type { %struct.uint64v8_t }
%struct.uint64v8_t = type { [8 x i64] }
%struct.ArithFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.Segments = type { i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector }
%union.SegmentSelector = type { i16 }
%struct.AddressSpace = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.Reg = type { %union.anon }
%struct.GPR = type { i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg, i64, %struct.Reg }
%struct.X87Stack = type { [8 x %struct.anon.3] }
%struct.anon.3 = type { i64, double }
%struct.MMX = type { [8 x %struct.anon.4] }
%struct.anon.4 = type { i64, %union.vec64_t }
%union.vec64_t = type { %struct.uint64v1_t }
%struct.uint64v1_t = type { [1 x i64] }
%struct.FPUStatusFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, [4 x i8] }
%union.FPU = type { %struct.anon.13 }
%struct.anon.13 = type { %struct.FpuFXSAVE, [96 x i8] }
%struct.FpuFXSAVE = type { %union.SegmentSelector, %union.SegmentSelector, %union.FPUAbridgedTagWord, i8, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, %union.FPUControlStatus, %union.FPUControlStatus, [8 x %struct.FPUStackElem], [16 x %union.vec128_t] }
%union.FPUAbridgedTagWord = type { i8 }
%union.FPUControlStatus = type { i32 }
%struct.FPUStackElem = type { %union.anon.11, [6 x i8] }
%union.anon.11 = type { %struct.float80_t }
%struct.float80_t = type { [10 x i8] }
%union.vec128_t = type { %struct.uint128v1_t }
%struct.uint128v1_t = type { [1 x i128] }
%struct.SegmentCaches = type { %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow, %struct.SegmentShadow }
%struct.SegmentShadow = type { %union.anon, i32, i32 }
%struct.Memory = type opaque
%struct.uint8v32_t = type { [32 x i8] }
%struct.anon.2 = type { i8, i8 }
%struct.int32v8_t = type { [8 x i32] }
%struct.float32v8_t = type { [8 x float] }
%struct.uint8v8_t = type { [8 x i8] }
%struct.uint8v16_t = type { [16 x i8] }
%struct.uint16v8_t = type { [8 x i16] }
%struct.uint8v4_t = type { [4 x i8] }
%struct.int16v4_t = type { [4 x i16] }
%struct.int32v4_t = type { [4 x i32] }
%struct.uint64v2_t = type { [2 x i64] }
%struct.uint64v4_t = type { [4 x i64] }
%struct.uint128v2_t = type { [2 x i128] }
%struct.uint16v16_t = type { [16 x i16] }
%struct.float64v4_t = type { [4 x double] }
%"class.(anonymous namespace)::BitMatrix" = type { %"class.std::bitset", [16 x [16 x i8]] }
%"class.std::bitset" = type { %struct.uint64v4_t }
%struct.bcd80_t = type { [9 x %union.FPUAbridgedTagWord], %union.FPUAbridgedTagWord }
%struct.FpuFSAVE = type { %union.SegmentSelector, i16, %union.SegmentSelector, i16, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, i32, %union.SegmentSelector, i16, [8 x %struct.FPUStackElem] }
%struct.anon.5 = type { i32, i32 }
@switch.table = private unnamed_addr constant [4 x i32] [i32 2048, i32 3072, i32 0, i32 1024]
@DR0 = external global i64, align 8
@DR1 = external global i64, align 8
@DR2 = external global i64, align 8
@DR3 = external global i64, align 8
@DR4 = external global i64, align 8
@DR5 = external global i64, align 8
@DR6 = external global i64, align 8
@DR7 = external global i64, align 8
@gCR0 = external global %union.anon, align 1
@gCR1 = external global %union.anon, align 1
@gCR2 = external global %union.anon, align 1
@gCR3 = external global %union.anon, align 1
@gCR4 = external global %union.anon, align 1
@gCR8 = external global %union.anon, align 1
@seg_400370__init = internal constant %seg_400370__init_type <{ [23 x i8] c"H\83\EC\08H\8B\05}\0C \00H\85\C0t\02\FF\D0H\83\C4\08\C3" }>
@seg_400390__text = internal constant %seg_400390__text_type <{ [354 x i8] c"1\EDI\89\D1^H\89\E2H\83\E4\F0PTI\C7\C0\F0\04@\00H\C7\C1\80\04@\00H\C7\C7x\04@\00\FF\156\0C \00\F4\0F\1FD\00\00\F3\C3f.\0F\1F\84\00\00\00\00\00\0F\1F@\00U\B8(\10`\00H=(\10`\00H\89\E5t\17\B8\00\00\00\00H\85\C0t\0D]\BF(\10`\00\FF\E0\0F\1FD\00\00]\C3f\0F\1FD\00\00\BE(\10`\00UH\81\EE(\10`\00H\89\E5H\C1\FE\03H\89\F0H\C1\E8?H\01\C6H\D1\FEt\15\B8\00\00\00\00H\85\C0t\0B]\BF(\10`\00\FF\E0\0F\1F\00]\C3f\0F\1FD\00\00\80=\E1\0B \00\00u\17UH\89\E5\E8~\FF\FF\FF\C6\05\CF\0B \00\01]\C3\0F\1FD\00\00\F3\C3\0F\1F@\00f.\0F\1F\84\00\00\00\00\00UH\89\E5]\EB\89\90U\C3f\0F\1FD\00\00AWAVI\89\D7AUATL\8D%\BE\09 \00UH\8D-\BE\09 \00SA\89\FDI\89\F6L)\E5H\83\EC\08H\C1\FD\03\E8\BF\FE\FF\FFH\85\EDt 1\DB\0F\1F\84\00\00\00\00\00L\89\FAL\89\F6D\89\EFA\FF\14\DCH\83\C3\01H9\DDu\EAH\83\C4\08[]A\5CA]A^A_\C3\90f.\0F\1F\84\00\00\00\00\00\F3\C3" }>
@seg_4004f4__fini = internal constant %seg_4004f4__fini_type <{ [9 x i8] c"H\83\EC\08H\83\C4\08\C3" }>
@seg_400500__rodata = internal constant %seg_400500__rodata_type <{ [4 x i8] c"\01\00\02\00" }>
@seg_400504__eh_frame_hdr = internal constant %seg_400504__eh_frame_hdr_type <{ [52 x i8] c"\01\1B\03;0\00\00\00\05\00\00\00\8C\FE\FF\FFL\00\00\00\BC\FE\FF\FFx\00\00\00t\FF\FF\FF\8C\00\00\00|\FF\FF\FF\A4\00\00\00\EC\FF\FF\FF\EC\00\00\00" }>
@seg_400538__eh_frame = internal constant %seg_400538__eh_frame_type <{ [208 x i8] c"\14\00\00\00\00\00\00\00\01zR\00\01x\10\01\1B\0C\07\08\90\01\07\10\10\00\00\00\1C\00\00\008\FE\FF\FF+\00\00\00\00\00\00\00\14\00\00\00\00\00\00\00\01zR\00\01x\10\01\1B\0C\07\08\90\01\00\00\10\00\00\00\1C\00\00\00<\FE\FF\FF\02\00\00\00\00\00\00\00\14\00\00\000\00\00\00\E0\FE\FF\FF\02\00\00\00\00\00\00\00\00\00\00\00D\00\00\00H\00\00\00\D0\FE\FF\FFe\00\00\00\00B\0E\10\8F\02B\0E\18\8E\03E\0E \8D\04B\0E(\8C\05H\0E0\86\06H\0E8\83\07M\0E@r\0E8A\0E0A\0E(B\0E B\0E\18B\0E\10B\0E\08\00\10\00\00\00\90\00\00\00\F8\FE\FF\FF\02\00\00\00\00\00\00\00\00\00\00\00" }>
@seg_601018__data = internal global %seg_601018__data_type zeroinitializer
@__bss_start = global %__bss_start_type zeroinitializer
@0 = internal global i1 false
%seg_400504__fini_type = type <{ [9 x i8] }>
%seg_400510__rodata_type = type <{ [4 x i8] }>
%seg_400514__eh_frame_hdr_type = type <{ [52 x i8] }>
%seg_400548__eh_frame_type = type <{ [208 x i8] }>
@seg_400504__fini = internal constant %seg_400504__fini_type <{ [9 x i8] c"H\83\EC\08H\83\C4\08\C3" }>
@seg_400510__rodata = internal constant %seg_400510__rodata_type <{ [4 x i8] c"\01\00\02\00" }>
@seg_400514__eh_frame_hdr = internal constant %seg_400514__eh_frame_hdr_type <{ [52 x i8] c"\01\1B\03;0\00\00\00\05\00\00\00|\FE\FF\FFL\00\00\00\AC\FE\FF\FFx\00\00\00d\FF\FF\FF\8C\00\00\00|\FF\FF\FF\A4\00\00\00\EC\FF\FF\FF\EC\00\00\00" }>
@seg_400548__eh_frame = internal constant %seg_400548__eh_frame_type <{ [208 x i8] c"\14\00\00\00\00\00\00\00\01zR\00\01x\10\01\1B\0C\07\08\90\01\07\10\10\00\00\00\1C\00\00\00(\FE\FF\FF+\00\00\00\00\00\00\00\14\00\00\00\00\00\00\00\01zR\00\01x\10\01\1B\0C\07\08\90\01\00\00\10\00\00\00\1C\00\00\00,\FE\FF\FF\02\00\00\00\00\00\00\00\14\00\00\000\00\00\00\D0\FE\FF\FF\0C\00\00\00\00\00\00\00\00\00\00\00D\00\00\00H\00\00\00\D0\FE\FF\FFe\00\00\00\00B\0E\10\8F\02B\0E\18\8E\03E\0E \8D\04B\0E(\8C\05H\0E0\86\06H\0E8\83\07M\0E@r\0E8A\0E0A\0E(B\0E B\0E\18B\0E\10B\0E\08\00\10\00\00\00\90\00\00\00\F8\FE\FF\FF\02\00\00\00\00\00\00\00\00\00\00\00" }>

; Function Declaraions
declare %struct.Memory* @__remill_error(%struct.State* dereferenceable(3376), i64, %struct.Memory*)
declare i32 @__remill_fpu_exception_test_and_clear(i32, i32)
declare void @llvm.lifetime.start(i64, i8* nocapture)
declare void @llvm.lifetime.end(i64, i8* nocapture)
declare i32 @llvm.ctpop.i32(i32)
declare i32 @llvm.bswap.i32(i32)
declare i64 @llvm.bswap.i64(i64)
declare i32 @llvm.cttz.i32(i32, i1)
declare i64 @llvm.cttz.i64(i64, i1)
declare i32 @llvm.ctlz.i32(i32, i1)
declare i64 @llvm.ctlz.i64(i64, i1)
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1)
declare double @llvm.nearbyint.f64(double)
declare double @llvm.fabs.f64(double)
declare <4 x double> @llvm.nearbyint.v4f64(<4 x double>)
declare <4 x double> @llvm.fabs.v4f64(<4 x double>)
declare double @llvm.trunc.f64(double)
declare <4 x double> @llvm.trunc.v4f64(<4 x double>)
declare float @llvm.nearbyint.f32(float)
declare float @llvm.fabs.f32(float)
declare <4 x float> @llvm.nearbyint.v4f32(<4 x float>)
declare <4 x float> @llvm.fabs.v4f32(<4 x float>)
declare float @llvm.trunc.f32(float)
declare <4 x float> @llvm.trunc.v4f32(<4 x float>)
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1)

; Buiitins  
declare double @atan(double) local_unnamed_addr #9
declare double @cos(double) local_unnamed_addr #9
declare double @sin(double) local_unnamed_addr #9
declare double @sqrt(double) local_unnamed_addr #9
declare double @tan(double) local_unnamed_addr #9
declare extern_weak x86_64_sysvcc i64 @abort() #18
declare extern_weak x86_64_sysvcc i64 @abs(i64) #18
declare extern_weak x86_64_sysvcc i64 @asin(i64) #18
declare extern_weak x86_64_sysvcc i64 @atof(i64) #18
declare extern_weak x86_64_sysvcc i64 @atoi(i64) #18
declare extern_weak x86_64_sysvcc i64 @atol(i64) #18
declare extern_weak x86_64_sysvcc i64 @calloc(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @clock() #18
declare extern_weak x86_64_sysvcc i64 @cosf(i64) #18
declare extern_weak x86_64_sysvcc i64 @exit(i64) #18
declare extern_weak x86_64_sysvcc i64 @exp(i64) #18
declare extern_weak x86_64_sysvcc i64 @fflush(i64) #18
declare extern_weak x86_64_sysvcc i64 @floor(i64) #18
declare extern_weak x86_64_sysvcc i64 @fprintf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @fputs(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @free(i64) #18
declare extern_weak x86_64_sysvcc i64 @fwrite(i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @getchar() #18
declare extern_weak x86_64_sysvcc i64 @gettimeofday(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @__isoc99_fscanf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @__isoc99_scanf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @__isoc99_sscanf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @log(i64) #18
declare extern_weak x86_64_sysvcc i64 @lrand48() #18
declare extern_weak x86_64_sysvcc i64 @malloc(i64) #18
declare extern_weak x86_64_sysvcc i64 @memalign(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @memcpy(i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @memset(i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @perror(i64) #18
declare extern_weak x86_64_sysvcc i64 @posix_memalign(i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @pow(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @printf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @putchar(i64) #18
declare extern_weak x86_64_sysvcc i64 @puts(i64) #18
declare extern_weak x86_64_sysvcc i64 @rand() #18
declare extern_weak x86_64_sysvcc i64 @random() #18
declare extern_weak x86_64_sysvcc i64 @realloc(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @seed48(i64) #18
declare extern_weak x86_64_sysvcc i64 @sinf(i64) #18
declare extern_weak x86_64_sysvcc i64 @sprintf(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @srand(i64) #18
declare extern_weak x86_64_sysvcc i64 @strcat(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @strcmp(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @strcpy(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @strdup(i64) #18
declare extern_weak x86_64_sysvcc i64 @strlen(i64) #18
declare extern_weak x86_64_sysvcc i64 @strncmp(i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @strtoll(i64, i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @time(i64) #18
declare extern_weak x86_64_sysvcc i64 @ungetc(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @pthread_join(i64, i64) #18
declare extern_weak x86_64_sysvcc i64 @pthread_create(i64, i64, i64, i64) #18

declare %struct.Memory* @__remill_function_call(%struct.State* dereferenceable(3376), i64, %struct.Memory*)

define internal %struct.Memory* @ext_pthread_create(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64)* @pthread_create to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_pthread_join(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @pthread_join to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_gettimeofday(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @gettimeofday to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext___isoc99_scanf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @__isoc99_scanf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}


define internal %struct.Memory* @ext_fflush(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @fflush to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext___isoc99_fscanf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @__isoc99_fscanf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext___isoc99_sscanf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @__isoc99_sscanf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_memcpy(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64)* @memcpy to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_posix_memalign(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64)* @posix_memalign to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_fprintf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @fprintf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_memset(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64)* @memset to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_perror(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @perror to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_floor(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @floor to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_malloc(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @malloc to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_atoi(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @atoi to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strcpy(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @strcpy to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_ungetc(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @ungetc to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_pow(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @pow to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_seed48(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @seed48 to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_asin(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @asin to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strdup(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @strdup to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_cosf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @cosf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_abs(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @abs to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_abort(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 ()* @abort to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_exit(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @exit to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_putchar(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @putchar to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strncmp(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64)* @strncmp to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strcmp(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @strcmp to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_rand(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 ()* @rand to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_calloc(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @calloc to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_fwrite(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64)* @fwrite to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_realloc(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @realloc to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}


define internal %struct.Memory* @ext_log(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @log to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}


define internal %struct.Memory* @ext_exp(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @exp to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_sprintf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @sprintf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strcat(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @strcat to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_puts(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @puts to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_atol(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @atol to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_atof(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @atof to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_clock(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 ()* @clock to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strlen(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @strlen to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_free(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @free to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_srand(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @srand to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_fputs(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @fputs to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_strtoll(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64)* @strtoll to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_sinf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @sinf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_time(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64)* @time to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_printf(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64)* @printf to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_memalign(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 (i64, i64)* @memalign to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_random(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 ()* @random to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_lrand48(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 ()* @lrand48 to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

define internal %struct.Memory* @ext_getchar(%struct.State*, i64, %struct.Memory*) #18 {
  %4 = call %struct.Memory* @__remill_function_call(%struct.State* %0, i64 ptrtoint (i64 ()* @getchar to i64), %struct.Memory* %2)
  ret %struct.Memory* %4
}

declare %struct.Memory* @ext_sqrt(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias)
declare %struct.Memory* @ext_atan(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias)
declare %struct.Memory* @ext_cos(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias) 
declare %struct.Memory* @ext_tan(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias) 
declare %struct.Memory* @ext_sin(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias) 

declare %struct.Memory* @sub_400550.Fit(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias readnone returned)
declare %struct.Memory* @sub_4005e0.Place(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias readnone returned)
declare %struct.Memory* @sub_400770.Trial(%struct.State* noalias dereferenceable(3376), i64, %struct.Memory* noalias readnone returned)

; Data Access Globals
%G_0x603a94_type = type <{ [4 x i8] }>
@G_0x603a94= global %G_0x603a94_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x603aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x603aa0___rsi_4_= global %G_0x603aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6042a0___rsi_4__type = type <{ [4 x i8] }>
@G_0x6042a0___rsi_4_= global %G_0x6042a0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x604aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x604aa0___rsi_4_= global %G_0x604aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6052a0___rsi_4__type = type <{ [4 x i8] }>
@G_0x6052a0___rsi_4_= global %G_0x6052a0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x605aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x605aa0___rsi_4_= global %G_0x605aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6062a0___rsi_4__type = type <{ [4 x i8] }>
@G_0x6062a0___rsi_4_= global %G_0x6062a0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x606aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x606aa0___rsi_4_= global %G_0x606aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6072a0___rsi_4__type = type <{ [4 x i8] }>
@G_0x6072a0___rsi_4_= global %G_0x6072a0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x607aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x607aa0___rsi_4_= global %G_0x607aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6082a0___rsi_4__type = type <{ [4 x i8] }>
@G_0x6082a0___rsi_4_= global %G_0x6082a0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x608aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x608aa0___rsi_4_= global %G_0x608aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6092a0___rsi_4__type = type <{ [4 x i8] }>
@G_0x6092a0___rsi_4_= global %G_0x6092a0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x609aa0___rsi_4__type = type <{ [4 x i8] }>
@G_0x609aa0___rsi_4_= global %G_0x609aa0___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x60df50___rax_4__type = type <{ [4 x i8] }>
@G_0x60df50___rax_4_= global %G_0x60df50___rax_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x60df50___rsi_4__type = type <{ [4 x i8] }>
@G_0x60df50___rsi_4_= global %G_0x60df50___rsi_4__type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610250_type = type <{ [4 x i8] }>
@G_0x610250= global %G_0x610250_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610254_type = type <{ [4 x i8] }>
@G_0x610254= global %G_0x610254_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610258_type = type <{ [4 x i8] }>
@G_0x610258= global %G_0x610258_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x61025c_type = type <{ [4 x i8] }>
@G_0x61025c= global %G_0x61025c_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610260_type = type <{ [4 x i8] }>
@G_0x610260= global %G_0x610260_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610264_type = type <{ [4 x i8] }>
@G_0x610264= global %G_0x610264_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610268_type = type <{ [4 x i8] }>
@G_0x610268= global %G_0x610268_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x61026c_type = type <{ [4 x i8] }>
@G_0x61026c= global %G_0x61026c_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610270_type = type <{ [4 x i8] }>
@G_0x610270= global %G_0x610270_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610274_type = type <{ [4 x i8] }>
@G_0x610274= global %G_0x610274_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610278_type = type <{ [4 x i8] }>
@G_0x610278= global %G_0x610278_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x61027c_type = type <{ [4 x i8] }>
@G_0x61027c= global %G_0x61027c_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x610280_type = type <{ [4 x i8] }>
@G_0x610280= global %G_0x610280_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191c4_type = type <{ [4 x i8] }>
@G_0x6191c4= global %G_0x6191c4_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191e0_type = type <{ [4 x i8] }>
@G_0x6191e0= global %G_0x6191e0_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191e4_type = type <{ [4 x i8] }>
@G_0x6191e4= global %G_0x6191e4_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191e8_type = type <{ [4 x i8] }>
@G_0x6191e8= global %G_0x6191e8_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191ec_type = type <{ [4 x i8] }>
@G_0x6191ec= global %G_0x6191ec_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191f0_type = type <{ [4 x i8] }>
@G_0x6191f0= global %G_0x6191f0_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191f4_type = type <{ [4 x i8] }>
@G_0x6191f4= global %G_0x6191f4_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191f8_type = type <{ [4 x i8] }>
@G_0x6191f8= global %G_0x6191f8_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x6191fc_type = type <{ [4 x i8] }>
@G_0x6191fc= global %G_0x6191fc_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619200_type = type <{ [4 x i8] }>
@G_0x619200= global %G_0x619200_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619204_type = type <{ [4 x i8] }>
@G_0x619204= global %G_0x619204_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619208_type = type <{ [4 x i8] }>
@G_0x619208= global %G_0x619208_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x61920c_type = type <{ [4 x i8] }>
@G_0x61920c= global %G_0x61920c_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619210_type = type <{ [4 x i8] }>
@G_0x619210= global %G_0x619210_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619214_type = type <{ [4 x i8] }>
@G_0x619214= global %G_0x619214_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619218_type = type <{ [4 x i8] }>
@G_0x619218= global %G_0x619218_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x61921c_type = type <{ [4 x i8] }>
@G_0x61921c= global %G_0x61921c_type <{ [4 x i8] c"\00\00\00\00" }>
%G_0x619220_type = type <{ [4 x i8] }>
@G_0x619220= global %G_0x619220_type <{ [4 x i8] c"\00\00\00\00" }>
%G__0x401344_type = type <{ [8 x i8] }>
@G__0x401344= global %G__0x401344_type <{ [8 x i8] c"\00\00\00\00\00\00\00\00" }>
%G__0x401356_type = type <{ [8 x i8] }>
@G__0x401356= global %G__0x401356_type <{ [8 x i8] c"\00\00\00\00\00\00\00\00" }>
%G__0x401369_type = type <{ [8 x i8] }>
@G__0x401369= global %G__0x401369_type <{ [8 x i8] c"\00\00\00\00\00\00\00\00" }>
%G__0x40137c_type = type <{ [8 x i8] }>
@G__0x40137c= global %G__0x40137c_type <{ [8 x i8] c"\00\00\00\00\00\00\00\00" }>
%G__0x603aa0_type = type <{ [8 x i8] }>
@G__0x603aa0= global %G__0x603aa0_type <{ [8 x i8] c"\00\00\00\00\00\00\00\00" }>


define %struct.Memory* @Puzzle(%struct.State* noalias , i64, %struct.Memory* noalias) alwaysinline  {
entry:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i64 0, i32 6, i32 33, i32 0, i32 0
  store i64 %1, i64* %3, align 8
  %BRANCH_TAKEN = alloca i8, align 1
  store i8 0, i8* %BRANCH_TAKEN, align 1
  %MEMORY = alloca %struct.Memory*, align 8
  store %struct.Memory* %2, %struct.Memory** %MEMORY, align 8

  ; Code: .Puzzle:	 RIP: 400840	 Bytes: 0
  ; Code: pushq %rbp	 RIP: 400840	 Bytes: 1
  %loadMem_400840 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400840 = call %struct.Memory* @routine_pushq__rbp(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400840)
  store %struct.Memory* %call_400840, %struct.Memory** %MEMORY

  ; Code: movq %rsp, %rbp	 RIP: 400841	 Bytes: 3
  %loadMem_400841 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400841 = call %struct.Memory* @routine_movq__rsp___rbp(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400841)
  store %struct.Memory* %call_400841, %struct.Memory** %MEMORY

  ; Code: subq $0x30, %rsp	 RIP: 400844	 Bytes: 4
  %loadMem_400844 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400844 = call %struct.Memory* @routine_subq__0x30___rsp(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400844)
  store %struct.Memory* %call_400844, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x10(%rbp)	 RIP: 400848	 Bytes: 7
  %loadMem_400848 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400848 = call %struct.Memory* @routine_movl__0x0__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400848)
  store %struct.Memory* %call_400848, %struct.Memory** %MEMORY

  ; Code: .L_40084f:	 RIP: 40084f	 Bytes: 0
  br label %block_.L_40084f
block_.L_40084f:

  ; Code: cmpl $0x1ff, -0x10(%rbp)	 RIP: 40084f	 Bytes: 7
  %loadMem_40084f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40084f = call %struct.Memory* @routine_cmpl__0x1ff__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40084f)
  store %struct.Memory* %call_40084f, %struct.Memory** %MEMORY

  ; Code: jg .L_400879	 RIP: 400856	 Bytes: 6
  %loadMem_400856 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400856 = call %struct.Memory* @routine_jg_.L_400879(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400856, i8* %BRANCH_TAKEN, i64 35, i64 6, i64 6)
  store %struct.Memory* %call_400856, %struct.Memory** %MEMORY

  %loadBr_400856 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400856 = icmp eq i8 %loadBr_400856, 1
  br i1 %cmpBr_400856, label %block_.L_400879, label %block_40085c

block_40085c:
  ; Code: movslq -0x10(%rbp), %rax	 RIP: 40085c	 Bytes: 4
  %loadMem_40085c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40085c = call %struct.Memory* @routine_movslq_MINUS0x10__rbp____rax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40085c)
  store %struct.Memory* %call_40085c, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x60df50(,%rax,4)	 RIP: 400860	 Bytes: 11
  %loadMem_400860 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400860 = call %struct.Memory* @routine_movl__0x1__0x60df50___rax_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400860)
  store %struct.Memory* %call_400860, %struct.Memory** %MEMORY

  ; Code: movl -0x10(%rbp), %eax	 RIP: 40086b	 Bytes: 3
  %loadMem_40086b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40086b = call %struct.Memory* @routine_movl_MINUS0x10__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40086b)
  store %struct.Memory* %call_40086b, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 40086e	 Bytes: 3
  %loadMem_40086e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40086e = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40086e)
  store %struct.Memory* %call_40086e, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x10(%rbp)	 RIP: 400871	 Bytes: 3
  %loadMem_400871 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400871 = call %struct.Memory* @routine_movl__eax__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400871)
  store %struct.Memory* %call_400871, %struct.Memory** %MEMORY

  ; Code: jmpq .L_40084f	 RIP: 400874	 Bytes: 5
  %loadMem_400874 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400874 = call %struct.Memory* @routine_jmpq_.L_40084f(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400874, i64 -37, i64 5)
  store %struct.Memory* %call_400874, %struct.Memory** %MEMORY

  br label %block_.L_40084f

  ; Code: .L_400879:	 RIP: 400879	 Bytes: 0
block_.L_400879:

  ; Code: movl $0x1, -0x4(%rbp)	 RIP: 400879	 Bytes: 7
  %loadMem_400879 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400879 = call %struct.Memory* @routine_movl__0x1__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400879)
  store %struct.Memory* %call_400879, %struct.Memory** %MEMORY

  ; Code: .L_400880:	 RIP: 400880	 Bytes: 0
  br label %block_.L_400880
block_.L_400880:

  ; Code: cmpl $0x5, -0x4(%rbp)	 RIP: 400880	 Bytes: 4
  %loadMem_400880 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400880 = call %struct.Memory* @routine_cmpl__0x5__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400880)
  store %struct.Memory* %call_400880, %struct.Memory** %MEMORY

  ; Code: jg .L_400901	 RIP: 400884	 Bytes: 6
  %loadMem_400884 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400884 = call %struct.Memory* @routine_jg_.L_400901(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400884, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400884, %struct.Memory** %MEMORY

  %loadBr_400884 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400884 = icmp eq i8 %loadBr_400884, 1
  br i1 %cmpBr_400884, label %block_.L_400901, label %block_40088a

block_40088a:
  ; Code: movl $0x1, -0x8(%rbp)	 RIP: 40088a	 Bytes: 7
  %loadMem_40088a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40088a = call %struct.Memory* @routine_movl__0x1__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40088a)
  store %struct.Memory* %call_40088a, %struct.Memory** %MEMORY

  ; Code: .L_400891:	 RIP: 400891	 Bytes: 0
  br label %block_.L_400891
block_.L_400891:

  ; Code: cmpl $0x5, -0x8(%rbp)	 RIP: 400891	 Bytes: 4
  %loadMem_400891 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400891 = call %struct.Memory* @routine_cmpl__0x5__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400891)
  store %struct.Memory* %call_400891, %struct.Memory** %MEMORY

  ; Code: jg .L_4008ee	 RIP: 400895	 Bytes: 6
  %loadMem_400895 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400895 = call %struct.Memory* @routine_jg_.L_4008ee(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400895, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400895, %struct.Memory** %MEMORY

  %loadBr_400895 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400895 = icmp eq i8 %loadBr_400895, 1
  br i1 %cmpBr_400895, label %block_.L_4008ee, label %block_40089b

block_40089b:
  ; Code: movl $0x1, -0xc(%rbp)	 RIP: 40089b	 Bytes: 7
  %loadMem_40089b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40089b = call %struct.Memory* @routine_movl__0x1__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40089b)
  store %struct.Memory* %call_40089b, %struct.Memory** %MEMORY

  ; Code: .L_4008a2:	 RIP: 4008a2	 Bytes: 0
  br label %block_.L_4008a2
block_.L_4008a2:

  ; Code: cmpl $0x5, -0xc(%rbp)	 RIP: 4008a2	 Bytes: 4
  %loadMem_4008a2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008a2 = call %struct.Memory* @routine_cmpl__0x5__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008a2)
  store %struct.Memory* %call_4008a2, %struct.Memory** %MEMORY

  ; Code: jg .L_4008db	 RIP: 4008a6	 Bytes: 6
  %loadMem_4008a6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008a6 = call %struct.Memory* @routine_jg_.L_4008db(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008a6, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_4008a6, %struct.Memory** %MEMORY

  %loadBr_4008a6 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_4008a6 = icmp eq i8 %loadBr_4008a6, 1
  br i1 %cmpBr_4008a6, label %block_.L_4008db, label %block_4008ac

block_4008ac:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 4008ac	 Bytes: 3
  %loadMem_4008ac = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008ac = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008ac)
  store %struct.Memory* %call_4008ac, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 4008af	 Bytes: 3
  %loadMem_4008af = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008af = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008af)
  store %struct.Memory* %call_4008af, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 4008b2	 Bytes: 3
  %loadMem_4008b2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008b2 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008b2)
  store %struct.Memory* %call_4008b2, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 4008b5	 Bytes: 3
  %loadMem_4008b5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008b5 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008b5)
  store %struct.Memory* %call_4008b5, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 4008b8	 Bytes: 2
  %loadMem_4008b8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008b8 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008b8)
  store %struct.Memory* %call_4008b8, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 4008ba	 Bytes: 3
  %loadMem_4008ba = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008ba = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008ba)
  store %struct.Memory* %call_4008ba, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 4008bd	 Bytes: 2
  %loadMem_4008bd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008bd = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008bd)
  store %struct.Memory* %call_4008bd, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 4008bf	 Bytes: 3
  %loadMem_4008bf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008bf = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008bf)
  store %struct.Memory* %call_4008bf, %struct.Memory** %MEMORY

  ; Code: movl $0x0, 0x60df50(,%rsi,4)	 RIP: 4008c2	 Bytes: 11
  %loadMem_4008c2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008c2 = call %struct.Memory* @routine_movl__0x0__0x60df50___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008c2)
  store %struct.Memory* %call_4008c2, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 4008cd	 Bytes: 3
  %loadMem_4008cd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008cd = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008cd)
  store %struct.Memory* %call_4008cd, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4008d0	 Bytes: 3
  %loadMem_4008d0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008d0 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008d0)
  store %struct.Memory* %call_4008d0, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 4008d3	 Bytes: 3
  %loadMem_4008d3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008d3 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008d3)
  store %struct.Memory* %call_4008d3, %struct.Memory** %MEMORY

  ; Code: jmpq .L_4008a2	 RIP: 4008d6	 Bytes: 5
  %loadMem_4008d6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008d6 = call %struct.Memory* @routine_jmpq_.L_4008a2(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008d6, i64 -52, i64 5)
  store %struct.Memory* %call_4008d6, %struct.Memory** %MEMORY

  br label %block_.L_4008a2

  ; Code: .L_4008db:	 RIP: 4008db	 Bytes: 0
block_.L_4008db:

  ; Code: jmpq .L_4008e0	 RIP: 4008db	 Bytes: 5
  %loadMem_4008db = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008db = call %struct.Memory* @routine_jmpq_.L_4008e0(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008db, i64 5, i64 5)
  store %struct.Memory* %call_4008db, %struct.Memory** %MEMORY

  br label %block_.L_4008e0

  ; Code: .L_4008e0:	 RIP: 4008e0	 Bytes: 0
block_.L_4008e0:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 4008e0	 Bytes: 3
  %loadMem_4008e0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008e0 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008e0)
  store %struct.Memory* %call_4008e0, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4008e3	 Bytes: 3
  %loadMem_4008e3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008e3 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008e3)
  store %struct.Memory* %call_4008e3, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 4008e6	 Bytes: 3
  %loadMem_4008e6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008e6 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008e6)
  store %struct.Memory* %call_4008e6, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400891	 RIP: 4008e9	 Bytes: 5
  %loadMem_4008e9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008e9 = call %struct.Memory* @routine_jmpq_.L_400891(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008e9, i64 -88, i64 5)
  store %struct.Memory* %call_4008e9, %struct.Memory** %MEMORY

  br label %block_.L_400891

  ; Code: .L_4008ee:	 RIP: 4008ee	 Bytes: 0
block_.L_4008ee:

  ; Code: jmpq .L_4008f3	 RIP: 4008ee	 Bytes: 5
  %loadMem_4008ee = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008ee = call %struct.Memory* @routine_jmpq_.L_4008f3(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008ee, i64 5, i64 5)
  store %struct.Memory* %call_4008ee, %struct.Memory** %MEMORY

  br label %block_.L_4008f3

  ; Code: .L_4008f3:	 RIP: 4008f3	 Bytes: 0
block_.L_4008f3:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 4008f3	 Bytes: 3
  %loadMem_4008f3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008f3 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008f3)
  store %struct.Memory* %call_4008f3, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4008f6	 Bytes: 3
  %loadMem_4008f6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008f6 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008f6)
  store %struct.Memory* %call_4008f6, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 4008f9	 Bytes: 3
  %loadMem_4008f9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008f9 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008f9)
  store %struct.Memory* %call_4008f9, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400880	 RIP: 4008fc	 Bytes: 5
  %loadMem_4008fc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4008fc = call %struct.Memory* @routine_jmpq_.L_400880(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4008fc, i64 -124, i64 5)
  store %struct.Memory* %call_4008fc, %struct.Memory** %MEMORY

  br label %block_.L_400880

  ; Code: .L_400901:	 RIP: 400901	 Bytes: 0
block_.L_400901:

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400901	 Bytes: 7
  %loadMem_400901 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400901 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400901)
  store %struct.Memory* %call_400901, %struct.Memory** %MEMORY

  ; Code: .L_400908:	 RIP: 400908	 Bytes: 0
  br label %block_.L_400908
block_.L_400908:

  ; Code: cmpl $0xc, -0x4(%rbp)	 RIP: 400908	 Bytes: 4
  %loadMem_400908 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400908 = call %struct.Memory* @routine_cmpl__0xc__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400908)
  store %struct.Memory* %call_400908, %struct.Memory** %MEMORY

  ; Code: jg .L_400967	 RIP: 40090c	 Bytes: 6
  %loadMem_40090c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40090c = call %struct.Memory* @routine_jg_.L_400967(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40090c, i8* %BRANCH_TAKEN, i64 91, i64 6, i64 6)
  store %struct.Memory* %call_40090c, %struct.Memory** %MEMORY

  %loadBr_40090c = load i8, i8* %BRANCH_TAKEN
  %cmpBr_40090c = icmp eq i8 %loadBr_40090c, 1
  br i1 %cmpBr_40090c, label %block_.L_400967, label %block_400912

block_400912:
  ; Code: movl $0x0, -0x10(%rbp)	 RIP: 400912	 Bytes: 7
  %loadMem_400912 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400912 = call %struct.Memory* @routine_movl__0x0__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400912)
  store %struct.Memory* %call_400912, %struct.Memory** %MEMORY

  ; Code: .L_400919:	 RIP: 400919	 Bytes: 0
  br label %block_.L_400919
block_.L_400919:

  ; Code: cmpl $0x1ff, -0x10(%rbp)	 RIP: 400919	 Bytes: 7
  %loadMem_400919 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400919 = call %struct.Memory* @routine_cmpl__0x1ff__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400919)
  store %struct.Memory* %call_400919, %struct.Memory** %MEMORY

  ; Code: jg .L_400954	 RIP: 400920	 Bytes: 6
  %loadMem_400920 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400920 = call %struct.Memory* @routine_jg_.L_400954(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400920, i8* %BRANCH_TAKEN, i64 52, i64 6, i64 6)
  store %struct.Memory* %call_400920, %struct.Memory** %MEMORY

  %loadBr_400920 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400920 = icmp eq i8 %loadBr_400920, 1
  br i1 %cmpBr_400920, label %block_.L_400954, label %block_400926

block_400926:
  ; Code: movq $0x603aa0, %rax	 RIP: 400926	 Bytes: 10
  %loadMem_400926 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400926 = call %struct.Memory* @routine_movq__0x603aa0___rax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400926)
  store %struct.Memory* %call_400926, %struct.Memory** %MEMORY

  ; Code: movslq -0x4(%rbp), %rcx	 RIP: 400930	 Bytes: 4
  %loadMem_400930 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400930 = call %struct.Memory* @routine_movslq_MINUS0x4__rbp____rcx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400930)
  store %struct.Memory* %call_400930, %struct.Memory** %MEMORY

  ; Code: shlq $0xb, %rcx	 RIP: 400934	 Bytes: 4
  %loadMem_400934 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400934 = call %struct.Memory* @routine_shlq__0xb___rcx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400934)
  store %struct.Memory* %call_400934, %struct.Memory** %MEMORY

  ; Code: addq %rcx, %rax	 RIP: 400938	 Bytes: 3
  %loadMem_400938 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400938 = call %struct.Memory* @routine_addq__rcx___rax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400938)
  store %struct.Memory* %call_400938, %struct.Memory** %MEMORY

  ; Code: movslq -0x10(%rbp), %rcx	 RIP: 40093b	 Bytes: 4
  %loadMem_40093b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40093b = call %struct.Memory* @routine_movslq_MINUS0x10__rbp____rcx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40093b)
  store %struct.Memory* %call_40093b, %struct.Memory** %MEMORY

  ; Code: movl $0x0, (%rax,%rcx,4)	 RIP: 40093f	 Bytes: 7
  %loadMem_40093f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40093f = call %struct.Memory* @routine_movl__0x0____rax__rcx_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40093f)
  store %struct.Memory* %call_40093f, %struct.Memory** %MEMORY

  ; Code: movl -0x10(%rbp), %eax	 RIP: 400946	 Bytes: 3
  %loadMem_400946 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400946 = call %struct.Memory* @routine_movl_MINUS0x10__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400946)
  store %struct.Memory* %call_400946, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400949	 Bytes: 3
  %loadMem_400949 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400949 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400949)
  store %struct.Memory* %call_400949, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x10(%rbp)	 RIP: 40094c	 Bytes: 3
  %loadMem_40094c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40094c = call %struct.Memory* @routine_movl__eax__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40094c)
  store %struct.Memory* %call_40094c, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400919	 RIP: 40094f	 Bytes: 5
  %loadMem_40094f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40094f = call %struct.Memory* @routine_jmpq_.L_400919(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40094f, i64 -54, i64 5)
  store %struct.Memory* %call_40094f, %struct.Memory** %MEMORY

  br label %block_.L_400919

  ; Code: .L_400954:	 RIP: 400954	 Bytes: 0
block_.L_400954:

  ; Code: jmpq .L_400959	 RIP: 400954	 Bytes: 5
  %loadMem_400954 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400954 = call %struct.Memory* @routine_jmpq_.L_400959(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400954, i64 5, i64 5)
  store %struct.Memory* %call_400954, %struct.Memory** %MEMORY

  br label %block_.L_400959

  ; Code: .L_400959:	 RIP: 400959	 Bytes: 0
block_.L_400959:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400959	 Bytes: 3
  %loadMem_400959 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400959 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400959)
  store %struct.Memory* %call_400959, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 40095c	 Bytes: 3
  %loadMem_40095c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40095c = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40095c)
  store %struct.Memory* %call_40095c, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 40095f	 Bytes: 3
  %loadMem_40095f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40095f = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40095f)
  store %struct.Memory* %call_40095f, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400908	 RIP: 400962	 Bytes: 5
  %loadMem_400962 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400962 = call %struct.Memory* @routine_jmpq_.L_400908(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400962, i64 -90, i64 5)
  store %struct.Memory* %call_400962, %struct.Memory** %MEMORY

  br label %block_.L_400908

  ; Code: .L_400967:	 RIP: 400967	 Bytes: 0
block_.L_400967:

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400967	 Bytes: 7
  %loadMem_400967 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400967 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400967)
  store %struct.Memory* %call_400967, %struct.Memory** %MEMORY

  ; Code: .L_40096e:	 RIP: 40096e	 Bytes: 0
  br label %block_.L_40096e
block_.L_40096e:

  ; Code: cmpl $0x3, -0x4(%rbp)	 RIP: 40096e	 Bytes: 4
  %loadMem_40096e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40096e = call %struct.Memory* @routine_cmpl__0x3__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40096e)
  store %struct.Memory* %call_40096e, %struct.Memory** %MEMORY

  ; Code: jg .L_4009ef	 RIP: 400972	 Bytes: 6
  %loadMem_400972 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400972 = call %struct.Memory* @routine_jg_.L_4009ef(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400972, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400972, %struct.Memory** %MEMORY

  %loadBr_400972 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400972 = icmp eq i8 %loadBr_400972, 1
  br i1 %cmpBr_400972, label %block_.L_4009ef, label %block_400978

block_400978:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400978	 Bytes: 7
  %loadMem_400978 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400978 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400978)
  store %struct.Memory* %call_400978, %struct.Memory** %MEMORY

  ; Code: .L_40097f:	 RIP: 40097f	 Bytes: 0
  br label %block_.L_40097f
block_.L_40097f:

  ; Code: cmpl $0x1, -0x8(%rbp)	 RIP: 40097f	 Bytes: 4
  %loadMem_40097f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40097f = call %struct.Memory* @routine_cmpl__0x1__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40097f)
  store %struct.Memory* %call_40097f, %struct.Memory** %MEMORY

  ; Code: jg .L_4009dc	 RIP: 400983	 Bytes: 6
  %loadMem_400983 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400983 = call %struct.Memory* @routine_jg_.L_4009dc(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400983, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400983, %struct.Memory** %MEMORY

  %loadBr_400983 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400983 = icmp eq i8 %loadBr_400983, 1
  br i1 %cmpBr_400983, label %block_.L_4009dc, label %block_400989

block_400989:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400989	 Bytes: 7
  %loadMem_400989 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400989 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400989)
  store %struct.Memory* %call_400989, %struct.Memory** %MEMORY

  ; Code: .L_400990:	 RIP: 400990	 Bytes: 0
  br label %block_.L_400990
block_.L_400990:

  ; Code: cmpl $0x0, -0xc(%rbp)	 RIP: 400990	 Bytes: 4
  %loadMem_400990 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400990 = call %struct.Memory* @routine_cmpl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400990)
  store %struct.Memory* %call_400990, %struct.Memory** %MEMORY

  ; Code: jg .L_4009c9	 RIP: 400994	 Bytes: 6
  %loadMem_400994 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400994 = call %struct.Memory* @routine_jg_.L_4009c9(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400994, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400994, %struct.Memory** %MEMORY

  %loadBr_400994 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400994 = icmp eq i8 %loadBr_400994, 1
  br i1 %cmpBr_400994, label %block_.L_4009c9, label %block_40099a

block_40099a:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 40099a	 Bytes: 3
  %loadMem_40099a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40099a = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40099a)
  store %struct.Memory* %call_40099a, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 40099d	 Bytes: 3
  %loadMem_40099d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40099d = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40099d)
  store %struct.Memory* %call_40099d, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 4009a0	 Bytes: 3
  %loadMem_4009a0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009a0 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009a0)
  store %struct.Memory* %call_4009a0, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 4009a3	 Bytes: 3
  %loadMem_4009a3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009a3 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009a3)
  store %struct.Memory* %call_4009a3, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 4009a6	 Bytes: 2
  %loadMem_4009a6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009a6 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009a6)
  store %struct.Memory* %call_4009a6, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 4009a8	 Bytes: 3
  %loadMem_4009a8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009a8 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009a8)
  store %struct.Memory* %call_4009a8, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 4009ab	 Bytes: 2
  %loadMem_4009ab = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009ab = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009ab)
  store %struct.Memory* %call_4009ab, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 4009ad	 Bytes: 3
  %loadMem_4009ad = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009ad = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009ad)
  store %struct.Memory* %call_4009ad, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x603aa0(,%rsi,4)	 RIP: 4009b0	 Bytes: 11
  %loadMem_4009b0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009b0 = call %struct.Memory* @routine_movl__0x1__0x603aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009b0)
  store %struct.Memory* %call_4009b0, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 4009bb	 Bytes: 3
  %loadMem_4009bb = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009bb = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009bb)
  store %struct.Memory* %call_4009bb, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4009be	 Bytes: 3
  %loadMem_4009be = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009be = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009be)
  store %struct.Memory* %call_4009be, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 4009c1	 Bytes: 3
  %loadMem_4009c1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009c1 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009c1)
  store %struct.Memory* %call_4009c1, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400990	 RIP: 4009c4	 Bytes: 5
  %loadMem_4009c4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009c4 = call %struct.Memory* @routine_jmpq_.L_400990(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009c4, i64 -52, i64 5)
  store %struct.Memory* %call_4009c4, %struct.Memory** %MEMORY

  br label %block_.L_400990

  ; Code: .L_4009c9:	 RIP: 4009c9	 Bytes: 0
block_.L_4009c9:

  ; Code: jmpq .L_4009ce	 RIP: 4009c9	 Bytes: 5
  %loadMem_4009c9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009c9 = call %struct.Memory* @routine_jmpq_.L_4009ce(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009c9, i64 5, i64 5)
  store %struct.Memory* %call_4009c9, %struct.Memory** %MEMORY

  br label %block_.L_4009ce

  ; Code: .L_4009ce:	 RIP: 4009ce	 Bytes: 0
block_.L_4009ce:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 4009ce	 Bytes: 3
  %loadMem_4009ce = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009ce = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009ce)
  store %struct.Memory* %call_4009ce, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4009d1	 Bytes: 3
  %loadMem_4009d1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009d1 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009d1)
  store %struct.Memory* %call_4009d1, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 4009d4	 Bytes: 3
  %loadMem_4009d4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009d4 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009d4)
  store %struct.Memory* %call_4009d4, %struct.Memory** %MEMORY

  ; Code: jmpq .L_40097f	 RIP: 4009d7	 Bytes: 5
  %loadMem_4009d7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009d7 = call %struct.Memory* @routine_jmpq_.L_40097f(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009d7, i64 -88, i64 5)
  store %struct.Memory* %call_4009d7, %struct.Memory** %MEMORY

  br label %block_.L_40097f

  ; Code: .L_4009dc:	 RIP: 4009dc	 Bytes: 0
block_.L_4009dc:

  ; Code: jmpq .L_4009e1	 RIP: 4009dc	 Bytes: 5
  %loadMem_4009dc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009dc = call %struct.Memory* @routine_jmpq_.L_4009e1(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009dc, i64 5, i64 5)
  store %struct.Memory* %call_4009dc, %struct.Memory** %MEMORY

  br label %block_.L_4009e1

  ; Code: .L_4009e1:	 RIP: 4009e1	 Bytes: 0
block_.L_4009e1:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 4009e1	 Bytes: 3
  %loadMem_4009e1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009e1 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009e1)
  store %struct.Memory* %call_4009e1, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4009e4	 Bytes: 3
  %loadMem_4009e4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009e4 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009e4)
  store %struct.Memory* %call_4009e4, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 4009e7	 Bytes: 3
  %loadMem_4009e7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009e7 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009e7)
  store %struct.Memory* %call_4009e7, %struct.Memory** %MEMORY

  ; Code: jmpq .L_40096e	 RIP: 4009ea	 Bytes: 5
  %loadMem_4009ea = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009ea = call %struct.Memory* @routine_jmpq_.L_40096e(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009ea, i64 -124, i64 5)
  store %struct.Memory* %call_4009ea, %struct.Memory** %MEMORY

  br label %block_.L_40096e

  ; Code: .L_4009ef:	 RIP: 4009ef	 Bytes: 0
block_.L_4009ef:

  ; Code: movl $0x0, 0x6191f0	 RIP: 4009ef	 Bytes: 11
  %loadMem_4009ef = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009ef = call %struct.Memory* @routine_movl__0x0__0x6191f0(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009ef)
  store %struct.Memory* %call_4009ef, %struct.Memory** %MEMORY

  ; Code: movl $0xb, 0x610250	 RIP: 4009fa	 Bytes: 11
  %loadMem_4009fa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4009fa = call %struct.Memory* @routine_movl__0xb__0x610250(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4009fa)
  store %struct.Memory* %call_4009fa, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400a05	 Bytes: 7
  %loadMem_400a05 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a05 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a05)
  store %struct.Memory* %call_400a05, %struct.Memory** %MEMORY

  ; Code: .L_400a0c:	 RIP: 400a0c	 Bytes: 0
  br label %block_.L_400a0c
block_.L_400a0c:

  ; Code: cmpl $0x1, -0x4(%rbp)	 RIP: 400a0c	 Bytes: 4
  %loadMem_400a0c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a0c = call %struct.Memory* @routine_cmpl__0x1__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a0c)
  store %struct.Memory* %call_400a0c, %struct.Memory** %MEMORY

  ; Code: jg .L_400a8d	 RIP: 400a10	 Bytes: 6
  %loadMem_400a10 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a10 = call %struct.Memory* @routine_jg_.L_400a8d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a10, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400a10, %struct.Memory** %MEMORY

  %loadBr_400a10 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400a10 = icmp eq i8 %loadBr_400a10, 1
  br i1 %cmpBr_400a10, label %block_.L_400a8d, label %block_400a16

block_400a16:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400a16	 Bytes: 7
  %loadMem_400a16 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a16 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a16)
  store %struct.Memory* %call_400a16, %struct.Memory** %MEMORY

  ; Code: .L_400a1d:	 RIP: 400a1d	 Bytes: 0
  br label %block_.L_400a1d
block_.L_400a1d:

  ; Code: cmpl $0x0, -0x8(%rbp)	 RIP: 400a1d	 Bytes: 4
  %loadMem_400a1d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a1d = call %struct.Memory* @routine_cmpl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a1d)
  store %struct.Memory* %call_400a1d, %struct.Memory** %MEMORY

  ; Code: jg .L_400a7a	 RIP: 400a21	 Bytes: 6
  %loadMem_400a21 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a21 = call %struct.Memory* @routine_jg_.L_400a7a(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a21, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400a21, %struct.Memory** %MEMORY

  %loadBr_400a21 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400a21 = icmp eq i8 %loadBr_400a21, 1
  br i1 %cmpBr_400a21, label %block_.L_400a7a, label %block_400a27

block_400a27:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400a27	 Bytes: 7
  %loadMem_400a27 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a27 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a27)
  store %struct.Memory* %call_400a27, %struct.Memory** %MEMORY

  ; Code: .L_400a2e:	 RIP: 400a2e	 Bytes: 0
  br label %block_.L_400a2e
block_.L_400a2e:

  ; Code: cmpl $0x3, -0xc(%rbp)	 RIP: 400a2e	 Bytes: 4
  %loadMem_400a2e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a2e = call %struct.Memory* @routine_cmpl__0x3__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a2e)
  store %struct.Memory* %call_400a2e, %struct.Memory** %MEMORY

  ; Code: jg .L_400a67	 RIP: 400a32	 Bytes: 6
  %loadMem_400a32 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a32 = call %struct.Memory* @routine_jg_.L_400a67(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a32, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400a32, %struct.Memory** %MEMORY

  %loadBr_400a32 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400a32 = icmp eq i8 %loadBr_400a32, 1
  br i1 %cmpBr_400a32, label %block_.L_400a67, label %block_400a38

block_400a38:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400a38	 Bytes: 3
  %loadMem_400a38 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a38 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a38)
  store %struct.Memory* %call_400a38, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400a3b	 Bytes: 3
  %loadMem_400a3b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a3b = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a3b)
  store %struct.Memory* %call_400a3b, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400a3e	 Bytes: 3
  %loadMem_400a3e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a3e = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a3e)
  store %struct.Memory* %call_400a3e, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400a41	 Bytes: 3
  %loadMem_400a41 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a41 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a41)
  store %struct.Memory* %call_400a41, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400a44	 Bytes: 2
  %loadMem_400a44 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a44 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a44)
  store %struct.Memory* %call_400a44, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400a46	 Bytes: 3
  %loadMem_400a46 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a46 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a46)
  store %struct.Memory* %call_400a46, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400a49	 Bytes: 2
  %loadMem_400a49 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a49 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a49)
  store %struct.Memory* %call_400a49, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400a4b	 Bytes: 3
  %loadMem_400a4b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a4b = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a4b)
  store %struct.Memory* %call_400a4b, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6042a0(,%rsi,4)	 RIP: 400a4e	 Bytes: 11
  %loadMem_400a4e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a4e = call %struct.Memory* @routine_movl__0x1__0x6042a0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a4e)
  store %struct.Memory* %call_400a4e, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400a59	 Bytes: 3
  %loadMem_400a59 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a59 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a59)
  store %struct.Memory* %call_400a59, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400a5c	 Bytes: 3
  %loadMem_400a5c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a5c = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a5c)
  store %struct.Memory* %call_400a5c, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400a5f	 Bytes: 3
  %loadMem_400a5f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a5f = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a5f)
  store %struct.Memory* %call_400a5f, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400a2e	 RIP: 400a62	 Bytes: 5
  %loadMem_400a62 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a62 = call %struct.Memory* @routine_jmpq_.L_400a2e(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a62, i64 -52, i64 5)
  store %struct.Memory* %call_400a62, %struct.Memory** %MEMORY

  br label %block_.L_400a2e

  ; Code: .L_400a67:	 RIP: 400a67	 Bytes: 0
block_.L_400a67:

  ; Code: jmpq .L_400a6c	 RIP: 400a67	 Bytes: 5
  %loadMem_400a67 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a67 = call %struct.Memory* @routine_jmpq_.L_400a6c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a67, i64 5, i64 5)
  store %struct.Memory* %call_400a67, %struct.Memory** %MEMORY

  br label %block_.L_400a6c

  ; Code: .L_400a6c:	 RIP: 400a6c	 Bytes: 0
block_.L_400a6c:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400a6c	 Bytes: 3
  %loadMem_400a6c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a6c = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a6c)
  store %struct.Memory* %call_400a6c, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400a6f	 Bytes: 3
  %loadMem_400a6f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a6f = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a6f)
  store %struct.Memory* %call_400a6f, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400a72	 Bytes: 3
  %loadMem_400a72 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a72 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a72)
  store %struct.Memory* %call_400a72, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400a1d	 RIP: 400a75	 Bytes: 5
  %loadMem_400a75 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a75 = call %struct.Memory* @routine_jmpq_.L_400a1d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a75, i64 -88, i64 5)
  store %struct.Memory* %call_400a75, %struct.Memory** %MEMORY

  br label %block_.L_400a1d

  ; Code: .L_400a7a:	 RIP: 400a7a	 Bytes: 0
block_.L_400a7a:

  ; Code: jmpq .L_400a7f	 RIP: 400a7a	 Bytes: 5
  %loadMem_400a7a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a7a = call %struct.Memory* @routine_jmpq_.L_400a7f(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a7a, i64 5, i64 5)
  store %struct.Memory* %call_400a7a, %struct.Memory** %MEMORY

  br label %block_.L_400a7f

  ; Code: .L_400a7f:	 RIP: 400a7f	 Bytes: 0
block_.L_400a7f:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400a7f	 Bytes: 3
  %loadMem_400a7f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a7f = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a7f)
  store %struct.Memory* %call_400a7f, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400a82	 Bytes: 3
  %loadMem_400a82 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a82 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a82)
  store %struct.Memory* %call_400a82, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400a85	 Bytes: 3
  %loadMem_400a85 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a85 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a85)
  store %struct.Memory* %call_400a85, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400a0c	 RIP: 400a88	 Bytes: 5
  %loadMem_400a88 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a88 = call %struct.Memory* @routine_jmpq_.L_400a0c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a88, i64 -124, i64 5)
  store %struct.Memory* %call_400a88, %struct.Memory** %MEMORY

  br label %block_.L_400a0c

  ; Code: .L_400a8d:	 RIP: 400a8d	 Bytes: 0
block_.L_400a8d:

  ; Code: movl $0x0, 0x6191f4	 RIP: 400a8d	 Bytes: 11
  %loadMem_400a8d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a8d = call %struct.Memory* @routine_movl__0x0__0x6191f4(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a8d)
  store %struct.Memory* %call_400a8d, %struct.Memory** %MEMORY

  ; Code: movl $0xc1, 0x610254	 RIP: 400a98	 Bytes: 11
  %loadMem_400a98 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400a98 = call %struct.Memory* @routine_movl__0xc1__0x610254(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400a98)
  store %struct.Memory* %call_400a98, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400aa3	 Bytes: 7
  %loadMem_400aa3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400aa3 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400aa3)
  store %struct.Memory* %call_400aa3, %struct.Memory** %MEMORY

  ; Code: .L_400aaa:	 RIP: 400aaa	 Bytes: 0
  br label %block_.L_400aaa
block_.L_400aaa:

  ; Code: cmpl $0x0, -0x4(%rbp)	 RIP: 400aaa	 Bytes: 4
  %loadMem_400aaa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400aaa = call %struct.Memory* @routine_cmpl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400aaa)
  store %struct.Memory* %call_400aaa, %struct.Memory** %MEMORY

  ; Code: jg .L_400b2b	 RIP: 400aae	 Bytes: 6
  %loadMem_400aae = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400aae = call %struct.Memory* @routine_jg_.L_400b2b(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400aae, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400aae, %struct.Memory** %MEMORY

  %loadBr_400aae = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400aae = icmp eq i8 %loadBr_400aae, 1
  br i1 %cmpBr_400aae, label %block_.L_400b2b, label %block_400ab4

block_400ab4:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400ab4	 Bytes: 7
  %loadMem_400ab4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ab4 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ab4)
  store %struct.Memory* %call_400ab4, %struct.Memory** %MEMORY

  ; Code: .L_400abb:	 RIP: 400abb	 Bytes: 0
  br label %block_.L_400abb
block_.L_400abb:

  ; Code: cmpl $0x3, -0x8(%rbp)	 RIP: 400abb	 Bytes: 4
  %loadMem_400abb = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400abb = call %struct.Memory* @routine_cmpl__0x3__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400abb)
  store %struct.Memory* %call_400abb, %struct.Memory** %MEMORY

  ; Code: jg .L_400b18	 RIP: 400abf	 Bytes: 6
  %loadMem_400abf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400abf = call %struct.Memory* @routine_jg_.L_400b18(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400abf, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400abf, %struct.Memory** %MEMORY

  %loadBr_400abf = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400abf = icmp eq i8 %loadBr_400abf, 1
  br i1 %cmpBr_400abf, label %block_.L_400b18, label %block_400ac5

block_400ac5:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400ac5	 Bytes: 7
  %loadMem_400ac5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ac5 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ac5)
  store %struct.Memory* %call_400ac5, %struct.Memory** %MEMORY

  ; Code: .L_400acc:	 RIP: 400acc	 Bytes: 0
  br label %block_.L_400acc
block_.L_400acc:

  ; Code: cmpl $0x1, -0xc(%rbp)	 RIP: 400acc	 Bytes: 4
  %loadMem_400acc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400acc = call %struct.Memory* @routine_cmpl__0x1__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400acc)
  store %struct.Memory* %call_400acc, %struct.Memory** %MEMORY

  ; Code: jg .L_400b05	 RIP: 400ad0	 Bytes: 6
  %loadMem_400ad0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ad0 = call %struct.Memory* @routine_jg_.L_400b05(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ad0, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400ad0, %struct.Memory** %MEMORY

  %loadBr_400ad0 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400ad0 = icmp eq i8 %loadBr_400ad0, 1
  br i1 %cmpBr_400ad0, label %block_.L_400b05, label %block_400ad6

block_400ad6:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400ad6	 Bytes: 3
  %loadMem_400ad6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ad6 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ad6)
  store %struct.Memory* %call_400ad6, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400ad9	 Bytes: 3
  %loadMem_400ad9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ad9 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ad9)
  store %struct.Memory* %call_400ad9, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400adc	 Bytes: 3
  %loadMem_400adc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400adc = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400adc)
  store %struct.Memory* %call_400adc, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400adf	 Bytes: 3
  %loadMem_400adf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400adf = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400adf)
  store %struct.Memory* %call_400adf, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400ae2	 Bytes: 2
  %loadMem_400ae2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ae2 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ae2)
  store %struct.Memory* %call_400ae2, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400ae4	 Bytes: 3
  %loadMem_400ae4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ae4 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ae4)
  store %struct.Memory* %call_400ae4, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400ae7	 Bytes: 2
  %loadMem_400ae7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ae7 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ae7)
  store %struct.Memory* %call_400ae7, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400ae9	 Bytes: 3
  %loadMem_400ae9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ae9 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ae9)
  store %struct.Memory* %call_400ae9, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x604aa0(,%rsi,4)	 RIP: 400aec	 Bytes: 11
  %loadMem_400aec = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400aec = call %struct.Memory* @routine_movl__0x1__0x604aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400aec)
  store %struct.Memory* %call_400aec, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400af7	 Bytes: 3
  %loadMem_400af7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400af7 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400af7)
  store %struct.Memory* %call_400af7, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400afa	 Bytes: 3
  %loadMem_400afa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400afa = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400afa)
  store %struct.Memory* %call_400afa, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400afd	 Bytes: 3
  %loadMem_400afd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400afd = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400afd)
  store %struct.Memory* %call_400afd, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400acc	 RIP: 400b00	 Bytes: 5
  %loadMem_400b00 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b00 = call %struct.Memory* @routine_jmpq_.L_400acc(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b00, i64 -52, i64 5)
  store %struct.Memory* %call_400b00, %struct.Memory** %MEMORY

  br label %block_.L_400acc

  ; Code: .L_400b05:	 RIP: 400b05	 Bytes: 0
block_.L_400b05:

  ; Code: jmpq .L_400b0a	 RIP: 400b05	 Bytes: 5
  %loadMem_400b05 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b05 = call %struct.Memory* @routine_jmpq_.L_400b0a(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b05, i64 5, i64 5)
  store %struct.Memory* %call_400b05, %struct.Memory** %MEMORY

  br label %block_.L_400b0a

  ; Code: .L_400b0a:	 RIP: 400b0a	 Bytes: 0
block_.L_400b0a:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400b0a	 Bytes: 3
  %loadMem_400b0a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b0a = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b0a)
  store %struct.Memory* %call_400b0a, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400b0d	 Bytes: 3
  %loadMem_400b0d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b0d = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b0d)
  store %struct.Memory* %call_400b0d, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400b10	 Bytes: 3
  %loadMem_400b10 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b10 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b10)
  store %struct.Memory* %call_400b10, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400abb	 RIP: 400b13	 Bytes: 5
  %loadMem_400b13 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b13 = call %struct.Memory* @routine_jmpq_.L_400abb(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b13, i64 -88, i64 5)
  store %struct.Memory* %call_400b13, %struct.Memory** %MEMORY

  br label %block_.L_400abb

  ; Code: .L_400b18:	 RIP: 400b18	 Bytes: 0
block_.L_400b18:

  ; Code: jmpq .L_400b1d	 RIP: 400b18	 Bytes: 5
  %loadMem_400b18 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b18 = call %struct.Memory* @routine_jmpq_.L_400b1d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b18, i64 5, i64 5)
  store %struct.Memory* %call_400b18, %struct.Memory** %MEMORY

  br label %block_.L_400b1d

  ; Code: .L_400b1d:	 RIP: 400b1d	 Bytes: 0
block_.L_400b1d:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400b1d	 Bytes: 3
  %loadMem_400b1d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b1d = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b1d)
  store %struct.Memory* %call_400b1d, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400b20	 Bytes: 3
  %loadMem_400b20 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b20 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b20)
  store %struct.Memory* %call_400b20, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400b23	 Bytes: 3
  %loadMem_400b23 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b23 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b23)
  store %struct.Memory* %call_400b23, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400aaa	 RIP: 400b26	 Bytes: 5
  %loadMem_400b26 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b26 = call %struct.Memory* @routine_jmpq_.L_400aaa(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b26, i64 -124, i64 5)
  store %struct.Memory* %call_400b26, %struct.Memory** %MEMORY

  br label %block_.L_400aaa

  ; Code: .L_400b2b:	 RIP: 400b2b	 Bytes: 0
block_.L_400b2b:

  ; Code: movl $0x0, 0x6191f8	 RIP: 400b2b	 Bytes: 11
  %loadMem_400b2b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b2b = call %struct.Memory* @routine_movl__0x0__0x6191f8(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b2b)
  store %struct.Memory* %call_400b2b, %struct.Memory** %MEMORY

  ; Code: movl $0x58, 0x610258	 RIP: 400b36	 Bytes: 11
  %loadMem_400b36 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b36 = call %struct.Memory* @routine_movl__0x58__0x610258(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b36)
  store %struct.Memory* %call_400b36, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400b41	 Bytes: 7
  %loadMem_400b41 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b41 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b41)
  store %struct.Memory* %call_400b41, %struct.Memory** %MEMORY

  ; Code: .L_400b48:	 RIP: 400b48	 Bytes: 0
  br label %block_.L_400b48
block_.L_400b48:

  ; Code: cmpl $0x1, -0x4(%rbp)	 RIP: 400b48	 Bytes: 4
  %loadMem_400b48 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b48 = call %struct.Memory* @routine_cmpl__0x1__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b48)
  store %struct.Memory* %call_400b48, %struct.Memory** %MEMORY

  ; Code: jg .L_400bc9	 RIP: 400b4c	 Bytes: 6
  %loadMem_400b4c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b4c = call %struct.Memory* @routine_jg_.L_400bc9(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b4c, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400b4c, %struct.Memory** %MEMORY

  %loadBr_400b4c = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400b4c = icmp eq i8 %loadBr_400b4c, 1
  br i1 %cmpBr_400b4c, label %block_.L_400bc9, label %block_400b52

block_400b52:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400b52	 Bytes: 7
  %loadMem_400b52 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b52 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b52)
  store %struct.Memory* %call_400b52, %struct.Memory** %MEMORY

  ; Code: .L_400b59:	 RIP: 400b59	 Bytes: 0
  br label %block_.L_400b59
block_.L_400b59:

  ; Code: cmpl $0x3, -0x8(%rbp)	 RIP: 400b59	 Bytes: 4
  %loadMem_400b59 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b59 = call %struct.Memory* @routine_cmpl__0x3__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b59)
  store %struct.Memory* %call_400b59, %struct.Memory** %MEMORY

  ; Code: jg .L_400bb6	 RIP: 400b5d	 Bytes: 6
  %loadMem_400b5d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b5d = call %struct.Memory* @routine_jg_.L_400bb6(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b5d, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400b5d, %struct.Memory** %MEMORY

  %loadBr_400b5d = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400b5d = icmp eq i8 %loadBr_400b5d, 1
  br i1 %cmpBr_400b5d, label %block_.L_400bb6, label %block_400b63

block_400b63:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400b63	 Bytes: 7
  %loadMem_400b63 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b63 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b63)
  store %struct.Memory* %call_400b63, %struct.Memory** %MEMORY

  ; Code: .L_400b6a:	 RIP: 400b6a	 Bytes: 0
  br label %block_.L_400b6a
block_.L_400b6a:

  ; Code: cmpl $0x0, -0xc(%rbp)	 RIP: 400b6a	 Bytes: 4
  %loadMem_400b6a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b6a = call %struct.Memory* @routine_cmpl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b6a)
  store %struct.Memory* %call_400b6a, %struct.Memory** %MEMORY

  ; Code: jg .L_400ba3	 RIP: 400b6e	 Bytes: 6
  %loadMem_400b6e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b6e = call %struct.Memory* @routine_jg_.L_400ba3(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b6e, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400b6e, %struct.Memory** %MEMORY

  %loadBr_400b6e = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400b6e = icmp eq i8 %loadBr_400b6e, 1
  br i1 %cmpBr_400b6e, label %block_.L_400ba3, label %block_400b74

block_400b74:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400b74	 Bytes: 3
  %loadMem_400b74 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b74 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b74)
  store %struct.Memory* %call_400b74, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400b77	 Bytes: 3
  %loadMem_400b77 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b77 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b77)
  store %struct.Memory* %call_400b77, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400b7a	 Bytes: 3
  %loadMem_400b7a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b7a = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b7a)
  store %struct.Memory* %call_400b7a, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400b7d	 Bytes: 3
  %loadMem_400b7d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b7d = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b7d)
  store %struct.Memory* %call_400b7d, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400b80	 Bytes: 2
  %loadMem_400b80 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b80 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b80)
  store %struct.Memory* %call_400b80, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400b82	 Bytes: 3
  %loadMem_400b82 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b82 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b82)
  store %struct.Memory* %call_400b82, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400b85	 Bytes: 2
  %loadMem_400b85 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b85 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b85)
  store %struct.Memory* %call_400b85, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400b87	 Bytes: 3
  %loadMem_400b87 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b87 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b87)
  store %struct.Memory* %call_400b87, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6052a0(,%rsi,4)	 RIP: 400b8a	 Bytes: 11
  %loadMem_400b8a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b8a = call %struct.Memory* @routine_movl__0x1__0x6052a0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b8a)
  store %struct.Memory* %call_400b8a, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400b95	 Bytes: 3
  %loadMem_400b95 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b95 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b95)
  store %struct.Memory* %call_400b95, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400b98	 Bytes: 3
  %loadMem_400b98 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b98 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b98)
  store %struct.Memory* %call_400b98, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400b9b	 Bytes: 3
  %loadMem_400b9b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b9b = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b9b)
  store %struct.Memory* %call_400b9b, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400b6a	 RIP: 400b9e	 Bytes: 5
  %loadMem_400b9e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400b9e = call %struct.Memory* @routine_jmpq_.L_400b6a(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400b9e, i64 -52, i64 5)
  store %struct.Memory* %call_400b9e, %struct.Memory** %MEMORY

  br label %block_.L_400b6a

  ; Code: .L_400ba3:	 RIP: 400ba3	 Bytes: 0
block_.L_400ba3:

  ; Code: jmpq .L_400ba8	 RIP: 400ba3	 Bytes: 5
  %loadMem_400ba3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ba3 = call %struct.Memory* @routine_jmpq_.L_400ba8(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ba3, i64 5, i64 5)
  store %struct.Memory* %call_400ba3, %struct.Memory** %MEMORY

  br label %block_.L_400ba8

  ; Code: .L_400ba8:	 RIP: 400ba8	 Bytes: 0
block_.L_400ba8:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400ba8	 Bytes: 3
  %loadMem_400ba8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ba8 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ba8)
  store %struct.Memory* %call_400ba8, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400bab	 Bytes: 3
  %loadMem_400bab = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bab = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bab)
  store %struct.Memory* %call_400bab, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400bae	 Bytes: 3
  %loadMem_400bae = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bae = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bae)
  store %struct.Memory* %call_400bae, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400b59	 RIP: 400bb1	 Bytes: 5
  %loadMem_400bb1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bb1 = call %struct.Memory* @routine_jmpq_.L_400b59(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bb1, i64 -88, i64 5)
  store %struct.Memory* %call_400bb1, %struct.Memory** %MEMORY

  br label %block_.L_400b59

  ; Code: .L_400bb6:	 RIP: 400bb6	 Bytes: 0
block_.L_400bb6:

  ; Code: jmpq .L_400bbb	 RIP: 400bb6	 Bytes: 5
  %loadMem_400bb6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bb6 = call %struct.Memory* @routine_jmpq_.L_400bbb(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bb6, i64 5, i64 5)
  store %struct.Memory* %call_400bb6, %struct.Memory** %MEMORY

  br label %block_.L_400bbb

  ; Code: .L_400bbb:	 RIP: 400bbb	 Bytes: 0
block_.L_400bbb:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400bbb	 Bytes: 3
  %loadMem_400bbb = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bbb = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bbb)
  store %struct.Memory* %call_400bbb, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400bbe	 Bytes: 3
  %loadMem_400bbe = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bbe = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bbe)
  store %struct.Memory* %call_400bbe, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400bc1	 Bytes: 3
  %loadMem_400bc1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bc1 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bc1)
  store %struct.Memory* %call_400bc1, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400b48	 RIP: 400bc4	 Bytes: 5
  %loadMem_400bc4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bc4 = call %struct.Memory* @routine_jmpq_.L_400b48(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bc4, i64 -124, i64 5)
  store %struct.Memory* %call_400bc4, %struct.Memory** %MEMORY

  br label %block_.L_400b48

  ; Code: .L_400bc9:	 RIP: 400bc9	 Bytes: 0
block_.L_400bc9:

  ; Code: movl $0x0, 0x6191fc	 RIP: 400bc9	 Bytes: 11
  %loadMem_400bc9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bc9 = call %struct.Memory* @routine_movl__0x0__0x6191fc(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bc9)
  store %struct.Memory* %call_400bc9, %struct.Memory** %MEMORY

  ; Code: movl $0x19, 0x61025c	 RIP: 400bd4	 Bytes: 11
  %loadMem_400bd4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bd4 = call %struct.Memory* @routine_movl__0x19__0x61025c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bd4)
  store %struct.Memory* %call_400bd4, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400bdf	 Bytes: 7
  %loadMem_400bdf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bdf = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bdf)
  store %struct.Memory* %call_400bdf, %struct.Memory** %MEMORY

  ; Code: .L_400be6:	 RIP: 400be6	 Bytes: 0
  br label %block_.L_400be6
block_.L_400be6:

  ; Code: cmpl $0x3, -0x4(%rbp)	 RIP: 400be6	 Bytes: 4
  %loadMem_400be6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400be6 = call %struct.Memory* @routine_cmpl__0x3__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400be6)
  store %struct.Memory* %call_400be6, %struct.Memory** %MEMORY

  ; Code: jg .L_400c67	 RIP: 400bea	 Bytes: 6
  %loadMem_400bea = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bea = call %struct.Memory* @routine_jg_.L_400c67(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bea, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400bea, %struct.Memory** %MEMORY

  %loadBr_400bea = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400bea = icmp eq i8 %loadBr_400bea, 1
  br i1 %cmpBr_400bea, label %block_.L_400c67, label %block_400bf0

block_400bf0:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400bf0	 Bytes: 7
  %loadMem_400bf0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bf0 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bf0)
  store %struct.Memory* %call_400bf0, %struct.Memory** %MEMORY

  ; Code: .L_400bf7:	 RIP: 400bf7	 Bytes: 0
  br label %block_.L_400bf7
block_.L_400bf7:

  ; Code: cmpl $0x0, -0x8(%rbp)	 RIP: 400bf7	 Bytes: 4
  %loadMem_400bf7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bf7 = call %struct.Memory* @routine_cmpl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bf7)
  store %struct.Memory* %call_400bf7, %struct.Memory** %MEMORY

  ; Code: jg .L_400c54	 RIP: 400bfb	 Bytes: 6
  %loadMem_400bfb = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400bfb = call %struct.Memory* @routine_jg_.L_400c54(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400bfb, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400bfb, %struct.Memory** %MEMORY

  %loadBr_400bfb = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400bfb = icmp eq i8 %loadBr_400bfb, 1
  br i1 %cmpBr_400bfb, label %block_.L_400c54, label %block_400c01

block_400c01:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400c01	 Bytes: 7
  %loadMem_400c01 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c01 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c01)
  store %struct.Memory* %call_400c01, %struct.Memory** %MEMORY

  ; Code: .L_400c08:	 RIP: 400c08	 Bytes: 0
  br label %block_.L_400c08
block_.L_400c08:

  ; Code: cmpl $0x1, -0xc(%rbp)	 RIP: 400c08	 Bytes: 4
  %loadMem_400c08 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c08 = call %struct.Memory* @routine_cmpl__0x1__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c08)
  store %struct.Memory* %call_400c08, %struct.Memory** %MEMORY

  ; Code: jg .L_400c41	 RIP: 400c0c	 Bytes: 6
  %loadMem_400c0c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c0c = call %struct.Memory* @routine_jg_.L_400c41(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c0c, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400c0c, %struct.Memory** %MEMORY

  %loadBr_400c0c = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400c0c = icmp eq i8 %loadBr_400c0c, 1
  br i1 %cmpBr_400c0c, label %block_.L_400c41, label %block_400c12

block_400c12:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400c12	 Bytes: 3
  %loadMem_400c12 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c12 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c12)
  store %struct.Memory* %call_400c12, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400c15	 Bytes: 3
  %loadMem_400c15 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c15 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c15)
  store %struct.Memory* %call_400c15, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400c18	 Bytes: 3
  %loadMem_400c18 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c18 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c18)
  store %struct.Memory* %call_400c18, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400c1b	 Bytes: 3
  %loadMem_400c1b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c1b = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c1b)
  store %struct.Memory* %call_400c1b, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400c1e	 Bytes: 2
  %loadMem_400c1e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c1e = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c1e)
  store %struct.Memory* %call_400c1e, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400c20	 Bytes: 3
  %loadMem_400c20 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c20 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c20)
  store %struct.Memory* %call_400c20, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400c23	 Bytes: 2
  %loadMem_400c23 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c23 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c23)
  store %struct.Memory* %call_400c23, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400c25	 Bytes: 3
  %loadMem_400c25 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c25 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c25)
  store %struct.Memory* %call_400c25, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x605aa0(,%rsi,4)	 RIP: 400c28	 Bytes: 11
  %loadMem_400c28 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c28 = call %struct.Memory* @routine_movl__0x1__0x605aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c28)
  store %struct.Memory* %call_400c28, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400c33	 Bytes: 3
  %loadMem_400c33 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c33 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c33)
  store %struct.Memory* %call_400c33, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400c36	 Bytes: 3
  %loadMem_400c36 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c36 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c36)
  store %struct.Memory* %call_400c36, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400c39	 Bytes: 3
  %loadMem_400c39 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c39 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c39)
  store %struct.Memory* %call_400c39, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400c08	 RIP: 400c3c	 Bytes: 5
  %loadMem_400c3c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c3c = call %struct.Memory* @routine_jmpq_.L_400c08(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c3c, i64 -52, i64 5)
  store %struct.Memory* %call_400c3c, %struct.Memory** %MEMORY

  br label %block_.L_400c08

  ; Code: .L_400c41:	 RIP: 400c41	 Bytes: 0
block_.L_400c41:

  ; Code: jmpq .L_400c46	 RIP: 400c41	 Bytes: 5
  %loadMem_400c41 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c41 = call %struct.Memory* @routine_jmpq_.L_400c46(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c41, i64 5, i64 5)
  store %struct.Memory* %call_400c41, %struct.Memory** %MEMORY

  br label %block_.L_400c46

  ; Code: .L_400c46:	 RIP: 400c46	 Bytes: 0
block_.L_400c46:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400c46	 Bytes: 3
  %loadMem_400c46 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c46 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c46)
  store %struct.Memory* %call_400c46, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400c49	 Bytes: 3
  %loadMem_400c49 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c49 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c49)
  store %struct.Memory* %call_400c49, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400c4c	 Bytes: 3
  %loadMem_400c4c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c4c = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c4c)
  store %struct.Memory* %call_400c4c, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400bf7	 RIP: 400c4f	 Bytes: 5
  %loadMem_400c4f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c4f = call %struct.Memory* @routine_jmpq_.L_400bf7(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c4f, i64 -88, i64 5)
  store %struct.Memory* %call_400c4f, %struct.Memory** %MEMORY

  br label %block_.L_400bf7

  ; Code: .L_400c54:	 RIP: 400c54	 Bytes: 0
block_.L_400c54:

  ; Code: jmpq .L_400c59	 RIP: 400c54	 Bytes: 5
  %loadMem_400c54 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c54 = call %struct.Memory* @routine_jmpq_.L_400c59(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c54, i64 5, i64 5)
  store %struct.Memory* %call_400c54, %struct.Memory** %MEMORY

  br label %block_.L_400c59

  ; Code: .L_400c59:	 RIP: 400c59	 Bytes: 0
block_.L_400c59:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400c59	 Bytes: 3
  %loadMem_400c59 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c59 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c59)
  store %struct.Memory* %call_400c59, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400c5c	 Bytes: 3
  %loadMem_400c5c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c5c = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c5c)
  store %struct.Memory* %call_400c5c, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400c5f	 Bytes: 3
  %loadMem_400c5f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c5f = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c5f)
  store %struct.Memory* %call_400c5f, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400be6	 RIP: 400c62	 Bytes: 5
  %loadMem_400c62 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c62 = call %struct.Memory* @routine_jmpq_.L_400be6(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c62, i64 -124, i64 5)
  store %struct.Memory* %call_400c62, %struct.Memory** %MEMORY

  br label %block_.L_400be6

  ; Code: .L_400c67:	 RIP: 400c67	 Bytes: 0
block_.L_400c67:

  ; Code: movl $0x0, 0x619200	 RIP: 400c67	 Bytes: 11
  %loadMem_400c67 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c67 = call %struct.Memory* @routine_movl__0x0__0x619200(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c67)
  store %struct.Memory* %call_400c67, %struct.Memory** %MEMORY

  ; Code: movl $0x43, 0x610260	 RIP: 400c72	 Bytes: 11
  %loadMem_400c72 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c72 = call %struct.Memory* @routine_movl__0x43__0x610260(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c72)
  store %struct.Memory* %call_400c72, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400c7d	 Bytes: 7
  %loadMem_400c7d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c7d = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c7d)
  store %struct.Memory* %call_400c7d, %struct.Memory** %MEMORY

  ; Code: .L_400c84:	 RIP: 400c84	 Bytes: 0
  br label %block_.L_400c84
block_.L_400c84:

  ; Code: cmpl $0x0, -0x4(%rbp)	 RIP: 400c84	 Bytes: 4
  %loadMem_400c84 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c84 = call %struct.Memory* @routine_cmpl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c84)
  store %struct.Memory* %call_400c84, %struct.Memory** %MEMORY

  ; Code: jg .L_400d05	 RIP: 400c88	 Bytes: 6
  %loadMem_400c88 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c88 = call %struct.Memory* @routine_jg_.L_400d05(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c88, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400c88, %struct.Memory** %MEMORY

  %loadBr_400c88 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400c88 = icmp eq i8 %loadBr_400c88, 1
  br i1 %cmpBr_400c88, label %block_.L_400d05, label %block_400c8e

block_400c8e:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400c8e	 Bytes: 7
  %loadMem_400c8e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c8e = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c8e)
  store %struct.Memory* %call_400c8e, %struct.Memory** %MEMORY

  ; Code: .L_400c95:	 RIP: 400c95	 Bytes: 0
  br label %block_.L_400c95
block_.L_400c95:

  ; Code: cmpl $0x1, -0x8(%rbp)	 RIP: 400c95	 Bytes: 4
  %loadMem_400c95 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c95 = call %struct.Memory* @routine_cmpl__0x1__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c95)
  store %struct.Memory* %call_400c95, %struct.Memory** %MEMORY

  ; Code: jg .L_400cf2	 RIP: 400c99	 Bytes: 6
  %loadMem_400c99 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c99 = call %struct.Memory* @routine_jg_.L_400cf2(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c99, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400c99, %struct.Memory** %MEMORY

  %loadBr_400c99 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400c99 = icmp eq i8 %loadBr_400c99, 1
  br i1 %cmpBr_400c99, label %block_.L_400cf2, label %block_400c9f

block_400c9f:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400c9f	 Bytes: 7
  %loadMem_400c9f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400c9f = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400c9f)
  store %struct.Memory* %call_400c9f, %struct.Memory** %MEMORY

  ; Code: .L_400ca6:	 RIP: 400ca6	 Bytes: 0
  br label %block_.L_400ca6
block_.L_400ca6:

  ; Code: cmpl $0x3, -0xc(%rbp)	 RIP: 400ca6	 Bytes: 4
  %loadMem_400ca6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ca6 = call %struct.Memory* @routine_cmpl__0x3__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ca6)
  store %struct.Memory* %call_400ca6, %struct.Memory** %MEMORY

  ; Code: jg .L_400cdf	 RIP: 400caa	 Bytes: 6
  %loadMem_400caa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400caa = call %struct.Memory* @routine_jg_.L_400cdf(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400caa, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400caa, %struct.Memory** %MEMORY

  %loadBr_400caa = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400caa = icmp eq i8 %loadBr_400caa, 1
  br i1 %cmpBr_400caa, label %block_.L_400cdf, label %block_400cb0

block_400cb0:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400cb0	 Bytes: 3
  %loadMem_400cb0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cb0 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cb0)
  store %struct.Memory* %call_400cb0, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400cb3	 Bytes: 3
  %loadMem_400cb3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cb3 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cb3)
  store %struct.Memory* %call_400cb3, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400cb6	 Bytes: 3
  %loadMem_400cb6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cb6 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cb6)
  store %struct.Memory* %call_400cb6, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400cb9	 Bytes: 3
  %loadMem_400cb9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cb9 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cb9)
  store %struct.Memory* %call_400cb9, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400cbc	 Bytes: 2
  %loadMem_400cbc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cbc = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cbc)
  store %struct.Memory* %call_400cbc, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400cbe	 Bytes: 3
  %loadMem_400cbe = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cbe = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cbe)
  store %struct.Memory* %call_400cbe, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400cc1	 Bytes: 2
  %loadMem_400cc1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cc1 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cc1)
  store %struct.Memory* %call_400cc1, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400cc3	 Bytes: 3
  %loadMem_400cc3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cc3 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cc3)
  store %struct.Memory* %call_400cc3, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6062a0(,%rsi,4)	 RIP: 400cc6	 Bytes: 11
  %loadMem_400cc6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cc6 = call %struct.Memory* @routine_movl__0x1__0x6062a0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cc6)
  store %struct.Memory* %call_400cc6, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400cd1	 Bytes: 3
  %loadMem_400cd1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cd1 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cd1)
  store %struct.Memory* %call_400cd1, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400cd4	 Bytes: 3
  %loadMem_400cd4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cd4 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cd4)
  store %struct.Memory* %call_400cd4, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400cd7	 Bytes: 3
  %loadMem_400cd7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cd7 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cd7)
  store %struct.Memory* %call_400cd7, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400ca6	 RIP: 400cda	 Bytes: 5
  %loadMem_400cda = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cda = call %struct.Memory* @routine_jmpq_.L_400ca6(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cda, i64 -52, i64 5)
  store %struct.Memory* %call_400cda, %struct.Memory** %MEMORY

  br label %block_.L_400ca6

  ; Code: .L_400cdf:	 RIP: 400cdf	 Bytes: 0
block_.L_400cdf:

  ; Code: jmpq .L_400ce4	 RIP: 400cdf	 Bytes: 5
  %loadMem_400cdf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cdf = call %struct.Memory* @routine_jmpq_.L_400ce4(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cdf, i64 5, i64 5)
  store %struct.Memory* %call_400cdf, %struct.Memory** %MEMORY

  br label %block_.L_400ce4

  ; Code: .L_400ce4:	 RIP: 400ce4	 Bytes: 0
block_.L_400ce4:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400ce4	 Bytes: 3
  %loadMem_400ce4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ce4 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ce4)
  store %struct.Memory* %call_400ce4, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400ce7	 Bytes: 3
  %loadMem_400ce7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ce7 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ce7)
  store %struct.Memory* %call_400ce7, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400cea	 Bytes: 3
  %loadMem_400cea = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cea = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cea)
  store %struct.Memory* %call_400cea, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400c95	 RIP: 400ced	 Bytes: 5
  %loadMem_400ced = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ced = call %struct.Memory* @routine_jmpq_.L_400c95(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ced, i64 -88, i64 5)
  store %struct.Memory* %call_400ced, %struct.Memory** %MEMORY

  br label %block_.L_400c95

  ; Code: .L_400cf2:	 RIP: 400cf2	 Bytes: 0
block_.L_400cf2:

  ; Code: jmpq .L_400cf7	 RIP: 400cf2	 Bytes: 5
  %loadMem_400cf2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cf2 = call %struct.Memory* @routine_jmpq_.L_400cf7(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cf2, i64 5, i64 5)
  store %struct.Memory* %call_400cf2, %struct.Memory** %MEMORY

  br label %block_.L_400cf7

  ; Code: .L_400cf7:	 RIP: 400cf7	 Bytes: 0
block_.L_400cf7:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400cf7	 Bytes: 3
  %loadMem_400cf7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cf7 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cf7)
  store %struct.Memory* %call_400cf7, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400cfa	 Bytes: 3
  %loadMem_400cfa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cfa = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cfa)
  store %struct.Memory* %call_400cfa, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400cfd	 Bytes: 3
  %loadMem_400cfd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400cfd = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400cfd)
  store %struct.Memory* %call_400cfd, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400c84	 RIP: 400d00	 Bytes: 5
  %loadMem_400d00 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d00 = call %struct.Memory* @routine_jmpq_.L_400c84(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d00, i64 -124, i64 5)
  store %struct.Memory* %call_400d00, %struct.Memory** %MEMORY

  br label %block_.L_400c84

  ; Code: .L_400d05:	 RIP: 400d05	 Bytes: 0
block_.L_400d05:

  ; Code: movl $0x0, 0x619204	 RIP: 400d05	 Bytes: 11
  %loadMem_400d05 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d05 = call %struct.Memory* @routine_movl__0x0__0x619204(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d05)
  store %struct.Memory* %call_400d05, %struct.Memory** %MEMORY

  ; Code: movl $0xc8, 0x610264	 RIP: 400d10	 Bytes: 11
  %loadMem_400d10 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d10 = call %struct.Memory* @routine_movl__0xc8__0x610264(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d10)
  store %struct.Memory* %call_400d10, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400d1b	 Bytes: 7
  %loadMem_400d1b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d1b = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d1b)
  store %struct.Memory* %call_400d1b, %struct.Memory** %MEMORY

  ; Code: .L_400d22:	 RIP: 400d22	 Bytes: 0
  br label %block_.L_400d22
block_.L_400d22:

  ; Code: cmpl $0x2, -0x4(%rbp)	 RIP: 400d22	 Bytes: 4
  %loadMem_400d22 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d22 = call %struct.Memory* @routine_cmpl__0x2__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d22)
  store %struct.Memory* %call_400d22, %struct.Memory** %MEMORY

  ; Code: jg .L_400da3	 RIP: 400d26	 Bytes: 6
  %loadMem_400d26 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d26 = call %struct.Memory* @routine_jg_.L_400da3(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d26, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400d26, %struct.Memory** %MEMORY

  %loadBr_400d26 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400d26 = icmp eq i8 %loadBr_400d26, 1
  br i1 %cmpBr_400d26, label %block_.L_400da3, label %block_400d2c

block_400d2c:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400d2c	 Bytes: 7
  %loadMem_400d2c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d2c = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d2c)
  store %struct.Memory* %call_400d2c, %struct.Memory** %MEMORY

  ; Code: .L_400d33:	 RIP: 400d33	 Bytes: 0
  br label %block_.L_400d33
block_.L_400d33:

  ; Code: cmpl $0x0, -0x8(%rbp)	 RIP: 400d33	 Bytes: 4
  %loadMem_400d33 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d33 = call %struct.Memory* @routine_cmpl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d33)
  store %struct.Memory* %call_400d33, %struct.Memory** %MEMORY

  ; Code: jg .L_400d90	 RIP: 400d37	 Bytes: 6
  %loadMem_400d37 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d37 = call %struct.Memory* @routine_jg_.L_400d90(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d37, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400d37, %struct.Memory** %MEMORY

  %loadBr_400d37 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400d37 = icmp eq i8 %loadBr_400d37, 1
  br i1 %cmpBr_400d37, label %block_.L_400d90, label %block_400d3d

block_400d3d:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400d3d	 Bytes: 7
  %loadMem_400d3d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d3d = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d3d)
  store %struct.Memory* %call_400d3d, %struct.Memory** %MEMORY

  ; Code: .L_400d44:	 RIP: 400d44	 Bytes: 0
  br label %block_.L_400d44
block_.L_400d44:

  ; Code: cmpl $0x0, -0xc(%rbp)	 RIP: 400d44	 Bytes: 4
  %loadMem_400d44 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d44 = call %struct.Memory* @routine_cmpl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d44)
  store %struct.Memory* %call_400d44, %struct.Memory** %MEMORY

  ; Code: jg .L_400d7d	 RIP: 400d48	 Bytes: 6
  %loadMem_400d48 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d48 = call %struct.Memory* @routine_jg_.L_400d7d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d48, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400d48, %struct.Memory** %MEMORY

  %loadBr_400d48 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400d48 = icmp eq i8 %loadBr_400d48, 1
  br i1 %cmpBr_400d48, label %block_.L_400d7d, label %block_400d4e

block_400d4e:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400d4e	 Bytes: 3
  %loadMem_400d4e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d4e = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d4e)
  store %struct.Memory* %call_400d4e, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400d51	 Bytes: 3
  %loadMem_400d51 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d51 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d51)
  store %struct.Memory* %call_400d51, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400d54	 Bytes: 3
  %loadMem_400d54 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d54 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d54)
  store %struct.Memory* %call_400d54, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400d57	 Bytes: 3
  %loadMem_400d57 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d57 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d57)
  store %struct.Memory* %call_400d57, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400d5a	 Bytes: 2
  %loadMem_400d5a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d5a = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d5a)
  store %struct.Memory* %call_400d5a, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400d5c	 Bytes: 3
  %loadMem_400d5c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d5c = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d5c)
  store %struct.Memory* %call_400d5c, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400d5f	 Bytes: 2
  %loadMem_400d5f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d5f = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d5f)
  store %struct.Memory* %call_400d5f, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400d61	 Bytes: 3
  %loadMem_400d61 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d61 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d61)
  store %struct.Memory* %call_400d61, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x606aa0(,%rsi,4)	 RIP: 400d64	 Bytes: 11
  %loadMem_400d64 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d64 = call %struct.Memory* @routine_movl__0x1__0x606aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d64)
  store %struct.Memory* %call_400d64, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400d6f	 Bytes: 3
  %loadMem_400d6f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d6f = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d6f)
  store %struct.Memory* %call_400d6f, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400d72	 Bytes: 3
  %loadMem_400d72 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d72 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d72)
  store %struct.Memory* %call_400d72, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400d75	 Bytes: 3
  %loadMem_400d75 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d75 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d75)
  store %struct.Memory* %call_400d75, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400d44	 RIP: 400d78	 Bytes: 5
  %loadMem_400d78 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d78 = call %struct.Memory* @routine_jmpq_.L_400d44(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d78, i64 -52, i64 5)
  store %struct.Memory* %call_400d78, %struct.Memory** %MEMORY

  br label %block_.L_400d44

  ; Code: .L_400d7d:	 RIP: 400d7d	 Bytes: 0
block_.L_400d7d:

  ; Code: jmpq .L_400d82	 RIP: 400d7d	 Bytes: 5
  %loadMem_400d7d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d7d = call %struct.Memory* @routine_jmpq_.L_400d82(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d7d, i64 5, i64 5)
  store %struct.Memory* %call_400d7d, %struct.Memory** %MEMORY

  br label %block_.L_400d82

  ; Code: .L_400d82:	 RIP: 400d82	 Bytes: 0
block_.L_400d82:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400d82	 Bytes: 3
  %loadMem_400d82 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d82 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d82)
  store %struct.Memory* %call_400d82, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400d85	 Bytes: 3
  %loadMem_400d85 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d85 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d85)
  store %struct.Memory* %call_400d85, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400d88	 Bytes: 3
  %loadMem_400d88 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d88 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d88)
  store %struct.Memory* %call_400d88, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400d33	 RIP: 400d8b	 Bytes: 5
  %loadMem_400d8b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d8b = call %struct.Memory* @routine_jmpq_.L_400d33(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d8b, i64 -88, i64 5)
  store %struct.Memory* %call_400d8b, %struct.Memory** %MEMORY

  br label %block_.L_400d33

  ; Code: .L_400d90:	 RIP: 400d90	 Bytes: 0
block_.L_400d90:

  ; Code: jmpq .L_400d95	 RIP: 400d90	 Bytes: 5
  %loadMem_400d90 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d90 = call %struct.Memory* @routine_jmpq_.L_400d95(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d90, i64 5, i64 5)
  store %struct.Memory* %call_400d90, %struct.Memory** %MEMORY

  br label %block_.L_400d95

  ; Code: .L_400d95:	 RIP: 400d95	 Bytes: 0
block_.L_400d95:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400d95	 Bytes: 3
  %loadMem_400d95 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d95 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d95)
  store %struct.Memory* %call_400d95, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400d98	 Bytes: 3
  %loadMem_400d98 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d98 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d98)
  store %struct.Memory* %call_400d98, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400d9b	 Bytes: 3
  %loadMem_400d9b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d9b = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d9b)
  store %struct.Memory* %call_400d9b, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400d22	 RIP: 400d9e	 Bytes: 5
  %loadMem_400d9e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400d9e = call %struct.Memory* @routine_jmpq_.L_400d22(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400d9e, i64 -124, i64 5)
  store %struct.Memory* %call_400d9e, %struct.Memory** %MEMORY

  br label %block_.L_400d22

  ; Code: .L_400da3:	 RIP: 400da3	 Bytes: 0
block_.L_400da3:

  ; Code: movl $0x1, 0x619208	 RIP: 400da3	 Bytes: 11
  %loadMem_400da3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400da3 = call %struct.Memory* @routine_movl__0x1__0x619208(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400da3)
  store %struct.Memory* %call_400da3, %struct.Memory** %MEMORY

  ; Code: movl $0x2, 0x610268	 RIP: 400dae	 Bytes: 11
  %loadMem_400dae = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dae = call %struct.Memory* @routine_movl__0x2__0x610268(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dae)
  store %struct.Memory* %call_400dae, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400db9	 Bytes: 7
  %loadMem_400db9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400db9 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400db9)
  store %struct.Memory* %call_400db9, %struct.Memory** %MEMORY

  ; Code: .L_400dc0:	 RIP: 400dc0	 Bytes: 0
  br label %block_.L_400dc0
block_.L_400dc0:

  ; Code: cmpl $0x0, -0x4(%rbp)	 RIP: 400dc0	 Bytes: 4
  %loadMem_400dc0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dc0 = call %struct.Memory* @routine_cmpl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dc0)
  store %struct.Memory* %call_400dc0, %struct.Memory** %MEMORY

  ; Code: jg .L_400e41	 RIP: 400dc4	 Bytes: 6
  %loadMem_400dc4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dc4 = call %struct.Memory* @routine_jg_.L_400e41(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dc4, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400dc4, %struct.Memory** %MEMORY

  %loadBr_400dc4 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400dc4 = icmp eq i8 %loadBr_400dc4, 1
  br i1 %cmpBr_400dc4, label %block_.L_400e41, label %block_400dca

block_400dca:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400dca	 Bytes: 7
  %loadMem_400dca = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dca = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dca)
  store %struct.Memory* %call_400dca, %struct.Memory** %MEMORY

  ; Code: .L_400dd1:	 RIP: 400dd1	 Bytes: 0
  br label %block_.L_400dd1
block_.L_400dd1:

  ; Code: cmpl $0x2, -0x8(%rbp)	 RIP: 400dd1	 Bytes: 4
  %loadMem_400dd1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dd1 = call %struct.Memory* @routine_cmpl__0x2__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dd1)
  store %struct.Memory* %call_400dd1, %struct.Memory** %MEMORY

  ; Code: jg .L_400e2e	 RIP: 400dd5	 Bytes: 6
  %loadMem_400dd5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dd5 = call %struct.Memory* @routine_jg_.L_400e2e(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dd5, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400dd5, %struct.Memory** %MEMORY

  %loadBr_400dd5 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400dd5 = icmp eq i8 %loadBr_400dd5, 1
  br i1 %cmpBr_400dd5, label %block_.L_400e2e, label %block_400ddb

block_400ddb:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400ddb	 Bytes: 7
  %loadMem_400ddb = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ddb = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ddb)
  store %struct.Memory* %call_400ddb, %struct.Memory** %MEMORY

  ; Code: .L_400de2:	 RIP: 400de2	 Bytes: 0
  br label %block_.L_400de2
block_.L_400de2:

  ; Code: cmpl $0x0, -0xc(%rbp)	 RIP: 400de2	 Bytes: 4
  %loadMem_400de2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400de2 = call %struct.Memory* @routine_cmpl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400de2)
  store %struct.Memory* %call_400de2, %struct.Memory** %MEMORY

  ; Code: jg .L_400e1b	 RIP: 400de6	 Bytes: 6
  %loadMem_400de6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400de6 = call %struct.Memory* @routine_jg_.L_400e1b(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400de6, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400de6, %struct.Memory** %MEMORY

  %loadBr_400de6 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400de6 = icmp eq i8 %loadBr_400de6, 1
  br i1 %cmpBr_400de6, label %block_.L_400e1b, label %block_400dec

block_400dec:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400dec	 Bytes: 3
  %loadMem_400dec = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dec = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dec)
  store %struct.Memory* %call_400dec, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400def	 Bytes: 3
  %loadMem_400def = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400def = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400def)
  store %struct.Memory* %call_400def, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400df2	 Bytes: 3
  %loadMem_400df2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400df2 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400df2)
  store %struct.Memory* %call_400df2, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400df5	 Bytes: 3
  %loadMem_400df5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400df5 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400df5)
  store %struct.Memory* %call_400df5, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400df8	 Bytes: 2
  %loadMem_400df8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400df8 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400df8)
  store %struct.Memory* %call_400df8, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400dfa	 Bytes: 3
  %loadMem_400dfa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dfa = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dfa)
  store %struct.Memory* %call_400dfa, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400dfd	 Bytes: 2
  %loadMem_400dfd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dfd = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dfd)
  store %struct.Memory* %call_400dfd, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400dff	 Bytes: 3
  %loadMem_400dff = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400dff = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400dff)
  store %struct.Memory* %call_400dff, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6072a0(,%rsi,4)	 RIP: 400e02	 Bytes: 11
  %loadMem_400e02 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e02 = call %struct.Memory* @routine_movl__0x1__0x6072a0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e02)
  store %struct.Memory* %call_400e02, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400e0d	 Bytes: 3
  %loadMem_400e0d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e0d = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e0d)
  store %struct.Memory* %call_400e0d, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400e10	 Bytes: 3
  %loadMem_400e10 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e10 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e10)
  store %struct.Memory* %call_400e10, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400e13	 Bytes: 3
  %loadMem_400e13 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e13 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e13)
  store %struct.Memory* %call_400e13, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400de2	 RIP: 400e16	 Bytes: 5
  %loadMem_400e16 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e16 = call %struct.Memory* @routine_jmpq_.L_400de2(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e16, i64 -52, i64 5)
  store %struct.Memory* %call_400e16, %struct.Memory** %MEMORY

  br label %block_.L_400de2

  ; Code: .L_400e1b:	 RIP: 400e1b	 Bytes: 0
block_.L_400e1b:

  ; Code: jmpq .L_400e20	 RIP: 400e1b	 Bytes: 5
  %loadMem_400e1b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e1b = call %struct.Memory* @routine_jmpq_.L_400e20(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e1b, i64 5, i64 5)
  store %struct.Memory* %call_400e1b, %struct.Memory** %MEMORY

  br label %block_.L_400e20

  ; Code: .L_400e20:	 RIP: 400e20	 Bytes: 0
block_.L_400e20:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400e20	 Bytes: 3
  %loadMem_400e20 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e20 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e20)
  store %struct.Memory* %call_400e20, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400e23	 Bytes: 3
  %loadMem_400e23 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e23 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e23)
  store %struct.Memory* %call_400e23, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400e26	 Bytes: 3
  %loadMem_400e26 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e26 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e26)
  store %struct.Memory* %call_400e26, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400dd1	 RIP: 400e29	 Bytes: 5
  %loadMem_400e29 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e29 = call %struct.Memory* @routine_jmpq_.L_400dd1(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e29, i64 -88, i64 5)
  store %struct.Memory* %call_400e29, %struct.Memory** %MEMORY

  br label %block_.L_400dd1

  ; Code: .L_400e2e:	 RIP: 400e2e	 Bytes: 0
block_.L_400e2e:

  ; Code: jmpq .L_400e33	 RIP: 400e2e	 Bytes: 5
  %loadMem_400e2e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e2e = call %struct.Memory* @routine_jmpq_.L_400e33(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e2e, i64 5, i64 5)
  store %struct.Memory* %call_400e2e, %struct.Memory** %MEMORY

  br label %block_.L_400e33

  ; Code: .L_400e33:	 RIP: 400e33	 Bytes: 0
block_.L_400e33:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400e33	 Bytes: 3
  %loadMem_400e33 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e33 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e33)
  store %struct.Memory* %call_400e33, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400e36	 Bytes: 3
  %loadMem_400e36 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e36 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e36)
  store %struct.Memory* %call_400e36, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400e39	 Bytes: 3
  %loadMem_400e39 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e39 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e39)
  store %struct.Memory* %call_400e39, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400dc0	 RIP: 400e3c	 Bytes: 5
  %loadMem_400e3c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e3c = call %struct.Memory* @routine_jmpq_.L_400dc0(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e3c, i64 -124, i64 5)
  store %struct.Memory* %call_400e3c, %struct.Memory** %MEMORY

  br label %block_.L_400dc0

  ; Code: .L_400e41:	 RIP: 400e41	 Bytes: 0
block_.L_400e41:

  ; Code: movl $0x1, 0x61920c	 RIP: 400e41	 Bytes: 11
  %loadMem_400e41 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e41 = call %struct.Memory* @routine_movl__0x1__0x61920c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e41)
  store %struct.Memory* %call_400e41, %struct.Memory** %MEMORY

  ; Code: movl $0x10, 0x61026c	 RIP: 400e4c	 Bytes: 11
  %loadMem_400e4c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e4c = call %struct.Memory* @routine_movl__0x10__0x61026c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e4c)
  store %struct.Memory* %call_400e4c, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400e57	 Bytes: 7
  %loadMem_400e57 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e57 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e57)
  store %struct.Memory* %call_400e57, %struct.Memory** %MEMORY

  ; Code: .L_400e5e:	 RIP: 400e5e	 Bytes: 0
  br label %block_.L_400e5e
block_.L_400e5e:

  ; Code: cmpl $0x0, -0x4(%rbp)	 RIP: 400e5e	 Bytes: 4
  %loadMem_400e5e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e5e = call %struct.Memory* @routine_cmpl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e5e)
  store %struct.Memory* %call_400e5e, %struct.Memory** %MEMORY

  ; Code: jg .L_400edf	 RIP: 400e62	 Bytes: 6
  %loadMem_400e62 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e62 = call %struct.Memory* @routine_jg_.L_400edf(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e62, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400e62, %struct.Memory** %MEMORY

  %loadBr_400e62 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400e62 = icmp eq i8 %loadBr_400e62, 1
  br i1 %cmpBr_400e62, label %block_.L_400edf, label %block_400e68

block_400e68:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400e68	 Bytes: 7
  %loadMem_400e68 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e68 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e68)
  store %struct.Memory* %call_400e68, %struct.Memory** %MEMORY

  ; Code: .L_400e6f:	 RIP: 400e6f	 Bytes: 0
  br label %block_.L_400e6f
block_.L_400e6f:

  ; Code: cmpl $0x0, -0x8(%rbp)	 RIP: 400e6f	 Bytes: 4
  %loadMem_400e6f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e6f = call %struct.Memory* @routine_cmpl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e6f)
  store %struct.Memory* %call_400e6f, %struct.Memory** %MEMORY

  ; Code: jg .L_400ecc	 RIP: 400e73	 Bytes: 6
  %loadMem_400e73 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e73 = call %struct.Memory* @routine_jg_.L_400ecc(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e73, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400e73, %struct.Memory** %MEMORY

  %loadBr_400e73 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400e73 = icmp eq i8 %loadBr_400e73, 1
  br i1 %cmpBr_400e73, label %block_.L_400ecc, label %block_400e79

block_400e79:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400e79	 Bytes: 7
  %loadMem_400e79 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e79 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e79)
  store %struct.Memory* %call_400e79, %struct.Memory** %MEMORY

  ; Code: .L_400e80:	 RIP: 400e80	 Bytes: 0
  br label %block_.L_400e80
block_.L_400e80:

  ; Code: cmpl $0x2, -0xc(%rbp)	 RIP: 400e80	 Bytes: 4
  %loadMem_400e80 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e80 = call %struct.Memory* @routine_cmpl__0x2__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e80)
  store %struct.Memory* %call_400e80, %struct.Memory** %MEMORY

  ; Code: jg .L_400eb9	 RIP: 400e84	 Bytes: 6
  %loadMem_400e84 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e84 = call %struct.Memory* @routine_jg_.L_400eb9(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e84, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400e84, %struct.Memory** %MEMORY

  %loadBr_400e84 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400e84 = icmp eq i8 %loadBr_400e84, 1
  br i1 %cmpBr_400e84, label %block_.L_400eb9, label %block_400e8a

block_400e8a:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400e8a	 Bytes: 3
  %loadMem_400e8a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e8a = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e8a)
  store %struct.Memory* %call_400e8a, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400e8d	 Bytes: 3
  %loadMem_400e8d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e8d = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e8d)
  store %struct.Memory* %call_400e8d, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400e90	 Bytes: 3
  %loadMem_400e90 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e90 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e90)
  store %struct.Memory* %call_400e90, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400e93	 Bytes: 3
  %loadMem_400e93 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e93 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e93)
  store %struct.Memory* %call_400e93, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400e96	 Bytes: 2
  %loadMem_400e96 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e96 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e96)
  store %struct.Memory* %call_400e96, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400e98	 Bytes: 3
  %loadMem_400e98 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e98 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e98)
  store %struct.Memory* %call_400e98, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400e9b	 Bytes: 2
  %loadMem_400e9b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e9b = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e9b)
  store %struct.Memory* %call_400e9b, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400e9d	 Bytes: 3
  %loadMem_400e9d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400e9d = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400e9d)
  store %struct.Memory* %call_400e9d, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x607aa0(,%rsi,4)	 RIP: 400ea0	 Bytes: 11
  %loadMem_400ea0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ea0 = call %struct.Memory* @routine_movl__0x1__0x607aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ea0)
  store %struct.Memory* %call_400ea0, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400eab	 Bytes: 3
  %loadMem_400eab = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eab = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eab)
  store %struct.Memory* %call_400eab, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400eae	 Bytes: 3
  %loadMem_400eae = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eae = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eae)
  store %struct.Memory* %call_400eae, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400eb1	 Bytes: 3
  %loadMem_400eb1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eb1 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eb1)
  store %struct.Memory* %call_400eb1, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400e80	 RIP: 400eb4	 Bytes: 5
  %loadMem_400eb4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eb4 = call %struct.Memory* @routine_jmpq_.L_400e80(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eb4, i64 -52, i64 5)
  store %struct.Memory* %call_400eb4, %struct.Memory** %MEMORY

  br label %block_.L_400e80

  ; Code: .L_400eb9:	 RIP: 400eb9	 Bytes: 0
block_.L_400eb9:

  ; Code: jmpq .L_400ebe	 RIP: 400eb9	 Bytes: 5
  %loadMem_400eb9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eb9 = call %struct.Memory* @routine_jmpq_.L_400ebe(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eb9, i64 5, i64 5)
  store %struct.Memory* %call_400eb9, %struct.Memory** %MEMORY

  br label %block_.L_400ebe

  ; Code: .L_400ebe:	 RIP: 400ebe	 Bytes: 0
block_.L_400ebe:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400ebe	 Bytes: 3
  %loadMem_400ebe = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ebe = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ebe)
  store %struct.Memory* %call_400ebe, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400ec1	 Bytes: 3
  %loadMem_400ec1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ec1 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ec1)
  store %struct.Memory* %call_400ec1, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400ec4	 Bytes: 3
  %loadMem_400ec4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ec4 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ec4)
  store %struct.Memory* %call_400ec4, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400e6f	 RIP: 400ec7	 Bytes: 5
  %loadMem_400ec7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ec7 = call %struct.Memory* @routine_jmpq_.L_400e6f(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ec7, i64 -88, i64 5)
  store %struct.Memory* %call_400ec7, %struct.Memory** %MEMORY

  br label %block_.L_400e6f

  ; Code: .L_400ecc:	 RIP: 400ecc	 Bytes: 0
block_.L_400ecc:

  ; Code: jmpq .L_400ed1	 RIP: 400ecc	 Bytes: 5
  %loadMem_400ecc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ecc = call %struct.Memory* @routine_jmpq_.L_400ed1(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ecc, i64 5, i64 5)
  store %struct.Memory* %call_400ecc, %struct.Memory** %MEMORY

  br label %block_.L_400ed1

  ; Code: .L_400ed1:	 RIP: 400ed1	 Bytes: 0
block_.L_400ed1:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400ed1	 Bytes: 3
  %loadMem_400ed1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ed1 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ed1)
  store %struct.Memory* %call_400ed1, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400ed4	 Bytes: 3
  %loadMem_400ed4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ed4 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ed4)
  store %struct.Memory* %call_400ed4, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400ed7	 Bytes: 3
  %loadMem_400ed7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ed7 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ed7)
  store %struct.Memory* %call_400ed7, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400e5e	 RIP: 400eda	 Bytes: 5
  %loadMem_400eda = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eda = call %struct.Memory* @routine_jmpq_.L_400e5e(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eda, i64 -124, i64 5)
  store %struct.Memory* %call_400eda, %struct.Memory** %MEMORY

  br label %block_.L_400e5e

  ; Code: .L_400edf:	 RIP: 400edf	 Bytes: 0
block_.L_400edf:

  ; Code: movl $0x1, 0x619210	 RIP: 400edf	 Bytes: 11
  %loadMem_400edf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400edf = call %struct.Memory* @routine_movl__0x1__0x619210(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400edf)
  store %struct.Memory* %call_400edf, %struct.Memory** %MEMORY

  ; Code: movl $0x80, 0x610270	 RIP: 400eea	 Bytes: 11
  %loadMem_400eea = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400eea = call %struct.Memory* @routine_movl__0x80__0x610270(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400eea)
  store %struct.Memory* %call_400eea, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400ef5	 Bytes: 7
  %loadMem_400ef5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ef5 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ef5)
  store %struct.Memory* %call_400ef5, %struct.Memory** %MEMORY

  ; Code: .L_400efc:	 RIP: 400efc	 Bytes: 0
  br label %block_.L_400efc
block_.L_400efc:

  ; Code: cmpl $0x1, -0x4(%rbp)	 RIP: 400efc	 Bytes: 4
  %loadMem_400efc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400efc = call %struct.Memory* @routine_cmpl__0x1__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400efc)
  store %struct.Memory* %call_400efc, %struct.Memory** %MEMORY

  ; Code: jg .L_400f7d	 RIP: 400f00	 Bytes: 6
  %loadMem_400f00 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f00 = call %struct.Memory* @routine_jg_.L_400f7d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f00, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400f00, %struct.Memory** %MEMORY

  %loadBr_400f00 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400f00 = icmp eq i8 %loadBr_400f00, 1
  br i1 %cmpBr_400f00, label %block_.L_400f7d, label %block_400f06

block_400f06:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400f06	 Bytes: 7
  %loadMem_400f06 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f06 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f06)
  store %struct.Memory* %call_400f06, %struct.Memory** %MEMORY

  ; Code: .L_400f0d:	 RIP: 400f0d	 Bytes: 0
  br label %block_.L_400f0d
block_.L_400f0d:

  ; Code: cmpl $0x1, -0x8(%rbp)	 RIP: 400f0d	 Bytes: 4
  %loadMem_400f0d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f0d = call %struct.Memory* @routine_cmpl__0x1__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f0d)
  store %struct.Memory* %call_400f0d, %struct.Memory** %MEMORY

  ; Code: jg .L_400f6a	 RIP: 400f11	 Bytes: 6
  %loadMem_400f11 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f11 = call %struct.Memory* @routine_jg_.L_400f6a(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f11, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400f11, %struct.Memory** %MEMORY

  %loadBr_400f11 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400f11 = icmp eq i8 %loadBr_400f11, 1
  br i1 %cmpBr_400f11, label %block_.L_400f6a, label %block_400f17

block_400f17:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400f17	 Bytes: 7
  %loadMem_400f17 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f17 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f17)
  store %struct.Memory* %call_400f17, %struct.Memory** %MEMORY

  ; Code: .L_400f1e:	 RIP: 400f1e	 Bytes: 0
  br label %block_.L_400f1e
block_.L_400f1e:

  ; Code: cmpl $0x0, -0xc(%rbp)	 RIP: 400f1e	 Bytes: 4
  %loadMem_400f1e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f1e = call %struct.Memory* @routine_cmpl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f1e)
  store %struct.Memory* %call_400f1e, %struct.Memory** %MEMORY

  ; Code: jg .L_400f57	 RIP: 400f22	 Bytes: 6
  %loadMem_400f22 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f22 = call %struct.Memory* @routine_jg_.L_400f57(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f22, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400f22, %struct.Memory** %MEMORY

  %loadBr_400f22 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400f22 = icmp eq i8 %loadBr_400f22, 1
  br i1 %cmpBr_400f22, label %block_.L_400f57, label %block_400f28

block_400f28:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400f28	 Bytes: 3
  %loadMem_400f28 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f28 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f28)
  store %struct.Memory* %call_400f28, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400f2b	 Bytes: 3
  %loadMem_400f2b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f2b = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f2b)
  store %struct.Memory* %call_400f2b, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400f2e	 Bytes: 3
  %loadMem_400f2e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f2e = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f2e)
  store %struct.Memory* %call_400f2e, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400f31	 Bytes: 3
  %loadMem_400f31 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f31 = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f31)
  store %struct.Memory* %call_400f31, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400f34	 Bytes: 2
  %loadMem_400f34 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f34 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f34)
  store %struct.Memory* %call_400f34, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400f36	 Bytes: 3
  %loadMem_400f36 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f36 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f36)
  store %struct.Memory* %call_400f36, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400f39	 Bytes: 2
  %loadMem_400f39 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f39 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f39)
  store %struct.Memory* %call_400f39, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400f3b	 Bytes: 3
  %loadMem_400f3b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f3b = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f3b)
  store %struct.Memory* %call_400f3b, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6082a0(,%rsi,4)	 RIP: 400f3e	 Bytes: 11
  %loadMem_400f3e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f3e = call %struct.Memory* @routine_movl__0x1__0x6082a0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f3e)
  store %struct.Memory* %call_400f3e, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400f49	 Bytes: 3
  %loadMem_400f49 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f49 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f49)
  store %struct.Memory* %call_400f49, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400f4c	 Bytes: 3
  %loadMem_400f4c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f4c = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f4c)
  store %struct.Memory* %call_400f4c, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400f4f	 Bytes: 3
  %loadMem_400f4f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f4f = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f4f)
  store %struct.Memory* %call_400f4f, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400f1e	 RIP: 400f52	 Bytes: 5
  %loadMem_400f52 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f52 = call %struct.Memory* @routine_jmpq_.L_400f1e(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f52, i64 -52, i64 5)
  store %struct.Memory* %call_400f52, %struct.Memory** %MEMORY

  br label %block_.L_400f1e

  ; Code: .L_400f57:	 RIP: 400f57	 Bytes: 0
block_.L_400f57:

  ; Code: jmpq .L_400f5c	 RIP: 400f57	 Bytes: 5
  %loadMem_400f57 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f57 = call %struct.Memory* @routine_jmpq_.L_400f5c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f57, i64 5, i64 5)
  store %struct.Memory* %call_400f57, %struct.Memory** %MEMORY

  br label %block_.L_400f5c

  ; Code: .L_400f5c:	 RIP: 400f5c	 Bytes: 0
block_.L_400f5c:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400f5c	 Bytes: 3
  %loadMem_400f5c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f5c = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f5c)
  store %struct.Memory* %call_400f5c, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400f5f	 Bytes: 3
  %loadMem_400f5f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f5f = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f5f)
  store %struct.Memory* %call_400f5f, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 400f62	 Bytes: 3
  %loadMem_400f62 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f62 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f62)
  store %struct.Memory* %call_400f62, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400f0d	 RIP: 400f65	 Bytes: 5
  %loadMem_400f65 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f65 = call %struct.Memory* @routine_jmpq_.L_400f0d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f65, i64 -88, i64 5)
  store %struct.Memory* %call_400f65, %struct.Memory** %MEMORY

  br label %block_.L_400f0d

  ; Code: .L_400f6a:	 RIP: 400f6a	 Bytes: 0
block_.L_400f6a:

  ; Code: jmpq .L_400f6f	 RIP: 400f6a	 Bytes: 5
  %loadMem_400f6a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f6a = call %struct.Memory* @routine_jmpq_.L_400f6f(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f6a, i64 5, i64 5)
  store %struct.Memory* %call_400f6a, %struct.Memory** %MEMORY

  br label %block_.L_400f6f

  ; Code: .L_400f6f:	 RIP: 400f6f	 Bytes: 0
block_.L_400f6f:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 400f6f	 Bytes: 3
  %loadMem_400f6f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f6f = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f6f)
  store %struct.Memory* %call_400f6f, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400f72	 Bytes: 3
  %loadMem_400f72 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f72 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f72)
  store %struct.Memory* %call_400f72, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 400f75	 Bytes: 3
  %loadMem_400f75 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f75 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f75)
  store %struct.Memory* %call_400f75, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400efc	 RIP: 400f78	 Bytes: 5
  %loadMem_400f78 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f78 = call %struct.Memory* @routine_jmpq_.L_400efc(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f78, i64 -124, i64 5)
  store %struct.Memory* %call_400f78, %struct.Memory** %MEMORY

  br label %block_.L_400efc

  ; Code: .L_400f7d:	 RIP: 400f7d	 Bytes: 0
block_.L_400f7d:

  ; Code: movl $0x2, 0x619214	 RIP: 400f7d	 Bytes: 11
  %loadMem_400f7d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f7d = call %struct.Memory* @routine_movl__0x2__0x619214(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f7d)
  store %struct.Memory* %call_400f7d, %struct.Memory** %MEMORY

  ; Code: movl $0x9, 0x610274	 RIP: 400f88	 Bytes: 11
  %loadMem_400f88 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f88 = call %struct.Memory* @routine_movl__0x9__0x610274(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f88)
  store %struct.Memory* %call_400f88, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 400f93	 Bytes: 7
  %loadMem_400f93 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f93 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f93)
  store %struct.Memory* %call_400f93, %struct.Memory** %MEMORY

  ; Code: .L_400f9a:	 RIP: 400f9a	 Bytes: 0
  br label %block_.L_400f9a
block_.L_400f9a:

  ; Code: cmpl $0x1, -0x4(%rbp)	 RIP: 400f9a	 Bytes: 4
  %loadMem_400f9a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f9a = call %struct.Memory* @routine_cmpl__0x1__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f9a)
  store %struct.Memory* %call_400f9a, %struct.Memory** %MEMORY

  ; Code: jg .L_40101b	 RIP: 400f9e	 Bytes: 6
  %loadMem_400f9e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400f9e = call %struct.Memory* @routine_jg_.L_40101b(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400f9e, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_400f9e, %struct.Memory** %MEMORY

  %loadBr_400f9e = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400f9e = icmp eq i8 %loadBr_400f9e, 1
  br i1 %cmpBr_400f9e, label %block_.L_40101b, label %block_400fa4

block_400fa4:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 400fa4	 Bytes: 7
  %loadMem_400fa4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fa4 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fa4)
  store %struct.Memory* %call_400fa4, %struct.Memory** %MEMORY

  ; Code: .L_400fab:	 RIP: 400fab	 Bytes: 0
  br label %block_.L_400fab
block_.L_400fab:

  ; Code: cmpl $0x0, -0x8(%rbp)	 RIP: 400fab	 Bytes: 4
  %loadMem_400fab = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fab = call %struct.Memory* @routine_cmpl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fab)
  store %struct.Memory* %call_400fab, %struct.Memory** %MEMORY

  ; Code: jg .L_401008	 RIP: 400faf	 Bytes: 6
  %loadMem_400faf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400faf = call %struct.Memory* @routine_jg_.L_401008(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400faf, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_400faf, %struct.Memory** %MEMORY

  %loadBr_400faf = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400faf = icmp eq i8 %loadBr_400faf, 1
  br i1 %cmpBr_400faf, label %block_.L_401008, label %block_400fb5

block_400fb5:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 400fb5	 Bytes: 7
  %loadMem_400fb5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fb5 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fb5)
  store %struct.Memory* %call_400fb5, %struct.Memory** %MEMORY

  ; Code: .L_400fbc:	 RIP: 400fbc	 Bytes: 0
  br label %block_.L_400fbc
block_.L_400fbc:

  ; Code: cmpl $0x1, -0xc(%rbp)	 RIP: 400fbc	 Bytes: 4
  %loadMem_400fbc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fbc = call %struct.Memory* @routine_cmpl__0x1__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fbc)
  store %struct.Memory* %call_400fbc, %struct.Memory** %MEMORY

  ; Code: jg .L_400ff5	 RIP: 400fc0	 Bytes: 6
  %loadMem_400fc0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fc0 = call %struct.Memory* @routine_jg_.L_400ff5(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fc0, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_400fc0, %struct.Memory** %MEMORY

  %loadBr_400fc0 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_400fc0 = icmp eq i8 %loadBr_400fc0, 1
  br i1 %cmpBr_400fc0, label %block_.L_400ff5, label %block_400fc6

block_400fc6:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 400fc6	 Bytes: 3
  %loadMem_400fc6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fc6 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fc6)
  store %struct.Memory* %call_400fc6, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 400fc9	 Bytes: 3
  %loadMem_400fc9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fc9 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fc9)
  store %struct.Memory* %call_400fc9, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 400fcc	 Bytes: 3
  %loadMem_400fcc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fcc = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fcc)
  store %struct.Memory* %call_400fcc, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 400fcf	 Bytes: 3
  %loadMem_400fcf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fcf = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fcf)
  store %struct.Memory* %call_400fcf, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 400fd2	 Bytes: 2
  %loadMem_400fd2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fd2 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fd2)
  store %struct.Memory* %call_400fd2, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 400fd4	 Bytes: 3
  %loadMem_400fd4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fd4 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fd4)
  store %struct.Memory* %call_400fd4, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 400fd7	 Bytes: 2
  %loadMem_400fd7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fd7 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fd7)
  store %struct.Memory* %call_400fd7, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 400fd9	 Bytes: 3
  %loadMem_400fd9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fd9 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fd9)
  store %struct.Memory* %call_400fd9, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x608aa0(,%rsi,4)	 RIP: 400fdc	 Bytes: 11
  %loadMem_400fdc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fdc = call %struct.Memory* @routine_movl__0x1__0x608aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fdc)
  store %struct.Memory* %call_400fdc, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 400fe7	 Bytes: 3
  %loadMem_400fe7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fe7 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fe7)
  store %struct.Memory* %call_400fe7, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400fea	 Bytes: 3
  %loadMem_400fea = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fea = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fea)
  store %struct.Memory* %call_400fea, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 400fed	 Bytes: 3
  %loadMem_400fed = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400fed = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400fed)
  store %struct.Memory* %call_400fed, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400fbc	 RIP: 400ff0	 Bytes: 5
  %loadMem_400ff0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ff0 = call %struct.Memory* @routine_jmpq_.L_400fbc(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ff0, i64 -52, i64 5)
  store %struct.Memory* %call_400ff0, %struct.Memory** %MEMORY

  br label %block_.L_400fbc

  ; Code: .L_400ff5:	 RIP: 400ff5	 Bytes: 0
block_.L_400ff5:

  ; Code: jmpq .L_400ffa	 RIP: 400ff5	 Bytes: 5
  %loadMem_400ff5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ff5 = call %struct.Memory* @routine_jmpq_.L_400ffa(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ff5, i64 5, i64 5)
  store %struct.Memory* %call_400ff5, %struct.Memory** %MEMORY

  br label %block_.L_400ffa

  ; Code: .L_400ffa:	 RIP: 400ffa	 Bytes: 0
block_.L_400ffa:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 400ffa	 Bytes: 3
  %loadMem_400ffa = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ffa = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ffa)
  store %struct.Memory* %call_400ffa, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 400ffd	 Bytes: 3
  %loadMem_400ffd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_400ffd = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_400ffd)
  store %struct.Memory* %call_400ffd, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 401000	 Bytes: 3
  %loadMem_401000 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401000 = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401000)
  store %struct.Memory* %call_401000, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400fab	 RIP: 401003	 Bytes: 5
  %loadMem_401003 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401003 = call %struct.Memory* @routine_jmpq_.L_400fab(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401003, i64 -88, i64 5)
  store %struct.Memory* %call_401003, %struct.Memory** %MEMORY

  br label %block_.L_400fab

  ; Code: .L_401008:	 RIP: 401008	 Bytes: 0
block_.L_401008:

  ; Code: jmpq .L_40100d	 RIP: 401008	 Bytes: 5
  %loadMem_401008 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401008 = call %struct.Memory* @routine_jmpq_.L_40100d(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401008, i64 5, i64 5)
  store %struct.Memory* %call_401008, %struct.Memory** %MEMORY

  br label %block_.L_40100d

  ; Code: .L_40100d:	 RIP: 40100d	 Bytes: 0
block_.L_40100d:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 40100d	 Bytes: 3
  %loadMem_40100d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40100d = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40100d)
  store %struct.Memory* %call_40100d, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 401010	 Bytes: 3
  %loadMem_401010 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401010 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401010)
  store %struct.Memory* %call_401010, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 401013	 Bytes: 3
  %loadMem_401013 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401013 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401013)
  store %struct.Memory* %call_401013, %struct.Memory** %MEMORY

  ; Code: jmpq .L_400f9a	 RIP: 401016	 Bytes: 5
  %loadMem_401016 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401016 = call %struct.Memory* @routine_jmpq_.L_400f9a(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401016, i64 -124, i64 5)
  store %struct.Memory* %call_401016, %struct.Memory** %MEMORY

  br label %block_.L_400f9a

  ; Code: .L_40101b:	 RIP: 40101b	 Bytes: 0
block_.L_40101b:

  ; Code: movl $0x2, 0x619218	 RIP: 40101b	 Bytes: 11
  %loadMem_40101b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40101b = call %struct.Memory* @routine_movl__0x2__0x619218(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40101b)
  store %struct.Memory* %call_40101b, %struct.Memory** %MEMORY

  ; Code: movl $0x41, 0x610278	 RIP: 401026	 Bytes: 11
  %loadMem_401026 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401026 = call %struct.Memory* @routine_movl__0x41__0x610278(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401026)
  store %struct.Memory* %call_401026, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 401031	 Bytes: 7
  %loadMem_401031 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401031 = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401031)
  store %struct.Memory* %call_401031, %struct.Memory** %MEMORY

  ; Code: .L_401038:	 RIP: 401038	 Bytes: 0
  br label %block_.L_401038
block_.L_401038:

  ; Code: cmpl $0x0, -0x4(%rbp)	 RIP: 401038	 Bytes: 4
  %loadMem_401038 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401038 = call %struct.Memory* @routine_cmpl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401038)
  store %struct.Memory* %call_401038, %struct.Memory** %MEMORY

  ; Code: jg .L_4010b9	 RIP: 40103c	 Bytes: 6
  %loadMem_40103c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40103c = call %struct.Memory* @routine_jg_.L_4010b9(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40103c, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_40103c, %struct.Memory** %MEMORY

  %loadBr_40103c = load i8, i8* %BRANCH_TAKEN
  %cmpBr_40103c = icmp eq i8 %loadBr_40103c, 1
  br i1 %cmpBr_40103c, label %block_.L_4010b9, label %block_401042

block_401042:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 401042	 Bytes: 7
  %loadMem_401042 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401042 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401042)
  store %struct.Memory* %call_401042, %struct.Memory** %MEMORY

  ; Code: .L_401049:	 RIP: 401049	 Bytes: 0
  br label %block_.L_401049
block_.L_401049:

  ; Code: cmpl $0x1, -0x8(%rbp)	 RIP: 401049	 Bytes: 4
  %loadMem_401049 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401049 = call %struct.Memory* @routine_cmpl__0x1__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401049)
  store %struct.Memory* %call_401049, %struct.Memory** %MEMORY

  ; Code: jg .L_4010a6	 RIP: 40104d	 Bytes: 6
  %loadMem_40104d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40104d = call %struct.Memory* @routine_jg_.L_4010a6(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40104d, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_40104d, %struct.Memory** %MEMORY

  %loadBr_40104d = load i8, i8* %BRANCH_TAKEN
  %cmpBr_40104d = icmp eq i8 %loadBr_40104d, 1
  br i1 %cmpBr_40104d, label %block_.L_4010a6, label %block_401053

block_401053:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 401053	 Bytes: 7
  %loadMem_401053 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401053 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401053)
  store %struct.Memory* %call_401053, %struct.Memory** %MEMORY

  ; Code: .L_40105a:	 RIP: 40105a	 Bytes: 0
  br label %block_.L_40105a
block_.L_40105a:

  ; Code: cmpl $0x1, -0xc(%rbp)	 RIP: 40105a	 Bytes: 4
  %loadMem_40105a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40105a = call %struct.Memory* @routine_cmpl__0x1__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40105a)
  store %struct.Memory* %call_40105a, %struct.Memory** %MEMORY

  ; Code: jg .L_401093	 RIP: 40105e	 Bytes: 6
  %loadMem_40105e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40105e = call %struct.Memory* @routine_jg_.L_401093(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40105e, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_40105e, %struct.Memory** %MEMORY

  %loadBr_40105e = load i8, i8* %BRANCH_TAKEN
  %cmpBr_40105e = icmp eq i8 %loadBr_40105e, 1
  br i1 %cmpBr_40105e, label %block_.L_401093, label %block_401064

block_401064:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 401064	 Bytes: 3
  %loadMem_401064 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401064 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401064)
  store %struct.Memory* %call_401064, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 401067	 Bytes: 3
  %loadMem_401067 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401067 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401067)
  store %struct.Memory* %call_401067, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 40106a	 Bytes: 3
  %loadMem_40106a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40106a = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40106a)
  store %struct.Memory* %call_40106a, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 40106d	 Bytes: 3
  %loadMem_40106d = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40106d = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40106d)
  store %struct.Memory* %call_40106d, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 401070	 Bytes: 2
  %loadMem_401070 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401070 = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401070)
  store %struct.Memory* %call_401070, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 401072	 Bytes: 3
  %loadMem_401072 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401072 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401072)
  store %struct.Memory* %call_401072, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 401075	 Bytes: 2
  %loadMem_401075 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401075 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401075)
  store %struct.Memory* %call_401075, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 401077	 Bytes: 3
  %loadMem_401077 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401077 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401077)
  store %struct.Memory* %call_401077, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6092a0(,%rsi,4)	 RIP: 40107a	 Bytes: 11
  %loadMem_40107a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40107a = call %struct.Memory* @routine_movl__0x1__0x6092a0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40107a)
  store %struct.Memory* %call_40107a, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 401085	 Bytes: 3
  %loadMem_401085 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401085 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401085)
  store %struct.Memory* %call_401085, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 401088	 Bytes: 3
  %loadMem_401088 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401088 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401088)
  store %struct.Memory* %call_401088, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 40108b	 Bytes: 3
  %loadMem_40108b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40108b = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40108b)
  store %struct.Memory* %call_40108b, %struct.Memory** %MEMORY

  ; Code: jmpq .L_40105a	 RIP: 40108e	 Bytes: 5
  %loadMem_40108e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40108e = call %struct.Memory* @routine_jmpq_.L_40105a(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40108e, i64 -52, i64 5)
  store %struct.Memory* %call_40108e, %struct.Memory** %MEMORY

  br label %block_.L_40105a

  ; Code: .L_401093:	 RIP: 401093	 Bytes: 0
block_.L_401093:

  ; Code: jmpq .L_401098	 RIP: 401093	 Bytes: 5
  %loadMem_401093 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401093 = call %struct.Memory* @routine_jmpq_.L_401098(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401093, i64 5, i64 5)
  store %struct.Memory* %call_401093, %struct.Memory** %MEMORY

  br label %block_.L_401098

  ; Code: .L_401098:	 RIP: 401098	 Bytes: 0
block_.L_401098:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 401098	 Bytes: 3
  %loadMem_401098 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401098 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401098)
  store %struct.Memory* %call_401098, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 40109b	 Bytes: 3
  %loadMem_40109b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40109b = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40109b)
  store %struct.Memory* %call_40109b, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 40109e	 Bytes: 3
  %loadMem_40109e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40109e = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40109e)
  store %struct.Memory* %call_40109e, %struct.Memory** %MEMORY

  ; Code: jmpq .L_401049	 RIP: 4010a1	 Bytes: 5
  %loadMem_4010a1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010a1 = call %struct.Memory* @routine_jmpq_.L_401049(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010a1, i64 -88, i64 5)
  store %struct.Memory* %call_4010a1, %struct.Memory** %MEMORY

  br label %block_.L_401049

  ; Code: .L_4010a6:	 RIP: 4010a6	 Bytes: 0
block_.L_4010a6:

  ; Code: jmpq .L_4010ab	 RIP: 4010a6	 Bytes: 5
  %loadMem_4010a6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010a6 = call %struct.Memory* @routine_jmpq_.L_4010ab(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010a6, i64 5, i64 5)
  store %struct.Memory* %call_4010a6, %struct.Memory** %MEMORY

  br label %block_.L_4010ab

  ; Code: .L_4010ab:	 RIP: 4010ab	 Bytes: 0
block_.L_4010ab:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 4010ab	 Bytes: 3
  %loadMem_4010ab = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010ab = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010ab)
  store %struct.Memory* %call_4010ab, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 4010ae	 Bytes: 3
  %loadMem_4010ae = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010ae = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010ae)
  store %struct.Memory* %call_4010ae, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 4010b1	 Bytes: 3
  %loadMem_4010b1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010b1 = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010b1)
  store %struct.Memory* %call_4010b1, %struct.Memory** %MEMORY

  ; Code: jmpq .L_401038	 RIP: 4010b4	 Bytes: 5
  %loadMem_4010b4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010b4 = call %struct.Memory* @routine_jmpq_.L_401038(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010b4, i64 -124, i64 5)
  store %struct.Memory* %call_4010b4, %struct.Memory** %MEMORY

  br label %block_.L_401038

  ; Code: .L_4010b9:	 RIP: 4010b9	 Bytes: 0
block_.L_4010b9:

  ; Code: movl $0x2, 0x61921c	 RIP: 4010b9	 Bytes: 11
  %loadMem_4010b9 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010b9 = call %struct.Memory* @routine_movl__0x2__0x61921c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010b9)
  store %struct.Memory* %call_4010b9, %struct.Memory** %MEMORY

  ; Code: movl $0x48, 0x61027c	 RIP: 4010c4	 Bytes: 11
  %loadMem_4010c4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010c4 = call %struct.Memory* @routine_movl__0x48__0x61027c(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010c4)
  store %struct.Memory* %call_4010c4, %struct.Memory** %MEMORY

  ; Code: movl $0x0, -0x4(%rbp)	 RIP: 4010cf	 Bytes: 7
  %loadMem_4010cf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010cf = call %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010cf)
  store %struct.Memory* %call_4010cf, %struct.Memory** %MEMORY

  ; Code: .L_4010d6:	 RIP: 4010d6	 Bytes: 0
  br label %block_.L_4010d6
block_.L_4010d6:

  ; Code: cmpl $0x1, -0x4(%rbp)	 RIP: 4010d6	 Bytes: 4
  %loadMem_4010d6 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010d6 = call %struct.Memory* @routine_cmpl__0x1__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010d6)
  store %struct.Memory* %call_4010d6, %struct.Memory** %MEMORY

  ; Code: jg .L_401157	 RIP: 4010da	 Bytes: 6
  %loadMem_4010da = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010da = call %struct.Memory* @routine_jg_.L_401157(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010da, i8* %BRANCH_TAKEN, i64 125, i64 6, i64 6)
  store %struct.Memory* %call_4010da, %struct.Memory** %MEMORY

  %loadBr_4010da = load i8, i8* %BRANCH_TAKEN
  %cmpBr_4010da = icmp eq i8 %loadBr_4010da, 1
  br i1 %cmpBr_4010da, label %block_.L_401157, label %block_4010e0

block_4010e0:
  ; Code: movl $0x0, -0x8(%rbp)	 RIP: 4010e0	 Bytes: 7
  %loadMem_4010e0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010e0 = call %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010e0)
  store %struct.Memory* %call_4010e0, %struct.Memory** %MEMORY

  ; Code: .L_4010e7:	 RIP: 4010e7	 Bytes: 0
  br label %block_.L_4010e7
block_.L_4010e7:

  ; Code: cmpl $0x1, -0x8(%rbp)	 RIP: 4010e7	 Bytes: 4
  %loadMem_4010e7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010e7 = call %struct.Memory* @routine_cmpl__0x1__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010e7)
  store %struct.Memory* %call_4010e7, %struct.Memory** %MEMORY

  ; Code: jg .L_401144	 RIP: 4010eb	 Bytes: 6
  %loadMem_4010eb = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010eb = call %struct.Memory* @routine_jg_.L_401144(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010eb, i8* %BRANCH_TAKEN, i64 89, i64 6, i64 6)
  store %struct.Memory* %call_4010eb, %struct.Memory** %MEMORY

  %loadBr_4010eb = load i8, i8* %BRANCH_TAKEN
  %cmpBr_4010eb = icmp eq i8 %loadBr_4010eb, 1
  br i1 %cmpBr_4010eb, label %block_.L_401144, label %block_4010f1

block_4010f1:
  ; Code: movl $0x0, -0xc(%rbp)	 RIP: 4010f1	 Bytes: 7
  %loadMem_4010f1 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010f1 = call %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010f1)
  store %struct.Memory* %call_4010f1, %struct.Memory** %MEMORY

  ; Code: .L_4010f8:	 RIP: 4010f8	 Bytes: 0
  br label %block_.L_4010f8
block_.L_4010f8:

  ; Code: cmpl $0x1, -0xc(%rbp)	 RIP: 4010f8	 Bytes: 4
  %loadMem_4010f8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010f8 = call %struct.Memory* @routine_cmpl__0x1__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010f8)
  store %struct.Memory* %call_4010f8, %struct.Memory** %MEMORY

  ; Code: jg .L_401131	 RIP: 4010fc	 Bytes: 6
  %loadMem_4010fc = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4010fc = call %struct.Memory* @routine_jg_.L_401131(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4010fc, i8* %BRANCH_TAKEN, i64 53, i64 6, i64 6)
  store %struct.Memory* %call_4010fc, %struct.Memory** %MEMORY

  %loadBr_4010fc = load i8, i8* %BRANCH_TAKEN
  %cmpBr_4010fc = icmp eq i8 %loadBr_4010fc, 1
  br i1 %cmpBr_4010fc, label %block_.L_401131, label %block_401102

block_401102:
  ; Code: movl -0x4(%rbp), %eax	 RIP: 401102	 Bytes: 3
  %loadMem_401102 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401102 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401102)
  store %struct.Memory* %call_401102, %struct.Memory** %MEMORY

  ; Code: movl -0x8(%rbp), %ecx	 RIP: 401105	 Bytes: 3
  %loadMem_401105 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401105 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401105)
  store %struct.Memory* %call_401105, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %edx	 RIP: 401108	 Bytes: 3
  %loadMem_401108 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401108 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401108)
  store %struct.Memory* %call_401108, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %edx	 RIP: 40110b	 Bytes: 3
  %loadMem_40110b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40110b = call %struct.Memory* @routine_shll__0x3___edx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40110b)
  store %struct.Memory* %call_40110b, %struct.Memory** %MEMORY

  ; Code: addl %edx, %ecx	 RIP: 40110e	 Bytes: 2
  %loadMem_40110e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40110e = call %struct.Memory* @routine_addl__edx___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40110e)
  store %struct.Memory* %call_40110e, %struct.Memory** %MEMORY

  ; Code: shll $0x3, %ecx	 RIP: 401110	 Bytes: 3
  %loadMem_401110 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401110 = call %struct.Memory* @routine_shll__0x3___ecx(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401110)
  store %struct.Memory* %call_401110, %struct.Memory** %MEMORY

  ; Code: addl %ecx, %eax	 RIP: 401113	 Bytes: 2
  %loadMem_401113 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401113 = call %struct.Memory* @routine_addl__ecx___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401113)
  store %struct.Memory* %call_401113, %struct.Memory** %MEMORY

  ; Code: movslq %eax, %rsi	 RIP: 401115	 Bytes: 3
  %loadMem_401115 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401115 = call %struct.Memory* @routine_movslq__eax___rsi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401115)
  store %struct.Memory* %call_401115, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x609aa0(,%rsi,4)	 RIP: 401118	 Bytes: 11
  %loadMem_401118 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401118 = call %struct.Memory* @routine_movl__0x1__0x609aa0___rsi_4_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401118)
  store %struct.Memory* %call_401118, %struct.Memory** %MEMORY

  ; Code: movl -0xc(%rbp), %eax	 RIP: 401123	 Bytes: 3
  %loadMem_401123 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401123 = call %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401123)
  store %struct.Memory* %call_401123, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 401126	 Bytes: 3
  %loadMem_401126 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401126 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401126)
  store %struct.Memory* %call_401126, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0xc(%rbp)	 RIP: 401129	 Bytes: 3
  %loadMem_401129 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401129 = call %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401129)
  store %struct.Memory* %call_401129, %struct.Memory** %MEMORY

  ; Code: jmpq .L_4010f8	 RIP: 40112c	 Bytes: 5
  %loadMem_40112c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40112c = call %struct.Memory* @routine_jmpq_.L_4010f8(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40112c, i64 -52, i64 5)
  store %struct.Memory* %call_40112c, %struct.Memory** %MEMORY

  br label %block_.L_4010f8

  ; Code: .L_401131:	 RIP: 401131	 Bytes: 0
block_.L_401131:

  ; Code: jmpq .L_401136	 RIP: 401131	 Bytes: 5
  %loadMem_401131 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401131 = call %struct.Memory* @routine_jmpq_.L_401136(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401131, i64 5, i64 5)
  store %struct.Memory* %call_401131, %struct.Memory** %MEMORY

  br label %block_.L_401136

  ; Code: .L_401136:	 RIP: 401136	 Bytes: 0
block_.L_401136:

  ; Code: movl -0x8(%rbp), %eax	 RIP: 401136	 Bytes: 3
  %loadMem_401136 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401136 = call %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401136)
  store %struct.Memory* %call_401136, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 401139	 Bytes: 3
  %loadMem_401139 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401139 = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401139)
  store %struct.Memory* %call_401139, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x8(%rbp)	 RIP: 40113c	 Bytes: 3
  %loadMem_40113c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40113c = call %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40113c)
  store %struct.Memory* %call_40113c, %struct.Memory** %MEMORY

  ; Code: jmpq .L_4010e7	 RIP: 40113f	 Bytes: 5
  %loadMem_40113f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40113f = call %struct.Memory* @routine_jmpq_.L_4010e7(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40113f, i64 -88, i64 5)
  store %struct.Memory* %call_40113f, %struct.Memory** %MEMORY

  br label %block_.L_4010e7

  ; Code: .L_401144:	 RIP: 401144	 Bytes: 0
block_.L_401144:

  ; Code: jmpq .L_401149	 RIP: 401144	 Bytes: 5
  %loadMem_401144 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401144 = call %struct.Memory* @routine_jmpq_.L_401149(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401144, i64 5, i64 5)
  store %struct.Memory* %call_401144, %struct.Memory** %MEMORY

  br label %block_.L_401149

  ; Code: .L_401149:	 RIP: 401149	 Bytes: 0
block_.L_401149:

  ; Code: movl -0x4(%rbp), %eax	 RIP: 401149	 Bytes: 3
  %loadMem_401149 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401149 = call %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401149)
  store %struct.Memory* %call_401149, %struct.Memory** %MEMORY

  ; Code: addl $0x1, %eax	 RIP: 40114c	 Bytes: 3
  %loadMem_40114c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40114c = call %struct.Memory* @routine_addl__0x1___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40114c)
  store %struct.Memory* %call_40114c, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x4(%rbp)	 RIP: 40114f	 Bytes: 3
  %loadMem_40114f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40114f = call %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40114f)
  store %struct.Memory* %call_40114f, %struct.Memory** %MEMORY

  ; Code: jmpq .L_4010d6	 RIP: 401152	 Bytes: 5
  %loadMem_401152 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401152 = call %struct.Memory* @routine_jmpq_.L_4010d6(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401152, i64 -124, i64 5)
  store %struct.Memory* %call_401152, %struct.Memory** %MEMORY

  br label %block_.L_4010d6

  ; Code: .L_401157:	 RIP: 401157	 Bytes: 0
block_.L_401157:

  ; Code: xorl %edi, %edi	 RIP: 401157	 Bytes: 2
  %loadMem_401157 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401157 = call %struct.Memory* @routine_xorl__edi___edi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401157)
  store %struct.Memory* %call_401157, %struct.Memory** %MEMORY

  ; Code: movl $0x3, 0x619220	 RIP: 401159	 Bytes: 11
  %loadMem_401159 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401159 = call %struct.Memory* @routine_movl__0x3__0x619220(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401159)
  store %struct.Memory* %call_401159, %struct.Memory** %MEMORY

  ; Code: movl $0x49, 0x610280	 RIP: 401164	 Bytes: 11
  %loadMem_401164 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401164 = call %struct.Memory* @routine_movl__0x49__0x610280(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401164)
  store %struct.Memory* %call_401164, %struct.Memory** %MEMORY

  ; Code: movl $0xd, 0x6191e0	 RIP: 40116f	 Bytes: 11
  %loadMem_40116f = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40116f = call %struct.Memory* @routine_movl__0xd__0x6191e0(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40116f)
  store %struct.Memory* %call_40116f, %struct.Memory** %MEMORY

  ; Code: movl $0x3, 0x6191e4	 RIP: 40117a	 Bytes: 11
  %loadMem_40117a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40117a = call %struct.Memory* @routine_movl__0x3__0x6191e4(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40117a)
  store %struct.Memory* %call_40117a, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6191e8	 RIP: 401185	 Bytes: 11
  %loadMem_401185 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401185 = call %struct.Memory* @routine_movl__0x1__0x6191e8(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401185)
  store %struct.Memory* %call_401185, %struct.Memory** %MEMORY

  ; Code: movl $0x1, 0x6191ec	 RIP: 401190	 Bytes: 11
  %loadMem_401190 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401190 = call %struct.Memory* @routine_movl__0x1__0x6191ec(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401190)
  store %struct.Memory* %call_401190, %struct.Memory** %MEMORY

  ; Code: movl $0x49, -0x10(%rbp)	 RIP: 40119b	 Bytes: 7
  %loadMem_40119b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40119b = call %struct.Memory* @routine_movl__0x49__MINUS0x10__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40119b)
  store %struct.Memory* %call_40119b, %struct.Memory** %MEMORY

  ; Code: movl $0x0, 0x6191c4	 RIP: 4011a2	 Bytes: 11
  %loadMem_4011a2 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011a2 = call %struct.Memory* @routine_movl__0x0__0x6191c4(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011a2)
  store %struct.Memory* %call_4011a2, %struct.Memory** %MEMORY

  ; Code: movl -0x10(%rbp), %esi	 RIP: 4011ad	 Bytes: 3
  %loadMem_4011ad = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011ad = call %struct.Memory* @routine_movl_MINUS0x10__rbp____esi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011ad)
  store %struct.Memory* %call_4011ad, %struct.Memory** %MEMORY

  ; Code: callq .Fit	 RIP: 4011b0	 Bytes: 5
  %loadMem1_4011b0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_4011b0 = call %struct.Memory* @routine_callq_.Fit(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_4011b0, i64 -3168, i64 5, i64 5)
  store %struct.Memory* %call1_4011b0, %struct.Memory** %MEMORY

  %loadMem2_4011b0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_4011b0 = load i64, i64* %3
  %call2_4011b0 = call %struct.Memory* @sub_400550.Fit(%struct.State* %0, i64  %loadPC_4011b0, %struct.Memory* %loadMem2_4011b0)
  store %struct.Memory* %call2_4011b0, %struct.Memory** %MEMORY

  ; Code: cmpl $0x0, %eax	 RIP: 4011b5	 Bytes: 3
  %loadMem_4011b5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011b5 = call %struct.Memory* @routine_cmpl__0x0___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011b5)
  store %struct.Memory* %call_4011b5, %struct.Memory** %MEMORY

  ; Code: je .L_4011d4	 RIP: 4011b8	 Bytes: 6
  %loadMem_4011b8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011b8 = call %struct.Memory* @routine_je_.L_4011d4(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011b8, i8* %BRANCH_TAKEN, i64 28, i64 6, i64 6)
  store %struct.Memory* %call_4011b8, %struct.Memory** %MEMORY

  %loadBr_4011b8 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_4011b8 = icmp eq i8 %loadBr_4011b8, 1
  br i1 %cmpBr_4011b8, label %block_.L_4011d4, label %block_4011be

block_4011be:
  ; Code: xorl %edi, %edi	 RIP: 4011be	 Bytes: 2
  %loadMem_4011be = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011be = call %struct.Memory* @routine_xorl__edi___edi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011be)
  store %struct.Memory* %call_4011be, %struct.Memory** %MEMORY

  ; Code: movl -0x10(%rbp), %esi	 RIP: 4011c0	 Bytes: 3
  %loadMem_4011c0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011c0 = call %struct.Memory* @routine_movl_MINUS0x10__rbp____esi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011c0)
  store %struct.Memory* %call_4011c0, %struct.Memory** %MEMORY

  ; Code: callq .Place	 RIP: 4011c3	 Bytes: 5
  %loadMem1_4011c3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_4011c3 = call %struct.Memory* @routine_callq_.Place(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_4011c3, i64 -3043, i64 5, i64 5)
  store %struct.Memory* %call1_4011c3, %struct.Memory** %MEMORY

  %loadMem2_4011c3 = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_4011c3 = load i64, i64* %3
  %call2_4011c3 = call %struct.Memory* @sub_4005e0.Place(%struct.State* %0, i64  %loadPC_4011c3, %struct.Memory* %loadMem2_4011c3)
  store %struct.Memory* %call2_4011c3, %struct.Memory** %MEMORY

  ; Code: movl %eax, 0x603a94	 RIP: 4011c8	 Bytes: 7
  %loadMem_4011c8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011c8 = call %struct.Memory* @routine_movl__eax__0x603a94(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011c8)
  store %struct.Memory* %call_4011c8, %struct.Memory** %MEMORY

  ; Code: jmpq .L_4011e8	 RIP: 4011cf	 Bytes: 5
  %loadMem_4011cf = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011cf = call %struct.Memory* @routine_jmpq_.L_4011e8(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011cf, i64 25, i64 5)
  store %struct.Memory* %call_4011cf, %struct.Memory** %MEMORY

  br label %block_.L_4011e8

  ; Code: .L_4011d4:	 RIP: 4011d4	 Bytes: 0
block_.L_4011d4:

  ; Code: movq $0x401344, %rdi	 RIP: 4011d4	 Bytes: 10
  %loadMem_4011d4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011d4 = call %struct.Memory* @routine_movq__0x401344___rdi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011d4)
  store %struct.Memory* %call_4011d4, %struct.Memory** %MEMORY

  ; Code: movb $0x0, %al	 RIP: 4011de	 Bytes: 2
  %loadMem_4011de = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011de = call %struct.Memory* @routine_movb__0x0___al(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011de)
  store %struct.Memory* %call_4011de, %struct.Memory** %MEMORY

  ; Code: callq .printf_plt	 RIP: 4011e0	 Bytes: 5
  %loadMem1_4011e0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_4011e0 = call %struct.Memory* @routine_callq_.printf_plt(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_4011e0, i64 -3568, i64 5, i64 5)
  store %struct.Memory* %call1_4011e0, %struct.Memory** %MEMORY

  %loadMem2_4011e0 = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_4011e0 = load i64, i64* %3
  %call2_4011e0 = call %struct.Memory* @ext_printf(%struct.State* %0, i64  %loadPC_4011e0, %struct.Memory* %loadMem2_4011e0)
  store %struct.Memory* %call2_4011e0, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x14(%rbp)	 RIP: 4011e5	 Bytes: 3
  %loadMem_4011e5 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011e5 = call %struct.Memory* @routine_movl__eax__MINUS0x14__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011e5)
  store %struct.Memory* %call_4011e5, %struct.Memory** %MEMORY

  ; Code: .L_4011e8:	 RIP: 4011e8	 Bytes: 0
  br label %block_.L_4011e8
block_.L_4011e8:

  ; Code: movl 0x603a94, %edi	 RIP: 4011e8	 Bytes: 7
  %loadMem_4011e8 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011e8 = call %struct.Memory* @routine_movl_0x603a94___edi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011e8)
  store %struct.Memory* %call_4011e8, %struct.Memory** %MEMORY

  ; Code: callq .Trial	 RIP: 4011ef	 Bytes: 5
  %loadMem1_4011ef = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_4011ef = call %struct.Memory* @routine_callq_.Trial(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_4011ef, i64 -2687, i64 5, i64 5)
  store %struct.Memory* %call1_4011ef, %struct.Memory** %MEMORY

  %loadMem2_4011ef = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_4011ef = load i64, i64* %3
  %call2_4011ef = call %struct.Memory* @sub_400770.Trial(%struct.State* %0, i64  %loadPC_4011ef, %struct.Memory* %loadMem2_4011ef)
  store %struct.Memory* %call2_4011ef, %struct.Memory** %MEMORY

  ; Code: cmpl $0x0, %eax	 RIP: 4011f4	 Bytes: 3
  %loadMem_4011f4 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011f4 = call %struct.Memory* @routine_cmpl__0x0___eax(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011f4)
  store %struct.Memory* %call_4011f4, %struct.Memory** %MEMORY

  ; Code: jne .L_401216	 RIP: 4011f7	 Bytes: 6
  %loadMem_4011f7 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011f7 = call %struct.Memory* @routine_jne_.L_401216(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011f7, i8* %BRANCH_TAKEN, i64 31, i64 6, i64 6)
  store %struct.Memory* %call_4011f7, %struct.Memory** %MEMORY

  %loadBr_4011f7 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_4011f7 = icmp eq i8 %loadBr_4011f7, 1
  br i1 %cmpBr_4011f7, label %block_.L_401216, label %block_4011fd

block_4011fd:
  ; Code: movq $0x401356, %rdi	 RIP: 4011fd	 Bytes: 10
  %loadMem_4011fd = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_4011fd = call %struct.Memory* @routine_movq__0x401356___rdi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_4011fd)
  store %struct.Memory* %call_4011fd, %struct.Memory** %MEMORY

  ; Code: movb $0x0, %al	 RIP: 401207	 Bytes: 2
  %loadMem_401207 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401207 = call %struct.Memory* @routine_movb__0x0___al(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401207)
  store %struct.Memory* %call_401207, %struct.Memory** %MEMORY

  ; Code: callq .printf_plt	 RIP: 401209	 Bytes: 5
  %loadMem1_401209 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_401209 = call %struct.Memory* @routine_callq_.printf_plt(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_401209, i64 -3609, i64 5, i64 5)
  store %struct.Memory* %call1_401209, %struct.Memory** %MEMORY

  %loadMem2_401209 = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_401209 = load i64, i64* %3
  %call2_401209 = call %struct.Memory* @ext_printf(%struct.State* %0, i64  %loadPC_401209, %struct.Memory* %loadMem2_401209)
  store %struct.Memory* %call2_401209, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x18(%rbp)	 RIP: 40120e	 Bytes: 3
  %loadMem_40120e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40120e = call %struct.Memory* @routine_movl__eax__MINUS0x18__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40120e)
  store %struct.Memory* %call_40120e, %struct.Memory** %MEMORY

  ; Code: jmpq .L_401240	 RIP: 401211	 Bytes: 5
  %loadMem_401211 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401211 = call %struct.Memory* @routine_jmpq_.L_401240(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401211, i64 47, i64 5)
  store %struct.Memory* %call_401211, %struct.Memory** %MEMORY

  br label %block_.L_401240

  ; Code: .L_401216:	 RIP: 401216	 Bytes: 0
block_.L_401216:

  ; Code: cmpl $0x7d5, 0x6191c4	 RIP: 401216	 Bytes: 11
  %loadMem_401216 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401216 = call %struct.Memory* @routine_cmpl__0x7d5__0x6191c4(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401216)
  store %struct.Memory* %call_401216, %struct.Memory** %MEMORY

  ; Code: je .L_40123b	 RIP: 401221	 Bytes: 6
  %loadMem_401221 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401221 = call %struct.Memory* @routine_je_.L_40123b(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401221, i8* %BRANCH_TAKEN, i64 26, i64 6, i64 6)
  store %struct.Memory* %call_401221, %struct.Memory** %MEMORY

  %loadBr_401221 = load i8, i8* %BRANCH_TAKEN
  %cmpBr_401221 = icmp eq i8 %loadBr_401221, 1
  br i1 %cmpBr_401221, label %block_.L_40123b, label %block_401227

block_401227:
  ; Code: movq $0x401369, %rdi	 RIP: 401227	 Bytes: 10
  %loadMem_401227 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401227 = call %struct.Memory* @routine_movq__0x401369___rdi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401227)
  store %struct.Memory* %call_401227, %struct.Memory** %MEMORY

  ; Code: movb $0x0, %al	 RIP: 401231	 Bytes: 2
  %loadMem_401231 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401231 = call %struct.Memory* @routine_movb__0x0___al(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401231)
  store %struct.Memory* %call_401231, %struct.Memory** %MEMORY

  ; Code: callq .printf_plt	 RIP: 401233	 Bytes: 5
  %loadMem1_401233 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_401233 = call %struct.Memory* @routine_callq_.printf_plt(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_401233, i64 -3651, i64 5, i64 5)
  store %struct.Memory* %call1_401233, %struct.Memory** %MEMORY

  %loadMem2_401233 = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_401233 = load i64, i64* %3
  %call2_401233 = call %struct.Memory* @ext_printf(%struct.State* %0, i64  %loadPC_401233, %struct.Memory* %loadMem2_401233)
  store %struct.Memory* %call2_401233, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x1c(%rbp)	 RIP: 401238	 Bytes: 3
  %loadMem_401238 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401238 = call %struct.Memory* @routine_movl__eax__MINUS0x1c__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401238)
  store %struct.Memory* %call_401238, %struct.Memory** %MEMORY

  ; Code: .L_40123b:	 RIP: 40123b	 Bytes: 0
  br label %block_.L_40123b
block_.L_40123b:

  ; Code: jmpq .L_401240	 RIP: 40123b	 Bytes: 5
  %loadMem_40123b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40123b = call %struct.Memory* @routine_jmpq_.L_401240(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40123b, i64 5, i64 5)
  store %struct.Memory* %call_40123b, %struct.Memory** %MEMORY

  br label %block_.L_401240

  ; Code: .L_401240:	 RIP: 401240	 Bytes: 0
block_.L_401240:

  ; Code: movq $0x40137c, %rdi	 RIP: 401240	 Bytes: 10
  %loadMem_401240 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401240 = call %struct.Memory* @routine_movq__0x40137c___rdi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401240)
  store %struct.Memory* %call_401240, %struct.Memory** %MEMORY

  ; Code: movl 0x603a94, %esi	 RIP: 40124a	 Bytes: 7
  %loadMem_40124a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40124a = call %struct.Memory* @routine_movl_0x603a94___esi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40124a)
  store %struct.Memory* %call_40124a, %struct.Memory** %MEMORY

  ; Code: movb $0x0, %al	 RIP: 401251	 Bytes: 2
  %loadMem_401251 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401251 = call %struct.Memory* @routine_movb__0x0___al(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401251)
  store %struct.Memory* %call_401251, %struct.Memory** %MEMORY

  ; Code: callq .printf_plt	 RIP: 401253	 Bytes: 5
  %loadMem1_401253 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_401253 = call %struct.Memory* @routine_callq_.printf_plt(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_401253, i64 -3683, i64 5, i64 5)
  store %struct.Memory* %call1_401253, %struct.Memory** %MEMORY

  %loadMem2_401253 = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_401253 = load i64, i64* %3
  %call2_401253 = call %struct.Memory* @ext_printf(%struct.State* %0, i64  %loadPC_401253, %struct.Memory* %loadMem2_401253)
  store %struct.Memory* %call2_401253, %struct.Memory** %MEMORY

  ; Code: movq $0x40137c, %rdi	 RIP: 401258	 Bytes: 10
  %loadMem_401258 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401258 = call %struct.Memory* @routine_movq__0x40137c___rdi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401258)
  store %struct.Memory* %call_401258, %struct.Memory** %MEMORY

  ; Code: movl 0x6191c4, %esi	 RIP: 401262	 Bytes: 7
  %loadMem_401262 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401262 = call %struct.Memory* @routine_movl_0x6191c4___esi(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401262)
  store %struct.Memory* %call_401262, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x20(%rbp)	 RIP: 401269	 Bytes: 3
  %loadMem_401269 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401269 = call %struct.Memory* @routine_movl__eax__MINUS0x20__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401269)
  store %struct.Memory* %call_401269, %struct.Memory** %MEMORY

  ; Code: movb $0x0, %al	 RIP: 40126c	 Bytes: 2
  %loadMem_40126c = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40126c = call %struct.Memory* @routine_movb__0x0___al(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40126c)
  store %struct.Memory* %call_40126c, %struct.Memory** %MEMORY

  ; Code: callq .printf_plt	 RIP: 40126e	 Bytes: 5
  %loadMem1_40126e = load %struct.Memory*, %struct.Memory** %MEMORY
  %call1_40126e = call %struct.Memory* @routine_callq_.printf_plt(%struct.State* %0, i64  0, %struct.Memory* %loadMem1_40126e, i64 -3710, i64 5, i64 5)
  store %struct.Memory* %call1_40126e, %struct.Memory** %MEMORY

  %loadMem2_40126e = load %struct.Memory*, %struct.Memory** %MEMORY
  %loadPC_40126e = load i64, i64* %3
  %call2_40126e = call %struct.Memory* @ext_printf(%struct.State* %0, i64  %loadPC_40126e, %struct.Memory* %loadMem2_40126e)
  store %struct.Memory* %call2_40126e, %struct.Memory** %MEMORY

  ; Code: movl %eax, -0x24(%rbp)	 RIP: 401273	 Bytes: 3
  %loadMem_401273 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401273 = call %struct.Memory* @routine_movl__eax__MINUS0x24__rbp_(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401273)
  store %struct.Memory* %call_401273, %struct.Memory** %MEMORY

  ; Code: addq $0x30, %rsp	 RIP: 401276	 Bytes: 4
  %loadMem_401276 = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_401276 = call %struct.Memory* @routine_addq__0x30___rsp(%struct.State* %0, i64  0, %struct.Memory* %loadMem_401276)
  store %struct.Memory* %call_401276, %struct.Memory** %MEMORY

  ; Code: popq %rbp	 RIP: 40127a	 Bytes: 1
  %loadMem_40127a = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40127a = call %struct.Memory* @routine_popq__rbp(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40127a)
  store %struct.Memory* %call_40127a, %struct.Memory** %MEMORY

  ; Code: retq	 RIP: 40127b	 Bytes: 1
  %loadMem_40127b = load %struct.Memory*, %struct.Memory** %MEMORY
  %call_40127b = call %struct.Memory* @routine_retq(%struct.State* %0, i64  0, %struct.Memory* %loadMem_40127b)
  store %struct.Memory* %call_40127b, %struct.Memory** %MEMORY

  ret %struct.Memory* %call_40127b
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L4PUSHI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory*, %struct.State* nocapture dereferenceable(3376), i64) #0 {
  %4 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 13, i32 0, i32 0
  %5 = load i64, i64* %4, align 8
  %6 = add i64 %5, -8
  %7 = inttoptr i64 %6 to i64*
  store i64 %2, i64* %7
  store i64 %6, i64* %4, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_pushq__rbp(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 1
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4PUSHI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %9)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2RnImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone returned, %struct.State* nocapture readnone dereferenceable(3376), i64* nocapture, i64) #2 {
  store i64 %3, i64* %2, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movq__rsp___rbp(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 13
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSP = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RSP
  %13 = load i64, i64* %PC
  %14 = add i64 %13, 3
  store i64 %14, i64* %PC
  %15 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2RnImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RBP, i64 %12)
  ret %struct.Memory* %15
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3SUBI3RnWImE2RnImE2InImEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = sub i64 %3, %4
  store i64 %6, i64* %2, align 8
  %7 = icmp ult i64 %3, %4
  %8 = zext i1 %7 to i8
  %9 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %8, i8* %9, align 1
  %10 = trunc i64 %6 to i32
  %11 = and i32 %10, 255
  %12 = tail call i32 @llvm.ctpop.i32(i32 %11) #22
  %13 = trunc i32 %12 to i8
  %14 = and i8 %13, 1
  %15 = xor i8 %14, 1
  %16 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %15, i8* %16, align 1
  %17 = xor i64 %4, %3
  %18 = xor i64 %17, %6
  %19 = lshr i64 %18, 4
  %20 = trunc i64 %19 to i8
  %21 = and i8 %20, 1
  %22 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %21, i8* %22, align 1
  %23 = icmp eq i64 %6, 0
  %24 = zext i1 %23 to i8
  %25 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %24, i8* %25, align 1
  %26 = lshr i64 %6, 63
  %27 = trunc i64 %26 to i8
  %28 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %27, i8* %28, align 1
  %29 = lshr i64 %3, 63
  %30 = lshr i64 %4, 63
  %31 = xor i64 %30, %29
  %32 = xor i64 %26, %29
  %33 = add   i64 %32, %31
  %34 = icmp eq i64 %33, 2
  %35 = zext i1 %34 to i8
  %36 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %35, i8* %36, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_subq__0x30___rsp(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 13
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSP
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 4
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3SUBI3RnWImE2RnImE2InImEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RSP, i64 %9, i64 48)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone, %struct.State* nocapture readnone dereferenceable(3376), i64, i64) #8 {
  %5 = trunc i64 %3 to i32
  %6 = inttoptr i64 %2 to i32*
  store i32 %5, i32* %6
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movl__0x0__MINUS0x10__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 16
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory*, %struct.State* nocapture dereferenceable(3376), i64, i64) #0 {
  %5 = inttoptr i64 %2 to i32*
  %6 = load i32, i32* %5
  %7 = trunc i64 %3 to i32
  %8 = sub i32 %6, %7
  %9 = icmp ult i32 %6, %7
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = and i32 %8, 255
  %13 = tail call i32 @llvm.ctpop.i32(i32 %12) #22
  %14 = trunc i32 %13 to i8
  %15 = and i8 %14, 1
  %16 = xor i8 %15, 1
  %17 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %16, i8* %17, align 1
  %18 = xor i32 %6, %7
  %19 = xor i32 %18, %8
  %20 = lshr i32 %19, 4
  %21 = trunc i32 %20 to i8
  %22 = and i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %22, i8* %23, align 1
  %24 = icmp eq i32 %8, 0
  %25 = zext i1 %24 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %8, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %6, 31
  %31 = lshr i32 %7, 31
  %32 = xor i32 %30, %31
  %33 = xor i32 %27, %30
  %34 = add   i32 %33, %32
  %35 = icmp eq i32 %34, 2
  %36 = zext i1 %35 to i8
  %37 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %36, i8* %37, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_cmpl__0x1ff__MINUS0x10__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 16
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 511)
  ret %struct.Memory* %13
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i8* nocapture, i64, i64) #2 {
  %6 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  %7 = load i8, i8* %6, align 1
  %8 = icmp eq i8 %7, 0
  %9 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  %10 = load i8, i8* %9, align 1
  %11 = icmp ne i8 %10, 0
  %12 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  %13 = load i8, i8* %12, align 1
  %14 = icmp ne i8 %13, 0
  %15 = xor i1 %11, %14
  %16 = xor i1 %15, true
  %17 = and i1 %8, %16
  %18 = zext i1 %17 to i8
  store i8 %18, i8* %2, align 1
  %19 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 33, i32 0, i32 0
  %20 = select i1 %17, i64 %3, i64 %4
  store i64 %20, i64* %19, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_jg_.L_400879(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L5MOVSXI3RnWImE2MnIjElEEP6MemoryS6_R5StateT_T0_(%struct.Memory* returned, %struct.State* nocapture readnone dereferenceable(3376), i64* nocapture, i64) #0 {
  %5 = inttoptr i64 %3 to i32*
  %6 = load i32, i32* %5
  %7 = sext i32 %6 to i64
  store i64 %7, i64* %2, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movslq_MINUS0x10__rbp____rax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 16
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 4
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L5MOVSXI3RnWImE2MnIjElEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %13)
  ret %struct.Memory* %16
}


define %struct.Memory* @routine_movl__0x1__0x60df50___rax_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RAX
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x60df50___rax_4__type* @G_0x60df50___rax_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* returned, %struct.State* nocapture readnone dereferenceable(3376), i64* nocapture, i64) #0 {
  %5 = inttoptr i64 %3 to i32*
  %6 = load i32, i32* %5
  %7 = zext i32 %6 to i64
  store i64 %7, i64* %2, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movl_MINUS0x10__rbp____eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 16
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %13)
  ret %struct.Memory* %16
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnIjE2InIjEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = trunc i64 %3 to i32
  %7 = trunc i64 %4 to i32
  %8 = add i32 %7, %6
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %2, align 8
  %10 = icmp ult i32 %8, %6
  %11 = icmp ult i32 %8, %7
  %12 = or i1 %10, %11
  %13 = zext i1 %12 to i8
  %14 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %13, i8* %14, align 1
  %15 = and i32 %8, 255
  %16 = tail call i32 @llvm.ctpop.i32(i32 %15) #22
  %17 = trunc i32 %16 to i8
  %18 = and i8 %17, 1
  %19 = xor i8 %18, 1
  %20 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %19, i8* %20, align 1
  %21 = xor i64 %4, %3
  %22 = trunc i64 %21 to i32
  %23 = xor i32 %22, %8
  %24 = lshr i32 %23, 4
  %25 = trunc i32 %24 to i8
  %26 = and i8 %25, 1
  %27 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %26, i8* %27, align 1
  %28 = icmp eq i32 %8, 0
  %29 = zext i1 %28 to i8
  %30 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %29, i8* %30, align 1
  %31 = lshr i32 %8, 31
  %32 = trunc i32 %31 to i8
  %33 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %32, i8* %33, align 1
  %34 = lshr i32 %6, 31
  %35 = lshr i32 %7, 31
  %36 = xor i32 %31, %34
  %37 = xor i32 %31, %35
  %38 = add   i32 %36, %37
  %39 = icmp eq i32 %38, 2
  %40 = zext i1 %39 to i8
  %41 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %40, i8* %41, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_addl__0x1___eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RAX
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnIjE2InIjEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %9, i64 1)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone, %struct.State* nocapture readnone dereferenceable(3376), i64, i64) #8 {
  %5 = trunc i64 %3 to i32
  %6 = inttoptr i64 %2 to i32*
  store i32 %5, i32* %6
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movl__eax__MINUS0x10__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 16
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64) #2 {
  %4 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 33, i32 0, i32 0
  store i64 %2, i64* %4, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_jmpq_.L_40084f(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x1__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 1)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_cmpl__0x5__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 5)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400901(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl__0x1__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 1)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_cmpl__0x5__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 5)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_4008ee(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl__0x1__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 1)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_cmpl__0x5__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 5)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_4008db(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl_MINUS0x4__rbp____eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 4
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %13)
  ret %struct.Memory* %16
}


define %struct.Memory* @routine_movl_MINUS0x8__rbp____ecx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 5
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RCX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 8
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RCX, i64 %13)
  ret %struct.Memory* %16
}


define %struct.Memory* @routine_movl_MINUS0xc__rbp____edx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 7
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 12
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RDX, i64 %13)
  ret %struct.Memory* %16
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3SHLI3RnWImE2RnIjE2InIjEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* readnone, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = trunc i64 %4 to i5
  switch i5 %6, label %13 [
    i5 0, label %45
    i5 1, label %7
  ]

; <label>:7:                                      ; preds = %5
  %8 = trunc i64 %3 to i32
  %9 = shl i32 %8, 1
  %10 = icmp slt i32 %8, 0
  %11 = icmp slt i32 %9, 0
  %12 = xor i1 %10, %11
  br label %23

; <label>:13:                                     ; preds = %5
  %14 = and i64 %4, 31
  %15 = add   i64 %14, 4294967295
  %16 = and i64 %3, 4294967295
  %17 = and i64 %15, 4294967295
  %18 = shl i64 %16, %17
  %19 = trunc i64 %18 to i32
  %20 = icmp slt i32 %19, 0
  %21 = icmp ne i8 0, 0
  %22 = shl i32 %19, 1
  br label %23

; <label>:23:                                     ; preds = %13, %7
  %24 = phi i1 [ %10, %7 ], [ %20, %13 ]
  %25 = phi i1 [ %12, %7 ], [ %21, %13 ]
  %26 = phi i32 [ %9, %7 ], [ %22, %13 ]
  %27 = zext i32 %26 to i64
  store i64 %27, i64* %2, align 8
  %28 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  %29 = zext i1 %24 to i8
  store i8 %29, i8* %28, align 1
  %30 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  %31 = and i32 %26, 254
  %32 = tail call i32 @llvm.ctpop.i32(i32 %31) #22
  %33 = trunc i32 %32 to i8
  %34 = and i8 %33, 1
  %35 = xor i8 %34, 1
  store i8 %35, i8* %30, align 1
  %36 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 0, i8* %36, align 1
  %37 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  %38 = icmp eq i32 %26, 0
  %39 = zext i1 %38 to i8
  store i8 %39, i8* %37, align 1
  %40 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  %41 = lshr i32 %26, 31
  %42 = trunc i32 %41 to i8
  store i8 %42, i8* %40, align 1
  %43 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  %44 = zext i1 %25 to i8
  store i8 %44, i8* %43, align 1
  br label %45

; <label>:45:                                     ; preds = %23, %5
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_shll__0x3___edx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 7
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDX = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RDX
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3SHLI3RnWImE2RnIjE2InIjEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RDX, i64 %9, i64 3)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnIjES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = trunc i64 %3 to i32
  %7 = trunc i64 %4 to i32
  %8 = add i32 %7, %6
  %9 = zext i32 %8 to i64
  store i64 %9, i64* %2, align 8
  %10 = icmp ult i32 %8, %6
  %11 = icmp ult i32 %8, %7
  %12 = or i1 %10, %11
  %13 = zext i1 %12 to i8
  %14 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %13, i8* %14, align 1
  %15 = and i32 %8, 255
  %16 = tail call i32 @llvm.ctpop.i32(i32 %15) #22
  %17 = trunc i32 %16 to i8
  %18 = and i8 %17, 1
  %19 = xor i8 %18, 1
  %20 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %19, i8* %20, align 1
  %21 = xor i64 %4, %3
  %22 = trunc i64 %21 to i32
  %23 = xor i32 %22, %8
  %24 = lshr i32 %23, 4
  %25 = trunc i32 %24 to i8
  %26 = and i8 %25, 1
  %27 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %26, i8* %27, align 1
  %28 = icmp eq i32 %8, 0
  %29 = zext i1 %28 to i8
  %30 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %29, i8* %30, align 1
  %31 = lshr i32 %8, 31
  %32 = trunc i32 %31 to i8
  %33 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %32, i8* %33, align 1
  %34 = lshr i32 %6, 31
  %35 = lshr i32 %7, 31
  %36 = xor i32 %31, %34
  %37 = xor i32 %31, %35
  %38 = add   i32 %36, %37
  %39 = icmp eq i32 %38, 2
  %40 = zext i1 %39 to i8
  %41 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %40, i8* %41, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_addl__edx___ecx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 7
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EDX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 5
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RCX = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RCX
  %13 = load i32, i32* %EDX
  %14 = zext i32 %13 to i64
  %15 = load i64, i64* %PC
  %16 = add i64 %15, 2
  store i64 %16, i64* %PC
  %17 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnIjES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RCX, i64 %12, i64 %14)
  ret %struct.Memory* %17
}


define %struct.Memory* @routine_shll__0x3___ecx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 5
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RCX = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RCX
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3SHLI3RnWImE2RnIjE2InIjEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RCX, i64 %9, i64 3)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_addl__ecx___eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 5
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %ECX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 1
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RAX = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RAX
  %13 = load i32, i32* %ECX
  %14 = zext i32 %13 to i64
  %15 = load i64, i64* %PC
  %16 = add i64 %15, 2
  store i64 %16, i64* %PC
  %17 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnIjES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %12, i64 %14)
  ret %struct.Memory* %17
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L5MOVSXI3RnWImE2RnIjElEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone returned, %struct.State* nocapture readnone dereferenceable(3376), i64* nocapture, i64) #2 {
  %5 = shl i64 %3, 32
  %6 = ashr exact i64 %5, 32
  store i64 %6, i64* %2, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movslq__eax___rsi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 9
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RSI = bitcast %union.anon* %11 to i64*
  %12 = load i32, i32* %EAX
  %13 = zext i32 %12 to i64
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L5MOVSXI3RnWImE2RnIjElEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RSI, i64 %13)
  ret %struct.Memory* %16
}


define %struct.Memory* @routine_movl__0x0__0x60df50___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x60df50___rsi_4__type* @G_0x60df50___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 0)
  ret %struct.Memory* %14
}


define %struct.Memory* @routine_movl_MINUS0xc__rbp____eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 12
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %13)
  ret %struct.Memory* %16
}




define %struct.Memory* @routine_movl__eax__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 12
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}


define %struct.Memory* @routine_jmpq_.L_4008a2(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_4008e0(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl_MINUS0x8__rbp____eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 8
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %13)
  ret %struct.Memory* %16
}




define %struct.Memory* @routine_movl__eax__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 8
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}


define %struct.Memory* @routine_jmpq_.L_400891(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_4008f3(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}






define %struct.Memory* @routine_movl__eax__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 4
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}


define %struct.Memory* @routine_jmpq_.L_400880(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_cmpl__0xc__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 12)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400967(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400954(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2InImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone returned, %struct.State* nocapture readnone dereferenceable(3376), i64* nocapture, i64) #2 {
  store i64 %3, i64* %2, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movq__0x603aa0___rax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 10
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2InImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 ptrtoint( %G__0x603aa0_type* @G__0x603aa0 to i64))
  ret %struct.Memory* %11
}


define %struct.Memory* @routine_movslq_MINUS0x4__rbp____rcx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 5
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RCX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 4
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 4
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L5MOVSXI3RnWImE2MnIjElEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RCX, i64 %13)
  ret %struct.Memory* %16
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3SHLI3RnWImE2RnImE2InImEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* readnone, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = trunc i64 %4 to i6
  switch i6 %6, label %12 [
    i6 0, label %41
    i6 1, label %7
  ]

; <label>:7:                                      ; preds = %5
  %8 = shl i64 %3, 1
  %9 = icmp slt i64 %3, 0
  %10 = icmp slt i64 %8, 0
  %11 = xor i1 %9, %10
  br label %19

; <label>:12:                                     ; preds = %5
  %13 = and i64 %4, 63
  %14 = add  i64 %13, -1
  %15 = shl i64 %3, %14
  %16 = icmp slt i64 %15, 0
  %17 = icmp ne i8 0, 0
  %18 = shl i64 %15, 1
  br label %19

; <label>:19:                                     ; preds = %12, %7
  %20 = phi i1 [ %9, %7 ], [ %16, %12 ]
  %21 = phi i1 [ %11, %7 ], [ %17, %12 ]
  %22 = phi i64 [ %8, %7 ], [ %18, %12 ]
  store i64 %22, i64* %2, align 8
  %23 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  %24 = zext i1 %20 to i8
  store i8 %24, i8* %23, align 1
  %25 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  %26 = trunc i64 %22 to i32
  %27 = and i32 %26, 254
  %28 = tail call i32 @llvm.ctpop.i32(i32 %27) #22
  %29 = trunc i32 %28 to i8
  %30 = and i8 %29, 1
  %31 = xor i8 %30, 1
  store i8 %31, i8* %25, align 1
  %32 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 0, i8* %32, align 1
  %33 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  %34 = icmp eq i64 %22, 0
  %35 = zext i1 %34 to i8
  store i8 %35, i8* %33, align 1
  %36 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  %37 = lshr i64 %22, 63
  %38 = trunc i64 %37 to i8
  store i8 %38, i8* %36, align 1
  %39 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  %40 = zext i1 %21 to i8
  store i8 %40, i8* %39, align 1
  br label %41

; <label>:41:                                     ; preds = %19, %5
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_shlq__0xb___rcx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 5
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RCX = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RCX
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 4
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3SHLI3RnWImE2RnImE2InImEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RCX, i64 %9, i64 11)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnImES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = add i64 %4, %3
  store i64 %6, i64* %2, align 8
  %7 = icmp ult i64 %6, %3
  %8 = icmp ult i64 %6, %4
  %9 = or i1 %7, %8
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = trunc i64 %6 to i32
  %13 = and i32 %12, 255
  %14 = tail call i32 @llvm.ctpop.i32(i32 %13) #22
  %15 = trunc i32 %14 to i8
  %16 = and i8 %15, 1
  %17 = xor i8 %16, 1
  %18 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %17, i8* %18, align 1
  %19 = xor i64 %4, %3
  %20 = xor i64 %19, %6
  %21 = lshr i64 %20, 4
  %22 = trunc i64 %21 to i8
  %23 = and i8 %22, 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %23, i8* %24, align 1
  %25 = icmp eq i64 %6, 0
  %26 = zext i1 %25 to i8
  %27 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %26, i8* %27, align 1
  %28 = lshr i64 %6, 63
  %29 = trunc i64 %28 to i8
  %30 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %29, i8* %30, align 1
  %31 = lshr i64 %3, 63
  %32 = lshr i64 %4, 63
  %33 = xor i64 %28, %31
  %34 = xor i64 %28, %32
  %35 = add   i64 %33, %34
  %36 = icmp eq i64 %35, 2
  %37 = zext i1 %36 to i8
  %38 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %37, i8* %38, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_addq__rcx___rax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 5
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RCX = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RAX
  %13 = load i64, i64* %RCX
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnImES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RAX, i64 %12, i64 %13)
  ret %struct.Memory* %16
}


define %struct.Memory* @routine_movslq_MINUS0x10__rbp____rcx(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 5
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RCX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 16
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 4
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L5MOVSXI3RnWImE2MnIjElEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RCX, i64 %13)
  ret %struct.Memory* %16
}


define %struct.Memory* @routine_movl__0x0____rax__rcx_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RAX = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 5
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RCX = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RAX
  %13 = load i64, i64* %RCX
  %14 = mul i64 %13, 4
  %15 = add i64 %14, %12
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 7
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %15, i64 0)
  ret %struct.Memory* %18
}








define %struct.Memory* @routine_jmpq_.L_400919(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400959(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400908(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}




define %struct.Memory* @routine_cmpl__0x3__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 3)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_4009ef(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl__0x0__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_cmpl__0x1__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 1)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_4009dc(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl__0x0__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_cmpl__0x0__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_4009c9(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x603aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x603aa0___rsi_4__type* @G_0x603aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400990(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_4009ce(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_40097f(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_4009e1(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_40096e(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__0x6191f0(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191f0_type* @G_0x6191f0 to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0xb__0x610250(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610250_type* @G_0x610250 to i64), i64 11)
  ret %struct.Memory* %8
}




define %struct.Memory* @routine_cmpl__0x1__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 1)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400a8d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}




define %struct.Memory* @routine_cmpl__0x0__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400a7a(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}




define %struct.Memory* @routine_cmpl__0x3__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 3)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400a67(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x6042a0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x6042a0___rsi_4__type* @G_0x6042a0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400a2e(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400a6c(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400a1d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400a7f(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400a0c(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__0x6191f4(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191f4_type* @G_0x6191f4 to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0xc1__0x610254(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610254_type* @G_0x610254 to i64), i64 193)
  ret %struct.Memory* %8
}




define %struct.Memory* @routine_cmpl__0x0__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400b2b(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}




define %struct.Memory* @routine_cmpl__0x3__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 3)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400b18(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}




define %struct.Memory* @routine_cmpl__0x1__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 1)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400b05(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x604aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x604aa0___rsi_4__type* @G_0x604aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400acc(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400b0a(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400abb(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400b1d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400aaa(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__0x6191f8(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191f8_type* @G_0x6191f8 to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x58__0x610258(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610258_type* @G_0x610258 to i64), i64 88)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_400bc9(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400bb6(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400ba3(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x6052a0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x6052a0___rsi_4__type* @G_0x6052a0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400b6a(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400ba8(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400b59(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400bbb(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400b48(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__0x6191fc(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191fc_type* @G_0x6191fc to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x19__0x61025c(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x61025c_type* @G_0x61025c to i64), i64 25)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_400c67(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400c54(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400c41(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x605aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x605aa0___rsi_4__type* @G_0x605aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400c08(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400c46(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400bf7(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400c59(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400be6(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__0x619200(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619200_type* @G_0x619200 to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x43__0x610260(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610260_type* @G_0x610260 to i64), i64 67)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_400d05(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400cf2(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400cdf(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x6062a0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x6062a0___rsi_4__type* @G_0x6062a0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400ca6(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400ce4(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400c95(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400cf7(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400c84(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x0__0x619204(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619204_type* @G_0x619204 to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0xc8__0x610264(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610264_type* @G_0x610264 to i64), i64 200)
  ret %struct.Memory* %8
}




define %struct.Memory* @routine_cmpl__0x2__MINUS0x4__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 4
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 2)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400da3(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400d90(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400d7d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x606aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x606aa0___rsi_4__type* @G_0x606aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400d44(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400d82(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400d33(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400d95(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400d22(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x1__0x619208(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619208_type* @G_0x619208 to i64), i64 1)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x2__0x610268(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610268_type* @G_0x610268 to i64), i64 2)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_400e41(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}




define %struct.Memory* @routine_cmpl__0x2__MINUS0x8__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 8
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 2)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400e2e(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400e1b(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x6072a0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x6072a0___rsi_4__type* @G_0x6072a0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400de2(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400e20(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400dd1(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400e33(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400dc0(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x1__0x61920c(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x61920c_type* @G_0x61920c to i64), i64 1)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x10__0x61026c(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x61026c_type* @G_0x61026c to i64), i64 16)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_400edf(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400ecc(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}




define %struct.Memory* @routine_cmpl__0x2__MINUS0xc__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 12
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 4
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 2)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jg_.L_400eb9(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x607aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x607aa0___rsi_4__type* @G_0x607aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400e80(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400ebe(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400e6f(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400ed1(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400e5e(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x1__0x619210(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619210_type* @G_0x619210 to i64), i64 1)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x80__0x610270(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610270_type* @G_0x610270 to i64), i64 128)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_400f7d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400f6a(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400f57(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x6082a0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x6082a0___rsi_4__type* @G_0x6082a0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400f1e(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400f5c(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400f0d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400f6f(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400efc(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x2__0x619214(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619214_type* @G_0x619214 to i64), i64 2)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x9__0x610274(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610274_type* @G_0x610274 to i64), i64 9)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_40101b(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_401008(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_400ff5(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x608aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x608aa0___rsi_4__type* @G_0x608aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_400fbc(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_400ffa(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400fab(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_40100d(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_400f9a(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x2__0x619218(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619218_type* @G_0x619218 to i64), i64 2)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x41__0x610278(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610278_type* @G_0x610278 to i64), i64 65)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_4010b9(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_4010a6(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_401093(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x6092a0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x6092a0___rsi_4__type* @G_0x6092a0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_40105a(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_401098(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_401049(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_4010ab(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_401038(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movl__0x2__0x61921c(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x61921c_type* @G_0x61921c to i64), i64 2)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x48__0x61027c(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x61027c_type* @G_0x61027c to i64), i64 72)
  ret %struct.Memory* %8
}






define %struct.Memory* @routine_jg_.L_401157(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_401144(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_jg_.L_401131(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4JNLEEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


















define %struct.Memory* @routine_movl__0x1__0x609aa0___rsi_4_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSI
  %10 = mul i64 %9, 4
  %11 = add i64 %10, ptrtoint( %G_0x609aa0___rsi_4__type* @G_0x609aa0___rsi_4_ to i64)
  %12 = load i64, i64* %PC
  %13 = add i64 %12, 11
  store i64 %13, i64* %PC
  %14 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %11, i64 1)
  ret %struct.Memory* %14
}








define %struct.Memory* @routine_jmpq_.L_4010f8(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_401136(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_4010e7(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_jmpq_.L_401149(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}








define %struct.Memory* @routine_jmpq_.L_4010d6(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3XORI3RnWImE2RnIjES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = xor i64 %4, %3
  %7 = trunc i64 %6 to i32
  %8 = and i64 %6, 4294967295
  store i64 %8, i64* %2, align 8
  %9 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 0, i8* %9, align 1
  %10 = and i32 %7, 255
  %11 = tail call i32 @llvm.ctpop.i32(i32 %10) #22
  %12 = trunc i32 %11 to i8
  %13 = and i8 %12, 1
  %14 = xor i8 %13, 1
  %15 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %14, i8* %15, align 1
  %16 = icmp eq i32 %7, 0
  %17 = zext i1 %16 to i8
  %18 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %17, i8* %18, align 1
  %19 = lshr i32 %7, 31
  %20 = trunc i32 %19 to i8
  %21 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %20, i8* %21, align 1
  %22 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 0, i8* %22, align 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 0, i8* %23, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_xorl__edi___edi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 11
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EDI = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 11
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RDI = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RDI
  %13 = load i32, i32* %EDI
  %14 = zext i32 %13 to i64
  %15 = load i64, i64* %PC
  %16 = add i64 %15, 2
  store i64 %16, i64* %PC
  %17 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3XORI3RnWImE2RnIjES4_EEP6MemoryS6_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RDI, i64 %12, i64 %14)
  ret %struct.Memory* %17
}


define %struct.Memory* @routine_movl__0x3__0x619220(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x619220_type* @G_0x619220 to i64), i64 3)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x49__0x610280(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x610280_type* @G_0x610280 to i64), i64 73)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0xd__0x6191e0(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191e0_type* @G_0x6191e0 to i64), i64 13)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x3__0x6191e4(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191e4_type* @G_0x6191e4 to i64), i64 3)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x1__0x6191e8(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191e8_type* @G_0x6191e8 to i64), i64 1)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x1__0x6191ec(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191ec_type* @G_0x6191ec to i64), i64 1)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl__0x49__MINUS0x10__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RBP
  %10 = sub i64 %9, 16
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 73)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_movl__0x0__0x6191c4(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191c4_type* @G_0x6191c4 to i64), i64 0)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_movl_MINUS0x10__rbp____esi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 16
  %14 = load i64, i64* %PC
  %15 = add i64 %14, 3
  store i64 %15, i64* %PC
  %16 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RSI, i64 %13)
  ret %struct.Memory* %16
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L4CALLI2InImEEEP6MemoryS4_R5StateT_S2_(%struct.Memory* readnone, %struct.State* nocapture dereferenceable(3376), i64, i64) #0 {
  %5 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 13, i32 0, i32 0
  %6 = load i64, i64* %5, align 8
  %7 = add i64 %6, -8
  %8 = inttoptr i64 %7 to i64*
  store i64 %3, i64* %8
  store i64 %7, i64* %5, align 8
  %9 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 33, i32 0, i32 0
  store i64 %2, i64* %9, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_callq_.Fit(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4CALLI2InImEEEP6MemoryS4_R5StateT_S2_(%struct.Memory* %2, %struct.State* %0, i64 %7, i64 %9)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2RnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64, i64) #0 {
  %5 = trunc i64 %2 to i32
  %6 = trunc i64 %3 to i32
  %7 = sub i32 %5, %6
  %8 = icmp ult i32 %5, %6
  %9 = zext i1 %8 to i8
  %10 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %9, i8* %10, align 1
  %11 = and i32 %7, 255
  %12 = tail call i32 @llvm.ctpop.i32(i32 %11) #22
  %13 = trunc i32 %12 to i8
  %14 = and i8 %13, 1
  %15 = xor i8 %14, 1
  %16 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %15, i8* %16, align 1
  %17 = xor i64 %3, %2
  %18 = trunc i64 %17 to i32
  %19 = xor i32 %18, %7
  %20 = lshr i32 %19, 4
  %21 = trunc i32 %20 to i8
  %22 = and i8 %21, 1
  %23 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %22, i8* %23, align 1
  %24 = icmp eq i32 %7, 0
  %25 = zext i1 %24 to i8
  %26 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %25, i8* %26, align 1
  %27 = lshr i32 %7, 31
  %28 = trunc i32 %27 to i8
  %29 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %28, i8* %29, align 1
  %30 = lshr i32 %5, 31
  %31 = lshr i32 %6, 31
  %32 = xor i32 %31, %30
  %33 = xor i32 %27, %30
  %34 = add   i32 %33, %32
  %35 = icmp eq i32 %34, 2
  %36 = zext i1 %35 to i8
  %37 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %36, i8* %37, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_cmpl__0x0___eax(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = load i32, i32* %EAX
  %10 = zext i32 %9 to i64
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 3
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2RnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %10, i64 0)
  ret %struct.Memory* %13
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L2JZEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i8* nocapture, i64, i64) #2 {
  %6 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  %7 = load i8, i8* %6, align 1
  store i8 %7, i8* %2, align 1
  %8 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 33, i32 0, i32 0
  %9 = icmp ne i8 %7, 0
  %10 = select i1 %9, i64 %3, i64 %4
  store i64 %10, i64* %8, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_je_.L_4011d4(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L2JZEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}






define %struct.Memory* @routine_callq_.Place(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4CALLI2InImEEEP6MemoryS4_R5StateT_S2_(%struct.Memory* %2, %struct.State* %0, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl__eax__0x603a94(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = load i32, i32* %EAX
  %10 = zext i32 %9 to i64
  %11 = load i64, i64* %PC
  %12 = add i64 %11, 7
  store i64 %12, i64* %PC
  %13 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x603a94_type* @G_0x603a94 to i64), i64 %10)
  ret %struct.Memory* %13
}


define %struct.Memory* @routine_jmpq_.L_4011e8(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_movq__0x401344___rdi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 11
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 10
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2InImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RDI, i64 ptrtoint( %G__0x401344_type* @G__0x401344 to i64))
  ret %struct.Memory* %11
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWIhE2InIhEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* readnone returned, %struct.State* nocapture readnone dereferenceable(3376), i8* nocapture, i64) #2 {
  %5 = trunc i64 %3 to i8
  store i8 %5, i8* %2, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_movb__0x0___al(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %9 = bitcast %union.anon* %8 to %struct.anon.2*
  %AL = getelementptr inbounds %struct.anon.2, %struct.anon.2* %9, i32 0, i32 0
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 2
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWIhE2InIhEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i8* %AL, i64 0)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_callq_.printf_plt(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4CALLI2InImEEEP6MemoryS4_R5StateT_S2_(%struct.Memory* %2, %struct.State* %0, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movl__eax__MINUS0x14__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 20
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}


define %struct.Memory* @routine_movl_0x603a94___edi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 11
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 7
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RDI, i64 ptrtoint( %G_0x603a94_type* @G_0x603a94 to i64))
  ret %struct.Memory* %11
}


define %struct.Memory* @routine_callq_.Trial(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L4CALLI2InImEEEP6MemoryS4_R5StateT_S2_(%struct.Memory* %2, %struct.State* %0, i64 %7, i64 %9)
  ret %struct.Memory* %12
}



define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3JNZEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i8* nocapture, i64, i64) #2 {
  %6 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  %7 = load i8, i8* %6, align 1
  %8 = icmp eq i8 %7, 0
  %9 = zext i1 %8 to i8
  store i8 %9, i8* %2, align 1
  %10 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 33, i32 0, i32 0
  %11 = select i1 %8, i64 %3, i64 %4
  store i64 %11, i64* %10, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_jne_.L_401216(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JNZEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movq__0x401356___rdi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 11
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 10
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2InImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RDI, i64 ptrtoint( %G__0x401356_type* @G__0x401356 to i64))
  ret %struct.Memory* %11
}






define %struct.Memory* @routine_movl__eax__MINUS0x18__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 24
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}


define %struct.Memory* @routine_jmpq_.L_401240(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i64 %rel_off1, i64 %rel_off2) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  store i64 %9, i64* %PC
  %10 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3JMPI2InImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64 %7)
  ret %struct.Memory* %10
}


define %struct.Memory* @routine_cmpl__0x7d5__0x6191c4(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 11
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3CMPI2MnIjE2InIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 ptrtoint( %G_0x6191c4_type* @G_0x6191c4 to i64), i64 2005)
  ret %struct.Memory* %8
}


define %struct.Memory* @routine_je_.L_40123b(%struct.State*  dereferenceable(3376), i64, %struct.Memory*, i8* %BRANCH_TAKEN, i64 %rel_off1, i64 %rel_off2, i64 %rel_off3) {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, %rel_off1
  %8 = load i64, i64* %PC
  %9 = add i64 %8, %rel_off2
  %10 = load i64, i64* %PC
  %11 = add i64 %10, %rel_off3
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L2JZEP6MemoryR5State3RnWIhE2InImES7_(%struct.Memory* %2, %struct.State* %0, i8* %BRANCH_TAKEN, i64 %7, i64 %9)
  ret %struct.Memory* %12
}


define %struct.Memory* @routine_movq__0x401369___rdi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 11
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 10
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2InImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RDI, i64 ptrtoint( %G__0x401369_type* @G__0x401369 to i64))
  ret %struct.Memory* %11
}






define %struct.Memory* @routine_movl__eax__MINUS0x1c__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 28
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}




define %struct.Memory* @routine_movq__0x40137c___rdi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 11
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RDI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 10
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2InImEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RDI, i64 ptrtoint( %G__0x40137c_type* @G__0x40137c to i64))
  ret %struct.Memory* %11
}


define %struct.Memory* @routine_movl_0x603a94___esi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 7
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RSI, i64 ptrtoint( %G_0x603a94_type* @G_0x603a94 to i64))
  ret %struct.Memory* %11
}








define %struct.Memory* @routine_movl_0x6191c4___esi(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 9
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSI = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 7
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3RnWImE2MnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64* %RSI, i64 ptrtoint( %G_0x6191c4_type* @G_0x6191c4 to i64))
  ret %struct.Memory* %11
}


define %struct.Memory* @routine_movl__eax__MINUS0x20__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 32
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}






define %struct.Memory* @routine_movl__eax__MINUS0x24__rbp_(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 1
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %EAX = bitcast %union.anon* %8 to i32*
  %9 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %10 = getelementptr inbounds %struct.GPR, %struct.GPR* %9, i32 0, i32 15
  %11 = getelementptr inbounds %struct.Reg, %struct.Reg* %10, i32 0, i32 0
  %RBP = bitcast %union.anon* %11 to i64*
  %12 = load i64, i64* %RBP
  %13 = sub i64 %12, 36
  %14 = load i32, i32* %EAX
  %15 = zext i32 %14 to i64
  %16 = load i64, i64* %PC
  %17 = add i64 %16, 3
  store i64 %17, i64* %PC
  %18 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3MOVI3MnWIjE2RnIjEEEP6MemoryS6_R5StateT_T0_(%struct.Memory* %2, %struct.State* %0, i64 %13, i64 %15)
  ret %struct.Memory* %18
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnImE2InImEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* readnone returned, %struct.State* nocapture dereferenceable(3376), i64* nocapture, i64, i64) #0 {
  %6 = add i64 %4, %3
  store i64 %6, i64* %2, align 8
  %7 = icmp ult i64 %6, %3
  %8 = icmp ult i64 %6, %4
  %9 = or i1 %7, %8
  %10 = zext i1 %9 to i8
  %11 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 1
  store i8 %10, i8* %11, align 1
  %12 = trunc i64 %6 to i32
  %13 = and i32 %12, 255
  %14 = tail call i32 @llvm.ctpop.i32(i32 %13) #22
  %15 = trunc i32 %14 to i8
  %16 = and i8 %15, 1
  %17 = xor i8 %16, 1
  %18 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 3
  store i8 %17, i8* %18, align 1
  %19 = xor i64 %4, %3
  %20 = xor i64 %19, %6
  %21 = lshr i64 %20, 4
  %22 = trunc i64 %21 to i8
  %23 = and i8 %22, 1
  %24 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 5
  store i8 %23, i8* %24, align 1
  %25 = icmp eq i64 %6, 0
  %26 = zext i1 %25 to i8
  %27 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 7
  store i8 %26, i8* %27, align 1
  %28 = lshr i64 %6, 63
  %29 = trunc i64 %28 to i8
  %30 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 9
  store i8 %29, i8* %30, align 1
  %31 = lshr i64 %3, 63
  %32 = lshr i64 %4, 63
  %33 = xor i64 %28, %31
  %34 = xor i64 %28, %32
  %35 = add   i64 %33, %34
  %36 = icmp eq i64 %35, 2
  %37 = zext i1 %36 to i8
  %38 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 2, i32 13
  store i8 %37, i8* %38, align 1
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_addq__0x30___rsp(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 13
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RSP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %RSP
  %10 = load i64, i64* %PC
  %11 = add i64 %10, 4
  store i64 %11, i64* %PC
  %12 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3ADDI3RnWImE2RnImE2InImEEEP6MemoryS8_R5StateT_T0_T1_(%struct.Memory* %2, %struct.State* %0, i64* %RSP, i64 %9, i64 48)
  ret %struct.Memory* %12
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3POPI3RnWImEEEP6MemoryS4_R5StateT_(%struct.Memory*, %struct.State* nocapture dereferenceable(3376), i64* nocapture) #0 {
  %4 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 13, i32 0, i32 0
  %5 = load i64, i64* %4, align 8
  %6 = add i64 %5, 8
  %7 = inttoptr i64 %5 to i64*
  %8 = load i64, i64* %7
  store i64 %8, i64* %2, align 8
  store i64 %6, i64* %4, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_popq__rbp(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %7 = getelementptr inbounds %struct.GPR, %struct.GPR* %6, i32 0, i32 15
  %8 = getelementptr inbounds %struct.Reg, %struct.Reg* %7, i32 0, i32 0
  %RBP = bitcast %union.anon* %8 to i64*
  %9 = load i64, i64* %PC
  %10 = add i64 %9, 1
  store i64 %10, i64* %PC
  %11 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3POPI3RnWImEEEP6MemoryS4_R5StateT_(%struct.Memory* %2, %struct.State* %0, i64* %RBP)
  ret %struct.Memory* %11
}

define internal %struct.Memory* @_ZN12_GLOBAL__N_1L3RETEP6MemoryR5State(%struct.Memory* returned, %struct.State* nocapture dereferenceable(3376)) #0 {
  %3 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 33, i32 0, i32 0
  %4 = getelementptr inbounds %struct.State, %struct.State* %1, i64 0, i32 6, i32 13, i32 0, i32 0
  %5 = load i64, i64* %4, align 8
  %6 = inttoptr i64 %5 to i64*
  %7 = load i64, i64* %6
  store i64 %7, i64* %3, align 8
  %8 = add i64 %5, 8
  store i64 %8, i64* %4, align 8
  ret %struct.Memory* %0
}

define %struct.Memory* @routine_retq(%struct.State*  dereferenceable(3376), i64, %struct.Memory* ) #19 {
block_400478:
  %3 = getelementptr inbounds %struct.State, %struct.State* %0, i32 0, i32 6
  %4 = getelementptr inbounds %struct.GPR, %struct.GPR* %3, i32 0, i32 33
  %5 = getelementptr inbounds %struct.Reg, %struct.Reg* %4, i32 0, i32 0
  %PC = bitcast %union.anon* %5 to i64*
  %6 = load i64, i64* %PC
  %7 = add i64 %6, 1
  store i64 %7, i64* %PC
  %8 = call %struct.Memory* @_ZN12_GLOBAL__N_1L3RETEP6MemoryR5State(%struct.Memory* %2, %struct.State* %0)
  ret %struct.Memory* %8
}

